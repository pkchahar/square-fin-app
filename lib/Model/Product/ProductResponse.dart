class ProductResponse {
  final bool status;
  final String message;
  final List<ProductData> data;

  ProductResponse({
    required this.status,
    required this.message,
    required this.data,
  });

  factory ProductResponse.fromJson(Map<String, dynamic> json) {
    var dataJson = json['Data'];

    if (dataJson == null || dataJson.isEmpty) {
      return ProductResponse(
        status: json['status'],
        message: json['message'],
        data: [],
      );
    }

    if (dataJson is List) {
      final dataList =
          dataJson.map((item) => ProductData.fromJson(item)).toList();

      return ProductResponse(
        status: json['status'],
        message: json['message'],
        data: dataList,
      );
    } else {
      throw const FormatException("Unexpected format for 'Data' field");
    }
  }
}

class ProductData {
  final String id;
  final String name;
  final String icon;
  final String parent;
  final String tagline;
  final String status;
  final String createdAt;
  final String isIntegrated;
  final String redirectLink;
  final String haveForm;

  ProductData({
    required this.id,
    required this.name,
    required this.icon,
    required this.parent,
    required this.tagline,
    required this.status,
    required this.createdAt,
    required this.isIntegrated,
    required this.redirectLink,
    required this.haveForm,
  });

  factory ProductData.fromJson(Map<String, dynamic> json) {
    return ProductData(
      id: json['id'] ?? "",
      name: json['name'] ?? "",
      icon: json['icon'] ?? "",
      parent: json['parent'] ?? "",
      tagline: json['tagline'] ?? "",
      status: json['status'] ?? "",
      createdAt: json['created_at'] ?? "",
      isIntegrated: json['is_integrated'] ?? "1",
      haveForm: json['haveForm'] ?? "0",
      redirectLink: json['redirect_link'] ?? "https://www.squarefin.in/",
    );
  }

  @override
  bool operator == (Object other) =>
      identical(this, other) ||
          other is ProductData && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}

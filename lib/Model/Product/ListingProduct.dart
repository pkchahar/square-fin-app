class ListingProductResponse {
  bool status;
  String message;
  List<ListingProduct> data;

  ListingProductResponse({
    required this.status,
    required this.message,
    required this.data,
  });

  factory ListingProductResponse.fromJson(Map<String, dynamic> json) {
    var dataList = <ListingProduct>[];
    if (json['Data'] != null) {
      dataList = List<ListingProduct>.from(
          json['Data'].map((data) => ListingProduct.fromJson(data)));
    }

    return ListingProductResponse(
      status: json['status'],
      message: json['message'],
      data: dataList,
    );
  }
}

class ListingProduct {
  int srNo;
  String id;
  String category;
  String subcategory;
  String name;
  String companyName;
  String icon;
  String amount;
  String amtFlag;
  String shortDescription;
  String features;
  String feesCharges;
  String instructions;
  String link;
  String joiningFee;
  String annualFee;
  String maxCreditScore;
  String minCreditScore;
  String maxAge;
  String minAge;
  String itr;
  String maxSalary;
  String minSalary;
  String termsAndConditions;
  String trainingLink;
  String multipleBanar;
  List<dynamic> multipleBanner;
  String status;
  String createdAt;
  String shareUrl;
  bool isChecked;

  ListingProduct({
    required this.srNo,
    required this.id,
    required this.category,
    required this.subcategory,
    required this.name,
    required this.companyName,
    required this.icon,
    required this.amount,
    required this.amtFlag,
    required this.shortDescription,
    required this.features,
    required this.feesCharges,
    required this.instructions,
    required this.link,
    required this.joiningFee,
    required this.annualFee,
    required this.maxCreditScore,
    required this.minCreditScore,
    required this.maxAge,
    required this.minAge,
    required this.itr,
    required this.maxSalary,
    required this.minSalary,
    required this.termsAndConditions,
    required this.trainingLink,
    required this.multipleBanar,
    required this.multipleBanner,
    required this.status,
    required this.createdAt,
    required this.shareUrl,
    required this.isChecked,
  });

  factory ListingProduct.fromJson(Map<String, dynamic> json) {
    return ListingProduct(
      srNo: json['SrNo'],
      id: json['Id'],
      category: json['category'] ?? "",
      subcategory: json['subcategory'] ?? "",
      name: json['name'] ?? "",
      companyName: json['companyname'] ?? "",
      icon: json['icon'] ?? "",
      amount: json['amount'] ?? "",
      amtFlag: json['payout_type'] ?? "",
      shortDescription: json['shortdescription'] ?? "",
      features: json['features'] ?? "",
      feesCharges: json['feescharges'] ?? "",
      instructions: json['instructions'] ?? "",
      link: json['link'] ?? "",
      joiningFee: json['joining_fee'] ?? "",
      annualFee: json['annual_fee'] ?? "",
      maxCreditScore: json['max_credit_score'] ?? "",
      minCreditScore: json['min_credit_score'] ?? "",
      maxAge: json['max_age'] ?? "",
      minAge: json['min_age'] ?? "",
      itr: json['itr'] ?? "",
      maxSalary: json['max_salary'] ?? "",
      minSalary: json['min_salary'] ?? "",
      termsAndConditions: json['terms_and_conditions'] ?? "",
      trainingLink: json['tranning_link'] ?? "",
      multipleBanar: json['multiple_banar'] ?? "",
      multipleBanner: json['multiple_banners'] ?? [],
      status: json['status'] ?? "",
      createdAt: json['created_at'] ?? "",
      shareUrl: json['share_url'] ?? "",
      isChecked: json['isChecked'] ?? "",
    );
  }
}

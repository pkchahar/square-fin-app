class LeadData {
  final String id;
  final String name;
  final String email;
  final String mobile;
  final String status;
  final String remark;
  final String productId;
  final String referredId;
  final String createdAt;
  final String productName;
  final String icon;
  final String leadId;
  final String payout;
  final String panNumber;
  final String campainURL;
  final String lastSync;
  final String leadType;
  final String otherData;
  final String offeredPlayin;
  final String archived;
  final String lead_id_primary;
  final bool is_ticket_rasied;
  final dynamic ticket_id;

  LeadData({
    required this.id,
    required this.name,
    required this.email,
    required this.mobile,
    required this.status,
    required this.remark,
    required this.productId,
    required this.referredId,
    required this.createdAt,
    required this.productName,
    required this.icon,
    required this.leadId,
    required this.payout,
    required this.panNumber,
    required this.campainURL,
    required this.lastSync,
    required this.leadType,
    required this.otherData,
    required this.offeredPlayin,
    required this.archived,
    required this.lead_id_primary,
    required this.is_ticket_rasied,
    required this.ticket_id

  });

  factory LeadData.fromJson(Map<String, dynamic> json) {
    return LeadData(
      id: json['id'] ?? "",
      name: json['name'] ?? "",
      email: json['email'] ?? "",
      mobile: json['mobile'] ?? "",
      status: json['status'] ?? "",
      remark: json['remark'] ?? "",
      productId: json['product_id'] ?? "",
      referredId: json['refferd_id'] ?? "",
      createdAt: json['created_at'] ?? "",
      productName: json['productname'] ?? "",
      icon: json['icon'] ?? "",
      leadId: json['lead_id'] ?? "",
      payout: json['offered_payin'] ?? "",
      panNumber: json['pannumber'] ?? "",
      campainURL: json['campaign_url'] ?? "",
      lastSync: json['last_sync'] ?? "",
      leadType: json['lead_type'] ?? "",
      otherData: json['other_data'] ?? "",
      offeredPlayin: json['offered_payin'] ?? "",
      archived: json['archived'] ?? "",
      lead_id_primary: json['lead_id_primary']??"",
      is_ticket_rasied: json['is_ticket_raised']??'',
      ticket_id: json['ticket_id']??''
    );
  }
}

class MyCreditLeads {
  final bool status;
  final String message;
  final List<LeadData> data;

  MyCreditLeads({
    required this.status,
    required this.message,
    required this.data,
  });

  factory MyCreditLeads.fromJson(Map<String, dynamic> json) {
    var dataList = <LeadData>[];
    if (json['data'] != null) {
      dataList = List<LeadData>.from(
          json['data'].map((data) => LeadData.fromJson(data)));
    }

    return MyCreditLeads(
      status: json['status'],
      message: json['msg'],
      data: dataList,
    );
  }
}

class BankList {
  BankList({
      this.id,
      this.bankName,});

  BankList.fromJson(dynamic json) {
    id = json['Id'];
    bankName = json['Name'];
  }
  String? id;
  String? bankName;

  Map<String?, dynamic> toJson() {
    final map = <String?, dynamic>{};
    map['Id'] = id;
    map['Name'] = bankName;
    return map;
  }

}
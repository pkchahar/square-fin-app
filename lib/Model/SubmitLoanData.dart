class SubmitLoanData {
  SubmitLoanData({
      this.loanType, 
      this.userId, 
      this.name, 
      this.email, 
      this.mobile, 
      this.loanApplicationAmount, 
      this.jobProfile, 
      this.address, 
      this.product, 
      this.carModel, 
      this.occupationDetail, 
      this.productCategory, 
      this.vehicleOwnership, 
      this.businessVintage, 
      this.businessTurnover, 
      this.propertyValue, 
      this.propertyType, 
      this.others, 
      this.homeloanCategory, 
      this.minimumSalary, 
      this.companyName, 
      this.workingExperience, 
      this.vehicleType, 
      this.productType, 
      this.bankName, 
      this.subject, 
      this.message, 
      this.status, 
      this.type, 
      this.addStamp,});

  SubmitLoanData.fromJson(dynamic json) {
    loanType = json['Loan_Type'];
    userId = json['User_Id'];
    name = json['Name'];
    email = json['Email'];
    mobile = json['Mobile'];
    loanApplicationAmount = json['Loan_Application_Amount'];
    jobProfile = json['job_profile'];
    address = json['Address'];
    product = json['product'];
    carModel = json['car_model'];
    occupationDetail = json['occupation_detail'];
    productCategory = json['product_category'];
    vehicleOwnership = json['vehicle_ownership'];
    businessVintage = json['business_vintage'];
    businessTurnover = json['business_turnover'];
    propertyValue = json['property_value'];
    propertyType = json['property_type'];
    others = json['others'];
    homeloanCategory = json['homeloan_category'];
    minimumSalary = json['minimum_salary'];
    companyName = json['company_name'];
    workingExperience = json['working_experience'];
    vehicleType = json['vehicle_type'];
    productType = json['product_type'];
    bankName = json['bank_name'];
    subject = json['subject'];
    message = json['message'];
    status = json['Status'];
    type = json['Type'];
    addStamp = json['Add_Stamp'];
  }
  String? loanType;
  String? userId;
  String? name;
  String? email;
  String? mobile;
  String? loanApplicationAmount;
  String? jobProfile;
  String? address;
  String? product;
  String? carModel;
  String? occupationDetail;
  String? productCategory;
  String? vehicleOwnership;
  String? businessVintage;
  String? businessTurnover;
  String? propertyValue;
  String? propertyType;
  String? others;
  String? homeloanCategory;
  String? minimumSalary;
  String? companyName;
  String? workingExperience;
  String? vehicleType;
  String? productType;
  String? bankName;
  String? subject;
  String? message;
  String? status;
  String? type;
  String? addStamp;

  Map<String?, dynamic> toJson() {
    final map = <String?, dynamic>{};
    map['Loan_Type'] = loanType;
    map['User_Id'] = userId;
    map['Name'] = name;
    map['Email'] = email;
    map['Mobile'] = mobile;
    map['Loan_Application_Amount'] = loanApplicationAmount;
    map['job_profile'] = jobProfile;
    map['Address'] = address;
    map['product'] = product;
    map['car_model'] = carModel;
    map['occupation_detail'] = occupationDetail;
    map['product_category'] = productCategory;
    map['vehicle_ownership'] = vehicleOwnership;
    map['business_vintage'] = businessVintage;
    map['business_turnover'] = businessTurnover;
    map['property_value'] = propertyValue;
    map['property_type'] = propertyType;
    map['others'] = others;
    map['homeloan_category'] = homeloanCategory;
    map['minimum_salary'] = minimumSalary;
    map['company_name'] = companyName;
    map['working_experience'] = workingExperience;
    map['vehicle_type'] = vehicleType;
    map['product_type'] = productType;
    map['bank_name'] = bankName;
    map['subject'] = subject;
    map['message'] = message;
    map['Status'] = status;
    map['Type'] = type;
    map['Add_Stamp'] = addStamp;
    return map;
  }

}
class EmpData {
  EmpData({
    this.srNo,
    this.id,
    this.name,
    this.empId,
  });

  EmpData.fromJson(dynamic json) {
    srNo = json['SrNo'];
    id = json['Id'];
    name = json['Name'];
    empId = json['Emp_Id'];
  }

  int? srNo;
  String? id;
  String? name;
  String? empId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['SrNo'] = srNo;
    map['Id'] = id;
    map['Name'] = name;
    map['Emp_Id'] = empId;
    return map;
  }
}

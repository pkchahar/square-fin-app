class DashData {
  DashData({
    this.status,
    this.message,
    this.kycRemark,
    this.kycText,
    this.senctionAmount,
    this.disbursedAmount,
    this.totalRevenue,
    this.leadCounter,
    this.loginNo,
    this.kycStatus,
    this.pending,
    this.rejected,
    this.approved,
  });

  DashData.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'] ?? "";
    kycText = json['kyc_text'] ?? "";
    kycRemark = json['kyc_remark'] ?? "";
    senctionAmount = json['SenctionAmount'];
    disbursedAmount = json['DisbursedAmount'];
    totalRevenue = json['Revenue'];
    leadCounter = json['leadCounter'];
    loginNo = json['loginNo'];
    kycStatus = json['kyc_status'];
    pending = json['pending'];
    rejected = json['rejected'];
    approved = json['approved'];
  }

  int? status;
  String? message;
  String? kycText;
  String? kycRemark;
  int? senctionAmount;
  int? disbursedAmount;
  int? totalRevenue;
  int? leadCounter;
  int? loginNo;
  int? kycStatus;
  int? pending;
  int? rejected;
  int? approved;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    map['SenctionAmount'] = senctionAmount;
    map['DisbursedAmount'] = disbursedAmount;
    map['Revenue'] = totalRevenue;
    map['leadCounter'] = leadCounter;
    map['loginNo'] = loginNo;
    map['kyc_status'] = kycStatus;
    map['kyc_text'] = kycText;
    map['pending'] = pending;
    map['rejected'] = rejected;
    map['approved'] = approved;
    map['kyc_remark'] = kycRemark;
    return map;
  }
}

import 'SubmitLoanData.dart';

class LoanSubmit {
  LoanSubmit({
    this.status,
    this.message,
    this.data,
  });

  LoanSubmit.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? SubmitLoanData.fromJson(json['data']) : null;
  }

  int? status;
  String? message;
  SubmitLoanData? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data!.toJson();
    }
    return map;
  }
}

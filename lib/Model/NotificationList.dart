import 'NotificationListData.dart';

class NotificationList {
  NotificationList({
    this.data,
    this.status,
    this.message,
  });

  NotificationList.fromJson(dynamic json) {
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(NotificationListData.fromJson(v));
      });
    }
    status = json['status'];
    message = json['message'];
  }

  List<NotificationListData>? data;
  bool? status;
  String? message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    map['status'] = status;
    map['message'] = message;
    return map;
  }
}

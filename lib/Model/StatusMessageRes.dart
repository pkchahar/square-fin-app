class StatusMessageRes {
  StatusMessageRes({
      this.status, 
      this.message,});

  StatusMessageRes.fromJson(dynamic json) {
    status = json['Status'];
    message = json['Message'];
  }
  bool? status;
  String? message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Status'] = status;
    map['Message'] = message;
    return map;
  }

}
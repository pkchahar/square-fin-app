class FAQResponse {
  final List<FAQItem> data;
  final bool status;
  final String message;

  FAQResponse({
    required this.data,
    required this.status,
    required this.message,
  });

  factory FAQResponse.fromJson(Map<String, dynamic> json) {
    List<dynamic> data = json['data'];
    List<FAQItem> faqItems =
    data.map((item) => FAQItem.fromJson(item)).toList();
    return FAQResponse(
      data: faqItems,
      status: json['status'],
      message: json['message'],
    );
  }
}

class FAQItem {
  final String id;
  final String question;
  final String answer;
  final String categoryId;
  final String status;
  final String date;

  FAQItem({
    required this.id,
    required this.question,
    required this.answer,
    required this.categoryId,
    required this.status,
    required this.date,
  });

  factory FAQItem.fromJson(Map<String, dynamic> json) {
    return FAQItem(
      id: json['id'],
      question: json['question'],
      answer: json['answer'],
      categoryId: json['category_id'],
      status: json['status'],
      date: json['date'],
    );
  }
}

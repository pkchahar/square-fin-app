class GridListResponse {
  List<GridItem> data;
  bool status;
  String message;

  GridListResponse({required this.data, required this.status, required this.message});

  factory GridListResponse.fromJson(Map<String, dynamic> json) {
    var dataList = json['data'] as List;
    List<GridItem> gridItems = dataList.map((item) => GridItem.fromJson(item)).toList();

    return GridListResponse(
      data: gridItems,
      status: json['status'],
      message: json['message'],
    );
  }
}

class GridItem {
  String id;
  String name;
  String status;
  String createdAt;

  GridItem({required this.id, required this.name, required this.status, required this.createdAt});

  factory GridItem.fromJson(Map<String, dynamic> json) {
    return GridItem(
      id: json['id'] ?? '',
      name: json['name'] ?? '',
      status: json['status'] ?? '',
      createdAt: json['create_at'] ?? '',
    );
  }
}

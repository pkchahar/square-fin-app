class PosterRes {
  List<PosterData> data;
  bool status;

  PosterRes({required this.data, required this.status});

  factory PosterRes.fromJson(Map<String, dynamic> json) {
    return PosterRes(
      data: json['Data'] != null
          ? List<PosterData>.from(json['Data'].map((x) => PosterData.fromJson(x)))
          : [],
      status: json['status'] ?? false,
    );
  }

  // Method to convert a PosterRes object to JSON
  Map<String, dynamic> toJson() {
    return {
      'Data': data.map((x) => x.toJson()).toList(),
      'status': status,
    };
  }
}

class PosterData {
  int srNo;
  String id;
  String title;
  String status;
  String file;

  PosterData({
    required this.srNo,
    required this.id,
    required this.title,
    required this.status,
    required this.file,
  });

  factory PosterData.fromJson(Map<String, dynamic> json) {
    return PosterData(
      srNo: json['SrNo'] ?? 0,
      id: json['id'] ?? "",
      title: json['title'] ?? "",
      status: json['status'] ?? "",
      file: json['file'] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'SrNo': srNo,
      'id': id,
      'title': title,
      'status': status,
      'file': file,
    };
  }
}

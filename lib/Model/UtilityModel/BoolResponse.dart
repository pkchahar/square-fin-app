class BoolResponse {
  final bool status;
  final String message;
  final String msg;

  BoolResponse({
    required this.status,
    required this.message,
    required this.msg,
  });

  factory BoolResponse.fromJson(Map<String, dynamic> json) {
    return BoolResponse(
      status: json['status'] as bool? ?? false,
      message: json['message'] as String? ?? "",
      msg: json['msg'] as String? ?? "",
    );
  }
}

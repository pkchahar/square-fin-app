/// status : 1
/// message : "Successfully submit..!"
/// data : {"Name":"naman","Email":"email@gm.com","subject":"Fixed Deposit","message":"test","Mobile":"8006700172","Add_stamp":"2023-04-04 16:15:39","Status":"Intrested","Loan_Type":"Contact"}

class SubmitQuery {
  SubmitQuery({
      num? status, 
      String? message, 
      Data? data,}){
    _status = status;
    _message = message;
    _data = data;
}

  SubmitQuery.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _status;
  String? _message;
  Data? _data;
SubmitQuery copyWith({  num? status,
  String? message,
  Data? data,
}) => SubmitQuery(  status: status ?? _status,
  message: message ?? _message,
  data: data ?? _data,
);
  num? get status => _status;
  String? get message => _message;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// Name : "naman"
/// Email : "email@gm.com"
/// subject : "Fixed Deposit"
/// message : "test"
/// Mobile : "8006700172"
/// Add_stamp : "2023-04-04 16:15:39"
/// Status : "Intrested"
/// Loan_Type : "Contact"

class Data {
  Data({
      String? name, 
      String? email, 
      String? subject, 
      String? message, 
      String? mobile, 
      String? addStamp, 
      String? status, 
      String? loanType,}){
    _name = name;
    _email = email;
    _subject = subject;
    _message = message;
    _mobile = mobile;
    _addStamp = addStamp;
    _status = status;
    _loanType = loanType;
}

  Data.fromJson(dynamic json) {
    _name = json['Name'];
    _email = json['Email'];
    _subject = json['subject'];
    _message = json['message'];
    _mobile = json['Mobile'];
    _addStamp = json['Add_stamp'];
    _status = json['Status'];
    _loanType = json['Loan_Type'];
  }
  String? _name;
  String? _email;
  String? _subject;
  String? _message;
  String? _mobile;
  String? _addStamp;
  String? _status;
  String? _loanType;
Data copyWith({  String? name,
  String? email,
  String? subject,
  String? message,
  String? mobile,
  String? addStamp,
  String? status,
  String? loanType,
}) => Data(  name: name ?? _name,
  email: email ?? _email,
  subject: subject ?? _subject,
  message: message ?? _message,
  mobile: mobile ?? _mobile,
  addStamp: addStamp ?? _addStamp,
  status: status ?? _status,
  loanType: loanType ?? _loanType,
);
  String? get name => _name;
  String? get email => _email;
  String? get subject => _subject;
  String? get message => _message;
  String? get mobile => _mobile;
  String? get addStamp => _addStamp;
  String? get status => _status;
  String? get loanType => _loanType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Name'] = _name;
    map['Email'] = _email;
    map['subject'] = _subject;
    map['message'] = _message;
    map['Mobile'] = _mobile;
    map['Add_stamp'] = _addStamp;
    map['Status'] = _status;
    map['Loan_Type'] = _loanType;
    return map;
  }

}
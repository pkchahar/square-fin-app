class BannerResponse {
  int status;
  String message;
  List<BannerItem> data;

  BannerResponse({required this.status, required this.message, required this.data});

  factory BannerResponse.fromJson(Map<String, dynamic> json) {
    return BannerResponse(
      status: json['status'],
      message: json['message'],
      data: (json['data'] as List<dynamic>)
          .map((item) => BannerItem.fromJson(item))
          .toList(),
    );
  }
}

class BannerItem {
  int srNo;
  String id;
  String title;
  String description;
  String image;
  String insertDate;

  BannerItem({
    required this.srNo,
    required this.id,
    required this.title,
    required this.description,
    required this.image,
    required this.insertDate,
  });

  factory BannerItem.fromJson(Map<String, dynamic> json) {
    return BannerItem(
      srNo: json['SrNo'],
      id: json['id'],
      title: json['title'],
      description: json['discription'],
      image: json['image'],
      insertDate: json['insert_date'],
    );
  }
}

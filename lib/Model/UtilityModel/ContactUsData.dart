class ContactUsData {
  ContactUsData({
      this.name, 
      this.email, 
      this.subject, 
      this.message, 
      this.addStamp, 
      this.status, 
      this.loanType,});

  ContactUsData.fromJson(dynamic json) {
    name = json['Name'];
    email = json['Email'];
    subject = json['subject'];
    message = json['message'];
    addStamp = json['Add_stamp'];
    status = json['Status'];
    loanType = json['Loan_Type'];
  }
  String? name;
  String? email;
  String? subject;
  String? message;
  String? addStamp;
  String? status;
  String? loanType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Name'] = name;
    map['Email'] = email;
    map['subject'] = subject;
    map['message'] = message;
    map['Add_stamp'] = addStamp;
    map['Status'] = status;
    map['Loan_Type'] = loanType;
    return map;
  }

}
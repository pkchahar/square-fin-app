class MyDashData {
  String label;
  String revenue;
  String business;

  MyDashData(this.label, this.revenue, this.business);
}

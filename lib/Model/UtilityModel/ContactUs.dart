import 'ContactUsData.dart';

class ContactUs {
  ContactUs({
    this.status,
    this.message,
    this.data,
  });

  ContactUs.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? ContactUsData.fromJson(json['data']) : null;
  }

  int? status;
  String? message;
  ContactUsData? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    final data = this.data;
    if (data != null) {
      map['data'] = data.toJson();
    }
    return map;
  }
}

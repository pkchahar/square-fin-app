import 'EmpData.dart';

class EmployeeList {
  EmployeeList({
    this.status,
    this.message,
    this.data,
  });

  EmployeeList.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['Data'] != null) {
      data = [];
      json['Data'].forEach((v) {
        data?.add(EmpData.fromJson(v));
      });
    }
  }

  bool? status;
  String? message;
  List<EmpData>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    final data = this.data;
    if (data != null) {
      map['Data'] = data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

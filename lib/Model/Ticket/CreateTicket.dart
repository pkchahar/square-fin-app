class CreateTicket {
  final bool status;
  final String message;

  CreateTicket({required this.status, required this.message});

  factory CreateTicket.fromJson(Map<String, dynamic> json) {
    return CreateTicket(
      status: json['status'] as bool,
      message: json['message'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'message': message,
    };
  }
}

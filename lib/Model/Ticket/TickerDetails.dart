class TicketDetail {
  final bool status;
  final String message;
  final DataDetails data;

  TicketDetail({
    required this.status,
    required this.message,
    required this.data,
  });

  factory TicketDetail.fromJson(Map<String, dynamic> json) {
    return TicketDetail(
      status: json['status'],
      message: json['message'],
      data: DataDetails.fromJson(json['data']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'message': message,
      'data': data.toJson(),
    };
  }
}

class DataDetails {
  final List<LeadData> leadData;
  final List<TicketConversation> ticketConversation;
  final String heading;

  DataDetails({
    required this.leadData,
    required this.ticketConversation,
    required this.heading,
  });

  factory DataDetails.fromJson(Map<String, dynamic> json) {
    var leadDataList = json['lead_data'] as List;
    var ticketConversationList = json['ticket_conversation'] as List;

    return DataDetails(
      leadData: leadDataList.map((i) => LeadData.fromJson(i)).toList(),
      ticketConversation: ticketConversationList.map((i) => TicketConversation.fromJson(i)).toList(),
      heading: json['heading'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'lead_data': leadData.map((e) => e.toJson()).toList(),
      'ticket_conversation': ticketConversation.map((e) => e.toJson()).toList(),
      'heading': heading,
    };
  }
}

class LeadData {
  final String lastUpdated;

  LeadData({required this.lastUpdated});

  factory LeadData.fromJson(Map<String, dynamic> json) {
    return LeadData(lastUpdated: json['last_updated']);
  }

  Map<String, dynamic> toJson() {
    return {'last_updated': lastUpdated};
  }
}

class TicketConversation {
  final String id;
  final String ticketid;
  final String message;
  final String byId;
  final String userType;
  final String date;
  final String senderName;

  TicketConversation({
    required this.id,
    required this.ticketid,
    required this.message,
    required this.byId,
    required this.userType,
    required this.date,
    required this.senderName,
  });

  factory TicketConversation.fromJson(Map<String, dynamic> json) {
    return TicketConversation(
      id: json['id'],
      ticketid: json['ticketid'],
      message: json['message'],
      byId: json['by_id'],
      userType: json['user_type'],
      date: json['date'],
      senderName: json['sender_name'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'ticketid': ticketid,
      'message': message,
      'by_id': byId,
      'user_type': userType,
      'date': date,
      'sender_name': senderName,
    };
  }
}

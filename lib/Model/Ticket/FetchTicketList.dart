class FetchTicketList {
  final bool? status;
  final String? message;
  final List<DataForList>? data;

  FetchTicketList({this.status, this.message, this.data});

  factory FetchTicketList.fromJson(Map<String, dynamic> json) {
    return FetchTicketList(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((item) => DataForList.fromJson(item as Map<String, dynamic>))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'message': message,
      'data': data?.map((item) => item.toJson()).toList(),
    };
  }
}

class DataForList {
  final String? id;
  final String? userid;
  final String? product_name;
  final String? leadid;
  final String? subject;
  final String? status;
  final String? date;
  // final dynamic? leadid;

  DataForList({
    this.id,
    this.userid,
    this.leadid,
    this.subject,
    this.status,
    this.date,
    this.product_name
  });

  factory DataForList.fromJson(Map<String, dynamic> json) {
    return DataForList(
      id: json['id'] as String?,
      userid: json['userid'] as String?,
      leadid: json['leadid'] as String?,
      subject: json['subject'] as String?,
      status: json['status'] as String?,
      date: json['date'] as String?,
      product_name: json['product_name'] as String?
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'userid': userid,
      'leadid': leadid,
      'subject': subject,
      'status': status,
      'date': date,
      'product_name': product_name
    };
  }
}

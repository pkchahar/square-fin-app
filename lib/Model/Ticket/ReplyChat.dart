class ReplyChat {
  final bool status;
  final String message;

  ReplyChat({required this.status, required this.message});

  factory ReplyChat.fromJson(Map<String, dynamic> json) {
    return ReplyChat(
      status: json['status'],
      message: json['message'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'message': message,
    };
  }
}

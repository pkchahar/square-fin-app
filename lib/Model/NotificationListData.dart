class NotificationListData {
  NotificationListData({
      this.srNo, 
      this.userId, 
      this.type, 
      this.title, 
      this.message, 
      this.route, 
      this.addStamp,});

  NotificationListData.fromJson(dynamic json) {
    srNo = json['SrNo'];
    userId = json['user_id'];
    type = json['Type'];
    title = json['Title'];
    message = json['Message'];
    route = json['Route'];
    addStamp = json['Add_Stamp'];
  }
  int? srNo;
  String? userId;
  String? type;
  String? title;
  String? message;
  String? route;
  String? addStamp;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['SrNo'] = srNo;
    map['user_id'] = userId;
    map['Type'] = type;
    map['Title'] = title;
    map['Message'] = message;
    map['Route'] = route;
    map['Add_Stamp'] = addStamp;
    return map;
  }

}
class OccupationData {
  OccupationData({
      this.id, 
      this.name,});

  OccupationData.fromJson(dynamic json) {
    id = json['Id'];
    name = json['Name'];
  }
  String? id;
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Id'] = id;
    map['Name'] = name;
    return map;
  }

}
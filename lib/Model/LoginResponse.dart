class LoginResponse {
  final bool status;
  final String message;
  final LoginData? data;

  LoginResponse({
    required this.status,
    required this.message,
    this.data,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      status: json['status'] ?? false,
      message: json['message'] ?? '',
      data: json['Data'] != null ? LoginData.fromJson(json['Data']) : null,
    );
  }
}
class LoginData {
  final String id;
  final String userId;
  final String userType;
  final String type;
  final String name;
  final String mobile;
  final String email;
  final String address;
  final String status;
  final String designationId;
  final String profileType;
  final String serviceLocation;
  final String parentId;
  final String reportingManagerId;
  final String companyId;
  final String zoneId;
  final String managementId;
  final String departmentId;
  final String additionalVerticalIds;
  final String hodId;
  final String nhId;
  final String zhId;
  final String rhId;
  final String chId;
  final String branchVerticalId;
  final String bosmId;
  final String branchId;
  final String subBranchId;
  final String posRmId;
  final String fleetReferenceId;
  final String tempPOSEmpID;
  final String franchiseeId;
  final String username;
  final String salutationType;
  final String resignStatus;
  final String remark;
  final String isEdit;
  final String isPOS;
  final String isMerge;
  final String isCrossSellPrivilege;
  final String posIsFranchise;
  final String isBA;
  final String isBP;
  final String isFleet;
  final String isDelete;
  final String isTransfer;
  final String webAgentStatus;
  final String s3Status;
  final String allowAgentLogin;
  final String irdaTDS;
  final String infraTDS;
  final String dob;
  final String doj;
  final String photo;
  final String otp;
  final String addStamp;
  final String updateStamp;
  final String lastLogin;
  final String primeStatus;
  final String currentCTC;
  final String isSales;
  final String employeeType;
  final String jwtToken;
  final String panNumber;
  final String aadhaarNumber;
  final String isMailVerified;

  LoginData({
    required this.id,
    required this.userId,
    required this.userType,
    required this.type,
    required this.name,
    required this.mobile,
    required this.email,
    required this.address,
    required this.status,
    required this.designationId,
    required this.profileType,
    required this.serviceLocation,
    required this.parentId,
    required this.reportingManagerId,
    required this.companyId,
    required this.zoneId,
    required this.managementId,
    required this.departmentId,
    required this.additionalVerticalIds,
    required this.hodId,
    required this.nhId,
    required this.zhId,
    required this.rhId,
    required this.chId,
    required this.branchVerticalId,
    required this.bosmId,
    required this.branchId,
    required this.subBranchId,
    required this.posRmId,
    required this.fleetReferenceId,
    required this.tempPOSEmpID,
    required this.franchiseeId,
    required this.username,
    required this.salutationType,
    required this.resignStatus,
    required this.remark,
    required this.isEdit,
    required this.isPOS,
    required this.isMerge,
    required this.isCrossSellPrivilege,
    required this.posIsFranchise,
    required this.isBA,
    required this.isBP,
    required this.isFleet,
    required this.isDelete,
    required this.isTransfer,
    required this.webAgentStatus,
    required this.s3Status,
    required this.allowAgentLogin,
    required this.irdaTDS,
    required this.infraTDS,
    required this.dob,
    required this.doj,
    required this.photo,
    required this.otp,
    required this.addStamp,
    required this.updateStamp,
    required this.lastLogin,
    required this.primeStatus,
    required this.currentCTC,
    required this.isSales,
    required this.employeeType,
    required this.jwtToken,
    required this.panNumber,
    required this.aadhaarNumber,
    required this.isMailVerified,
  });

  factory LoginData.fromJson(Map<String, dynamic> json) {
    return LoginData(
      id: json['Id'] ?? '',
      userId: json['User_Id'] ?? '',
      userType: json['User_Type'] ?? '',
      type: json['Type'] ?? '',
      name: json['Name'] ?? '',
      mobile: json['Mobile'] ?? '',
      email: json['Email'] ?? '',
      address: json['Address'] ?? '',
      status: json['Status'] ?? '',
      designationId: json['Designation_Id'] ?? '',
      profileType: json['Profile_Type'] ?? '',
      serviceLocation: json['Service_Location'] ?? '',
      parentId: json['Parent_Id'] ?? '',
      reportingManagerId: json['Reporting_Manager_Id'] ?? '',
      companyId: json['Company_Id'] ?? '',
      zoneId: json['Zone_Id'] ?? '',
      managementId: json['Management_Id'] ?? '',
      departmentId: json['Department_Id'] ?? '',
      additionalVerticalIds: json['Additional_Vertical_Ids'] ?? '',
      hodId: json['Hod_Id'] ?? '',
      nhId: json['NH_Id'] ?? '',
      zhId: json['ZH_Id'] ?? '',
      rhId: json['RH_Id'] ?? '',
      chId: json['CH_Id'] ?? '',
      branchVerticalId: json['Branch_Vertical_Id'] ?? '',
      bosmId: json['BOSM_Id'] ?? '',
      branchId: json['Branch_Id'] ?? '',
      subBranchId: json['SubBranch_Id'] ?? '',
      posRmId: json['POS_RM_Id'] ?? '',
      fleetReferenceId: json['Fleet_Reference_Id'] ?? '',
      tempPOSEmpID: json['Temp_POS_Emp_ID'] ?? '',
      franchiseeId: json['Franchisee_Id'] ?? '',
      username: json['Username'] ?? '',
      salutationType: json['Salutation_Type'] ?? '',
      resignStatus: json['Resign_Status'] ?? '',
      remark: json['Remark'] ?? '',
      isEdit: json['Is_Edit'] ?? '',
      isPOS: json['Is_POS'] ?? '',
      isMerge: json['Is_Merge'] ?? '',
      isCrossSellPrivilege: json['Is_Cross_Sell_Privilege'] ?? '',
      posIsFranchise: json['POS_Is_Franchise'] ?? '',
      isBA: json['Is_BA'] ?? '',
      isBP: json['Is_BP'] ?? '',
      isFleet: json['Is_Fleet'] ?? '',
      isDelete: json['Is_Delete'] ?? '',
      isTransfer: json['Is_Transfer'] ?? '',
      webAgentStatus: json['Web_Agent_Status'] ?? '',
      s3Status: json['S3_Status'] ?? '',
      allowAgentLogin: json['Allow_Agent_Login'] ?? '',
      irdaTDS: json['IRDA_TDS'] ?? '',
      infraTDS: json['INFRA_TDS'] ?? '',
      dob: json['DOB'] ?? '',
      doj: json['DOJ'] ?? '',
      photo: json['Photo'] ?? '',
      otp: json['OTP'] ?? '',
      addStamp: json['Add_Stamp'] ?? '',
      updateStamp: json['Update_Stamp'] ?? '',
      lastLogin: json['Last_Login'] ?? '',
      primeStatus: json['Prime_Status'] ?? '',
      currentCTC: json['Current_CTC'] ?? '',
      isSales: json['Is_Sales'] ?? '',
      employeeType: json['EmployeeType'] ?? '',
      jwtToken: json['jwtToken'] ?? '',
      panNumber: json['pan_number'] ?? '',
      aadhaarNumber: json['aadhaar_number'] ?? '',
      isMailVerified: json['is_mail_verified'] ?? '',
    );
  }
}


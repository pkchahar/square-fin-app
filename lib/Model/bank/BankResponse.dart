class BankResponse {
  List<BankData> data;
  bool status;
  String message;

  BankResponse({
    required this.data,
    required this.status,
    required this.message,
  });

  factory BankResponse.fromJson(Map<String, dynamic> json) {
    var dataList = <BankData>[];
    if (json['data'] != null) {
      dataList = List<BankData>.from(
          json['data'].map((data) => BankData.fromJson(data)));
    }
    return BankResponse(
      status: json['status'],
      message: json['message'],
      data: dataList,
    );
  }
}

class BankData {
  String id;
  String userId;
  String bankName;
  String branchName;
  String accountType;
  String accountNumber;
  String ifscCode;
  String isPrimary;
  String date;
  String kycStatus;

  BankData({
    required this.id,
    required this.userId,
    required this.bankName,
    required this.branchName,
    required this.accountType,
    required this.accountNumber,
    required this.ifscCode,
    required this.isPrimary,
    required this.date,
    required this.kycStatus,
  });

  factory BankData.fromJson(Map<String, dynamic> json) {
    return BankData(
      id: json['id'] ?? "",
      userId: json['userid'] ?? "",
      bankName: json['bank_name'] ?? "",
      branchName: json['branch_name'] ?? "",
      accountType: json['account_type'] ?? "",
      accountNumber: json['account_number'] ?? "",
      ifscCode: json['ifsc_code'] ?? "",
      isPrimary: json['is_primary'] ?? "",
      date: json['date'] ?? "",
      kycStatus: json['kyc_status'] ?? "",
    );
  }
}

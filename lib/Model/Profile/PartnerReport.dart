class PartnerReport {
  final bool status;
  final List<PartnerData> data;

  PartnerReport({
    required this.status,
    required this.data,
  });

  factory PartnerReport.fromJson(Map<String, dynamic> json) {
    return PartnerReport(
      status: json['status'] as bool,
      data: (json['data'] as List<dynamic>)
          .map((e) => PartnerData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'data': data.map((e) => e.toJson()).toList(),
    };
  }
}

class PartnerData {
  final int srNo;
  final String id;
  final String name;
  final String email;
  final String mobile;
  final String empId;
  final String date;

  PartnerData({
    required this.srNo,
    required this.id,
    required this.name,
    required this.email,
    required this.mobile,
    required this.empId,
    required this.date,
  });

  factory PartnerData.fromJson(Map<String, dynamic> json) {
    return PartnerData(
      srNo: json['SrNo'] as int,
      id: json['id'] as String,
      name: json['name'] as String,
      email: json['email'] as String,
      mobile: json['mobile'] as String,
      empId: json['emp_id'] as String,
      date: json['date'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'SrNo': srNo,
      'id': id,
      'name': name,
      'email': email,
      'mobile': mobile,
      'emp_id': empId,
      'date': date,
    };
  }
}

import 'FileName.dart';

class DocList {
  DocList({
      this.status, 
      this.fileName, 
      this.message,});

  DocList.fromJson(dynamic json) {
    status = json['status'];
    if (json['FileName'] != null) {
      fileName = [];
      json['FileName'].forEach((v) {
        fileName?.add(FileName.fromJson(v));
      });
    }
    message = json['Message'];
  }
  int? status;
  List<FileName>? fileName;
  String? message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    if (fileName != null) {
      map['FileName'] = fileName?.map((v) => v.toJson()).toList();
    }
    map['Message'] = message;
    return map;
  }

}
class CustomerReport {
  final bool status;
  final List<CustomerData> data;

  CustomerReport({
    required this.status,
    required this.data,
  });

  factory CustomerReport.fromJson(Map<String, dynamic> json) {
    var list = json['data'] as List;
    List<CustomerData> dataList = list.map((i) => CustomerData.fromJson(i)).toList();

    return CustomerReport(
      status: json['status'] as bool,
      data: dataList,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'data': data.map((e) => e.toJson()).toList(),
    };
  }
}

class CustomerData {
  final int srNo;
  final String pan;//
  final String name;//
  final String email;//
  final String mobile;//
  final String empId;//
  final String date;//

  CustomerData({
    required this.srNo,
    required this.pan,
    required this.name,
    required this.email,
    required this.mobile,
    required this.empId,
    required this.date,
  });

  factory CustomerData.fromJson(Map<String, dynamic> json) {
    return CustomerData(
      srNo: json['SrNo'] as int,
      pan: json['id'] as String,
      name: json['name'] as String,
      email: json['email'] as String,
      mobile: json['mobile'] as String,
      empId: json['emp_id'] as String,
      date: json['date'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'SrNo': srNo,
      'id': pan,
      'name': name,
      'email': email,
      'mobile': mobile,
      'emp_id': empId,
      'date': date,
    };
  }
}

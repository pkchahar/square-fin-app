class BusinessReport {
  final bool? status;
  final List<BusinessData>? data;

  BusinessReport({this.status, this.data});

  factory BusinessReport.fromJson(Map<String, dynamic> json) {
    return BusinessReport(
      status: json['status'] as bool?,
      data: (json['data'] as List<dynamic>?)
          ?.map((item) => BusinessData.fromJson(item as Map<String, dynamic>))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'data': data?.map((item) => item.toJson()).toList(),
    };
  }
}

class BusinessData {
  final int? srNo;
  final String? id;
  final String? srNumber;
  final String? customerType;
  final String? customerName;
  final String? customerMobile;
  final String? productName;
  final String? productType;
  final String? rm;
  final String? agent;
  final String? financeCoordinator;
  final String? financeManager;
  final String? loanApplicationAmount;
  final String? sanctionAmount;
  final String? disbursedAmount;
  final String? totalRevenue;
  final String? fileStatus;
  final String? srCreateDate;
  final String? srUpdateDate;
  final String? currentSrUserName;
  final String? srCreateUser;
  final int? isEdit;

  BusinessData({
    this.srNo,
    this.id,
    this.srNumber,
    this.customerType,
    this.customerName,
    this.customerMobile,
    this.productName,
    this.productType,
    this.rm,
    this.agent,
    this.financeCoordinator,
    this.financeManager,
    this.loanApplicationAmount,
    this.sanctionAmount,
    this.disbursedAmount,
    this.totalRevenue,
    this.fileStatus,
    this.srCreateDate,
    this.srUpdateDate,
    this.currentSrUserName,
    this.srCreateUser,
    this.isEdit,
  });

  factory BusinessData.fromJson(Map<String, dynamic> json) {
    return BusinessData(
      srNo: json['SrNo'] as int?,
      id: json['Id'] as String?,
      srNumber: json['SR_No'] as String?,
      customerType: json['Customer_Type'] as String?,
      customerName: json['Customer_Name'] as String?,
      customerMobile: json['Customer_Mobile'] as String?,
      productName: json['Product_Name'] as String?,
      productType: json['Product_Type'] as String?,
      rm: json['RM'] as String?,
      agent: json['Agent'] as String?,
      financeCoordinator: json['Finance_Coordinator'] as String?,
      financeManager: json['Finance_Manager'] as String?,
      loanApplicationAmount: json['Loan_Application_Amount'] as String?,
      sanctionAmount: json['SenctionAmount'] as String?,
      disbursedAmount: json['DisbursedAmount'] as String?,
      totalRevenue: json['Total_Revenue'] as String?,
      fileStatus: json['File_Status'] as String?,
      srCreateDate: json['SR_Create_Date'] as String?,
      srUpdateDate: json['SR_Update_Date'] as String?,
      currentSrUserName: json['CurrentSrUserName'] as String?,
      srCreateUser: json['SRCreateUser'] as String?,
      isEdit: json['Is_Edit'] as int?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'SrNo': srNo,
      'Id': id,
      'SR_No': srNumber,
      'Customer_Type': customerType,
      'Customer_Name': customerName,
      'Customer_Mobile': customerMobile,
      'Product_Name': productName,
      'Product_Type': productType,
      'RM': rm,
      'Agent': agent,
      'Finance_Coordinator': financeCoordinator,
      'Finance_Manager': financeManager,
      'Loan_Application_Amount': loanApplicationAmount,
      'SenctionAmount': sanctionAmount,
      'DisbursedAmount': disbursedAmount,
      'Total_Revenue': totalRevenue,
      'File_Status': fileStatus,
      'SR_Create_Date': srCreateDate,
      'SR_Update_Date': srUpdateDate,
      'CurrentSrUserName': currentSrUserName,
      'SRCreateUser': srCreateUser,
      'Is_Edit': isEdit,
    };
  }
}

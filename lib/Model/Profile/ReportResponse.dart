class ReportResponse {
  final bool status;
  final List<Lead> data;

  ReportResponse({
    required this.status,
    required this.data,
  });

  factory ReportResponse.fromJson(Map<String, dynamic> json) {
    return ReportResponse(
      status: json['status']  as bool,
      data: (json['data']  as List<dynamic>)
          .map((item) => Lead.fromJson(item as Map<String, dynamic>))
          .toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'data': data.map((item) => item.toJson()).toList(),
    };
  }
}

class Lead {
  final int srNo;
  final String id;
  final String refferdIds;
  final String name;
  final String email;
  final String createdAt;
  final String refferdname;
  final String mobile;
  final String productname;
  final String status;
  final String remark;
  final String? bsleadid;
  final String? campaignUrl;
  final String leadId;
  final String icon;
  final int isTicket;
  final dynamic ticketId;

  Lead({
    required this.srNo,
    required this.id,
    required this.refferdIds,
    required this.name,
    required this.email,
    required this.createdAt,
    required this.refferdname,
    required this.mobile,
    required this.productname,
    required this.status,
    required this.remark,
    this.bsleadid,
    this.campaignUrl,
    required this.leadId,
    required this.icon,
    required this.isTicket,
    required this.ticketId,
  });

  factory Lead.fromJson(Map<String, dynamic> json) {
    return Lead(
      srNo: json['SrNo'] ?? 0 ,
      id: json['id'] ?? '',
      refferdIds: json['refferd_ids'] ?? '',
      name: json['name'] ?? '',
      email: json['email'] ?? '',
      createdAt: json['created_at'] ?? '',
      refferdname: json['Refferdname'] ?? '',
      mobile: json['mobile'] ?? '',
      productname: json['Productname'] ?? '',
      status: json['status'] ?? '',
      remark: json['remark'] ?? '',
      bsleadid: json['bsleadid'] ?? '',
      campaignUrl: json['campaign_url'] ?? '',
      leadId: json['lead_id'] ?? '',
      icon: json['icon'] ?? '',
      isTicket: json['is_ticket'] ?? "",
      ticketId: json['ticketid'] ?? 0 as String ,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'SrNo': srNo,
      'id': id,
      'refferd_ids': refferdIds,
      'name': name,
      'email': email,
      'created_at': createdAt,
      'Refferdname': refferdname,
      'mobile': mobile,
      'Productname': productname,
      'status': status,
      'remark': remark,
      'bsleadid': bsleadid,
      'campaign_url': campaignUrl,
      'lead_id': leadId,
      'icon': icon,
      'is_ticket': isTicket,
      'ticketid': ticketId,
    };
  }
}

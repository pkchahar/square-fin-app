class FileName {
  FileName({
    this.documentType,
    this.documentFileName,
  });

  FileName.fromJson(dynamic json) {
    documentType = json['Document_Type'];
    documentFileName = json['Document_File_Name'];
  }

  String? documentType;
  String? documentFileName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Document_Type'] = documentType;
    map['Document_File_Name'] = documentFileName;
    return map;
  }
}

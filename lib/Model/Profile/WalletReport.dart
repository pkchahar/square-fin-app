class WalletReport {
  bool status;
  List<TransactionData> data;

  WalletReport({required this.status, required this.data});

  factory WalletReport.fromJson(Map<String, dynamic> json) {
    var list = json['data'] as List;
    List<TransactionData> dataList = list.map((i) => TransactionData.fromJson(i)).toList();

    return WalletReport(
      status: json['status'] as bool,
      data: dataList,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'data': data.map((v) => v.toJson()).toList(),
    };
  }
}

class TransactionData {
  int srNo;
  String id;
  String amount;
  String refferenceId;
  String transactionType;
  String userId;
  String remark;
  String transactionReason;
  String date;
  String withdrawalStatus;

  TransactionData({
    required this.srNo,
    required this.id,
    required this.amount,
    required this.refferenceId,
    required this.transactionType,
    required this.userId,
    required this.remark,
    required this.transactionReason,
    required this.date,
    required this.withdrawalStatus,
  });

  factory TransactionData.fromJson(Map<String, dynamic> json) {
    return TransactionData(
      srNo: json['SrNo'] as int,
      id: json['id'] as String,
      amount: json['amount'] as String,
      refferenceId: json['refference_id'] as String,
      transactionType: json['transaction_type'] as String,
      userId: json['userid'] as String,
      remark: json['remark'] as String,
      transactionReason: json['transaction_reason'] as String,
      date: json['date'] as String,
      withdrawalStatus: json['withdrawal_status'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'SrNo': srNo,
      'id': id,
      'amount': amount,
      'refference_id': refferenceId,
      'transaction_type': transactionType,
      'userid': userId,
      'remark': remark,
      'transaction_reason': transactionReason,
      'date': date,
      'withdrawal_status': withdrawalStatus,
    };
  }
}

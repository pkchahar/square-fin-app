class OtpMailVerification {
  final String status;
  final String message;

  OtpMailVerification({required this.status, required this.message});

  factory OtpMailVerification.fromJson(Map<String, dynamic> json) {
    return OtpMailVerification(
      status: json['status'].toString(),
      message: json['message'].toString(),
    );
  }


  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'message': message,
    };
  }
}

import 'Data.dart';

class PosList {
  PosList({
      this.status, 
      this.message, 
      this.data,});

  PosList.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    if (json['Data'] != null) {
      data = [];
      json['Data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
  }
  bool? status;
  String? message;
  List<Data>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    final data = this.data;
    if (data != null) {
      map['Data'] = data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

class AddPartnerModel {
  final bool status;
  final String msg;

  AddPartnerModel({required this.status, required this.msg});

  // Factory constructor to create an instance from a JSON map
  factory AddPartnerModel.fromJson(Map<String, dynamic> json) {
    return AddPartnerModel(
      status: json['status'] ?? '',
      msg: json['msg'] ?? ''
    );
  }

  // Method to convert an instance to a JSON map
  Map<String, dynamic> toJson() {
    return {
      'status': status,
      'msg': msg,
    };
  }
}

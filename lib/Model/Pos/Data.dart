class Data {
  Data({
      this.srNo, 
      this.id, 
      this.tempStatus, 
      this.empId, 
      this.mergeCode, 
      this.mergeCode2, 
      this.mergeCode3, 
      this.type, 
      this.designationId, 
      this.profileType, 
      this.serviceLocation, 
      this.parentId, 
      this.reportingManagerId, 
      this.companyId, 
      this.zoneId, 
      this.managementId, 
      this.departmentId, 
      this.additionalVerticalIds, 
      this.coreline, 
      this.branchVerticalId, 
      this.bOSMId, 
      this.branchId, 
      this.subBranchId, 
      this.pOSRMId, 
      this.fleetReferenceId, 
      this.tempPOSEmpID, 
      this.franchiseeId, 
      this.username, 
      this.password, 
      this.pass, 
      this.salutationType, 
      this.name, 
      this.mobile, 
      this.email, 
      this.address, 
      this.status, 
      this.resignStatus, 
      this.remark, 
      this.isEdit, 
      this.isPOS, 
      this.isMerge, 
      this.isCrossSellPrivilege, 
      this.pOSIsFranchise, 
      this.isBA, 
      this.isBP, 
      this.isFleet, 
      this.isDelete, 
      this.isTransfer, 
      this.webAgentStatus, 
      this.s3Status, 
      this.allowAgentLogin, 
      this.irdatds, 
      this.infratds, 
      this.dob, 
      this.doj, 
      this.resignDate, 
      this.photo, 
      this.insertDate, 
      this.updateStamp, 
      this.lastLogin, 
      this.primeStatus, 
      this.currentCTC, 
      this.currentTier, 
      this.isSales, 
      this.employeeType, 
      this.teleRm, 
      this.fcmKey,});

  Data.fromJson(dynamic json) {
    srNo = json['SrNo'];
    id = json['Id'];
    tempStatus = json['Temp_Status'];
    empId = json['Emp_Id'];
    mergeCode = json['Merge_Code'];
    mergeCode2 = json['Merge_Code_2'];
    mergeCode3 = json['Merge_Code_3'];
    type = json['Type'];
    designationId = json['Designation_Id'];
    profileType = json['Profile_Type'];
    serviceLocation = json['Service_Location'];
    parentId = json['Parent_Id'];
    reportingManagerId = json['Reporting_Manager_Id'];
    companyId = json['Company_Id'];
    zoneId = json['Zone_Id'];
    managementId = json['Management_Id'];
    departmentId = json['Department_Id'];
    additionalVerticalIds = json['Additional_Vertical_Ids'];
    coreline = json['Coreline'];
    branchVerticalId = json['Branch_Vertical_Id'];
    bOSMId = json['BOSM_Id'];
    branchId = json['Branch_Id'];
    subBranchId = json['SubBranch_Id'];
    pOSRMId = json['POS_RM_Id'];
    fleetReferenceId = json['Fleet_Reference_Id'];
    tempPOSEmpID = json['Temp_POS_Emp_ID'];
    franchiseeId = json['Franchisee_Id'];
    username = json['Username'];
    password = json['Password'];
    pass = json['Pass'];
    salutationType = json['Salutation_Type'];
    name = json['Name'];
    mobile = json['Mobile'];
    email = json['Email'];
    address = json['Address'];
    status = json['Status'];
    resignStatus = json['Resign_Status'];
    remark = json['Remark'];
    isEdit = json['Is_Edit'];
    isPOS = json['Is_POS'];
    isMerge = json['Is_Merge'];
    isCrossSellPrivilege = json['Is_Cross_Sell_Privilege'];
    pOSIsFranchise = json['POS_Is_Franchise'];
    isBA = json['Is_BA'];
    isBP = json['Is_BP'];
    isFleet = json['Is_Fleet'];
    isDelete = json['Is_Delete'];
    isTransfer = json['Is_Transfer'];
    webAgentStatus = json['Web_Agent_Status'];
    s3Status = json['S3_Status'];
    allowAgentLogin = json['Allow_Agent_Login'];
    irdatds = json['IRDA_TDS'];
    infratds = json['INFRA_TDS'];
    dob = json['DOB'];
    doj = json['DOJ'];
    resignDate = json['Resign_Date'];
    photo = json['Photo'];
    insertDate = json['insert_date'];
    updateStamp = json['Update_Stamp'];
    lastLogin = json['Last_Login'];
    primeStatus = json['Prime_Status'];
    currentCTC = json['Current_CTC'];
    currentTier = json['Current_Tier'];
    isSales = json['Is_Sales'];
    employeeType = json['EmployeeType'];
    teleRm = json['TeleRm'];
    fcmKey = json['fcm_key'];
  }
  int? srNo;
  String ? id;
  String ? tempStatus;
  String ? empId;
  String ? mergeCode;
  String ? mergeCode2;
  String ? mergeCode3;
  String ? type;
  String ? designationId;
  String ? profileType;
  String ? serviceLocation;
  String ? parentId;
  String ? reportingManagerId;
  String ? companyId;
  String ? zoneId;
  String ? managementId;
  String ? departmentId;
  String ? additionalVerticalIds;
  String ? coreline;
  String ? branchVerticalId;
  String ? bOSMId;
  String ? branchId;
  String ? subBranchId;
  String ? pOSRMId;
  String ? fleetReferenceId;
  String ? tempPOSEmpID;
  String ? franchiseeId;
  String ? username;
  String ? password;
  String ? pass;
  String ? salutationType;
  String ? name;
  String ? mobile;
  String ? email;
  String ? address;
  String ? status;
  String ? resignStatus;
  String ? remark;
  String ? isEdit;
  String ? isPOS;
  String ? isMerge;
  String ? isCrossSellPrivilege;
  String ? pOSIsFranchise;
  String ? isBA;
  String ? isBP;
  String ? isFleet;
  String ? isDelete;
  String ? isTransfer;
  String ? webAgentStatus;
  String ? s3Status;
  String ? allowAgentLogin;
  String ? irdatds;
  String ? infratds;
  String ? dob;
  String ? doj;
  String ? resignDate;
  String ? photo;
  String ? insertDate;
  String ? updateStamp;
  String ? lastLogin;
  String ? primeStatus;
  String ? currentCTC;
  String ? currentTier;
  String ? isSales;
  String ? employeeType;
  String ? teleRm;
  String ? fcmKey;

  Map<String ?, dynamic> toJson() {
    final map = <String ?, dynamic>{};
    map['SrNo'] = srNo;
    map['Id'] = id;
    map['Temp_Status'] = tempStatus;
    map['Emp_Id'] = empId;
    map['Merge_Code'] = mergeCode;
    map['Merge_Code_2'] = mergeCode2;
    map['Merge_Code_3'] = mergeCode3;
    map['Type'] = type;
    map['Designation_Id'] = designationId;
    map['Profile_Type'] = profileType;
    map['Service_Location'] = serviceLocation;
    map['Parent_Id'] = parentId;
    map['Reporting_Manager_Id'] = reportingManagerId;
    map['Company_Id'] = companyId;
    map['Zone_Id'] = zoneId;
    map['Management_Id'] = managementId;
    map['Department_Id'] = departmentId;
    map['Additional_Vertical_Ids'] = additionalVerticalIds;
    map['Coreline'] = coreline;
    map['Branch_Vertical_Id'] = branchVerticalId;
    map['BOSM_Id'] = bOSMId;
    map['Branch_Id'] = branchId;
    map['SubBranch_Id'] = subBranchId;
    map['POS_RM_Id'] = pOSRMId;
    map['Fleet_Reference_Id'] = fleetReferenceId;
    map['Temp_POS_Emp_ID'] = tempPOSEmpID;
    map['Franchisee_Id'] = franchiseeId;
    map['Username'] = username;
    map['Password'] = password;
    map['Pass'] = pass;
    map['Salutation_Type'] = salutationType;
    map['Name'] = name;
    map['Mobile'] = mobile;
    map['Email'] = email;
    map['Address'] = address;
    map['Status'] = status;
    map['Resign_Status'] = resignStatus;
    map['Remark'] = remark;
    map['Is_Edit'] = isEdit;
    map['Is_POS'] = isPOS;
    map['Is_Merge'] = isMerge;
    map['Is_Cross_Sell_Privilege'] = isCrossSellPrivilege;
    map['POS_Is_Franchise'] = pOSIsFranchise;
    map['Is_BA'] = isBA;
    map['Is_BP'] = isBP;
    map['Is_Fleet'] = isFleet;
    map['Is_Delete'] = isDelete;
    map['Is_Transfer'] = isTransfer;
    map['Web_Agent_Status'] = webAgentStatus;
    map['S3_Status'] = s3Status;
    map['Allow_Agent_Login'] = allowAgentLogin;
    map['IRDA_TDS'] = irdatds;
    map['INFRA_TDS'] = infratds;
    map['DOB'] = dob;
    map['DOJ'] = doj;
    map['Resign_Date'] = resignDate;
    map['Photo'] = photo;
    map['insert_date'] = insertDate;
    map['Update_Stamp'] = updateStamp;
    map['Last_Login'] = lastLogin;
    map['Prime_Status'] = primeStatus;
    map['Current_CTC'] = currentCTC;
    map['Current_Tier'] = currentTier;
    map['Is_Sales'] = isSales;
    map['EmployeeType'] = employeeType;
    map['TeleRm'] = teleRm;
    map['fcm_key'] = fcmKey;
    return map;
  }

}
class UploadFile {
  UploadFile({
      this.status, 
      this.message, 
      this.fileName,});

  UploadFile.fromJson(dynamic json) {
    status = json['Status'];
    message = json['Message'];
    fileName = json['FileName'];
  }
  bool? status;
  String? message;
  String? fileName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Status'] = status;
    map['Message'] = message;
    map['FileName'] = fileName;
    return map;
  }

}
class PartnerList {
  final dynamic srNo;
  final String id;
  final String name;
  final String email;
  final String mobile;
  final String empId;
  final String date;

  PartnerList({
    required this.srNo,
    required this.id,
    required this.name,
    required this.email,
    required this.mobile,
    required this.empId,
    required this.date,
  });

  factory PartnerList.fromJson(Map<String, dynamic> json) {
    return PartnerList(
      srNo: json['SrNo'] ?? '',
      id: json['id'] ?? '',
      name: json['name'] ?? '',
      email: json['email'] ?? '',
      mobile: json['mobile'] ?? '',
      empId: json['emp_id'] ?? '',
      date: json['date'] ?? ''
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'SrNo': srNo,
      'id': id,
      'name': name,
      'email': email,
      'mobile': mobile,
      'emp_id': empId,
      'date': date,
    };
  }
}

class PartnerRes {
  final List<PartnerList>? data;
  final bool? status;
  final String? msg;

  PartnerRes({
    this.data,
    this.status,
    this.msg,
  });

  factory PartnerRes.fromJson(Map<String, dynamic> json) {
    return PartnerRes(
      data: (json['Data'] as List<dynamic>?)
          ?.map((e) => PartnerList.fromJson(e as Map<String, dynamic>))
          .toList(),
      status: json['status'] as bool?,
      msg: json['msg'] as String?,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'Data': data?.map((e) => e.toJson()).toList(),
      'status': status,
      'msg': msg,
    };
  }
}

import 'BankList.dart';

class Bank {
  Bank({
      this.bankList,
      this.status, 
      this.message,});

  Bank.fromJson(dynamic json) {
    if (json['Data'] != null) {
      bankList = [];
      json['Data'].forEach((v) {
        bankList!.add(BankList.fromJson(v));
      });
    }
    status = json['status'];
    message = json['message'];
  }
  List<BankList>? bankList;
  int? status;
  String? message;

  Map<String?, dynamic> toJson() {
    final map = <String?, dynamic>{};
    if (bankList != null) {
      map['Data'] = bankList!.map((v) => v.toJson()).toList();
    }
    map['status'] = status;
    map['message'] = message;
    return map;
  }

}
class WalletTransac {
  final List<TransactionData> data;
  final bool status;
  final String message;

  WalletTransac({
    required this.data,
    required this.status,
    required this.message,
  });

  factory WalletTransac.fromJson(Map<String, dynamic> json) {

    var dataList = <TransactionData>[];
    if (json['data'] != null) {
    dataList = List<TransactionData>.from(json['data'].map((data) => TransactionData.fromJson(data)));
    }

    return WalletTransac(
    status: json['status'],
    message: json['message'],
    data: dataList,
    );
  }
}

class TransactionData {
  final String id;
  final String amount;
  final String transactionType;
  final String referenceId;
  final String transactionReason;
  final String withdrawalStatus;
  final String userId;
  final String remark;
  final String date;

  TransactionData({
    required this.id,
    required this.amount,
    required this.transactionType,
    required this.referenceId,
    required this.transactionReason,
    required this.withdrawalStatus,
    required this.userId,
    required this.remark,
    required this.date,
  });

  factory TransactionData.fromJson(Map<String, dynamic> json) {
    return TransactionData(
      id: json['id'] as String,
      amount: json['amount'] as String,
      transactionType: json['transaction_type'] as String,
      referenceId: json['refference_id'] as String,
      transactionReason: json['transaction_reason'] as String,
      withdrawalStatus: json['withdrawal_status'] as String,
      userId: json['userid'] as String,
      remark: json['remark'] as String,
      date: json['date'] as String,
    );
  }
}

class NewWalletResponse {
  bool? status;
  String? message;
  List<MonthData>? data;

  NewWalletResponse({this.status, this.message, this.data});

  factory NewWalletResponse.fromJson(Map<String, dynamic> json) {
    return NewWalletResponse(
      status: json['status'],
      message: json['message'],
      data: json['data'] != null
          ? (json['data'] as List<dynamic>).map((monthData) {
        return MonthData.fromJson(monthData as Map<String, dynamic>);
      }).toList()
          : null,
    );
  }


}

class MonthData {
  Map<String, List<Transaction>>? monthData;

  MonthData({this.monthData});

  factory MonthData.fromJson(Map<String, dynamic> json) {

    print("jsonjson");
    return MonthData(
      monthData: json.keys.map((key) {
        return MapEntry(
          key,
          List<Transaction>.from(json[key].map((x) => Transaction.fromJson(x))),
        );
      }).fold<Map<String, List<Transaction>>>({}, (prev, entry) {
        // prev ??= {};
        prev.addAll({entry.key: entry.value});
        return prev;
      }),
    );
  }
}


class Transaction {
  String? id;
  String? amount;
  String? transactionType;
  String? referenceId;
  String? userId;
  String? remark;
  String? transactionReason;
  String? date;
  String? withdrawalStatus;

  Transaction({
    this.id,
    this.amount,
    this.transactionType,
    this.referenceId,
    this.userId,
    this.remark,
    this.transactionReason,
    this.date,
    this.withdrawalStatus,
  });
  factory Transaction.fromJson(Map<String, dynamic> json) {
    return Transaction(
      id: json['id'],
      amount: json['amount'],
      transactionType: json['transaction_type'],
      referenceId: json['refference_id'],
      userId: json['userid'],
      remark: json['remark'],
      transactionReason: json['transaction_reason'],
      date: json['date'],
      withdrawalStatus: json['withdrawal_status'],
    );
  }
}

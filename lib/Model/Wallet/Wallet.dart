class Wallet {
  final WalletData? data;
  final bool status;
  final String message;

  Wallet({
    this.data,
    required this.status,
    required this.message,
  });

  factory Wallet.fromJson(Map<String, dynamic> json) {
    return Wallet(
      data: WalletData.fromJson(json['data'] as Map<String, dynamic>?),
      status: json['status'] as bool,
      message: json['message'] as String,
    );
  }
}

class WalletData {
  final dynamic walletBalance;
  final dynamic withdrawableBalance;
  final dynamic totalEarning;

  WalletData({
    required this.walletBalance,
    required this.withdrawableBalance,
    required this.totalEarning,
  });

  factory WalletData.fromJson(Map<String, dynamic>? json) {
    return WalletData(
      walletBalance: json?['wallet_balance'].toString(),
      withdrawableBalance: json?['withdrawable_balance'].toString(),
      totalEarning: json?['total_earning'].toString(),
    );
  }
}

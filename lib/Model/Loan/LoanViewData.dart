class LoanViewData {
  LoanViewData({
      this.srNo, 
      this.id, 
      this.name, 
      this.email, 
      this.mobile, 
      this.loanType, 
      this.customerType, 
      this.location, 
      this.loanAmount, 
      this.salarySlip, 
      this.product, 
      this.status, 
      this.typess, 
      this.jobProfile, 
      this.businessVintage, 
      this.businessTurnover, 
      this.minimumSalary, 
      this.companyName, 
      this.workingExpirence, 
      this.vehicleType, 
      this.productType, 
      this.others, 
      this.occupationDetail, 
      this.bankName, 
      this.propertyValue, 
      this.propertyType, 
      this.homeLoanCategory, 
      this.carModel, 
      this.productCategory, 
      this.subject, 
      this.message, 
      this.insertDate,});

  LoanViewData.fromJson(dynamic json) {
    srNo = json['SrNo'];
    id = json['id'];
    name = json['name'];
    email = json['email'];
    mobile = json['mobile'];
    loanType = json['loanType'];
    customerType = json['customerType'];
    location = json['location'];
    loanAmount = json['loanAmount'];
    salarySlip = json['salarySlip'];
    product = json['product'];
    status = json['status'];
    typess = json['typess'];
    jobProfile = json['jobProfile'];
    businessVintage = json['businessVintage'];
    businessTurnover = json['businessTurnover'];
    minimumSalary = json['minimumSalary'];
    companyName = json['companyName'];
    workingExpirence = json['workingExpirence'];
    vehicleType = json['vehicleType'];
    productType = json['productType'];
    others = json['others'];
    occupationDetail = json['occupationDetail'];
    bankName = json['bankName'];
    propertyValue = json['propertyValue'];
    propertyType = json['propertyType'];
    homeLoanCategory = json['homeLoanCategory'];
    carModel = json['carModel'];
    productCategory = json['productCategory'];
    subject = json['subject'];
    message = json['message'];
    insertDate = json['insert_date'];
  }
  int? srNo;
  String? id;
  String? name;
  String? email;
  String? mobile;
  String? loanType;
  String? customerType;
  String? location;
  String? loanAmount;
  String? salarySlip;
  String? product;
  String? status;
  String? typess;
  String? jobProfile;
  String? businessVintage;
  String? businessTurnover;
  String? minimumSalary;
  String? companyName;
  String? workingExpirence;
  String? vehicleType;
  String? productType;
  String? others;
  String? occupationDetail;
  String? bankName;
  String? propertyValue;
  String? propertyType;
  String? homeLoanCategory;
  String? carModel;
  String? productCategory;
  String? subject;
  String? message;
  String? insertDate;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['SrNo'] = srNo;
    map['id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['mobile'] = mobile;
    map['loanType'] = loanType;
    map['customerType'] = customerType;
    map['location'] = location;
    map['loanAmount'] = loanAmount;
    map['salarySlip'] = salarySlip;
    map['product'] = product;
    map['status'] = status;//todo
    map['typess'] = typess;
    map['jobProfile'] = jobProfile;
    map['businessVintage'] = businessVintage;
    map['businessTurnover'] = businessTurnover;
    map['minimumSalary'] = minimumSalary;
    map['companyName'] = companyName;
    map['workingExpirence'] = workingExpirence;
    map['vehicleType'] = vehicleType;
    map['productType'] = productType;
    map['others'] = others;
    map['occupationDetail'] = occupationDetail;
    map['bankName'] = bankName;
    map['propertyValue'] = propertyValue;
    map['propertyType'] = propertyType;
    map['homeLoanCategory'] = homeLoanCategory;
    map['carModel'] = carModel;
    map['productCategory'] = productCategory;
    map['subject'] = subject;
    map['message'] = message;
    map['insert_date'] = insertDate;
    return map;
  }

}
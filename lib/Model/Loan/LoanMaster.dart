import 'LoanMasterData.dart';

class LoanMaster {
  LoanMaster({
      this.status, 
      this.msg, 
      this.data,});

  LoanMaster.fromJson(dynamic json) {
    status = json['status'];
    msg = json['msg'];
    if (json['Data'] != null) {
      data = [];
      json['Data'].forEach((v) {
        data?.add(LoanMasterData.fromJson(v));
      });
    }
  }
  int? status;
  String? msg;
  List<LoanMasterData>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['msg'] = msg;
    final data = this.data;
    if (data != null) {
      map['Data'] = data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}
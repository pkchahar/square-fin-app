class DsaResponse {
  final bool? status;
  final String? message;
  final List<DsaData>? data;

  DsaResponse({this.status, this.message, this.data});

  // Factory constructor to create a DsaResponse object from JSON
  factory DsaResponse.fromJson(Map<String, dynamic> json) {
    return DsaResponse(
      status: json['Status'] as bool?,
      message: json['Message'] as String?,
      data: (json['Data'] as List<dynamic>?)
          ?.map((item) => DsaData.fromJson(item as Map<String, dynamic>))
          .toList(),
    );
  }

  // Method to convert DsaResponse object to JSON
  Map<String, dynamic> toJson() {
    return {
      'Status': status,
      'Message': message,
      'Data': data?.map((item) => item.toJson()).toList(),
    };
  }
}

class DsaData {
  final String? id;
  final String? name;

  DsaData({this.id, this.name});

  // Factory constructor to create a DsaData object from JSON
  factory DsaData.fromJson(Map<String, dynamic> json) {
    return DsaData(
      id: json['Id'] as String?,
      name: json['Name'] as String?,
    );
  }

  // Method to convert DsaData object to JSON
  Map<String, dynamic> toJson() {
    return {
      'Id': id,
      'Name': name,
    };
  }
}

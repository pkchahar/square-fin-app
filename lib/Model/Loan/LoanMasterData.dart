class LoanMasterData {
  LoanMasterData({
      this.url, 
      this.type,});

  LoanMasterData.fromJson(dynamic json) {
    url = json['Url'];
    type = json['Type'];
  }
  String? url;
  String? type;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Url'] = url;
    map['Type'] = type;
    return map;
  }

}
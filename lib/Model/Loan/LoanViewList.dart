import 'LoanViewData.dart';

class LoanViewList {
  LoanViewList({
      this.draw, 
      this.recordsTotal, 
      this.recordsFiltered, 
      this.data, 
      this.status,});

  LoanViewList.fromJson(dynamic json) {
    draw = json['draw'];
    recordsTotal = json['recordsTotal'];
    recordsFiltered = json['recordsFiltered'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(LoanViewData.fromJson(v));
      });
    }
    status = json['status'];
  }
  int? draw;
  int? recordsTotal;
  int? recordsFiltered;
  List<LoanViewData>? data;
  bool? status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['draw'] = draw;
    map['recordsTotal'] = recordsTotal;
    map['recordsFiltered'] = recordsFiltered;
    final data = this.data;
    if (data != null) {
      map['data'] = data.map((v) => v.toJson()).toList();
    }
    map['status'] = status;
    return map;
  }

}
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pi/firebase_options.dart';
import 'package:get_storage/get_storage.dart';
import 'activity/Utility Controllers/SplashScreen.dart';
import 'package:local_auth/local_auth.dart';

Future <void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  //Firebase added by Rohit
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  try {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
  } catch (e) {}
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: MyApp.customBlue, // Status bar color
    statusBarIconBrightness: Brightness.light, // For Android devices
    statusBarBrightness: Brightness.dark, // For iOS devices
  ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  static const Map<int, Color> blueShades = {
    50: Color(0xFFE3F2FD),
    100: Color(0xFFBBDEFB),
    200: Color(0xFF90CAF9),
    300: Color(0xFF64B5F6),
    400: Color(0xFF42A5F5),
    500: Color(0xFF2196F3),
    600: Color(0xFF1E88E5),
    700: Color(0xFF1976D2),
    800: Color(0xFF1565C0),
    900: Color(0xFF0D47A1),
  };

  static const MaterialColor customBlue = MaterialColor(0xFFE3F2FD, blueShades);

  @override
  Widget build(BuildContext context) {
    MaterialColor white = const MaterialColor(0xFFFFFFFF, <int, Color>{
      50: Colors.white,
      100: Colors.white,
      200: Colors.white,
      300: Colors.white,
      400: Colors.white,
      500: Colors.white,
      600: Colors.white,
      700: Colors.white,
      800: Colors.white,
      900: Colors.white,
    });
    var color = const Color(0xffe3f2ffd);

    return GetMaterialApp(
      title: 'Square Fin',
      theme: ThemeData(
        fontFamily: 'Poppins',
        brightness: Brightness.light,
        useMaterial3: false,
        primarySwatch: customBlue,
        secondaryHeaderColor: customBlue,
        appBarTheme: const AppBarTheme(
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black),
          toolbarTextStyle: TextStyle(color: Colors.black, fontSize: 18),
          titleTextStyle: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        textSelectionTheme: const TextSelectionThemeData(
          cursorColor: Colors.blue,
          selectionHandleColor: Colors.blueGrey,
          selectionColor: Colors.blueGrey,
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: const SplashScreen(),
    );
  }
}

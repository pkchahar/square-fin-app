import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/utils/progress_dialog.dart';
import 'package:pi/utils/AppUtils.dart';

import '../../network/ApiClient.dart';
import 'UpdateUserDetails.dart';
import 'change_password.dart';
import 'package:get/get.dart';
class VerifyPass extends StatefulWidget {
  const VerifyPass({super.key});

  @override
  _VerifyPass createState() => _VerifyPass();
}

class _VerifyPass extends State<VerifyPass> {
  var userMobile, otp;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _otpController = TextEditingController();
  AppClient appClient = AppClient();
  ProgressDialog progressDialog = ProgressDialog();
  bool isNewUser = false;
  String title = "Verify Account";

  @override
  void initState() {
    super.initState();
    getSharedData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Center(
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 100),
                  child: SizedBox(
                      height: 88,
                      width: 88,
                      child: Image.asset("res/images/ic_square_fin.png")),
                ),
                const SizedBox(
                  height: 26,
                ),
                Text(
                  title,
                  style: const TextStyle(
                      fontSize: 22,
                      color: Color(0xFF40C4F5),
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 30, right: 30),
                  child: Row(
                    children: [
                      const Text(
                        'OTP code we have sent to',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.grey, fontSize: 15),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          '$userMobile',
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              color: Color(0xFF40C4F5),
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.edit,
                            size: 18,
                            color: Colors.black26,
                          ))
                    ],
                  ),
                ),
                const SizedBox(height: 25.0),
                TextFormField(
                  controller: _otpController,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(6),
                  ],
                  decoration: const InputDecoration(
                    labelText: 'One Time Passcode(OTP)',
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: TextStyle(color: Colors.black54),
                  ),
                  keyboardType: TextInputType.phone,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter OTP';
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 10, left: 30, right: 30),
                  child: Text(
                    'Haven’t received verification code?',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.grey, fontSize: 15),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                GestureDetector(
                  onTap: () {
                    progressDialog.show(context);
                    if (isNewUser) {
                      getRegistrationReCode();
                    } else {
                      onSendForgotPass();
                    }
                  },
                  child: Container(
                      padding: const EdgeInsets.only(bottom: 1.0),
                      decoration: const BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  width: 1.0, color: Colors.lightBlue))),
                      child: const Text(
                        'Resend Code',
                        style: TextStyle(color: Colors.lightBlue),
                      )),
                ),
                const SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        progressDialog.show(context);
                        if (isNewUser) {
                          getRegistrationCode();
                        } else {
                          forgotPassVerify();
                        }
                      }
                    },
                    style: ButtonStyle(
                      backgroundColor:
                      WidgetStateProperty.all<Color>(Colors.lightBlue),
                      // Set the background color to blue
                      minimumSize: WidgetStateProperty.all<Size>(
                          const Size(double.infinity, 50)),
                      shape:
                      WidgetStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              10.0), // Set the border radius to 10.0 for rounded corners
                        ),
                      ), // Set the button width to full and height to 50
                    ),
                    child: const Text(
                      'Verify Now',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    )),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userMobile = preferences.getString(AppUtils.userMobile);
    isNewUser = preferences.getBool(AppUtils.newUser) ?? false;

    if (isNewUser) {
      title = "Verify Mobile Number";
    }
    setState(() {
      print(isNewUser);
    });
  }

  Future<void> forgotPassVerify() async {
    print(userMobile);
    otp = _otpController.text;
    Map parsed = await appClient.forgotPassVerify(userMobile, otp);
    dynamic status = parsed["status"];
    dynamic msg = parsed["message"];

    Fluttertoast.showToast(msg: '$msg');
    print(status);
    progressDialog.hide(context);

    if (status != null && status == true) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      print(userMobile);
      preferences.setString(AppUtils.userMobile, userMobile);

      Get.to(const ChangePass());
    }
  }

  Future<void> onSendForgotPass() async {
    print(userMobile);
    Map parsed = await appClient.getForGotPassOtp(userMobile);
    dynamic status = parsed["status"];
    dynamic msg = parsed["message"];

    Fluttertoast.showToast(msg: '$msg');
    print(status);
    progressDialog.hide(context);
    Fluttertoast.showToast(msg: '$msg');
  }

  Future<void> getRegistrationCode() async {
    print(userMobile);
    otp = _otpController.text;
    Map parsed =
        await appClient.getRegistrationCode(userMobile, "Verify", otp: otp);
    dynamic status = parsed["status"];
    dynamic msg = parsed["message"];
    Map mParsed = parsed["Data"] ?? {};
    dynamic id = mParsed["Id"];

    Fluttertoast.showToast(msg: '$msg');
    print(status);
    progressDialog.hide(context);

    if (status != null && status == true) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      print(userMobile);
      preferences.setString(AppUtils.userMobile, userMobile);
      preferences.setString(AppUtils.userId, id);

      Get.to(const UpdateUserDetails());
    } else {
      Fluttertoast.showToast(msg: '$msg');
    }
  }

  Future<void> getRegistrationReCode() async {
    print(userMobile);
    Map parsed = await appClient.getRegistrationCode(userMobile, "Send Otp");
    dynamic status = parsed["status"];
    dynamic msg = parsed["message"];

    Fluttertoast.showToast(msg: '$msg');
    print(status);
    progressDialog.hide(context);
  }
}

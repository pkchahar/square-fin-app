import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../../Model/Ticket/FetchTicketList.dart';
import '../../Model/Ticket/ReplyChat.dart';
import '../../Model/Ticket/TickerDetails.dart';
import '../../utils/AppUtils.dart';
import 'package:html/parser.dart';


class TicketDetailController extends GetxController {
  DataDetails dataDetail = DataDetails(leadData: [], ticketConversation: [], heading: '');
  var ticketId;
  var userName;
  var keyboardHeight = 0.0;
  var responseMsg;
  var chatStatus = false;
  List<DataForList> data = [];
  bool dataDetailsFetched = false;
  TextEditingController msg = TextEditingController();



  Timer? _timer;

  @override
  Future<void> onInit() async {
    await showTicketList();
    SharedPreferences sf = await SharedPreferences.getInstance();
    userName = sf.getString(AppUtils.userName);
    await getDetail();
    dataDetailsFetched = true;
    update();
    super.onInit();

    _timer = Timer.periodic(const Duration(seconds: 4), (timer) async {
      await getDetail();
      update();
    });

  }
  String parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString = parse(document.body?.text).documentElement!.text;
    return parsedString;
  }
  @override
  void onClose() {
    _timer?.cancel();
    super.onClose();
  }

  Future<void> getDetail() async {
    print('step1');
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.getTicketDetails);
      Map<String, dynamic> mData = {
        'Login_User_Id': preferences.getString(AppUtils.userId).toString(),
        'ticket_id': ticketId.toString()
      };
      print(ticketId.toString());
      print(mData);

      var response = await http.post(url, body: mData);
      if (response.statusCode == 200) {
        Map<String, dynamic> jsonResponse = jsonDecode(response.body);
        TicketDetail myResponse = TicketDetail.fromJson(jsonResponse);
        dataDetail = myResponse.data;
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> showTicketList() async {
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();
      Map<String, dynamic> mData = {'Login_User_Id': pref.getString(AppUtils.userId)};
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.getAllTickets);
      var response = await http.post(url, body: mData);
      print(url);
      print(response.statusCode);
      print(mData);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        FetchTicketList myResponse = FetchTicketList.fromJson(responseJson);
        data = myResponse.data!;
        print(response.body);
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> replyChat() async {
    try {
      SharedPreferences sf = await SharedPreferences.getInstance();
      Map<String, dynamic> mData = {
        'Login_User_Id': sf.getString(AppUtils.userId).toString(),
        'ticket_id': ticketId.toString(),
        'message': msg.text.toString()
      };
      print(mData);
      print('this is mData of reply chat');
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.ticketReply);
      var response = await http.post(url, body: mData);
      print(url);
      if (response.statusCode == 200) {
        Map<String, dynamic> jsonResponse = jsonDecode(response.body);
        ReplyChat myResponse = ReplyChat.fromJson(jsonResponse);
        chatStatus = myResponse.status;
        responseMsg = myResponse.message;
        if (chatStatus) {
          msg.text = '';
          await getDetail();
          update();
        }
        update();
        print(responseMsg);
        print('now this is response message for reply');
        print(response.body);
        print(chatStatus);
        print(myResponse.status);
        print('elephant');
        getDetail();
      }
    } catch (e) {
      print(e);
    }
  }
}

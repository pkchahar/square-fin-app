import 'package:flutter/material.dart';

class NotifactionScreen extends StatefulWidget {
  const NotifactionScreen({super.key});

  @override
  State<NotifactionScreen> createState() => _NotifactionScreenState();
}

class _NotifactionScreenState extends State<NotifactionScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Center(
            child: Text(
              "Notification",
              style: TextStyle(
                  color: Color(0xff1FC4F4),
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
            ),
          ),
          backgroundColor: const Color(0xffFFFFFF),
        ),
        body: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.2 - 40,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: const Color(0xffF5F5F5)),
                child: const Padding(
                  padding: EdgeInsets.only(top: 20.0),
                  child: ListTile(
                    leading: Icon(Icons.person),
                    title: Text(
                      "You have received money from Dodi\nTaison +32.00",
                      style: TextStyle(
                          color: Color(0xff1FC4F4),
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

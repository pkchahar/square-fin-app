import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pi/activity/Utility%20Controllers/Faqs/FaqsQuestionsUIController.dart';
import '../../../utils/AppUtils.dart';
import 'FaqsController.dart';

class FaqsUIController extends StatefulWidget {
  const FaqsUIController({super.key});

  @override
  _FaqsUIController createState() => _FaqsUIController();
}

class _FaqsUIController extends State<FaqsUIController> {
  @override
  void initState() {
    FaqsController;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FaqsController>(
      init: FaqsController(context: context),
      builder: (_) {
        return Scaffold(
          backgroundColor: AppUtils.backgroundColor,
          appBar: AppBar(
            title: const Text("FAQ's"),
          ),
          body: _.fetched
              ? const Center(
                  child: CircularProgressIndicator(
                  color: Colors.blue,
                ))
              : Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    // Container(
                    //   height: 30,
                    //   width: 300,
                    //     decoration: BoxDecoration(border:Border.all(width: 1,color: Colors.blue),borderRadius:BorderRadius.circular(11)),
                    //     child: Center(child: Text('We are here to help',style: TextStyle(fontWeight: FontWeight.bold),))),
                    _.gridList.isNotEmpty
                        ? Expanded(
                            child: GridView.builder(
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Colors.blue.shade50,
                                        border: Border.all(width: 0.3),
                                        borderRadius: BorderRadius.circular(11),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              _.gridList[index].name
                                                      .capitalize ??
                                                  "",
                                              style: const TextStyle(fontSize: 15),
                                              textAlign: TextAlign
                                                  .center, // Centers the text
                                              softWrap:
                                                  true, // Ensures text wraps if needed
                                            ),
                                            const Icon(
                                              Icons.arrow_forward_ios,
                                              size: 25,
                                              color: Colors.grey,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      String name = _.gridList[index].name;
                                      String id = _.gridList[index].id;
                                      Get.to(FaqsQuestionsUIController(
                                          name: name, id: id));
                                    },
                                  ),
                                );
                              },
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 1,
                                mainAxisExtent: 100,
                              ),
                              itemCount:
                                  _.gridList.length, // Remove the -1 here
                            ),
                          )
                        : Expanded(
                            child: Center(
                              child: SvgPicture.asset(AppUtils.blankImg,
                                  width: MediaQuery.of(context).size.width,
                                  height:
                                      MediaQuery.of(context).size.height * 0.33,
                                  fit: BoxFit.fill),
                            ),
                          )
                  ],
                ),
        );
      },
    );
  }
}

//
//

import 'dart:convert';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/network/ApiClient.dart';
import '../../../../utils/AppUtils.dart';
import '../../../Model/FAQ/GridListResponse.dart';

class FaqsController extends GetxController {
  BuildContext context;

  FaqsController({required this.context});

  bool _isLoading = false;

  bool fetched = false;

  bool get isLoading => _isLoading;
  String version = '',
      kycStatus = '0',
      userId = '',
      userType = '',
      userCode = '',
      userName = '',
      address = '',
      mobile = '',
      email = '',
      pan = '',
      dob = '',
      name = '',
      adhar = '',
      token = '';
  late String branch, accountNo, ifscCode, msg;
  bool isChecked = false;
  bool bankStatus = false;

  List<GridItem> gridList = [];

  @override
  void onInit() {
    super.onInit();
    _getAppVersion();
    getSharedData();
    fetchFAQgridList();
    print('elephant');
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    name = preferences.getString(AppUtils.userName) ?? "NA";
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    userCode = preferences.getString(AppUtils.userCode) ?? "";
    email = preferences.getString(AppUtils.userMail) ?? "";
    mobile = preferences.getString(AppUtils.userMobile) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    kycStatus = preferences.getString(AppUtils.kycStatus) ?? "0";

    //fetchBank();
    update();
  }

  Future<void> removeFcm() async {
    Map<String, String> hData = {
      'Authorization': 'Bearer $token',
    };
    Map<String, String> mData = {
      'Login_User_Id': userId,
      'Login_User_Type': userType,
      'platForm': AppClient.platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };

    if (kDebugMode) {
      print(mData);
    }
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.removeFcm);
      var response = await http.post(url, headers: hData, body: mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
      }
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> _getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    version = packageInfo.version;
    update();
  }

  void fetchFAQgridList() async {
    fetched = true;
    update();
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.fetchFAQGridList);
      print(url);
      var response = await http.get(url);
      print(response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        GridListResponse myResponse = GridListResponse.fromJson(responseJson);
        gridList.addAll(myResponse.data);
      }
      fetched = false;
      update();
    } catch (e) {
      print(e);
    }
  }

  List IconsList = [
    Icons.account_box_rounded,
    Icons.check_box,
    Icons.details_rounded,
    Icons.payments_outlined,
    Icons.leaderboard
  ];
}

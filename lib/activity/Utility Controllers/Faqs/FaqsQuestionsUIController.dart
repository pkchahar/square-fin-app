import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:pi/activity/Utility%20Controllers/Faqs/FaqsQuestionsController.dart';
import '../../../Model/FAQ/FAQResponse.dart';
import '../../../utils/AppUtils.dart';

class FaqsQuestionsUIController extends StatefulWidget {
  String name;
  String id;

  FaqsQuestionsUIController({super.key, required this.name, required this.id});

  @override
  _FaqsQuestionsUIController createState() => _FaqsQuestionsUIController();
}

class _FaqsQuestionsUIController extends State<FaqsQuestionsUIController> {
  FaqsQuestionsController faqsExpansionTileController =
      FaqsQuestionsController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FaqsQuestionsController>(
      init: FaqsQuestionsController(),
      builder: (_) {
        if (_.innerGridList.isEmpty && !_.isApiCalled) {
          _.fetchFAQList(widget.id);
        }

        return Scaffold(
          appBar: AppBar(
            title: Text(widget.name),
          ),
          body: SafeArea(
            child: _.isLoading
                ? const Center(
                    child: CircularProgressIndicator(
                      color: Colors.blueGrey,
                    ),
                  )
                : _.innerGridList.isNotEmpty
                    ? Column(
                        children: [
                          const SizedBox(height: 8),
                          Expanded(
                            child: ListView.builder(
                              itemCount: _.innerGridList.length,
                              itemBuilder: (context, index) {
                                FAQItem faq = _.innerGridList[index];
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(11),
                                      border: Border.all(
                                          color: Colors.black, width: 0.4),
                                    ),
                                    child: ExpansionTile(
                                      leading: const Icon(
                                        Icons.question_answer,
                                        color: Colors.green,
                                      ),
                                      title: Text(
                                        faq.question,
                                        style: const TextStyle(
                                          color: Colors.black,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      trailing:
                                          const Icon(Icons.arrow_drop_down),
                                      children: [
                                        const Divider(color: Colors.green),
                                        Padding(
                                          padding: const EdgeInsets.all(10),
                                          child: HtmlWidget(faq.answer),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      )
                    : Center(
                        child: SvgPicture.asset(
                          AppUtils.blankImg,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * 0.33,
                          fit: BoxFit.fill,
                        ),
                      ),
          ),
        );
      },
    );
  }
}

import 'dart:convert';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import '../../../Model/FAQ/FAQResponse.dart';
import '../../../utils/AppUtils.dart';

class FaqsQuestionsController extends GetxController {
  List<FAQItem> innerGridList = [];
  bool isApiCalled = false;
  bool _isLoading = false;

  bool get isLoading => _isLoading;

  void fetchFAQList(String categoryId) async {
    setLoading(true);
    isApiCalled = true;
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.fetchFAQList);
      var body = {'categroy_id': categoryId};
      var response = await http.post(url, body: body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        FAQResponse myResponse = FAQResponse.fromJson(responseJson);
        innerGridList.addAll(myResponse.data);
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
    update();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }
}

import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pi/activity/Loan/DashLoan.dart';
import 'package:pi/activity/POS%20Panel/PosListController/PartnerListUIController.dart';
import 'package:pi/activity/UserAccount/ReportUIController.dart';
import 'package:pi/activity/Utility%20Controllers/LoanTermActivity.dart';
import 'package:pi/activity/Utility%20Controllers/contact_us.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:screenshot/screenshot.dart';
import '../../utils/CustomAppBar.dart';
import '../../utils/WebviewController.dart';
import '../Product/ProductLeadViewController.dart';
import '../Utility Controllers/AboutUs.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../Model/UtilityModel/PosterRes.dart';
import '../../utils/AppUtils.dart';

class PosterImg extends StatefulWidget {
  final PosterData mPoster;

  const PosterImg({super.key, required this.mPoster});

  @override
  _PosterImgPageState createState() => _PosterImgPageState();
}

class _PosterImgPageState extends State<PosterImg> {
  String name = "";
  String cardNumber = "";
  String cardEmailId = "";
  String userProfile = "";
  String userId = "";

  @override
  void initState() {
    super.initState();
    _loadUserData(); // Load data when the widget is initialized
  }

  Future<void> _loadUserData() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      name = preferences.getString(AppUtils.userName) ?? "";
      name = AppUtils.capitalizeFirstWord(name.trim());
      cardNumber = preferences.getString(AppUtils.userMobile) ?? "";
      cardEmailId = preferences.getString(AppUtils.userMail) ?? "";
      userProfile = preferences.getString(AppUtils.userProfile) ?? "";
      userId = preferences.getString(AppUtils.userId) ?? "";

      userProfile = "https://api.policyonweb.com/API/uploads/profiles/"+userId+"/"+userProfile;
      print("userProfile");
      print(userProfile);
    });
  }

  ScreenshotController screenshotController = ScreenshotController();

  void takeScreenshot() {
    screenshotController.capture().then((Uint8List? image) {
      if (image != null) {
        _saveAndShareScreenshot(image);
      }
    }).catchError((onError) {
      print(onError);
    });
  }

  Future<void> _saveAndShareScreenshot(Uint8List image) async {
    final directory = await getTemporaryDirectory();
    final imagePath = '${directory.path}/screenshot.png';
    File file = File(imagePath);
    await file.writeAsBytes(image);

    if (await file.exists()) {
      // Share the image using share_plus
      await Share.shareXFiles([XFile(imagePath)],
          text: widget.mPoster.title);
    } else {
      print('File does not exist');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('Marketing Material'),
        backgroundColor: Colors.blue.shade50,
      ),
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            color: AppUtils.bgColor,
          ),
          SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Screenshot(
              controller: screenshotController,
              child: InkWell(
                onTap: () {
                  takeScreenshot();
                },
                child: Card(
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.network(
                            widget.mPoster.file,
                            fit: BoxFit.contain,
                            errorBuilder: (context, error, stackTrace) {
                              return Image.asset(
                                'res/images/no_image.png',
                                fit: BoxFit.contain,
                              );
                            },
                          ),
                        ),

                        Row(
                          children: [
                            ClipOval(
                              child: Image.network(
                                userProfile,
                                width: 70,
                                height: 70,
                                fit: BoxFit.cover,
                                errorBuilder: (context, error, stackTrace) {
                                  return Image.asset(
                                    'res/images/ic_square_fin.png',
                                    width: 70,
                                    height: 70,
                                    fit: BoxFit.fitHeight,
                                  );
                                },
                              ),
                            ),
                            const SizedBox(width: 8), // Spacing between image and text
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start, // Align text to the left
                                children: [
                                  Text(
                                    name,
                                    style: const TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    overflow: TextOverflow.clip, // Avoid overflow if text is too long
                                  ),
                                  const SizedBox(height: 3),
                                  Text(
                                    cardNumber,
                                    style: const TextStyle(
                                      fontSize: 12,
                                    ),
                                    overflow: TextOverflow.clip, // Avoid overflow
                                  ),
                                  Text(
                                    cardEmailId,
                                    style: const TextStyle(
                                      fontSize: 12,
                                    ),
                                    overflow: TextOverflow.clip, // Avoid overflow
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )


                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

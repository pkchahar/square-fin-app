import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pi/activity/UserAccount/OtpLoginViewController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/utils/progress_dialog.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:get/get.dart';

import '../../network/ApiClient.dart';

class UpdateUserDetails extends StatefulWidget {
  const UpdateUserDetails({super.key});

  @override
  _UpdateUserDetails createState() => _UpdateUserDetails();
}

class _UpdateUserDetails extends State<UpdateUserDetails> {
  var userMobile, userId, password;
  AppClient appClient = AppClient();
  ProgressDialog progressDialog = ProgressDialog();
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  late String name, email;
  var _passwordVisible = false;

  @override
  void initState() {
    super.initState();
    getSharedData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Center(
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 100),
                  child: SizedBox(
                      height: 88,
                      width: 88,
                      child: Image.asset("res/images/ic_square_fin.png")),
                ),
                const SizedBox(
                  height: 26,
                ),
                const Text(
                  'Update Details',
                  style: TextStyle(
                      fontSize: 22,
                      color: Color(0xFF40C4F5),
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 25,
                ),
                const SizedBox(height: 55.0),
                TextFormField(
                  controller: nameController,
                  onChanged: (value) {
                    setState(() {});
                  },
                  decoration: const InputDecoration(
                    hintText: 'Name',
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: TextStyle(color: Colors.black54),
                  ),
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Enter Name";
                    } else {
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: emailController,
                  onChanged: (value) {
                    name = nameController.text;
                    email = emailController.text;
                    setState(() {});
                  },
                  decoration: const InputDecoration(
                    hintText: 'Email Address',
                    border: OutlineInputBorder(),
                    labelText: 'Email Address',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: TextStyle(color: Colors.black54),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Enter a valid email";
                    } else {
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: passwordController,
                  onChanged: (value) {
                    setState(() {
                      password = value.trim();
                    });
                  },
                  obscureText: !_passwordVisible,
                  decoration: InputDecoration(
                    hintText: 'Enter Password',
                    border: const OutlineInputBorder(),
                    labelText: 'Password',
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: const TextStyle(color: Colors.black54),
                    suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        _passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Enter a valid password";
                    } else if (value.length < 8) {
                      return 'Password should be at least 8 characters long';
                    } else if (!RegExp(
                            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$&*~]).{8,}$')
                        .hasMatch(value)) {
                      return 'Password should contain at least one uppercase letter, one lowercase letter, one number and one special character (!@#\$&*~)';
                    } else {
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        updateDetail();
                      }
                    },
                    style: ButtonStyle(
                      backgroundColor:
                      WidgetStateProperty.all<Color>(Colors.lightBlue),
                      // Set the background color to blue
                      minimumSize: WidgetStateProperty.all<Size>(
                          const Size(double.infinity, 50)),
                      shape:
                      WidgetStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              10.0), // Set the border radius to 10.0 for rounded corners
                        ),
                      ), // Set the button width to full and height to 50
                    ),
                    child: const Text(
                      'Update Details',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    )),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userMobile = preferences.getString(AppUtils.userMobile);
    userId = preferences.getString(AppUtils.userId);
    setState(() {});
  }

  Future<void> updateDetail() async {
    name = nameController.text;
    email = emailController.text;
    password = passwordController.text;
    Map parsed =
        await appClient.updateDetail(userId, userMobile, name, email, password);
    dynamic status = parsed["status"];
    String msg = parsed["message"] ?? "Something Went Wrong!";

    Fluttertoast.showToast(msg: msg);
    print(status);
    progressDialog.hide(context);

    showMyDialog(status, "User Detail Update", msg);
  }

  showMyDialog(dynamic status, String title, String msg) async {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(
          title,
          style: const TextStyle(color: Colors.lightBlue),
        ),
        content: Text(
          msg,
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              if (status != null && status == true) {
                Get.offAll(() => const OtpLoginViewController());
              } else {
                Navigator.pop(context, 'Cancel');
              }
            },
            child: const Text('Login', style: TextStyle(color: Colors.black38)),
          ),
        ],
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../Model/Ticket/FetchTicketList.dart';
import 'TicketDetailsController.dart';

class TicketDetailUIController extends StatefulWidget {
  var ticketId;
  DataForList? obj;
    TicketDetailUIController({super.key,required this.ticketId, this.obj});

  @override
  State<TicketDetailUIController> createState() => _TicketDetailUIControllerState();
}


class _TicketDetailUIControllerState extends State<TicketDetailUIController> {
    TicketDetailController ticketDetailController = TicketDetailController();


  final ScrollController _scrollController = ScrollController();
  void scrollToEnd(){
    _scrollController.animateTo(_scrollController.position.minScrollExtent,duration: const Duration(milliseconds: 300,),curve: Curves.easeOut);
  }

  @override
  void initState() {
    //refreshScreen();
    // ticketDetailController.refreshScreen();
    super.initState();
    // Timer.periodic(Duration(seconds: 2),(timer){
    //   ticketDetailController.ticketId = widget.ticketId;
    //   ticketDetailController.getDetail();
    //   // Your code to be executed every 2 seconds
    // });

  }
    @override
    void dispose() {
      super.dispose();
    }
  @override
  Widget build(BuildContext context) {
    return GetBuilder<TicketDetailController>(
        init: TicketDetailController(),
        builder:(_){

         _.ticketId==null? _.ticketId = widget.ticketId:'';
          return Scaffold(
            resizeToAvoidBottomInset: true,
            appBar: AppBar(
              title: const Text('Ticket Details'),
            ),
            body: Stack( children: [

              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    opacity: 0.05,
                      colorFilter: ColorFilter.srgbToLinearGamma(),
                      image: AssetImage("res/images/chatBackground.jpg"),fit: BoxFit.cover)
                ),
              ),
              Container(
              child:_.dataDetailsFetched
                  ? Column(
                  children:[
                    widget.obj != null ? Card(
                      elevation: 1,
                      color: Colors.blue.shade50,
                      child: ExpansionTile(

                        title: Text(widget.obj!.product_name.toString(), style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.black)),
                        children: [
                          const Divider(thickness: 1,),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      constraints: const BoxConstraints(minWidth: 100),
                                      child: const Text('Date: ', textAlign: TextAlign.justify, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                                    ),
                                    Flexible(
                                      child: Text(DateFormat('dd-MM-yyyy').format(DateTime.parse(widget.obj!.date.toString())), style: const TextStyle(color: Colors.black)),
                                    ),
                                    const SizedBox(width: 40),
                                    Container(
                                      constraints: const BoxConstraints(minWidth: 60),
                                      child: const Text('Status: ', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                                    ),
                                    Flexible(
                                      child: Text(widget.obj!.status.toString(), style: const TextStyle(color: Colors.black)),
                                    ),
                                  ],
                                ),
                                const Divider(height: 8, thickness: 1),
                                Container(
                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
                                  child: Row(
                                    children: [
                                      Container(
                                        constraints: const BoxConstraints(minWidth: 100),
                                        child: const Text('Lead-Code: ', textAlign: TextAlign.start, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                                      ),
                                      Flexible(child: Text(widget.obj!.leadid.toString(), style: const TextStyle(color: Colors.black))),
                                    ],
                                  ),
                                ),
                                const Divider(height: 8, thickness: 1),
                                Container(
                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
                                  child: Row(
                                    children: [
                                      Container(
                                        constraints: const BoxConstraints(minWidth: 100),
                                        child: const Text('Subject: ', textAlign: TextAlign.start, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                                      ),
                                      Flexible(child: Text(widget.obj!.subject.toString(), style: const TextStyle(color: Colors.black))),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ) : const SizedBox(),

                    Expanded(
                      child:  SingleChildScrollView(
                        controller: _scrollController,
                        reverse: true,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: LayoutBuilder(
                              builder: (context, constraints) {
                                return ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: _.dataDetail.ticketConversation.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index) {
                                    bool isCurrentUser = _.dataDetail.ticketConversation[index].senderName.toString().toLowerCase() == _.userName.toString().toLowerCase();
                                    return Padding(
                                      padding: isCurrentUser ? const EdgeInsets.only(left: 50) : const EdgeInsets.only(right: 50),
                                      child: Align(
                                        alignment: isCurrentUser ? Alignment.centerRight : Alignment.centerLeft,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius:isCurrentUser? const BorderRadius.only(topLeft: Radius.circular(11),bottomLeft:  Radius.circular(11),bottomRight: Radius.circular(11)):const BorderRadius.only(topRight: Radius.circular(11),bottomLeft:  Radius.circular(11),bottomRight: Radius.circular(11)),
                                            color: isCurrentUser ? Colors.blue : Colors.grey.shade200,
                                          ),
                                          padding: const EdgeInsets.all(11),
                                          margin: const EdgeInsets.all(5), 
                                          child: Text(
                                            _.parseHtmlString(_.dataDetail.ticketConversation[index].message.toString()),
                                            style: TextStyle(
                                              color: isCurrentUser ? Colors.white : Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                          constraints: const BoxConstraints(maxHeight: 200),
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(22),color: Colors.white,
                              boxShadow: const [BoxShadow(offset: Offset(0, 0),blurRadius: 0,spreadRadius: 0.1)]
                          ),
                          child: Stack(
                              children:[ Padding(
                                padding: const EdgeInsets.only(left: 21,right: 35),
                                child: TextFormField(
                                  onTap: (){
                                    scrollToEnd();
                                  },
                                  controller: _.msg,
                                  maxLines: null,
                                  decoration: const InputDecoration(border: InputBorder.none),
                                ),
                              ),

                                Padding(
                                  padding: const EdgeInsets.only(top: 12,right: 8),
                                  child: Row(
                                    children: [
                                      const Spacer(),
                                      InkWell(
                                          onTap: (){
                                            setState(()  {
                                              if(_.msg.text.isNotEmpty){
                                                setState(() {
                                                  _.replyChat();
                                                  _.update();
                                                  scrollToEnd();
                                                });
                                              }
                                            });
                                          },
                                          child: const Icon(Icons.send,
                                            color: Colors.blue,)),
                                    ],
                                  ),
                                )
                              ])),
                    ),
                  ]
              )
                  : const SizedBox(),
            )])
          );
        }
    );
  }
}

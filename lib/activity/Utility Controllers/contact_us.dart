
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../../utils/AppUtils.dart';

class ContactUs extends StatefulWidget {
  const ContactUs({super.key});

  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUs> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  final _messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         elevation: 0,
        title: const Text('Contact Us'),
        backgroundColor: Colors.blue.shade50,
      ),
      // backgroundColor: Color(0xFFE3F2FD),
      body: Stack(children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          color:AppUtils.bgColor
        ),
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    // Padding(
                    //   padding: const EdgeInsets.only(left: 20, right: 20),
                    //   child: Image.asset("res/images/contact_us.png"),
                    // ),
                    Card(
                      elevation: 0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16,vertical: 10),
                            child: Text('Post A Query',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  // color: Colors.blue.shade500
                                )),
                          ),
                          Divider(
                            color: Colors.blue.shade50,
                            thickness: 1.0,
                          ),

                          Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 16),
                              child: Column(children: [
                                const SizedBox(height: 16.0),
                                TextFormField(
                                  controller: _nameController,
                                  decoration: const InputDecoration(
                                    labelText: 'Name',
                                    border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFE3F2FD)),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xFFE3F2FD))),

                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.lightBlue,
                                      ),
                                    ),
                                    labelStyle: TextStyle(color: Colors.black54),
                                  ),
                                  keyboardType: TextInputType.name,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter your name';
                                    }
                                    return null;
                                  },
                                ),
                                const SizedBox(height: 16.0),
                                TextFormField(
                                  controller: _emailController,
                                  decoration: const InputDecoration(
                                    labelText: 'Email',
                                    border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFE3F2FD)),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Color(0xFFE3F2FD))),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.lightBlue,
                                      ),
                                    ),
                                    labelStyle: TextStyle(color: Colors.black54),
                                  ),
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter your email address';
                                    } else if (!RegExp(r'\S+@\S+\.\S+')
                                        .hasMatch(value)) {
                                      return 'Please enter a valid email address';
                                    }
                                    return null;
                                  },
                                ),
                                const SizedBox(height: 16.0),
                                TextFormField(
                                  controller: _phoneController,
                                  decoration: const InputDecoration(
                                    labelText: 'Phone Number',
                                    border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFE3F2FD))),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Color(0xFFE3F2FD))),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.lightBlue,
                                      ),
                                    ),
                                    labelStyle: TextStyle(color: Colors.black54),
                                  ),
                                  keyboardType: TextInputType.phone,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter your Phone Number';
                                    }
                                    return null;
                                  },
                                ),
                                const SizedBox(height: 16.0),
                                TextFormField(
                                  controller: _messageController,
                                  decoration: const InputDecoration(
                                    labelText: 'Message',
                                    border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFE3F2FD))),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Color(0xFFE3F2FD))),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.lightBlue,
                                      ),
                                    ),
                                    labelStyle: TextStyle(color: Colors.black54),
                                  ),
                                  maxLines: 4,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter a message';
                                    }
                                    return null;
                                  },
                                ),
                                const SizedBox(height: 16.0),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [// onPressed: () {

                                      // },  Color(0xFF009688)),
                                      InkWell(
                                        onTap:(){
                                          if (_formKey.currentState!.validate()) {
                                            _showSuccessDialog();
                                          }
                                        },
                                        child: Container(
                                          height:30,
                                            width:126,
                                            padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 6),
                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(3),color: const Color(0xFF009688), ),
                                            child: const Center(child: Text("Submit",style: TextStyle(color: Colors.white),)))

                                      ),
                                    ],),
                                ),
                              ])
                          )],
                      ),
                    ),
                    const SizedBox(height: 10),
                    Card(
                      elevation: 0,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const  Image(image: AssetImage("res/images/contactUs/email.png")),
                                  Expanded(
                                      child: InkWell(
                                          onTap: () {
                                            launch(
                                                'mailto:info@squarefin.in?subject=Square Fin Query&body=Write something here');
                                          },
                                          child: const Padding(
                                            padding: EdgeInsets.only(left: 10),
                                            child: Text("info@squarefin.in",style: TextStyle(decoration:TextDecoration.underline),),
                                          ))),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const  Image(image: AssetImage("res/images/contactUs/phone-call.png")),
                                  Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 10),
                                        child: InkWell(
                                            onTap: () {
                                              launch('tel:+91 8306300415');
                                            },
                                            child: const Text("+91-8306300415")),
                                      ))
                                ],
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Image(image: AssetImage("res/images/contactUs/location.png")),
                                  Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 10),
                                        child: Text(
                                            "602-603, 6th Floor Trimurty`s V-Jai City Point, Ahinsa Circle C-Scheme, Jaipur"),
                                      ))
                                ],
                              ),
                            ),
                            const Divider(
                              color: Color(0xFFE3F2FD),
                              height: 0,thickness: 1,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [TextButton(
                                onPressed: () {
                                  launchUrlString(
                                      'https://www.twitter.com/Squarefin2');
                                },
                                style: ButtonStyle(
                                  padding: WidgetStateProperty.all<EdgeInsets>(const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0)),
                                  minimumSize: WidgetStateProperty.all<Size>(const Size(0, 0)), // Sets the minimum size to zero
                                  tapTargetSize: MaterialTapTargetSize.shrinkWrap, // Shrinks the tap target size
                                ),
                                child: const Image(image: AssetImage("res/images/contactUs/twitter.png")),
                              ),    TextButton(
                                onPressed: () {
                                  launchUrlString(
                                      'https://www.linkedin.com/company/squarefin-private-limited/');
                                },
                                style: ButtonStyle(
                                  padding: WidgetStateProperty.all<EdgeInsets>(const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0)),
                                  minimumSize: WidgetStateProperty.all<Size>(const Size(0, 0)), // Sets the minimum size to zero
                                  tapTargetSize: MaterialTapTargetSize.shrinkWrap, // Shrinks the tap target size
                                ),
                                child: const Image(image: AssetImage("res/images/contactUs/linkedin.png")),
                              ),
                                TextButton(
                                  onPressed: () {
                                    launchUrlString(
                                        'https://www.instagram.com/square_fin/');
                                  },
                                  style: ButtonStyle(
                                    padding: WidgetStateProperty.all<EdgeInsets>(const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0)),
                                    minimumSize: WidgetStateProperty.all<Size>(const Size(0, 0)), // Sets the minimum size to zero
                                    tapTargetSize: MaterialTapTargetSize.shrinkWrap, // Shrinks the tap target size
                                  ),
                                  child: const Image(image: AssetImage("res/images/contactUs/instagram.png")),
                                ),
                                TextButton(
                                  onPressed: () {
                                    launchUrlString(
                                        'https://www.facebook.com/squarefinoffical');
                                  },
                                  style: ButtonStyle(
                                    padding: WidgetStateProperty.all<EdgeInsets>(const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0)),
                                    minimumSize: WidgetStateProperty.all<Size>(const Size(0, 0)), // Sets the minimum size to zero
                                    tapTargetSize: MaterialTapTargetSize.shrinkWrap, // Shrinks the tap target size
                                  ),
                                  child: const Image(image: AssetImage("res/images/contactUs/facebook.png",)),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ),
      ],)
    );
  }

  void _showSuccessDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Success'),
          content: const Text('Your message has been sent.'),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

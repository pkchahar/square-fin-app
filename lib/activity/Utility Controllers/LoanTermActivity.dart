import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoanTermActivity extends StatelessWidget {
  var Partners={
    'Housing Development Finance Corporation Limited',
    'HDFC Bank',
    'ICICI Bank Limited',
    'Tata Capital Limited',
    'Yes Bank Limited',
    'Axis Finance Limited',
    'Fullerton Financial Holdings',
    'Kotak Mahindra Bank Limited',
    'Bajaj Finserv Limited',
    'Aditya Birla Capital Limited',
    'Edelweiss Finance',
    'Hero Fincorp Limited',
    'HDB Financial Services Limited',
    'Indostar Capital Finance Limited',
    'AU Small Finance Bank Limited',
    'Cholamandalam Investment And Finance Company Limited',
    'IDFC First Bank Limited',
    'ICICI Home Finance Company Limited',
    'Union Bank Limited',
    'SBI Home Finance Ltd.',
    'Aadhar Housing Finance Limited',
    'PNB Housing Finance Limited'
  };
  var carLoanList = {
    'Get an Instant Cash loan up to Rs. 50 Lakhs with Square Fin app.',
    'The APR for New car between 8.75% to 16% pa.',
    'The APR for Used car between 14% to 24% pa',
    'The Loan Tenure is between 6 Month to 36 Month.',
    'Instant loan transfer in bank account.',
    'Quick verification of car.',
  };
  var businessLoan = {
    'Get an Instant loan up to Rs. 1 Crore with Square Fin app.',
    'The APR in between 16% to 24% PA',
    'The Loan Tenure is between 6 Month to 72 Month.',
    'Instant loan transfer in bank account.',
    'Lowest processing fees in the industry.',
    'Available PAN India Basis.',
  };
  List<String> homeLoan = [
    'Get a loan up to Rs. 5 crores with Square Fin app',
    'The APR in between 9% to 15% PA',
    'The Loan Tenure is between 12 Month to 30 years',
    'Example how home loan work :-',
    ' ~ Loan Amount :- 50,00,000',
    ' ~ Tenure:- 60 months',
    ' ~ Interest rate :- 10% (13,74,113)',
    ' ~ Total amount :- 63,74,113',
    ' ~ Processing fees :- 51,775 Rs/- (Inclusive of GST)',
    ' ~ Disbursed amount 50,00,000 - 51,775 = 49,48,225',
    ' ~ EMI amount :- 1,06,235 (if Opted)',
    ' ~ Total amount payable :- 50,00,000+51,775 +13,74,113 = 64,25,888/-)',
    ' ~ Total cost :- 51,775+13,74,113 = 14,25,888',
    '(Note:- This is merely as example how loan works, the interest rates may differ based on the different risk criteria)',
  ];

  LoanTermActivity({super.key});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Loan Terms'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children:  [
              const SizedBox(height: 20,),
            Container(
            height: 150,
           // width: 400,
            decoration: const BoxDecoration(

            ),
            child: SvgPicture.asset(
              'res/images/terms11.svg',
              fit: BoxFit.contain,
            ),
          ),
              const SizedBox(height: 20.0),
              Row(
                children: [
                  const Text("Financial Solutions",style: TextStyle(fontSize: 20,fontWeight:FontWeight.bold),),
                  const SizedBox(width: 10,),
                 // color: Colors.lightBlueAccent,
                  //     height: 3,
                  //     width: 30,
                 Expanded(child: Transform.translate(offset: const Offset(0,5),
                 child: Container(color: Colors.lightBlueAccent,height: 3,width: 30,))),
                ],
              ),
              const SizedBox(height: 10.0),
              Container(
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.blue.shade50,
                      // border: Border.all(),
                      borderRadius: BorderRadius.circular(11)
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'We hereby state that our company M/s Squarefin Private Limited has entered into an agreement with RBI Registered NBFCs and Financial Institutions who will be providing loans to the leads given by us with the help of this App.',
                      textAlign: TextAlign.start,
                    ),
                  ),
                ),
              ),

              const SizedBox(height: 20.0),
              Container(
                //  color: Colors.blue.shade500,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.blue.shade50,
                      // border: Border.all(),
                      borderRadius: BorderRadius.circular(11)
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'We believe that each client is different regarding their financial needs and that it is crucial to consider them as individuals rather than as a group of consumers. As a result, we provide various types of finance solutions that are relevant and fit for each individual. Square Fin app is your ultimate solution for obtaining quick and hassle-free loans right from the convenience of your mobile device. Whether you are facing a financial emergency, unexpected bills, or seizing a time-sensitive opportunity, Square Fin app makes the lending process swift, secure, and accessible for everyone.',
                      textAlign: TextAlign.start,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20),

              Container(
                decoration: BoxDecoration(
                  color: Colors.blue.shade100,
                  borderRadius: BorderRadius.circular(11)
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Square Fin app offers-',
                        textAlign: TextAlign.start,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(height: 10),
                      Container(
                        //  color: Colors.blue.shade500,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.blue.shade50,
                              // border: Border.all(),
                              borderRadius: BorderRadius.circular(11)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  ' -	Home Loan',textAlign: TextAlign.start,
                                  style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(height: 10),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(11),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: ListView.builder(
                                      physics: const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: homeLoan.length,
                                      itemBuilder: (context, index) {
                                        final List<String> homeLoanList = homeLoan.toList();
                                        return Row(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            //Text('${index + 1}  '),
                                            Flexible(
                                              child: Text(
                                                '${homeLoanList[index]}\n',
                                                softWrap: true,
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                  const SizedBox(height: 20),


                  Container(
                    //  color: Colors.blue.shade500,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.blue.shade50,
                          // border: Border.all(),
                          borderRadius: BorderRadius.circular(11)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              ' -	Business Loan',
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(height: 10),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(11),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: businessLoan.length,
                                  itemBuilder: (context, index) {
                                    final List<String> businessList = businessLoan.toList();
                                    return Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('${index + 1}  '),
                                        Flexible(
                                          child: Text(
                                            '${businessList[index]}\n',
                                            softWrap: true,
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ),
                            ),
                          ],
                        )
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Container(
                    //  color: Colors.blue.shade500,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.blue.shade50,
                          // border: Border.all(),
                          borderRadius: BorderRadius.circular(11)
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                         crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              ' -	Car Loan',
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(height: 10),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(11),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: carLoanList.length,
                                  itemBuilder: (context, index) {
                                    final List<String> carloanList = carLoanList.toList();
                                    return Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('${index + 1}  '),
                                        Flexible(
                                          child: Text(
                                            '${carloanList[index]}\n',
                                            softWrap: true,
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ),
                            ),


                          ],
                        )
                      ),
                    ),
                  ),],),
                ),
              ),

              const SizedBox(height: 20),

              Container(
                //  color: Colors.blue.shade500,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.blue.shade100,
                      // border: Border.all(),
                      borderRadius: BorderRadius.circular(11)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'The following are our NBFC and Banking Partner - ',
                            textAlign: TextAlign.start,
                            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 20,),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.blue.shade50,
                              borderRadius: BorderRadius.circular(11)
                            ),
                           // height: MediaQuery.of(context).size.height,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ListView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: Partners.length,
                                itemBuilder: (context, index) {
                                  final List<String>partnerList= Partners.toList();
                                  return Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('${index+1}  '),
                                      Flexible(child: Text('${partnerList[index]}\n',softWrap: true,)),
                                    ],
                                  );
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );

  }
}
// SizedBox(height: 10),
// Text(' *	Housing Development Finance Corporation Limited'),
// SizedBox(height: 10),
// Text(' *	HDFC Bank'),
// SizedBox(height: 10),
// Text(' *	ICICI Bank Limited'),
// SizedBox(height: 10),
// Text(' *	Tata Capital Limited'),
// SizedBox(height: 10),
// Text(' *	Yes Bank Limited'),
// SizedBox(height: 10),
// Text(' *	Axis Finance Limited'),
// SizedBox(height: 10),
// Text(' *	Fullerton Financial Holdings'),
// SizedBox(height: 10),
// Text(' *	Kotak Mahindra Bank Limited'),
// SizedBox(height: 10),
// Text(' *	Bajaj Finserv Limited'),
// SizedBox(height: 10),
// Text(' *	Aditya Birla Capital Limited'),
// SizedBox(height: 10),
// Text(' *	Edelweiss Finance'),
// SizedBox(height: 10),
// Text(' *	Hero Fincorp Limited'),
// SizedBox(height: 10),
// Text(' *	HDB Financial Services Limited'),
// SizedBox(height: 10),
// Text(' *	Indostar Capital Finance Limited'),
// SizedBox(height: 10),
// Text(' *	AU Small Finance Bank Limited'),
// SizedBox(height: 10),
// Text(' *	Cholamandalam Investment And Finance Company Limited'),
// SizedBox(height: 10),
// Text(' *	IDFC First Bank Limited'),
// SizedBox(height: 10),
// Text(' *	ICICI Home Finance Company Limited'),
// SizedBox(height: 10),
// Text(' *	Union Bank Limited'),
// SizedBox(height: 10),
// Text(' *	SBI Home Finance Ltd.'),
// SizedBox(height: 10),
// Text(' *	Aadhar Housing Finance Limited'),
// SizedBox(height: 10),
// Text(' *	PNB Housing Finance Limited'),
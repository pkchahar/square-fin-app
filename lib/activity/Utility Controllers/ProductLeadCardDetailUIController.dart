
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

import '../../Model/Product/MyCreditLeads.dart';
import '../Product/ProductLeadController.dart';

class ProductLeadCardDetailUIController extends StatefulWidget {
  LeadData item;
   ProductLeadCardDetailUIController({super.key, required this.item});

  @override
  State<ProductLeadCardDetailUIController> createState() => _ProductLeadCardDetailUIControllerState();
}



class _ProductLeadCardDetailUIControllerState extends State<ProductLeadCardDetailUIController> {
  List  column1 =['widget.item.productName','widget.item.productId','widget.item.id','widget.item.name','widget.item.email','widget.item.mobile','widget.item.panNumber','widget.item.status',
    'widget.item.createdAt','widget.item.campainURL','widget.item.lastSync','widget.item.remark','widget.item.archived'
  ];
  // List  column1 =['productName','productId','id','name','email','mobile','panNumber','status',
  //   'createdAt','campainURL','lastSync','remark','archived'
  // ];
  List column0 =['Product Name:',
    'Product ID:','ID:','Name:','Email:','Phone No.','PAN Number','Status','Created At','Campaign URL:'
    ,'Last Sync:','Remark:','Archived:'
  ];

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductLeadController>(
        init: ProductLeadController(),
    builder: (_) {
      return Scaffold(
        appBar: AppBar(title: const Text(''),),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Container(
                        // height:MediaQuery.of(context).size.height*0.3,
                        // width: MediaQuery.of(context).size.width*0.6,
                        child: Image
                            .network(
                          widget.item.icon,
                          fit: BoxFit
                              .contain,
                          errorBuilder:
                              (context,
                              error,
                              stackTrace) {
                            return SvgPicture
                                .asset(
                                'res/images/no_image_icon.svg');
                          },
                          height: 35,
                          width: double.infinity
                        ),
                      ),
                      const Divider(color: Colors.grey,),
                      // Container(
                      //   height: MediaQuery.of(context).size.height,
                      //   child: ListView.builder(itemCount: column0.length,itemBuilder: (context, index){
                      //     var x = column1[index];
                      //     print(x);
                      //     print(widget.item.productId);
                      //     return Column(
                      //       children: [
                      //         Row(
                      //           crossAxisAlignment: CrossAxisAlignment.start,
                      //           children: [
                      //             Container(
                      //                 constraints: BoxConstraints(minWidth: 130),
                      //                 child: Text(column0[index])),
                      //             Flexible(child: Text(x)),
                      //           ],
                      //         ),
                      //         SizedBox(height: 20,),
                      //       ],
                      //     );
                      //   }),
                      // ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Product name: ',style: TextStyle(fontWeight: FontWeight.bold))),
                          Flexible(child: Text(widget.item.productName)),
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Product ID: ',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.productId))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('ID: ',style: TextStyle(fontWeight: FontWeight.bold))),
                          Flexible(child: Text(widget.item.id))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Name: ',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.name))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Email: ',style: TextStyle(fontWeight: FontWeight.bold))),
                          Flexible(child: Text(widget.item.email))
                        ],
                      ),
                      const SizedBox(height: 20,),

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Phone No: ',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.mobile))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('PAN number: ',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.panNumber))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Status:',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.status))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Created at:',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.createdAt))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Campaign url:',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.campainURL))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Last sync: ',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.lastSync))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Remark: ',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.remark))
                        ],
                      ),
                      const SizedBox(height: 20,),
                      widget.item.leadType!=''?Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Lead type: ',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.leadType))
                        ],
                      ):const SizedBox(),
                      widget.item.leadType!=''? const SizedBox(height: 20,):const SizedBox(),
                      widget.item.otherData!=''? Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Other Data: ',style: TextStyle(fontWeight: FontWeight.bold)))
                          ,Flexible(child: Text(widget.item.otherData))
                        ],
                      ):const SizedBox(),
                      widget.item.otherData!=''?const SizedBox(height: 20,):const SizedBox(),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              constraints: const BoxConstraints(minWidth: 130),
                              child: const Text('Archived: ',style: TextStyle(fontWeight: FontWeight.bold),))
                          ,Flexible(child: Text(widget.item.archived))
                        ],
                      ),
                      const SizedBox(height: 20,),

                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    });
  }
}

import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  const AboutUs({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: RichText(text: const TextSpan(text: 'SQUARE\t\t',style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),children:<TextSpan>[TextSpan(text: 'FIN™',style: TextStyle(color: Colors.blue))] ),)
      ),
      body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                const SizedBox(height: 16),
                Image.asset("res/images/about_us_img.jpg") ,
                const SizedBox(height: 16),
                // Divider(
                //   color: Colors.black,
                //   thickness: 1,
                // ),
                const SizedBox(height: 5.0),
                 Row(
                   children: [
                     const Text('Company Overview',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                   const SizedBox(width: 5,),
                   Transform.translate(
                     offset: const Offset(0, 5),
                     child: Container(
                       color: Colors.lightBlueAccent,
                       height: 3,
                       width: 190,
                     ),
                   )
                   ],
                 ),
                const SizedBox(height: 10,),
                Container(
                 // color: Colors.blue.shade500,
                child:Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.blue.shade50,
                        //border: Border.all(),
                        borderRadius: BorderRadius.circular(11)
                      ),
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'We hereby state that our company M/s Squarefin Private Limited has entered into an agreement with RBI Registered NBFCs and Financial Institutions who will be providing loans to the leads given by us with the help of this App.',
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ),

                const SizedBox(height: 30.0),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.blue.shade50,
                      // border: Border.all(),
                      borderRadius: BorderRadius.circular(11)
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'We believe that each client is different regarding their financial needs and that it is crucial to consider them as individuals rather than as a group of consumers. As a result, we provide various types of finance solutions that are relevant and fit for each individual.',
                      textAlign: TextAlign.start,
                    ),
                  ),
                ),
                const SizedBox(height: 30),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.blue.shade50,
                      // border: Border.all(),
                      borderRadius: BorderRadius.circular(11)
                  ),
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'We are the best financial service providers company as we provide service with-',
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(height: 10),
                        Text(
                          '- Comfort and Simplicity',
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(height: 10),
                        Text(
                          '- Search, compare, and apply instantly',
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(height: 10),
                        Text(
                          '- Help with documentation',
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(height: 10),
                        Text(
                          '- Advice and troubleshooting available for any paperwork',
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(height: 10),
                        Text(
                          '- No confusing financial terminology',
                          textAlign: TextAlign.start,
                        )
                      ],
                    )

                  ),

                )])),

                  const SizedBox(height: 30),
                  const Row(
                    children: [
                      Text(
                        'What do We Provide?',
                        textAlign: TextAlign.start,
                        style: TextStyle(

                          fontSize: 18.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      SizedBox(width: 10,),
                      // Transform.translate(
                      //   offset: Offset(0, 5),
                      //   child: Container(
                      //     color: Colors.lightBlueAccent,
                      //     height: 3,
                      //     width: 30,
                      //   ),
                      // )
                    ],
                  ),

                  const SizedBox(height: 10),
                  Container(
                  //  color: Colors.blue.shade500,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.blue.shade50,
                          // border: Border.all(),
                          borderRadius: BorderRadius.circular(11)
                      ),
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'We ensure that your application is granted and the funds are sent in a brief period. Therefore, a quick service provides you with the best financial boost possible most easily and conveniently. Visit our website pages to apply online or download our app.',
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                  const Row(
                    children: [
                      Text(
                        'Purchase financing',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      //SizedBox(width: 10,),
                      // Transform.translate(
                      //   offset: Offset(0, 5),
                      //   child: Container(
                      //     color: Colors.lightBlueAccent,
                      //     height: 3,
                      //     width: 30,
                      //   ),
                      // )
                    ],
                  ),
                  const SizedBox(height: 10),
                Container(
                 // color: Colors.blue.shade500,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.blue.shade50,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(11)
                    ),
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'The product is intended for buying single-family homes, apartments in affordable complexes, independent and semi-detached houses, cluster homes, and residential units. It also applies to newly built or under construction properties.',
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ),
                ),
                  const SizedBox(height: 30),
                  const Row(
                    children: [
                      Text(
                        'Our Goals and Purpose',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      SizedBox(width: 5,),
                      // Transform.translate(
                      //   offset: Offset(0, 5),
                      //   child: Container(
                      //     color: Colors.lightBlueAccent,
                      //     height: 3,
                      //     width: 30,
                      //   ),
                      // )
                    ],
                  ),

                  const SizedBox(height: 10),
                Container(
                  //color: Colors.blue.shade500,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.blue.shade50,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(11)
                    ),
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'We deliver financial freedom in a considerably more accessible and practical way thanks to technological improvements and the growing push towards digitalizing the economy.',
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ),
                ),
                  const SizedBox(height: 30),
                  const Row(
                    children: [
                      Text(
                        'The following features',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold
                        ),
                      ),SizedBox(width: 5,),
                      // Transform.translate(
                      //   offset: Offset(0, 5),
                      //   child: Container(
                      //     color: Colors.lightBlueAccent,
                      //     height: 3,
                      //     width: 30,
                      //   ),
                      // )
                    ],
                  ),
                  const SizedBox(height: 10),
                SizedBox(
                  width: double.infinity,
                  //color: Colors.blue.shade500,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.blue.shade50,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(11)
                    ),
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        '- Easy Online Application\n\n- Quick Time Consumption,',
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ),
                ),

                  const SizedBox(height: 30),
                  const Row(
                    children: [
                      Text(
                        'Our Profession',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold
                        ),
                      ),SizedBox(width: 5,),

                    ],
                  ),
                  const SizedBox(height: 10),
                SizedBox(
                  width: double.infinity,
                  //color: Colors.blue.shade500,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.blue.shade50,
                        // border: Border.all(),
                        borderRadius: BorderRadius.circular(11)
                    ),
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'We work with banking firms to manage client onboarding, decision-making, risk pricing, and execution using a virtually limitless variety of pertinent (un-)structured data. To assist lenders in automating data-driven risk and pricing decisions, we extract, categorize, and analyze information from hundreds of financial institutions.',
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ),
                ),

              ],),
                ),
               // const SizedBox(height: 32),
                // Text(
                //   'Contact Us',
                //   style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                // ),
                // SizedBox(height: 16),
                // Text(
                //   'Email: info@squarefin.in',
                //   style: TextStyle(fontSize: 16),
                // ),
                // SizedBox(height: 8),
                // Text(
                //   'Phone: (123) 456-7890',
                //   style: TextStyle(fontSize: 16),
                // ),

            ),
          );
  }
}

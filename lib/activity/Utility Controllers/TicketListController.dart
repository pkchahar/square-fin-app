import 'dart:convert';

import 'package:get/get.dart';
import 'package:pi/Model/Ticket/FetchTicketList.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../../utils/AppUtils.dart';

class TicketListController extends GetxController{
var ticketId = '206';
var productId = 'American Express Membership Rewards Credit Card';
var issue = 'this is issue with credit card, where credit card is delivered, but still status is rejected';
var answer = 'We apologize for this, Our Team is  trying to solve your problem as soon as possible, be patient.\nThankYou for your cooperation ';

List <DataForList> data =[];


@override
  Future<void>onInit() async {
await  showTicketList();
update();
  super.onInit();
}



showTicketList()
async {
  try{
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map<String, dynamic> mData={
      'Login_User_Id': pref.getString(AppUtils.userId)
    };

    var url = Uri.parse(AppUtils.baseUrl + AppUtils.getAllTickets);
    var response = await http.post(url,body: mData);
    print(url);
    print(response.statusCode);
    print(mData);
    if(response.statusCode ==200){
      Map<String, dynamic> responseJson = json.decode(response.body);
      FetchTicketList myResponse = FetchTicketList.fromJson(responseJson);
      data = myResponse.data!;
      print(response.body);
    }


  }catch(e){print(e);}
}


}
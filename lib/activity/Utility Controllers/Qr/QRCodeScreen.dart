import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../utils/AppUtils.dart';

class QRCodeScreen extends StatefulWidget {
  const QRCodeScreen({super.key});

  @override
  _QRCodeScreenState createState() => _QRCodeScreenState();
}

class _QRCodeScreenState extends State<QRCodeScreen> {
  String? name = "";
  String phone = "";
  String email = "";

  final String street = "602, 6th Floor, Trimurty V-Jai City Point, Ahinsa Circle, C-Scheme, Jaipur-302001, Rajasthan";
  final String website = "https://www.squarefin.in";

  @override
  void initState() {
    super.initState();
    _loadUserData();
  }

  Future<void> _loadUserData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      name = preferences.getString(AppUtils.userName)?.capitalizeFirst ?? "";
      phone = preferences.getString(AppUtils.userMobile) ?? "";
      email = preferences.getString(AppUtils.userMail) ?? "";
    });
  }

  @override
  Widget build(BuildContext context) {
    // Generate the vCard data
    final String vCardData = generateVCard(
      name: name ?? "",
      phone: phone,
      email: email,
      street: street,
      website: website,
      city: '',
      region: '',
      postalCode: '',
      country: '',
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text("vCard QR Code"),
      ),
      body: Center(
        child: QrImageView(
          data: vCardData,
          version: QrVersions.auto,
          size: 300.0,
          gapless: false,
        ),
      ),
    );
  }

  // Function to generate vCard string
  String generateVCard({
    required String name,
    required String phone,
    required String email,
    required String street,
    required String city,
    required String region,
    required String postalCode,
    required String country,
    required String website,
  }) {
    return '''
BEGIN:VCARD
VERSION:3.0
FN:$name
TEL:$phone
EMAIL:$email
ADR:$street
URL:$website
END:VCARD
    ''';
  }
}

void main() {
  runApp(const MaterialApp(
    home: QRCodeScreen(),
  ));
}

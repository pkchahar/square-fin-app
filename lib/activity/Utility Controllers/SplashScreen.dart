import 'dart:async';
import 'dart:io' show Platform, exit;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:pi/activity/Utility%20Controllers/newSplashUIController.dart';
import 'package:pi/utils/NotificationServices.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:pi/utils/BottomBarScreen.dart';
import '../../network/ApiClient.dart';
import 'package:package_info_plus/package_info_plus.dart';
import '../UserAccount/OtpLoginViewController.dart';

class SplashScreen extends StatefulWidget {
  // const SplashScreen({super.key});
  final int? timer;

  const SplashScreen({Key? key, this.timer}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final LocalAuthentication _localAuthentication = LocalAuthentication();
  bool? _isBiometricAvailable;
  // bool _isDeviceSecure = false;



  NotificationServices notificationServices = NotificationServices();
  var userId;
  String version = "";
  String reason='Put finger on Biometric sensor to unlock';

  bool hasBioSensor=false;
  LocalAuthentication authentication = LocalAuthentication();
  // bool isAuth = false;
  bool expireT = false;
  final LocalAuthentication auth = LocalAuthentication();
  int timer = 3;

  Future<void> _checkBiometric() async {
    try {
      List<BiometricType> availableBiometrics = await _localAuthentication.getAvailableBiometrics();
      _isBiometricAvailable = availableBiometrics.isNotEmpty? true : false;
      print(_isBiometricAvailable);
      // _isBiometricAvailable = availableBiometrics.isNotEmpty;
      // print(_isBiometricAvailable);
    } catch (e) {
      print(e);
    }
  }
  bool? isDeviceSupported;
  bool? startingCheck;
  Future<void>  _deviceSupported()async{

    isDeviceSupported = await  _localAuthentication.isDeviceSupported();
    print(isDeviceSupported);
    print('elephant');
  }



  @override
  void initState()  {
    super.initState();
    _deviceSupported();

    // notificationServices.requestNotificationPermission();
    // notificationServices.firebaseInit(context);
    // notificationServices.setupInteractMessage(context);
    // notificationServices.getDeviceToken().then((value) {
    //   print('device token');
    //   print(value);
    //
    // });
    initMain();
    getSharedData();

    Timer(Duration(seconds: widget.timer??3), () async {

      Timer(Duration(seconds: widget.timer??4), () async {
        setState(() {
          expireT = true;
        });
      });
      if (userId != null) {
        print('line 80 if statement ');
        _authenticate();
        // try{
        //   startingCheck = await _localAuthentication.authenticate(localizedReason: reason,options: const AuthenticationOptions(stickyAuth: true));
        //   print(startingCheck);
        //   print('elephant');
        //   print('elephant');
        //   if(startingCheck!){
        //     Get.to(BottomBarScreen());
        //   }
        // }catch(e){
        //       print(e);
        // }

      } else {
        print('line 95 else statement');
        SharedPreferences sf = await SharedPreferences.getInstance();
        bool? res = sf.getBool(AppUtils.showSplash)??true;
        print(res);
        res? Navigator.pushReplacement(context ,MaterialPageRoute(builder: (context) =>  const newSplashUIController())) :Navigator.pushReplacement(context ,MaterialPageRoute(builder: (context) => const OtpLoginViewController()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
    child: Scaffold(
    backgroundColor: Colors.white,
    body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        expireT?const SizedBox(height: 115,):const SizedBox(),
        Center(
          child: SizedBox(
          height: 200,
          width: 200,
          child: Image.asset("res/images/ic_square_fin.png")
          ),
        ),
        expireT?Padding(
          padding: const EdgeInsets.only(top: 10),
          child: ElevatedButton(onPressed: (){
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const SplashScreen(timer: 0)));
          },style: ElevatedButton.styleFrom(
            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(11))),
            minimumSize: const Size(150, 40),
            elevation: 0,
            // side: BorderSide(strokeAlign: BorderSide.strokeAlignOutside,width: 0.3),
            backgroundColor: Colors.lightBlue.shade300,
              textStyle: const TextStyle(fontSize: 16,)
          ), child: const Text('Back To Unlock',style: TextStyle(color: Colors.white,fontWeight: FontWeight.w700),),
          ),
        ):const SizedBox(),
        expireT?Padding(
          padding: const EdgeInsets.only(top: 10),
          child: ElevatedButton(onPressed: (){
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => exit(0)));
          },style: ElevatedButton.styleFrom(
            shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(11))),
            minimumSize: const Size(150, 40),
            elevation: 0,
            backgroundColor: Colors.transparent,
            textStyle: const TextStyle(fontSize: 16,),
            side: const BorderSide(strokeAlign: BorderSide.strokeAlignOutside,width: 0.3)

          ), child: const Text('Exit App Now'),),
        ):const SizedBox()
      ],
    ),
          ),
        );
  }
  Future<void> _authenticate() async {
    await _checkBiometric();
    try {
      if (_isBiometricAvailable!) {
        bool isAuthenticated = await _localAuthentication.authenticate(
          localizedReason: 'Authenticate to access the app',
         options: const AuthenticationOptions(stickyAuth: true,useErrorDialogs: true)
        );
        if (isAuthenticated) {
          // Proceed to the main screen
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context)=> const BottomBarScreen()),result: (route)=>false);
          setState(() {});
          // Get.to(BottomBarScreen());
          print('Going with auth');

        }
      }
      else {

        try{
          startingCheck = await _localAuthentication.authenticate(localizedReason: reason,options: const AuthenticationOptions(stickyAuth: true));
          print(startingCheck);

          print('elephant222222');
          print('elephant');
          if(startingCheck!){
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context)=> const BottomBarScreen()),result: (route)=>false);
          }
        }catch(e){
              print(e);
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context)=> const BottomBarScreen()),result: (route)=>false);

              /// here if above line is unCommented then only it can redirect to home if no pass is available
              /// but in IOS wrong password / 'cancel' leads to redirect it to home
              print('line 198 going to home');
        }
        setState(() {});
        // Get.to(BottomBarScreen());
      }
    } catch (e) {
      print(e);
      // Get.to(BottomBarScreen());
    }
  }
  // Future<void> checkBio() async {
  //     hasBioSensor = await authentication.canCheckBiometrics;
  //     print(hasBioSensor);
  //     if (hasBioSensor) {
  //       await getAuth();
  //     }
  // }

  // Future<void> getAuth() async {
  //   try {
  //     // var options = AuthenticationOptions( useErrorDialogs: false,);
  //     isAuth = await authentication.authenticate(
  //       localizedReason: reason,
  //       options: const AuthenticationOptions(stickyAuth: true,
  //           biometricOnly: false,
  //           sensitiveTransaction: false,
  //           useErrorDialogs: true)
  //     );
  //     if (isAuth) {
  //       // Get.to(BottomBarScreen());
  //     } else {
  //       reason = 'Try Again';
  //     }
  //   } on PlatformException catch (e) {
  //     print('Error authenticating biometrics: $e');
  //     // Get.to(BottomBarScreen());
  //     // Handle the error, such as displaying a message to the user.
  //   }
  //   //---------------------------------------------------------------------------
  //   // final List<BiometricType> availableBiometrics =
  //   // await auth.getAvailableBiometrics();
  //   //
  //   // if (availableBiometrics.isNotEmpty) {
  //   //   // Some biometrics are enrolled.
  //   // }else{
  //   //   Get.to(BottomBarScreen());
  //   //   print('going home cause not available');
  //   // }
  //   //
  //   // if (availableBiometrics.contains(BiometricType.strong) ||
  //   //     availableBiometrics.contains(BiometricType.face)) {
  //   //   // Specific types of biometrics are available.
  //   //   // Use checks like this with caution!
  //   // }
  //   //---------------------------------------------------------------------------
  // }
  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId);
    if (kDebugMode) {
      print('user id is $userId');
    }
    if (userId != null) {
      setState(() {});
    }
  }

  Future<void> initMain() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    version = packageInfo.version;
    if (Platform.isAndroid) {
      AppClient.platForm = "android";
    } else if (Platform.isIOS) {
      AppClient.platForm = "iOS";
    } else if (Platform.isFuchsia) {
      AppClient.platForm = "fuchsia";
    } else if (Platform.isLinux) {
      AppClient.platForm = "linux";
    } else if (Platform.isMacOS) {
      AppClient.platForm = "macOS";
    } else if (Platform.isWindows) {
      AppClient.platForm = "window";
    }
    preferences.setString(AppUtils.platForm, AppClient.platForm);
    preferences.setString(AppUtils.appVersion, version);
  }
}

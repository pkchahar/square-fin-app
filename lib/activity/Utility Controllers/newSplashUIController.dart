import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pi/activity/UserAccount/OtpLoginController.dart';
import 'package:pi/activity/UserAccount/OtpLoginViewController.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../utils/AppUtils.dart';

class newSplashUIController extends StatefulWidget {
  const newSplashUIController({super.key});

  @override
  State<newSplashUIController> createState() => _newSplashUIControllerState();
}

class _newSplashUIControllerState extends State<newSplashUIController> {
  var currentIndex = 0; // Initialize currentIndex to 0
  late PageController _pageController; // Declare a PageController

  @override
  void initState() {
    super.initState();
    _pageController = PageController(); // Initialize the PageController
  }

  @override
  void dispose() {
    _pageController.dispose(); // Dispose the PageController
    super.dispose();
  }

  sharedPref() async {
    SharedPreferences sf = await SharedPreferences.getInstance();
  }

  var imgList = [
    'res/images/profile/splash1.png',
    'res/images/profile/splash2.png',
    'res/images/profile/splash3.png'
  ];
  var txtList = [
    'Empowering Your Financial Freedom with Loans and Credit Cards!',
    'Instant Loans at Your Fingertips. Easy, Fast, Secure.',
    'Unlock Premium Credit Card Benefits. Apply Today!'
  ];

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OtpLoginController>(
      init: OtpLoginController(mContext: context),
      builder: (_) {
        return Scaffold(
          body: SafeArea(
            child: Stack(
              children: [
                Container(
                  color: Colors.white,
                  height: double.infinity,
                  width: double.infinity,
                ),
                PageView.builder(
                  controller: _pageController, // Assign the PageController
                  onPageChanged: (index) {
                    setState(() {
                      currentIndex = index;
                    });
                  },
                  scrollDirection: Axis.horizontal,
                  itemCount: imgList.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Image(
                            image: AssetImage(imgList[index]),
                            height: MediaQuery.of(context).size.height * 0.6,
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                        currentIndex == 0
                            ? const Center(child:  Padding(
                              padding: EdgeInsets.symmetric (horizontal: 16),
                              child: Text('Financial Freedom - Your Financial Partner',style: TextStyle(fontSize: 20),),
                            ))
                            : currentIndex == 1
                            ? const Center(child:  Padding(
                          padding: EdgeInsets.symmetric (horizontal: 16),
                              child: Text('Quick Loans Access',style: TextStyle(fontSize: 20),),
                            ))
                            : const Center(child:  Padding(
                          padding: EdgeInsets.symmetric (horizontal: 16),
                              child: Text('Premium Benefits',style: TextStyle(fontSize: 20),),
                            )),
                        Padding(
                          padding: const EdgeInsets.all(15),
                          child: Text(txtList[index]),
                        ),
                      ],
                    );
                  },
                ),
                Padding(
                  padding:
                  const EdgeInsets.only(bottom: 110, left: 15, right: 15),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      children: [
                        Expanded(
                          child: ElevatedButton(
                            onPressed: () async {
                              if (currentIndex < imgList.length - 1) {
                                _pageController.animateToPage(
                                  currentIndex + 1,
                                  duration: const Duration(milliseconds: 300),
                                  curve: Curves.easeIn,
                                );
                              } else {
                                SharedPreferences sf =
                                await SharedPreferences.getInstance();
                                sf.setBool(AppUtils.showSplash, false);
                                Get.offAll(const OtpLoginViewController());
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: const Color(0xFF0067FF)),
                            child:  Text(
                              currentIndex == imgList.length-1?'Get Started':'Next',
                              style: const TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 8,
                          width: 8,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: currentIndex != 0
                                ? Colors.blueGrey.shade100
                                : Colors.blueGrey,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          child: Container(
                            height: 8,
                            width: 8,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: currentIndex != 1
                                  ? Colors.blueGrey.shade100
                                  : Colors.blueGrey,
                            ),
                          ),
                        ),
                        Container(
                          height: 8,
                          width: 8,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: currentIndex != 2
                                ? Colors.blueGrey.shade100
                                : Colors.blueGrey,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 40, right: 20),
                      child: InkWell(
                        onTap: () async {
                          SharedPreferences sf =
                          await SharedPreferences.getInstance();
                          sf.setBool(AppUtils.showSplash, false);
                          Get.offAll(const OtpLoginViewController());
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: Colors.blue),
                          ),
                          child: const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Text(
                              'Skip',
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
            
                 const Align(
                     alignment: Alignment.bottomRight,
                     child: Image(image: AssetImage('res/images/profile/layer1.png'),height: 150,width: 100,)),
                // const Align(
                //      alignment: Alignment.topLeft,
                //      child: Image(image: AssetImage('res/images/profile/layer2.png'),height: 150,width: 100,)),
              ],
            ),
          ),
        );
      },
    );
  }
}

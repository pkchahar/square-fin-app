import 'package:flutter/material.dart';

class CommingSoonScreen extends StatefulWidget {
  String title = "";

  CommingSoonScreen({super.key, required this.title});

  @override
  State<CommingSoonScreen> createState() => _CommingSoonScreen();
}

class _CommingSoonScreen extends State<CommingSoonScreen> {
  String title = "";
  Map<String, String> mapData = {};

  @override
  void initState() {
    super.initState();
    title = widget.title;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.2 - 18,
          ),
          Center(
            child: Image.asset("res/images/dsa_report.png"),
          ),
          const Text(
            "Coming Soon",
            style: TextStyle(
                fontSize: 24,
                color: Color(0xff1FC4F4),
                fontWeight: FontWeight.w700),
          ),
        ],
      ),
    );
  }
}

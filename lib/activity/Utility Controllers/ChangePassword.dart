import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:pi/Model/BasicResponse.dart';
import 'package:pi/utils/progress_dialog.dart';
import 'package:pi/utils/AppUtils.dart';

import '../../network/ApiClient.dart';
import 'package:get/get.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({super.key});

  @override
  _ChangePassword createState() => _ChangePassword();
}

class _ChangePassword extends State<ChangePassword> {
  late String userId, userType, token, version;
  var _oldPasswordVisible = false;
  var _passwordVisible = false;
  var _rePasswordVisible = false;
  AppClient appClient = AppClient();
  ProgressDialog progressDialog = ProgressDialog();
  final _formKey = GlobalKey<FormState>();
  TextEditingController oldPassController = TextEditingController();
  TextEditingController passController = TextEditingController();
  TextEditingController rePassController = TextEditingController();
  late String oldPass, password, rePass;
  bool isPasswordStrong = false;

  @override
  void initState() {
    super.initState();
    getSharedData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text(
        "Change Password",
      )),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Center(
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 100),
                  child: SizedBox(
                      height: 88,
                      width: 88,
                      child: Image.asset("res/images/ic_square_fin.png")),
                ),
                const SizedBox(
                  height: 26,
                ),
                const Text(
                  'Change Password',
                  style: TextStyle(
                      fontSize: 22,
                      color: Color(0xFF40C4F5),
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 25,
                ),
                const SizedBox(height: 55.0),
                TextFormField(
                  controller: oldPassController,
                  onChanged: (value) {
                    setState(() {});
                  },
                  obscureText: !_oldPasswordVisible,
                  decoration: InputDecoration(
                    hintText: 'Enter Old Password',
                    border: const OutlineInputBorder(),
                    labelText: 'Old Password',
                    suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        _oldPasswordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        setState(() {
                          _oldPasswordVisible = !_oldPasswordVisible;
                        });
                      },
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: const TextStyle(color: Colors.black54),
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Enter a valid password";
                    } else {
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  // controller: passController,
                  onChanged: (value) {
                    setState(() {});
                  },
                  obscureText: !_passwordVisible,
                  decoration: InputDecoration(
                    hintText: 'Enter New Password',
                    border: const OutlineInputBorder(),
                    labelText: 'Password',
                    suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        _passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: const TextStyle(color: Colors.black54),
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter a password';
                    } else if (value.length < 8) {
                      return 'Password should be at least 8 characters long';
                    } else if (!RegExp(
                            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$&*~]).{8,}$')
                        .hasMatch(value)) {
                      return 'Password should contain at least one uppercase letter, one lowercase letter, one number and one special character (!@#\$&*~)';
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: rePassController,
                  onChanged: (value) {
                    password = passController.text;
                    rePass = rePassController.text;
                    setState(() {});
                  },
                  obscureText: !_rePasswordVisible,
                  decoration: InputDecoration(
                    hintText: 'Confirm New Password',
                    border: const OutlineInputBorder(),
                    labelText: 'Confirm Password',
                    suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        _rePasswordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        setState(() {
                          _rePasswordVisible = !_rePasswordVisible;
                        });
                      },
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: const TextStyle(color: Colors.black54),
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Enter a valid password";
                    } else if (password != rePass) {
                      return "Passwords should be same";
                    } else {
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                    onPressed: () {
                      password = passController.text ?? "";
                      oldPass = oldPassController.text ?? "";
                      if (_formKey.currentState!.validate()) {
                        updatePass();
                      }
                    },
                    style: ButtonStyle(
                      backgroundColor:
                      WidgetStateProperty.all<Color>(Colors.lightBlue),
                      // Set the background color to blue
                      minimumSize: WidgetStateProperty.all<Size>(
                          const Size(double.infinity, 50)),
                      shape:
                      WidgetStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              10.0), // Set the border radius to 10.0 for rounded corners
                        ),
                      ), // Set the button width to full and height to 50
                    ),
                    child: const Text(
                      'Update Password',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    )),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    setState(() {});
  }

  void updatePass() async {
    progressDialog.show(context);
    try {
      Map<String, String> hData = {
        'Authorization': 'Bearer $token',
      };

      Map<String, String> mData = {
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'oldPassword': oldPass,
        'newPassword': password,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.changePass);
      print(url);
      print(mData);
      var response = await http.post(url, body: mData, headers: hData);
      print(response.body);
      if (response.statusCode == 200) {
        BasicResponse loginResponse =
            BasicResponse.fromJson(jsonDecode(response.body));
        String msg = loginResponse.message ?? "Something went wrong";

        if (loginResponse.status!) {
          passController.text = "";
          oldPassController.text = "";
          rePassController.text = "";
          Get.snackbar("Password Update", msg);
        } else {
          Get.snackbar("Password Update Failed!", msg);
        }
      }
    } catch (e) {
      print(e);
    }
    progressDialog.hide(context);
  }
}

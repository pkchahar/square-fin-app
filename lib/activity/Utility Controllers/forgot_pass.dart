import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/activity/Utility%20Controllers/verify_otp.dart';
import 'package:pi/utils/progress_dialog.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:get/get.dart';

import '../../network/ApiClient.dart';

class ForgotPass extends StatefulWidget {
  const ForgotPass({super.key});

  @override
  _ForgotPass createState() => _ForgotPass();
}

class _ForgotPass extends State<ForgotPass> {
  var userMobile;
  String title = "Forgot Password";
  String imageUrl = "res/images/forgot.png";
  String des =
      "Enter your registered mobile number below to get forgot password OTP";
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _mobile = TextEditingController();
  AppClient appClient = AppClient();
  ProgressDialog progressDialog = ProgressDialog();
  bool isNewUser = false;
  String version = "";

  @override
  void initState() {
    getSharedData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Center(
              child: Column(children: [
                const SizedBox(
                  height: 75,
                ),
                Image.asset(imageUrl),
                const SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 30, right: 30),
                  child: Text(
                    des,
                    textAlign: TextAlign.center,
                    style: const TextStyle(color: Colors.grey, fontSize: 15),
                  ),
                ),
                const SizedBox(height: 25.0),
                TextFormField(
                  controller: _mobile,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(10),
                  ],
                  decoration: const InputDecoration(
                    labelText: 'Mobile Number',
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: TextStyle(color: Colors.black54),
                  ),
                  keyboardType: TextInputType.phone,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your Mobile Number';
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                    onPressed: () {
                      userMobile = _mobile.text;

                      if (_formKey.currentState!.validate()) {
                        progressDialog.show(context);
                        if (isNewUser) {
                          getRegistrationCode();
                        } else {
                          onSendForgotPass();
                        }
                      }
                    },
                    style: ButtonStyle(
                      backgroundColor:
                      WidgetStateProperty.all<Color>(Colors.lightBlue),
                      // Set the background color to blue
                      minimumSize: WidgetStateProperty.all<Size>(
                          const Size(double.infinity, 50)),
                      shape:
                      WidgetStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              10.0), // Set the border radius to 10.0 for rounded corners
                        ),
                      ), // Set the button width to full and height to 50
                    ),
                    child: const Text(
                      'Send Verification Code',
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    )),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> onSendForgotPass() async {
    print(userMobile);
    Map parsed = await appClient.getForGotPassOtp(userMobile);
    dynamic status = parsed["status"];
    dynamic msg = parsed["message"];

    Fluttertoast.showToast(msg: '$msg');
    print(status);
    progressDialog.hide(context);

    if (status != null && status == true) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      print(userMobile);
      preferences.setString(AppUtils.userMobile, userMobile);
      Get.to(const VerifyPass());
    } else {
      Fluttertoast.showToast(msg: '$msg');
    }
  }

  Future<void> getRegistrationCode() async {
    print(userMobile);
    Map parsed = await appClient.getRegistrationCode(
      userMobile,
      "Send Otp",
    );
    dynamic status = parsed["status"];
    String msg = parsed["message"];

    Fluttertoast.showToast(msg: msg);
    print(msg);
    progressDialog.hide(context);

    if (status != null && status == true) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      print(userMobile);
      preferences.setString(AppUtils.userMobile, userMobile);
      Get.to(const VerifyPass());
    } else {
      Fluttertoast.showToast(msg: msg);
    }
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    isNewUser = preferences.getBool(AppUtils.newUser) ?? false;
    print(isNewUser);

    if (isNewUser) {
      title = "Sign Up";
      imageUrl = "res/images/sign_up.png";
      des = "Enter your mobile number below to get verification code";
    } else {
      title = "Forgot Password";
      imageUrl = "res/images/forgot.png";
    }
    setState(() {});
  }
}

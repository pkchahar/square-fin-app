import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pi/activity/UserAccount/OtpLoginViewController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/utils/progress_dialog.dart';
import 'package:pi/utils/AppUtils.dart';

import '../../network/ApiClient.dart';
import 'package:get/get.dart';

class ChangePass extends StatefulWidget {
  const ChangePass({super.key});

  @override
  _ChangePass createState() => _ChangePass();
}

class _ChangePass extends State<ChangePass> {
  var userMobile;
  var _passwordVisible = false;
  var _rePasswordVisible = false;
  AppClient appClient = AppClient();
  ProgressDialog progressDialog = ProgressDialog();
  final _formKey = GlobalKey<FormState>();
  TextEditingController passController = TextEditingController();
  TextEditingController rePassController = TextEditingController();
  late String password, rePass;

  @override
  void initState() {
    super.initState();
    getSharedData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Center(
              child: Column(children: [
                Padding(
                  padding: const EdgeInsets.only(top: 100),
                  child: SizedBox(
                      height: 88,
                      width: 88,
                      child: Image.asset("res/images/ic_square_fin.png")),
                ),
                const SizedBox(
                  height: 26,
                ),
                const Text(
                  'Change Password',
                  style: TextStyle(
                      fontSize: 22,
                      color: Color(0xFF40C4F5),
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 25,
                ),
                const SizedBox(height: 55.0),
                TextFormField(
                  controller: passController,
                  onChanged: (value) {
                    setState(() {});
                  },
                  obscureText: !_passwordVisible,
                  decoration: InputDecoration(
                    hintText: 'Enter New Password',
                    border: const OutlineInputBorder(),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: const TextStyle(color: Colors.black54),
                    labelText: 'Password',
                    suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        _passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Enter a valid password";
                    } else if (value.length < 8) {
                      return 'Password should be at least 8 characters long';
                    } else if (!RegExp(
                            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$&*~]).{8,}$')
                        .hasMatch(value)) {
                      return 'Password should contain at least one uppercase letter, one lowercase letter, one number and one special character (!@#\$&*~)';
                    } else {
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: rePassController,
                  onChanged: (value) {
                    password = passController.text;
                    rePass = rePassController.text;
                    setState(() {});
                  },
                  obscureText: !_rePasswordVisible,
                  decoration: InputDecoration(
                    hintText: 'Confirm New Password',
                    border: const OutlineInputBorder(),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: const TextStyle(color: Colors.black54),
                    focusColor: Colors.lightBlue,
                    labelText: 'Confirm Password',
                    suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        _rePasswordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.grey,
                      ),
                      onPressed: () {
                        setState(() {
                          _rePasswordVisible = !_rePasswordVisible;
                        });
                      },
                    ),
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Enter a valid password";
                    } else if (password != rePass) {
                      return "Passwords should be same";
                    } else {
                      return null;
                    }
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        updatePass();
                      }
                    },
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.lightBlue),
                      // Set the background color to blue
                      minimumSize: WidgetStateProperty.all<Size>(
                          const Size(double.infinity, 50)),
                      shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              10.0), // Set the border radius to 10.0 for rounded corners
                        ),
                      ), // Set the button width to full and height to 50
                    ),
                    child: const Text(
                      'Update Password',
                      style: TextStyle(color: Colors.black, fontSize: 18),
                    )),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userMobile = preferences.getString(AppUtils.userMobile);
    print('user name is $userMobile');
    if (userMobile != null) {
      setState(() {});
    }
  }

  Future<void> updatePass() async {
    String password = passController.text;
    Map parsed = await appClient.updatePass(userMobile, password);
    dynamic status = parsed["status"];
    dynamic msg = parsed["message"];

    Fluttertoast.showToast(msg: '$msg');
    print(status);
    progressDialog.hide(context);

    if (status != null && status == true) {
      Get.offAll(() => const OtpLoginViewController());
    }
  }
}

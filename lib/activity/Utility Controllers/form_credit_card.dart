import 'package:flutter/material.dart';

class CreditForm extends StatefulWidget {
  const CreditForm({super.key});

  @override
  _Credit createState() => _Credit();
}

class _Credit extends State<CreditForm> {
  final _formKey = GlobalKey<FormState>();

  String _otherCredit = '1';
  String dropdownvalue = 'Account';
  String _selectedSalary = '10K-20K';
  var oc_items = [
    'Account',
    'Teacher',
    'Software Developer',
    'Doctor',
    'Banker',
    'Engineer'
  ];
  var salary_items = [
    '10K-20K',
    '20K-30K',
    '30K-40K',
    '40K-50K',
    '50K-60K',
    '60K-70K',
    '70K-80K',
    '80K-90K',
    '90K-1L',
    'Above 1L',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Apply For Credit Card'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Card(
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Text('Apply For Credit Card',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blue.shade500)),
                          Divider(
                            color: Colors.grey.shade400,
                            thickness: 1.0,
                          ),
                          const SizedBox(height: 16.0),
                          TextFormField(
                            decoration: const InputDecoration(
                              labelText: 'Name',
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.lightBlue,
                                ),
                              ),
                              labelStyle: TextStyle(color: Colors.black54),
                            ),
                            keyboardType: TextInputType.name,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Please enter your name';
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 16.0),
                          TextFormField(
                            decoration: const InputDecoration(
                              labelText: 'Email',
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.lightBlue,
                                ),
                              ),
                              labelStyle: TextStyle(color: Colors.black54),
                            ),
                            keyboardType: TextInputType.emailAddress,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter your email address';
                              } else if (!RegExp(r'\S+@\S+\.\S+')
                                  .hasMatch(value)) {
                                return 'Please enter a valid email address';
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 16.0),
                          TextFormField(
                            decoration: const InputDecoration(
                              labelText: 'Contact Number',
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.lightBlue,
                                ),
                              ),
                              labelStyle: TextStyle(color: Colors.black54),
                            ),
                            keyboardType: TextInputType.phone,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter your Contact Number';
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 16.0),
                          const Text('Do you have other credit card:'),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 100,
                                child: Expanded(
                                  child: RadioListTile(
                                    contentPadding: const EdgeInsets.only(
                                        // Add this
                                        left: 0,
                                        right: 0,
                                        bottom: 0,
                                        top: 0),
                                    title: const Text("Yes"),
                                    value: '1',
                                    groupValue: _otherCredit,
                                    onChanged: (String? value) {
                                      setState(() {
                                        _otherCredit = value!;
                                      });
                                    },
                                  ),
                                ),
                              ),
                              Expanded(
                                child: RadioListTile(
                                  contentPadding: const EdgeInsets.only(
                                      // Add this
                                      left: 0,
                                      right: 0,
                                      bottom: 0,
                                      top: 0),
                                  title: const Text("No"),
                                  value: '0',
                                  groupValue: _otherCredit,
                                  onChanged: (String? value) {
                                    setState(() {
                                      _otherCredit = value!;
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(3),
                                    border: Border.all(
                                        color: Colors.grey.shade400, width: 1)),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: DropdownButton<String>(
                                    hint: const Text('Select a Occupation '),
                                    value: dropdownvalue,
                                    icon: const Icon(Icons.arrow_drop_down),
                                    iconSize: 24,
                                    elevation: 16,
                                    onChanged: (String? newValue) {
                                      setState(() {
                                        dropdownvalue = newValue!;
                                      });
                                    },
                                    items: oc_items
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 5),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(3),
                                      border: Border.all(
                                          color: Colors.grey.shade400,
                                          width: 1)),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: DropdownButton<String>(
                                      hint: const Text('Select salary '),
                                      value: _selectedSalary,
                                      icon: const Icon(Icons.arrow_drop_down),
                                      iconSize: 24,
                                      elevation: 16,
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          _selectedSalary = newValue!;
                                        });
                                      },
                                      items: salary_items
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 8.0),
                          TextFormField(
                            decoration: const InputDecoration(
                              labelText: 'Address',
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.lightBlue,
                                ),
                              ),
                              labelStyle: TextStyle(color: Colors.black54),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter Address';
                              } else if (value.length > 10) {
                                return 'Please enter Address';
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 16.0),
                          ElevatedButton(
                            child: const Text('Submit Request'),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                _showSuccessDialog();
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  void _showSuccessDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Success'),
          content: const Text('Your account has been created successfully.'),
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
// CustomSearchableDropDown(
//                                     items: options,
//                                     label: 'Select Name',
//                                     decoration:
//                                     BoxDecoration(border: Border.all(color: Colors.blue)),
//                                     prefixIcon: Padding(
//                                       padding: const EdgeInsets.all(0.0),
//                                       child: Icon(Icons.search),
//                                     ),
//                                     dropDownMenuItems: options?.map((item) {
//                                       return item;
//                                     })?.toList() ??
//                                         [],
//                                     onChanged: (value) {
//                                       if (value != null) {
//                                         print(value['class'].toString());
//                                       } else {}
//                                     },
//                                   )

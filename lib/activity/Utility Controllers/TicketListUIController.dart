import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pi/activity/Utility%20Controllers/TicketListController.dart';
import 'package:pi/activity/Utility%20Controllers/TicketDetailUIController.dart';

import '../../Model/Ticket/FetchTicketList.dart';
import '../../utils/AppUtils.dart';

class TicketListUIController extends StatefulWidget {
  const TicketListUIController({super.key});

  @override
  State<TicketListUIController> createState() => _TicketListUIControllerState();

}


class _TicketListUIControllerState extends State<TicketListUIController> {

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GetBuilder<TicketListController>(
        init: TicketListController(),
    builder: (_) {
          return Scaffold(
      appBar: AppBar(title: const Text('My Ticket')),
      body: Column(
        children: [

             _.data.isNotEmpty?
      Expanded(
            child:ListView.builder(itemCount: _.data.length,itemBuilder:(context, index){
              return InkWell(
                onTap: (){
                  DataForList obj =  _.data[index];
                  Get.to(TicketDetailUIController(ticketId: _.data[index].id!,obj: obj,));
                },
                child:
                Card(
                  color: Colors.grey.shade50,
                  margin: const EdgeInsets.all(11),
                  elevation: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                                      decoration: BoxDecoration(
                                          borderRadius: const BorderRadius.only(
                                              topRight: Radius.circular(5),
                                              topLeft: Radius.circular(5)),
                                          border: Border(
                                              top: BorderSide(
                                                color: Colors.blue.shade100,
                                              ),
                                              // right: BorderSide(
                                              //   color: Colors.blue.shade100,
                                              // ),
                                              bottom: BorderSide(
                                                color: Colors.blue.shade100,
                                              )),
                                          color: Colors.blue.shade50),
                                      // padding: EdgeInsets.all(5),
                        child: Padding(
                          padding: const EdgeInsets.only(top:3,bottom: 3),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  constraints: const BoxConstraints(minWidth: 100),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text(_.data[index].product_name.toString(),textAlign: TextAlign.start,style: const TextStyle(fontWeight: FontWeight.bold),),
                                  )),
                              // Flexible(child: Text('American Express')),

                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(children: [
                          Row(
                            // mainAxisAlignment: MainAxisAlignment.start,
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                //padding: EdgeInsets.all(5),
                                  constraints: const BoxConstraints(minWidth: 100),
                                  child: const Text('Date: ',textAlign: TextAlign.justify,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black))),
                              Flexible(child: Text(DateFormat('dd-MM-yyyy',).format(DateTime.parse(_.data[index].date.toString())),style: const TextStyle(color: Colors.black),)),
                              const SizedBox(width: 40),
                              Container(
                                  constraints: const BoxConstraints(minWidth: 60),
                                  child: const Text('Status: ',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black))),
                              Flexible(child: Text(_.data[index].status.toString(),style: const TextStyle(color: Colors.black),)),

                            ],),
                          const Divider(height: 8,thickness: 1,),
                          Container(
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(5),),
                            // padding: EdgeInsets.all(5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                Container(
                                    constraints: const BoxConstraints(minWidth: 100),
                                    child: const Text('Lead-Code: ',textAlign: TextAlign.start,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),)),
                                Flexible(child: Text(_.data[index].leadid.toString(),style: const TextStyle(color: Colors.black),)),

                              ],
                            ),
                          ),
                          const Divider(height: 8,thickness: 1,),
                          Container(
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(5),),
                            // padding: EdgeInsets.all(5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                Container(
                                    constraints: const BoxConstraints(minWidth: 100),
                                    child: const Text('Subject: ',textAlign: TextAlign.start,style: TextStyle(fontWeight: FontWeight.bold,color:Colors.black),)),
                                Flexible(child: Text(_.data[index].subject.toString(),style: const TextStyle(color: Colors.black),)),

                                // Text('Date: ',textAlign: TextAlign.justify,style: TextStyle(fontWeight: FontWeight.bold)),
                                // Container(constraints: BoxConstraints(minWidth: 10),),
                                // Flexible(child: Text(_.data[index].date.toString())),
                              ],
                            ),
                          ),
                        ],),
                      ),

                    ],
                  ),
                ),
                // Container(
                //   margin: EdgeInsets.only(top: 8,bottom: 4),
                //   color: Colors.green.shade100,
                //   child: ListTile(
                //     shape: RoundedRectangleBorder(),
                //     style: ListTileStyle.drawer,
                //     leading: Text('${_.data[index].subject}'),
                //     title: Text('${_.data[index].message}',style: TextStyle(fontSize: 15),),
                //     subtitle: Text('${_.data[index].date}'),
                //     trailing: Text('${_.data[index].status}'),
                //
                //   ),
                // )
              );
            })
          ):Center(
               child: Padding(
                 padding:
                 const EdgeInsets.only(top: 113.0),
                 child: SvgPicture.asset(AppUtils.blankImg,
                     width:
                     MediaQuery.of(context).size.width,
                     height: MediaQuery.of(context)
                         .size
                         .height *
                         0.33,
                     fit: BoxFit.fill),
               ),
             )
        ],
      ),
    );
    });
  }
}


import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher_string.dart';

import 'ContactController.dart';

class ContactUIController extends StatefulWidget {
  const ContactUIController({super.key});

  @override
  _ContactUIController createState() => _ContactUIController();
}

class _ContactUIController extends State<ContactUIController> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ContactController>(
      init: ContactController(context: context),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Contact Us"),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Form(
                  key: _.formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Image.asset("res/images/contact_us.png"),
                      ),
                      Card(
                        elevation: 5,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Text('Post A Query',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue.shade500)),
                              Divider(
                                color: Colors.grey.shade400,
                                thickness: 1.0,
                              ),
                              const SizedBox(height: 16.0),
                              TextFormField(
                                controller: _.nameController,
                                decoration: const InputDecoration(
                                  labelText: 'Name',
                                  border: OutlineInputBorder(),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  labelStyle: TextStyle(color: Colors.black54),
                                ),
                                keyboardType: TextInputType.name,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter your name';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              TextFormField(
                                controller: _.emailController,
                                decoration: const InputDecoration(
                                  labelText: 'Email',
                                  border: OutlineInputBorder(),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  labelStyle: TextStyle(color: Colors.black54),
                                ),
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter your email address';
                                  } else if (!RegExp(r'\S+@\S+\.\S+')
                                      .hasMatch(value)) {
                                    return 'Please enter a valid email address';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              TextFormField(
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(10),
                                ],
                                controller: _.phoneController,
                                decoration: const InputDecoration(
                                  labelText: 'Phone Number',
                                  border: OutlineInputBorder(),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  labelStyle: TextStyle(color: Colors.black54),
                                ),
                                keyboardType: TextInputType.phone,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter your Phone Number';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              TextFormField(
                                controller: _.messageController,
                                decoration: const InputDecoration(
                                  labelText: 'Message',
                                  border: OutlineInputBorder(),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  labelStyle: TextStyle(color: Colors.black54),
                                ),
                                maxLines: 4,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter a message';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              ElevatedButton(
                                onPressed: () {
                                  if (_.formKey.currentState!.validate()) {
                                    String name = _.nameController.text ?? "";
                                    String email = _.emailController.text ?? "";
                                    String phone = _.phoneController.text ?? "";
                                    String msg = _.messageController.text ?? "";
                                    _.submitQuery(name, phone, email, msg);
                                  }
                                },
                                style: ButtonStyle(
                                  backgroundColor:
                                  WidgetStateProperty.all<Color>(Colors.lightBlue),
                                  // Set the background color to blue
                                  minimumSize: WidgetStateProperty.all<Size>(
                                      const Size(double.infinity, 50)),
                                  shape:
                                  WidgetStateProperty.all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          10.0), // Set the border radius to 10.0 for rounded corners
                                    ),
                                  ), // Set the button width to full and height to 50
                                ),
                                child: const Text('Submit'),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      Card(
                        elevation: 5,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Icon(Icons.mail, size: 14),
                                    Expanded(
                                        child: InkWell(
                                            onTap: () {
                                              launch(
                                                  'mailto:info@squarefin.in?subject=Square Fin Query&body=Write something here');
                                            },
                                            child: const Padding(
                                              padding: EdgeInsets.only(
                                                  left: 10),
                                              child: Text(" info@squarefin.in"),
                                            ))),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Icon(Icons.phone, size: 14),
                                    Expanded(
                                        child: Padding(
                                      padding: const EdgeInsets.only(left: 10),
                                      child: InkWell(
                                          onTap: () {
                                            launch('tel:+91 8306300415');
                                          },
                                          child: const Text("+91-8306300415")),
                                    ))
                                  ],
                                ),
                              ),
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(Icons.map, size: 14),
                                    Expanded(
                                        child: Padding(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                          "602-603, 6th Floor Trimurty`s V-Jai City Point, Ahinsa Circle C-Scheme, Jaipur"),
                                    ))
                                  ],
                                ),
                              ),
                              const Divider(
                                height: 1,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  IconButton(
                                    icon: const FaIcon(FontAwesomeIcons.facebook),
                                    onPressed: () {
                                      launchUrlString(
                                          'https://www.facebook.com/squarefinoffical');
                                    },
                                  ),
                                  IconButton(
                                    icon: const FaIcon(FontAwesomeIcons.twitter),
                                    onPressed: () {
                                      launchUrlString(
                                          'https://www.twitter.com/Squarefin2');
                                    },
                                  ),
                                  IconButton(
                                    icon: const FaIcon(FontAwesomeIcons.instagram),
                                    onPressed: () {
                                      launchUrlString(
                                          'https://www.instagram.com/square_fin/');
                                    },
                                  ),
                                  IconButton(
                                    icon: const FaIcon(FontAwesomeIcons.linkedin),
                                    onPressed: () {
                                      launchUrlString(
                                          'https://www.linkedin.com/company/squarefin-private-limited/');
                                    },
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        );
      },
    );
  }
}

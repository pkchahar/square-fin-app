import 'dart:convert';

// import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:pi/Model/Occupation.dart';
import 'package:pi/network/ApiClient.dart';

// import '../../../Model/Pos/PosList.dart';
import '../../../utils/AppUtils.dart';
import '../../Model/Loan/LoanMaster.dart';
import '../../Model/Loan/LoanMasterData.dart';
import '../../Model/Loan/LoanViewList.dart';

class LeadView extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;
  LoanViewList productsData = LoanViewList();
  List<LoanMasterData> productList = [];
  String userId = "", userType = "", token = "", version = "";

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void getProductListView(String product) async {
    setLoading(true);
    try {
      Map<String, String> hData = {
        'Authorization': 'Bearer $token',
      };

      Map<String, String> mData = {
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'Url': product,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.getLoanDetail);
      print(url);
      print(mData);
      var response = await http.post(url, body: mData, headers: hData);
      print(response.body);
      if (response.statusCode == 200) {
        productsData = LoanViewList.fromJson(jsonDecode(response.body));
        update();
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    getLeadMaster();
  }

  void getLeadMaster() async {
    try {
      Map<String, String> mData = {
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.viewLoanMaster);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        LoanMaster bank = LoanMaster.fromJson(jsonDecode(response.body));
        productList.addAll(bank.data as Iterable<LoanMasterData>);
        if (productList.isNotEmpty) {
          String product = productList[0].url ?? "/";
          getProductListView(product);
        }
        update();
      }
    } catch (e) {
      print(e);
    }
  }
}

// import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

// import '../../Model/Bank.dart';
// import '../../Model/BankList.dart';
// import '../../Model/Loan/LoanMaster.dart';
import '../../Model/Loan/LoanMasterData.dart';
// import '../../utils/AppUtils.dart';
import 'LeadView.dart';
// import 'package:http/http.dart' as http;

class LeadViewUI extends StatefulWidget {
  const LeadViewUI({super.key});

  @override
  _LeadViewUI createState() => _LeadViewUI();
}

class _LeadViewUI extends State<LeadViewUI> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LeadView>(
      init: LeadView(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("My Leads"),
          ),
          body: Stack(children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: DropdownButtonFormField<LoanMasterData>(
                items: _.productList.map((item) {
                  return DropdownMenuItem<LoanMasterData>(
                    value: item,
                    child: Text(item.type ?? ""),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    String productUrl = value?.url ?? "";
                    if (productUrl.isNotEmpty) {
                      _.getProductListView(productUrl);
                    }
                  });
                },
                decoration: const InputDecoration(
                  labelText: 'Lead Type',
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.lightBlue,
                    ),
                  ),
                  labelStyle: TextStyle(color: Colors.black54),
                ),
              ),
            ),
            _.isLoading
                ? const Center(child: CircularProgressIndicator())
                : _.productsData.data == null || _.productsData.data!.isEmpty
                    ? const Center(child: Text("Oops No data found"))
                    : Padding(
                        padding: const EdgeInsets.only(top: 70),
                        child: ListView.builder(
                          itemCount: _.productsData.data!.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.all(8),
                              child: Card(
                                elevation: 5,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 8),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Container(
                                            padding: const EdgeInsets.only(
                                                left: 8,
                                                right: 8,
                                                top: 3,
                                                bottom: 3),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  const BorderRadius.only(
                                                topLeft: Radius.circular(10.0),
                                                bottomRight:
                                                    Radius.circular(10.0),
                                              ),
                                              color: Colors.green[900],
                                            ),
                                            child: Text(
                                              _.productsData.data![index]
                                                  .status!,
                                              style: const TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Text(
                                        _.productsData.data![index].name!
                                            .toUpperCase(),
                                        style: TextStyle(
                                            color: Colors.indigo[600],
                                            fontWeight: FontWeight.w500),
                                      ),
                                      const Divider(),
                                      Row(
                                        children: [
                                          const Icon(
                                            Icons.email_outlined,
                                            size: 20,
                                            color: Colors.lightBlue,
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Text(_.productsData.data![index]
                                              .email!),
                                        ],
                                      ),
                                      const Divider(),
                                      Row(
                                        children: [
                                          const Icon(
                                            Icons.phone_android_outlined,
                                            size: 20,
                                            color: Colors.lightBlue,
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Text(_.productsData.data![index]
                                              .mobile!),
                                        ],
                                      ),
                                      const Divider(),
                                      Row(
                                        children: [
                                          const Icon(
                                            Icons.confirmation_num_outlined,
                                            size: 20,
                                            color: Colors.lightBlue,
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Text(_.productsData.data![index]
                                              .loanType!),
                                        ],
                                      ),
                                      const Divider(),
                                      Row(
                                        children: [
                                          const Icon(
                                            Icons.location_on_outlined,
                                            size: 20,
                                            color: Colors.lightBlue,
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Text(_.productsData.data![index]
                                              .location!),
                                        ],
                                      ),
                                      const Divider(),
                                      Row(
                                        children: [
                                          const Icon(
                                            Icons.calendar_month,
                                            size: 20,
                                            color: Colors.lightBlue,
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Text(_.productsData.data![index]
                                              .insertDate!),
                                        ],
                                      ),
                                      const Divider(),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
          ]),
        );
      },
    );
  }
}

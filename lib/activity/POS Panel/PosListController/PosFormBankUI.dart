
import 'package:custom_searchable_dropdown/custom_searchable_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pi/activity/POS%20Panel/PosListController/PosFormBank.dart';



class PosFormBankUI extends StatefulWidget {
  const PosFormBankUI({super.key});

  @override
  _PosFormBankUI createState() => _PosFormBankUI();
}

class _PosFormBankUI extends State<PosFormBankUI> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController branchController = TextEditingController();
  TextEditingController accountController = TextEditingController();
  TextEditingController ifscController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PosFormBank>(
      init: PosFormBank(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Bank Detail"),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            const SizedBox(height: 16.0),
                            CustomSearchableDropDown(
                              items: _.bankList,
                              label: 'Select Bank',
                              decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.circular(5)),
                              dropDownMenuItems: _.bankList.map((item) {
                                return item.bankName ?? "";
                              }).toList(),
                              onChanged: (value) {
                                if (value != null) {
                                  setState(() {
                                    _.selectedBank = value?.bankName ?? "";
                                  });
                                } else {}
                              },
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            TextFormField(
                              controller: branchController,
                              decoration: const InputDecoration(
                                labelText: 'Branch Name',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.lightBlue,
                                  ),
                                ),
                                labelStyle: TextStyle(color: Colors.black54),
                              ),
                              keyboardType: TextInputType.name,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Select Branch name';
                                }
                                return null;
                              },
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            DropdownButtonFormField<String>(
                              value: _.selectAccType,
                              items: const [
                                DropdownMenuItem(
                                    value: 'Saving', child: Text('Saving')),
                                DropdownMenuItem(
                                    value: 'Current', child: Text('Current')),
                                DropdownMenuItem(
                                    value: 'Salary', child: Text('Salary')),
                              ],
                              onChanged: (value) {
                                setState(() {
                                  _.selectAccType = value!;
                                });
                              },
                              decoration: const InputDecoration(
                                labelText: 'Account Type',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.lightBlue,
                                  ),
                                ),
                                labelStyle: TextStyle(color: Colors.black54),
                              ),
                            ),
                            const SizedBox(height: 16.0),
                            TextFormField(
                              controller: accountController,
                              decoration: const InputDecoration(
                                labelText: 'Account Number',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.lightBlue,
                                  ),
                                ),
                                labelStyle: TextStyle(color: Colors.black54),
                              ),
                              keyboardType: TextInputType.number,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Enter Account Number';
                                }
                                return null;
                              },
                            ),
                            const SizedBox(height: 16.0),
                            TextFormField(
                              controller: ifscController,
                              decoration: const InputDecoration(
                                labelText: 'IFSC Code',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.lightBlue,
                                  ),
                                ),
                                labelStyle: TextStyle(color: Colors.black54),
                              ),
                              keyboardType: TextInputType.name,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Enter IFSC Code';
                                }
                                return null;
                              },
                            ),
                            const SizedBox(height: 20.0),
                            ElevatedButton(
                              onPressed: () {
                                _.branch = branchController.text ?? "";
                                _.accountNo = accountController.text ?? "";
                                _.ifscCode = ifscController.text ?? "";

                                if (_formKey.currentState!.validate()) {
                                  _.onNext();
                                }
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    WidgetStateProperty.all<Color>(
                                        Colors.lightBlue),
                                // Set the background color to blue
                                minimumSize: WidgetStateProperty.all<Size>(
                                    const Size(double.infinity, 50)),
                                shape: WidgetStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        10.0), // Set the border radius to 10.0 for rounded corners
                                  ),
                                ), // Set the button width to full and height to 50
                              ),
                              child: const Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Text(
                                  'Next',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        );
      },
    );
  }
}

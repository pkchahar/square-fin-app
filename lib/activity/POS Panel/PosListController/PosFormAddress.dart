import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/network/ApiClient.dart';
import '../../../Model/StateData.dart';
import '../../../Model/StateList.dart';
import '../../../utils/AppUtils.dart';
import 'PosFormBankUI.dart';

class PosFormAddress extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;
  String selectedGender = 'Male';
  String selectedGraduation = '10th';
  String version = '',
      userId = '',
      userType = '',
      token = '',
      category = 'Individual';
  String isGst = '1';
  late String address1, address2;

  String selectedState = '';
  String selectAccDistrict = '';
  String selectAccCity = '';
  String selectedPin = "";

  List<StateData> stateList = [];
  List<StateData> districtList = [];
  List<StateData> cityList = [];
  List<StateData> pincodeList = [];

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";

    Map<String, String> lData = Get.find(tag: "mData");

    print("lData");
    print(lData);
    print("lData");

    getStateList();
  }

  void onNext() {
    Map<String, String> lData = Get.find(tag: "mData");

    lData.addEntries({
      'Addres_Line_1': address1,
      'Addres_Line_2': address2,
      'State_Id': selectedState,
      'District_Id': selectAccDistrict,
      'City_Id': selectAccCity,
      'Pincode': selectedPin,
    }.entries);

    Get.put(lData, tag: "mData");

    Map<String, String> hdata = Get.find(tag: "mData");

    print("ye aaya output");
    print(hdata);
    print("ye....");

    Get.to(() => const PosFormBankUI());
  }

  void getStateList() async {
    try {
      Map<String, String> mData = {
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.stateList);
      print(url);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        StateList bank = StateList.fromJson(jsonDecode(response.body));
        stateList.addAll(bank.data as Iterable<StateData>);
        print(stateList);
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void getDistrict() async {
    try {
      Map<String, String> mData = {
        'State_Id': selectedState,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.districtList);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        StateList bank = StateList.fromJson(jsonDecode(response.body));
        districtList.addAll(bank.data as Iterable<StateData>);
        print(districtList);
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void getCityList() async {
    try {
      Map<String, String> mData = {
        'District_Id': selectAccDistrict,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.cityList);
      var response = await http.post(url, body: mData);
      print(mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        StateList bank = StateList.fromJson(jsonDecode(response.body));
        cityList.addAll(bank.data as Iterable<StateData>);
        print(cityList);
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void getPincode() async {
    try {
      Map<String, String> mData = {
        'City_Id': selectAccCity,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.pincodeList);
      var response = await http.post(url, body: mData);
      print(url);
      print(mData);
      print(response.body);
      if (response.statusCode == 200) {
        StateList bank = StateList.fromJson(jsonDecode(response.body));
        pincodeList.addAll(bank.data as Iterable<StateData>);

        update();
      }
    } catch (e) {
      print(e);
    }
  }
}

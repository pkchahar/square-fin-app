import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../utils/AppUtils.dart';
import 'PartnerListController.dart';

class PartnerListUIController extends StatefulWidget {
  const PartnerListUIController({super.key});

  @override
  State<PartnerListUIController> createState() => _PartnerListUIControllerState();
}

  final formKey = GlobalKey<FormState>();
class _PartnerListUIControllerState extends State<PartnerListUIController> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PartnerListController>(
        init: PartnerListController(),
        builder: (_){
          return Scaffold(
            appBar: AppBar(title: Text('My Partners' ' (${_.myPartner.length})'),actions: [
                IconButton(
                onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Row(
                            children: [
                              const Text('Add New Partner!',style: TextStyle(color: Colors.black,fontSize: 16),),
                              const Spacer(),

                              IconButton(onPressed: (){ Get.back();
                              _.nameController.text ='';
                              _.emailController.text = '';
                              _.mobileController.text ='';}, icon: Icon(Icons.close,color: Colors.red.shade300))
                            ],
                          ),
                          content: SingleChildScrollView(
                            child: StatefulBuilder(
                              builder:
                                  (BuildContext context, StateSetter setState) {
                                return Form(
                                  key: formKey,
                                  child: Column(
                                    children: [
                                      TextFormField(
                                        controller: _.nameController,
                                        inputFormatters: [
                                          FilteringTextInputFormatter.deny(RegExp(r'[0-9]'))
                                        ],
                                        decoration: const InputDecoration(
                                          labelText: 'Enter Name',
                                          border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFD4E4F1))),
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Color(0xFFE3F2FD))),
                                          focusedBorder: OutlineInputBorder(),
                                          labelStyle:
                                              TextStyle(color: Colors.grey),
                                        ),
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Please enter Name';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(height: 16),
                                      TextFormField(
                                        controller: _.emailController,
                                        inputFormatters: [
                                          FilteringTextInputFormatter.allow(
                                              RegExp(r'[a-zA-Z0-9@._]')),
                                        ],
                                        decoration: const InputDecoration(
                                          labelText: 'Enter Email Id',
                                          border: OutlineInputBorder(),
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Color(0xFFE3F2FD))),
                                          focusedBorder: OutlineInputBorder(),
                                          labelStyle:
                                              TextStyle(color: Colors.grey),
                                        ),
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Please enter an email';
                                          } else if (!RegExp(
                                                  r'^[^@]+@[^@]+\.[^@]+')
                                              .hasMatch(value)) {
                                            return 'Please enter a valid email';
                                          } else if (!value.contains('@')) {
                                            return 'Email must contain "@"';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(height: 16),
                                      TextFormField(
                                        inputFormatters: [
                                          FilteringTextInputFormatter.digitsOnly
                                        ],
                                        maxLength: 10,
                                        controller: _.mobileController,
                                        decoration: const InputDecoration(
                                          labelText: 'Phone Number',
                                          border: OutlineInputBorder(),
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: Color(0xFFE3F2FD))),
                                          focusedBorder: OutlineInputBorder(),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType: TextInputType.phone,
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Please enter your Phone Number';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(height: 16),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                          actions: [
                            InkWell(
                                onTap:() async {
                                  if (formKey.currentState!.validate()) {
                                    await _.addPartner();
                                    if(_.msg == 'Partner Registered Successfully'){
                                      Navigator.pop(context);
                                      _.msg ='';
                                      _.nameController.text = '';
                                      _.emailController.text = '';
                                      _.mobileController.text ='';
                                      _.fetchPartner();
                                    }
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom:8.0),
                                  child: Container(
                                      height:30,
                                      width:126,
                                      // padding: EdgeInsets.symmetric(horizontal: 35, vertical: 6),
                                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(3),color: const Color(0xFF009688), ),
                                      child: const Center(child: Text("Add Partner",style: TextStyle(color: Colors.white),))),
                                )

                            ),
                            // ElevatedButton(
                            //   onPressed: () async {
                            //     if (formKey.currentState!.validate()) {
                            //       await _.addPartner();
                            //       if(_.msg == 'Partner Registered Successfully'){
                            //         Navigator.pop(context);
                            //         _.msg ='';
                            //         _.nameController.text = '';
                            //         _.emailController.text = '';
                            //         _.mobileController.text ='';
                            //         _.fetchPartner();
                            //       }
                            //     }
                            //   },
                            //   child: Text(
                            //     'Add Partner',
                            //     style: TextStyle(fontSize: 14),
                            //   ),
                            //   style: ElevatedButton.styleFrom(
                            //     backgroundColor: Colors.green.shade300,
                            //     shape: RoundedRectangleBorder(
                            //       borderRadius: BorderRadius.circular(10),
                            //     ),
                            //   ),
                            // ),
                          ],
                        );
                      },
                    );
                  },
                  icon: const Icon(Icons.add),
                ),
                IconButton(onPressed: (){
                setState(() {
                  if(_.searchBarBool){
                    _.reloadList();
                    _.update();
                  }
              _.searchBarBool = !_.searchBarBool;
                });
            }, icon: _.searchBarBool?const Icon(Icons.close):const Icon(Icons.search))
            ],),
            body:
              Stack(
                children: [
                  Container(
                    height: double.infinity,
                    width: double.infinity,
                    color: AppUtils.bgColor
                  ),
                  Column(children: [
                    _.searchBarBool?Padding(
                      padding: const EdgeInsets.only(
                          left: 13, right: 13, top: 8),
                      child: Container(
                        height:
                        MediaQuery.of(context).size.height * 0.04,
                        decoration: BoxDecoration(
                            color:
                            Colors.blue.shade100.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(width: 0.3)),
                        child: Center(
                          child: Row(
                            children: [
                              Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 14, left: 15, right: 15),
                                    child: TextFormField(
                                      autofocus: true,
                                      decoration: const InputDecoration(
                                        hintText: 'Search',
                                        border: InputBorder.none,
                                      ),
                                      onChanged: (value) {
                                        _.searchData(value);
                                      },
                                    ),
                                  )),
                              const Icon(
                                Icons.search_outlined,
                                size: 18,
                                color: Colors.blue,
                              ),
                              const SizedBox(
                                width: 10,
                              )
                            ],
                          ),
                        ),
                      ),
                    ):const SizedBox(),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child:
                        _.showIndigator?   const Center(
                            child: Center(
                                child: CircularProgressIndicator(color:Colors.blue)
                            )
                        ):
                        _.myPartner.isEmpty?
                        Center(child: SvgPicture.asset('res/images/ic_empty.svg',))

                            :ListView.builder(itemCount: _.myPartner.length,itemBuilder: (context, index){
                          var use = _.myPartner[_.myPartner.length-1-index];
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 2),
                            child: InkWell(
                              onTap:(){
                                setState((){
                                  _.searchBarBool = false;
                                });
                              },
                              child:Stack(
                                children: [
                                  Container(
                                    width: double.infinity, // Using double.infinity for full width
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(5),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          blurRadius: 5,
                                          spreadRadius: 0,
                                          offset: const Offset(0, 2),
                                        ),
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0), // Adding general padding for consistency
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Flexible(
                                                child: Text(
                                                  '${use.name} - ',
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 15,
                                                  ),
                                                  overflow: TextOverflow.ellipsis, // Ensures text wraps or is truncated
                                                ),
                                              ),
                                              Text(
                                                use.empId,
                                                style: const TextStyle(
                                                  color: Color(0xFF0067FF),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(height: 10),
                                          use.email.isNotEmpty
                                              ? InkWell(
                                            onTap: () async {
                                              final emailUri = Uri(
                                                scheme: 'mailto',
                                                path: use.email,
                                              );

                                              if (await launchUrl(emailUri, mode: LaunchMode.externalApplication)) {
                                                // Email client opened successfully
                                              } else {
                                                ScaffoldMessenger.of(context).showSnackBar(
                                                  const SnackBar(content: Text('Could not launch email client')),
                                                );
                                              }
                                            },
                                            child: Row(
                                              children: [
                                                Image.asset('res/images/contactUs/email.png'),
                                                const SizedBox(width: 8),
                                                Flexible(
                                                  child: Text(
                                                    use.email,
                                                    style: const TextStyle(fontSize: 12),
                                                    overflow: TextOverflow.ellipsis,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                              : const SizedBox(),
                                          if (use.email.isNotEmpty) Divider(thickness: 1, color: Colors.blue.shade100),
                                          use.mobile.isNotEmpty
                                              ? Row(
                                            children: [
                                              Image.asset('res/images/contactUs/phone-call.png'),
                                              const SizedBox(width: 8),
                                              InkWell(
                                                onTap: () async {
                                                  final phoneNo = Uri.parse('tel:+91${use.mobile}');
                                                  if (await launchUrl(phoneNo)) {
                                                    // Call initiated
                                                  } else {
                                                    ScaffoldMessenger.of(context).showSnackBar(
                                                      const SnackBar(content: Text('Could not launch Dialer pad')),
                                                    );
                                                  }
                                                },
                                                child: Text(
                                                  '+91 ${use.mobile}',
                                                  style: const TextStyle(fontSize: 12),
                                                ),
                                              ),
                                            ],
                                          )
                                              : const SizedBox(),
                                          if (use.mobile.isNotEmpty) Divider(thickness: 1, color: Colors.blue.shade100),
                                          Row(
                                            children: [
                                              Image.asset('res/images/contactUs/calendar.png'),
                                              const SizedBox(width: 8),
                                              Text(
                                                DateFormat('dd-MM-yyyy').format(
                                                  DateFormat('dd MMM yyyy hh:mm:ss a').parse(
                                                    _.myPartner[index].date,
                                                  ),
                                                ),
                                                style: const TextStyle(fontSize: 12),
                                              ),
                                              const SizedBox(width: 4),
                                              const Text('(Last Modified Date)', style: TextStyle(fontSize: 10)),
                                            ],
                                          ),
                                          const SizedBox(height: 8),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                            ),
                          );
                        }),
                      ),
                    )
                  ],),
                ],
              )
          );
        });
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pi/activity/POS%20Panel/PosListController/PosDetailController.dart';

class PosDetailUIController extends StatelessWidget {
  const PosDetailUIController({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PosDetailController>(
      init: PosDetailController(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Partner Detail"),
          ),
          body: _.isLoading
              ? const Center(child: CircularProgressIndicator())
              : _.productsData.data == null || _.productsData.data!.isEmpty
              ? const Center(child: Text("Oops No data found"))
              : ListView.builder(
            itemCount: _.productsData.data!.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(8),
                child: Card(
                  elevation: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Column(
                      crossAxisAlignment:
                      CrossAxisAlignment.start,
                      children: [

                        Text(
                          _.productsData.data![index].name!.toUpperCase(),
                          style: TextStyle(
                              color: Colors.indigo[600], fontWeight: FontWeight.w500),
                        ),
                        const Divider(),
                        Row(
                          children: [
                            const Icon(
                              Icons.email_outlined,
                              size: 20,
                              color: Colors.lightBlue,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(_.productsData.data![index]
                                .email!),
                          ],
                        ),
                        const Divider(),
                        Row(
                          children: [
                            const Icon(
                              Icons.phone_android_outlined,
                              size: 20,
                              color: Colors.lightBlue,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(_.productsData.data![index]
                                .mobile!),
                          ],
                        ),
                        const Divider(),
                        Row(
                          children: [
                            const Icon(
                              Icons.confirmation_num_outlined,
                              size: 20,
                              color: Colors.lightBlue,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(_.productsData.data![index]
                                .type!),
                          ],
                        ),
                        const Divider(),
                        Row(
                          children: [
                            const Icon(
                              Icons.location_on_outlined,
                              size: 20,
                              color: Colors.lightBlue,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Text(_.productsData.data![index]
                                  .address!),
                            ),
                          ],
                        ),
                        const Divider(),
                        Row(
                          children: [
                            const Icon(
                              Icons.calendar_month,
                              size: 20,
                              color: Colors.lightBlue,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(_.productsData.data![index]
                                .insertDate!),
                          ],
                        ),
                        const Divider(),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:pi/Model/Pos/AddPartnerModel.dart';
import '../../../utils/AppUtils.dart';

class PosFormPersonalController extends GetxController
{
// TextEditingController userIdController = TextEditingController();
TextEditingController nameController = TextEditingController();
TextEditingController emailController = TextEditingController();
TextEditingController mobileController = TextEditingController();
var status;
// var msg;

Future<void> addPartner() async {
  if(nameController.text.isEmpty|| nameController.text.length<2){
    AppUtils.showSnackBar('Error', 'Please Insert Name', false);
    return;
  }
  if(emailController.text.isEmpty){
    AppUtils.showSnackBar('Error', 'Please Insert Email', false);
    return;
  }
  if(mobileController.text.isEmpty || mobileController.text.length<10){
    AppUtils.showSnackBar('Error', 'Please Insert Valid Phone Number', false);
    return;
  }
  try{
    Map<String, dynamic> mData ={
      'userid': AppUtils.userId,
      'name'  : nameController.text,
      'email' : emailController.text,
      'mobile': mobileController.text
    };
    var url = Uri.parse(AppUtils.baseUrl + AppUtils.addPartner1);
    var response = await http.post(url,body: mData);
    if(response.statusCode == 200){
      Map<String, dynamic> responseJson = jsonDecode(response.body);
      AddPartnerModel myResponse = AddPartnerModel.fromJson(responseJson);
      status = myResponse.status;
      // msg = myResponse.msg;
      myResponse.msg.isEmpty?AppUtils.showSnackBar('Error','Fill Required Details' , false):AppUtils.showSnackBar('Notification',myResponse.msg , true);
    }
  }catch(e){print(e);}
}}
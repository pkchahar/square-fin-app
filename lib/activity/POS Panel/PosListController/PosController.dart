import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../Model/Pos/PosList.dart';
import '../../../network/ApiClient.dart';
import '../../../utils/AppUtils.dart';
import 'PosDetailUIController.dart';

class PosController extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;
  PosList productsData = PosList();
  late String userId, userType, token;
  String version = "";

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void _getAgentList(String userId, String userType) async {
    setLoading(true);
    try {
      Map<String, String> mData = {
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.agentList);
      print(url);
      print(mData);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        productsData = PosList.fromJson(jsonDecode(response.body));
        update();
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  void onclick(int index) {
    productsData.data![index];
    Get.to(const PosDetailUIController(), arguments: productsData.data![index]);
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    _getAgentList(userId, userType);
  }
}

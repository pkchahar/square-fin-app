import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/network/ApiClient.dart';
import '../../../Model/Bank.dart';
import '../../../Model/BankList.dart';
import '../../../utils/AppUtils.dart';
import 'PosFormDocUI.dart';

class PosFormBank extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;
  String version = '', userId = '', userType = '', token = '';
  late String selectedBank = 'SBI';
  late String selectAccType = 'Saving';
  List<BankList> bankList = [];

  late String branch, accountNo, ifscCode;

  String selectedState = '';
  String selectAccDistrict = '';
  String selectAccCity = '';
  String selectedPin = "";

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";

    Map<String, String> lData = Get.find(tag: "mData");

    print("lData");
    print(lData);
    print("lData");

    getBankList();
  }

  void onNext() {
    Map<String, String> lData = Get.find(tag: "mData");

    lData.addEntries({
      'Account_No': accountNo,
      'IFSC_Code': ifscCode,
      'Bank_Name': selectedBank,
      'Branch_Address': branch,
    }.entries);

    Get.put(lData, tag: "mData");

    Map<String, String> hdata = Get.find(tag: "mData");
    print(hdata);
    Get.to(() => const PosFormDocUI());
  }

  void getBankList() async {
    try {
      Map<String, String> mData = {
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.bankList);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        Bank bank = Bank.fromJson(jsonDecode(response.body));
        bankList.addAll(bank.bankList as Iterable<BankList>);
        print(bankList);
        update();
      }
    } catch (e) {
      print(e);
    }
  }
}

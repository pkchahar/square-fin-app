import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';


import 'PosFormDoc.dart';

class PosFormDocUI extends StatefulWidget {
  const PosFormDocUI({super.key});

  @override
  _PosFormDocUI createState() => _PosFormDocUI();
}

class _PosFormDocUI extends State<PosFormDocUI> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController branchController = TextEditingController();
  TextEditingController accountController = TextEditingController();
  TextEditingController ifscController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PosFormDoc>(
      init: PosFormDoc(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Upload documents"),
          ),
          body: Container(
            color:Colors.grey.shade100,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Form(
                    child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(

                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          const Padding(
                            padding: EdgeInsets.only(left: 8, bottom: 5),
                            child: Text(
                              "PAN Card",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration( color:Colors.white,
                              
                              border: Border.all(
                                color: Colors.black,
                                style: BorderStyle.solid,
                                width: 0.3,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: (_.pan.isNotEmpty)
                                        ? Text(
                                            _.pan,
                                            style: const TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.bold),
                                          )
                                        : const Text(
                                            'No File selected',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                  ),
                                  const SizedBox(
                                    width: 7,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextButton(
                                      onPressed: () async {
                                        showDialog(context: context, builder: (BuildContext context){
                                          return AlertDialog(title:const Text('Choose an action :'),
                                            actions: [

                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Column(children:[ IconButton(
                                                      onPressed: () async {
                                                        XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);
                                                        if (image == null) {
                                                          Get.back();
                                                          return;
                                                        }
                                                        File file = File(image.path);
                                                        PlatformFile platformFile = PlatformFile(
                                                          name: image.name,
                                                          path: image.path,
                                                          size: file.lengthSync(),
                                                          bytes: await file.readAsBytes(),
                                                        );
                                                        Get.back();
                                                        _.uploadFile(platformFile, "PanCard");
                                                        _.pan =  path.basename(file.path);
                                                        print(file.path);
                                                        setState(() {});
                                                      },
                                                      icon: const Icon(Icons.camera)),const Text('Camera')]),
                                                  const SizedBox(width: 30,),
                                                  Column(children:[ IconButton( onPressed: () async {
                                                    FilePickerResult? result = await FilePicker.platform.pickFiles();
                                                    if (result == null){Get.back();
                                                    return;
                                                    }
                                                    Get.back();
                                                    PlatformFile file = result.files.first;
                                                    _.uploadFile(file, "PanCard");
                                                    _.pan = file.name.toString();

                                                    print(file.name);
                                                    setState(() {});
                                                  }, icon: const Icon(Icons.photo_sharp)),const Text('Files')])
                                                ],
                                              ),

                                            ],
                                          );

                                        });
                                        // FilePickerResult? result =
                                        //     await FilePicker.platform.pickFiles();
                                        // if (result == null)
                                        //   return; // if user don't pick any thing then do nothing just return.
                                        // PlatformFile file = result.files.first;
                                        // _.uploadFile(file, "PanCard");
                                        // _.pan = file.name.toString();
                                        //
                                        // print(file.name);
                                        // setState(() {});
                                      },
                                      style: ButtonStyle(
                                        backgroundColor:
                                         WidgetStateProperty.all<Color>(Colors.grey.shade100
                                        ),
                                        // Set the background color to blue
                                         
                                        shape:
                                        WidgetStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                15.0), // Set the border radius to 10.0 for rounded corners
                                          ),
                                        ), // Set the button width to full and height to 50
                                      ),
                                      child: Icon(Icons.cloud_upload_outlined,size:25,color: Colors.blue.shade700,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 8, bottom: 5),
                            child: Text(
                              "Aadhar Card Front",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration( color:Colors.white,
                              border: Border.all(
                                color: Colors.black,
                                style: BorderStyle.solid,
                                width: 0.3,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: (_.aadharFront.isNotEmpty)
                                        ? Text(
                                            _.aadharFront,
                                            style: const TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.bold),
                                          )
                                        : const Text(
                                            'No File selected',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                  ),
                                  const SizedBox(
                                    width: 7,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextButton(
                                      onPressed: () async {
                                        showDialog(context: context, builder: (BuildContext context){
                                          return AlertDialog(title:const Text('Choose an action :'),
                                            // content:
                                            // Row(
                                            //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            //   children: [
                                            //     Column(children:[ IconButton(onPressed: (){}, icon: Icon(Icons.camera)),Text('Camera')]),
                                            //     Column(children:[ IconButton(onPressed: (){}, icon: Icon(Icons.photo_sharp)),Text('Files')])
                                            //   ],
                                            actions: [

                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Column(children:[ IconButton(
                                                      onPressed: () async {
                                                        XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);
                                                        if (image == null) {
                                                          Get.back();
                                                          return;
                                                        }
                                                        File file = File(image.path);
                                                        PlatformFile platformFile = PlatformFile(
                                                          name: image.name,
                                                          path: image.path,
                                                          size: file.lengthSync(),
                                                          bytes: await file.readAsBytes(),
                                                        );
                                                        Get.back();
                                                        _.uploadFile(platformFile, "AadharCardFront");
                                                        _.aadharFront =path.basename(file.path);

                                                        print(file.path);
                                                        setState(() {});
                                                      },
                                                      icon: const Icon(Icons.camera)),const Text('Camera')]),
                                                  const SizedBox(width: 30,),
                                                  Column(children:[ IconButton( onPressed: () async {
                                                    FilePickerResult? result = await FilePicker.platform.pickFiles();
                                                    if (result == null){Get.back();
                                                      return;
                                                    }
                                                    Get.back();
                                                    PlatformFile file = result.files.first;
                                                    _.uploadFile(file, "AadharCardFront");
                                                    _.aadharFront = file.name.toString();

                                                    print(file.name);
                                                    setState(() {});
                                          }, icon: const Icon(Icons.photo_sharp)),const Text('Files')])
                                                ],
                                              ),
                                            ],
                                          );

                                        });
                                      },
                                      style: ButtonStyle(
                                        backgroundColor:
                                         WidgetStateProperty.all<Color>(Colors.grey.shade100
                                        ),
                                        // Set the background color to blue
                                         
                                        shape:
                                        WidgetStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                15.0), // Set the border radius to 10.0 for rounded corners
                                          ),
                                        ), // Set the button width to full and height to 50
                                      ),
                                      child: Icon(Icons.cloud_upload_outlined,size:25,color: Colors.blue.shade700,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 8, bottom: 5),
                            child: Text(
                              "Aadhar Card Back",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration( color:Colors.white,
                              border: Border.all(
                                color: Colors.black,
                                style: BorderStyle.solid,
                                width: 0.3,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: (_.aadharBack.isNotEmpty)
                                        ? Text(
                                            _.aadharBack,
                                            style: const TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.bold),
                                          )
                                        : const Text(
                                            'No File selected',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                  ),
                                  const SizedBox(
                                    width: 7,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextButton(
                                      onPressed: () async {
                                        showDialog(context: context, builder: (BuildContext context){
                                          return AlertDialog(title:const Text('Choose an action :'),
                                            actions: [

                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Column(children:[ IconButton(
                                                      onPressed: () async {
                                                        XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);
                                                        if (image == null) {
                                                          Get.back();
                                                          return;
                                                        }
                                                        File file = File(image.path);
                                                        PlatformFile platformFile = PlatformFile(
                                                          name: image.name,
                                                          path: image.path,
                                                          size: file.lengthSync(),
                                                          bytes: await file.readAsBytes(),
                                                        );
                                                        // _.uploadFile(file, "AadharCardBack");
                                                        // _.aadharBack = file.name.toString();
                                                        Get.back();
                                                        _.uploadFile(platformFile, "AadharCardBack");
                                                        _.aadharBack =path.basename(file.path);

                                                        print(file.path);
                                                        setState(() {});
                                                      },
                                                      icon: const Icon(Icons.camera)),const Text('Camera')]),
                                                  const SizedBox(width: 30,),
                                                  Column(children:[ IconButton( onPressed: () async {
                                                    FilePickerResult? result = await FilePicker.platform.pickFiles();
                                                    if (result == null){Get.back();
                                                    return;
                                                    }
                                                    Get.back();
                                                    PlatformFile file = result.files.first;
                                                    _.uploadFile(file, "AadharCardBack");
                                                    _.aadharBack = file.name.toString();

                                                    print(file.name);
                                                    setState(() {});
                                                  }, icon: const Icon(Icons.photo_sharp)),const Text('Files')])
                                                ],
                                              ),

                                            ],
                                          );

                                        });
                                        // FilePickerResult? result =
                                        //     await FilePicker.platform.pickFiles();
                                        // if (result == null)
                                        //   return; // if user don't pick any thing then do nothing just return.
                                        // PlatformFile file = result.files.first;
                                        // _.uploadFile(file, "AadharCardBack");
                                        // _.aadharBack = file.name.toString();
                                        // print(file.name);
                                        // setState(() {});
                                      },
                                      style: ButtonStyle(
                                        backgroundColor:
                                         WidgetStateProperty.all<Color>(Colors.grey.shade100
                                        ),
                                        // Set the background color to blue
                                         
                                        shape:
                                        WidgetStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                15.0), // Set the border radius to 10.0 for rounded corners
                                          ),
                                        ), // Set the button width to full and height to 50
                                      ),
                                      child: Icon(Icons.cloud_upload_outlined,size:25,color: Colors.blue.shade700,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 8, bottom: 5),
                            child: Text(
                              "Education Qualification",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration( color:Colors.white,
                              border: Border.all(
                                color: Colors.black,
                                style: BorderStyle.solid,
                                width: 0.3,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: (_.qualification.isNotEmpty)
                                        ? Text(
                                            _.qualification,
                                            style: const TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.bold),
                                          )
                                        : const Text(
                                            'No File selected',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                  ),
                                  const SizedBox(
                                    width: 7,
                                  ),
                                  Container(
                                    child: Expanded(
                                      flex: 1,
                                      child: TextButton(
                                        onPressed: () async {

                                          showDialog(context: context, builder: (BuildContext context){
                                            return AlertDialog(title:const Text('Choose an action :'),
                                              actions: [

                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                  children: [
                                                    Column(children:[ IconButton(
                                                        onPressed: () async {
                                                          XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);
                                                          if (image == null) {
                                                            Get.back();
                                                            return;
                                                          }
                                                          File file = File(image.path);
                                                          PlatformFile platformFile = PlatformFile(
                                                            name: image.name,
                                                            path: image.path,
                                                            size: file.lengthSync(),
                                                            bytes: await file.readAsBytes(),
                                                          );
                                                          Get.back();
                                                          // _.uploadFile(file, "Qualification");
                                                          // _.qualification = file.name.toString();
                                                          _.uploadFile(platformFile, "Qualification");
                                                          _.qualification =path.basename(file.path);

                                                          print(file.path);
                                                          setState(() {});
                                                        },
                                                        icon: const Icon(Icons.camera)),const Text('Camera')]),
                                                    const SizedBox(width: 30,),
                                                    Column(children:[ IconButton( onPressed: () async {
                                                      FilePickerResult? result = await FilePicker.platform.pickFiles();
                                                      if (result == null){Get.back();
                                                      return;
                                                      }
                                                      Get.back();

                                                      PlatformFile file = result.files.first;
                                                      _.uploadFile(file, "Qualification");
                                                      _.qualification = file.name.toString();
                                                      print(file.name);
                                                      setState(() {});
                                                    }, icon: const Icon(Icons.photo_sharp)),const Text('Files')])
                                                  ],
                                                ),

                                              ],
                                            );

                                          });

                                          // FilePickerResult? result =
                                          //     await FilePicker.platform.pickFiles();
                                          // if (result == null)
                                          //   return; // if user don't pick any thing then do nothing just return.
                                          // PlatformFile file = result.files.first;
                                          // _.uploadFile(file, "Qualification");
                                          // _.qualification = file.name.toString();
                                          // print(file.name);
                                          // setState(() {});
                                        },
                                        style: ButtonStyle(
                                          backgroundColor:
                                           WidgetStateProperty.all<Color>(Colors.grey.shade100
                                          ),
                                          shape:
                                          WidgetStateProperty.all<RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(
                                                  15.0), // Set the border radius to 10.0 for rounded corners
                                            ),
                                          ), // Set the button width to full and height to 50
                                        ),
                                        child: Icon(Icons.cloud_upload_outlined,size:25,color: Colors.blue.shade700,),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 8, bottom: 5),
                            child: Text(
                              "Cancelled Cheque",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration( color:Colors.white,
                              border: Border.all(
                                color: Colors.black,
                                style: BorderStyle.solid,
                                width: 0.3,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child:
                                        (_.cheque.isNotEmpty)
                                            ? Text(
                                                _.cheque,
                                                style: const TextStyle(
                                                    color: Colors.green,
                                                    fontWeight: FontWeight.bold),
                                              )
                                            : const Text(
                                                'No File selected',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold),
                                              ),
                                  ),
                                  const SizedBox(
                                    width: 7,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextButton(
                                      onPressed: () async {
                                        showDialog(context: context, builder: (BuildContext context){
                                          return AlertDialog(title:const Text('Choose an action :'),
                                            actions: [

                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Column(children:[ IconButton(
                                                      onPressed: () async {
                                                        XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);
                                                        if (image == null) {
                                                          Get.back();
                                                          return;
                                                        }
                                                        File file = File(image.path);
                                                        PlatformFile platformFile = PlatformFile(
                                                          name: image.name,
                                                          path: image.path,
                                                          size: file.lengthSync(),
                                                          bytes: await file.readAsBytes(),
                                                        );
                                                        Get.back();
                                                        // _.uploadFile(file, "Cheque");
                                                        // _.cheque = file.name.toString();
                                                        _.uploadFile(platformFile, "Cheque");
                                                        _.cheque =path.basename(file.path);

                                                        print(file.path);
                                                        setState(() {});
                                                      },
                                                      icon: const Icon(Icons.camera)),const Text('Camera')]),
                                                  const SizedBox(width: 30,),
                                                  Column(children:[ IconButton( onPressed: () async {
                                                    FilePickerResult? result = await FilePicker.platform.pickFiles();
                                                    if (result == null){Get.back();
                                                    return;
                                                    }
                                                    Get.back();
                                                    PlatformFile file = result.files.first;
                                                    _.uploadFile(file, "Cheque");
                                                    _.cheque = file.name.toString();

                                                    print(file.name);
                                                    setState(() {});
                                                  }, icon: const Icon(Icons.photo_sharp)),const Text('Files')])
                                                ],
                                              ),

                                            ],
                                          );

                                        });
                                        // FilePickerResult? result =
                                        //     await FilePicker.platform.pickFiles();
                                        // if (result == null)
                                        //   return; // if user don't pick any thing then do nothing just return.
                                        // PlatformFile file = result.files.first;
                                        // _.uploadFile(file, "Cheque");
                                        // _.cheque = file.name.toString();
                                        // print(file.name);
                                        // setState(() {});
                                      },
                                       style: ButtonStyle(
                                        backgroundColor:
                                         WidgetStateProperty.all<Color>(Colors.grey.shade100
                                        ),
                                        // Set the background color to blue

                                        shape:
                                        WidgetStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                15.0), // Set the border radius to 10.0 for rounded corners
                                          ),
                                        ), // Set the button width to full and height to 50
                                      ),
                                      child: Icon(Icons.cloud_upload_outlined,size:25,color: Colors.blue.shade700,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 8, bottom: 5),
                            child: Text(
                              "Profile Photo",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration( color:Colors.white,
                              border: Border.all(
                                color: Colors.black,
                                style: BorderStyle.solid,
                                width: 0.3,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: (_.profile.isNotEmpty)
                                        ? Text(
                                            _.profile,
                                            style: const TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.bold),
                                          )
                                        : const Text(
                                            'No File selected',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                  ),
                                  const SizedBox(
                                    width: 7,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextButton(

                                      onPressed: () async {
                                        showDialog(context: context, builder: (BuildContext context){
                                          return AlertDialog(title:const Text('Choose an action :'),
                                            actions: [

                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Column(children:[ IconButton(
                                                      onPressed: () async {
                                                        XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);
                                                        if (image == null) {
                                                          Get.back();
                                                          return;
                                                        }
                                                        File file = File(image.path);
                                                        PlatformFile platformFile = PlatformFile(
                                                          name: image.name,
                                                          path: image.path,
                                                          size: file.lengthSync(),
                                                          bytes: await file.readAsBytes(),
                                                        );
                                                        Get.back();
                                                        // _.uploadFile(file, "Photo");
                                                        // _.profile = file.name.toString();
                                                        _.uploadFile(platformFile, "Photo");
                                                        _.profile =path.basename(file.path);

                                                        print(file.path);
                                                        setState(() {});
                                                      },
                                                      icon: const Icon(Icons.camera)),const Text('Camera')]),
                                                  const SizedBox(width: 30,),
                                                  Column(children:[ IconButton( onPressed: () async {
                                                    FilePickerResult? result = await FilePicker.platform.pickFiles();
                                                    if (result == null){Get.back();
                                                    return;
                                                    }
                                                    Get.back();
                                                    PlatformFile file = result.files.first;
                                                    _.uploadFile(file, "Photo");
                                                    _.profile = file.name.toString();

                                                    print(file.name);
                                                    setState(() {});
                                                  }, icon: const Icon(Icons.photo_sharp)),const Text('Files')])
                                                ],
                                              ),

                                            ],
                                          );

                                        });
                                        // FilePickerResult? result =
                                        //     await FilePicker.platform.pickFiles();
                                        // if (result == null)
                                        //   return; // if user don't pick any thing then do nothing just return.
                                        // PlatformFile file = result.files.first;
                                        // _.uploadFile(file, "Photo");
                                        // _.profile = file.name.toString();
                                        // print(file.name);
                                        // setState(() {});
                                      },
                                       style: ButtonStyle(
                                        backgroundColor:
                                         WidgetStateProperty.all<Color>(Colors.grey.shade100
                                        ),
                                        // Set the background color to blue
                                         
                                        shape:
                                        WidgetStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                15.0), // Set the border radius to 10.0 for rounded corners
                                          ),
                                        ), // Set the button width to full and height to 50
                                      ),
                                      child: Icon(Icons.cloud_upload_outlined,size:25,color: Colors.blue.shade700,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 8, bottom: 5),
                            child: Text(
                              "Signature",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration( color:Colors.white,
                              border: Border.all(
                                color: Colors.black,
                                style: BorderStyle.solid,
                                width: 0.3,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: (_.signature.isNotEmpty)
                                        ? Text(
                                            _.signature,
                                            style: const TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.bold),
                                          )
                                        : const Text(
                                            'No File selected',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                  ),
                                  const SizedBox(
                                    width: 7,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child:  TextButton(
                                      onPressed: () async {
                                        showDialog(context: context, builder: (BuildContext context){
                                          return AlertDialog(title:const Text('Choose an action :'),
                                            actions: [

                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Column(children:[ IconButton(
                                                      onPressed: () async {
                                                        XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);
                                                        if (image == null) {
                                                          Get.back();
                                                          return;
                                                        }
                                                        File file = File(image.path);
                                                        PlatformFile platformFile = PlatformFile(
                                                          name: image.name,
                                                          path: image.path,
                                                          size: file.lengthSync(),
                                                          bytes: await file.readAsBytes(),
                                                        );
                                                        Get.back();
                                                        // _.uploadFile(file, "Signature");
                                                        // _.signature = file.name.toString();
                                                        _.uploadFile(platformFile, "Signature");
                                                        _.signature =path.basename(file.path);

                                                        print(file.path);
                                                        setState(() {});
                                                      },
                                                      icon: const Icon(Icons.camera)),const Text('Camera')]),
                                                  const SizedBox(width: 30,),
                                                  Column(children:[ IconButton( onPressed: () async {
                                                    FilePickerResult? result = await FilePicker.platform.pickFiles();
                                                    if (result == null){Get.back();
                                                    return;
                                                    }
                                                    Get.back();
                                                    PlatformFile file = result.files.first;
                                                    _.uploadFile(file, "Signature");
                                                    _.signature = file.name.toString();

                                                    print(file.name);
                                                    setState(() {});
                                                  }, icon: const Icon(Icons.photo_sharp)),const Text('Files')])
                                                ],
                                              ),

                                            ],
                                          );

                                        });

                                        // ,,,,,,,,,,,,,,,,,,,

                                        // FilePickerResult? result =
                                        //     await FilePicker.platform.pickFiles();
                                        // if (result == null)
                                        //   return; // if user don't pick any thing then do nothing just return.
                                        // PlatformFile file = result.files.first;
                                        // _.uploadFile(file, "Signature");
                                        // _.signature = file.name.toString();
                                        // print(file.name);
                                        // setState(() {});
                                      },
                                       style: ButtonStyle(
                                        backgroundColor:
                                         WidgetStateProperty.all<Color>(Colors.grey.shade100
                                        ),
                                        // Set the background color to blue
                                         
                                        shape:
                                        WidgetStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                15.0), // Set the border radius to 10.0 for rounded corners
                                          ),
                                        ), // Set the button width to full and height to 50
                                      ),
                                      child: Icon(Icons.cloud_upload_outlined,size:25,color: Colors.blue.shade700,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 8, bottom: 5),
                            child: Text(
                              "Other",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration( color:Colors.white,
                              border: Border.all(
                                color: Colors.black,
                                style: BorderStyle.solid,
                                width: 0.3,
                              ),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: (_.other.isNotEmpty)
                                        ? Text(
                                            _.other,
                                            style: const TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.bold),
                                          )
                                        : const Text(
                                            'No File selected',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                  ),
                                  const SizedBox(
                                    width: 7,
                                  ),
                                  // SizedBox(width: 200,),
                                  Expanded(
                                    flex: 1,
                                    child: TextButton(
                                      onPressed: () async {
                                        showDialog(context: context, builder: (BuildContext context){
                                          return AlertDialog(title:const Text('Choose an action :'),
                                            actions: [

                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Column(children:[ IconButton(
                                                      onPressed: () async {
                                                        XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);
                                                        if (image == null) {
                                                          Get.back();
                                                          return;
                                                        }
                                                        File file = File(image.path);
                                                        PlatformFile platformFile = PlatformFile(
                                                          name: image.name,
                                                          path: image.path,
                                                          size: file.lengthSync(),
                                                          bytes: await file.readAsBytes(),
                                                        );
                                                        Get.back();
                                                        // _.uploadFile(file, "Other");
                                                        // _.other = file.name.toString();
                                                        _.uploadFile(platformFile, "Other");
                                                        _.other =path.basename(file.path);

                                                        print(file.path);
                                                        setState(() {});
                                                      },
                                                      icon: const Icon(Icons.camera)),const Text('Camera')]),
                                                  const SizedBox(width: 30,),
                                                  Column(children:[ IconButton( onPressed: () async {
                                                    FilePickerResult? result = await FilePicker.platform.pickFiles();
                                                    if (result == null){Get.back();
                                                    return;
                                                    }
                                                    Get.back();
                                                    PlatformFile file = result.files.first;
                                                    _.uploadFile(file, "Other");
                                                    _.other = file.name.toString();

                                                    print(file.name);
                                                    setState(() {});
                                                  }, icon: const Icon(Icons.photo_sharp)),const Text('Files')])
                                                ],
                                              ),
                                            ],
                                          );

                                        });

                                        // FilePickerResult? result =
                                        //     await FilePicker.platform.pickFiles();
                                        // if (result == null)
                                        //   return; // if user don't pick any thing then do nothing just return.
                                        // PlatformFile file = result.files.first;
                                        // _.uploadFile(file, "Other");
                                        // _.other = file.name.toString();
                                        // print(file.name);
                                        // setState(() {});
                                      },
                                       style: ButtonStyle(
                                        backgroundColor:
                                         WidgetStateProperty.all<Color>(Colors.grey.shade100
                                        ),
                                        // Set the background color to blue
                                        maximumSize: WidgetStateProperty.all<Size>(
                                            const Size(double.infinity, 50)),
                                        shape:
                                        WidgetStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                15.0), // Set the border radius to 10.0 for rounded corners
                                          ),
                                        ), // Set the button width to full and height to 50
                                      ),
                                      child: Icon(Icons.cloud_upload_outlined,size:25,color: Colors.blue.shade700,),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          (_.pan.isNotEmpty &&
                                  _.aadharFront.isNotEmpty &&
                                  _.aadharBack.isNotEmpty &&
                                  _.qualification.isNotEmpty &&
                                  _.cheque.isNotEmpty &&
                                  _.profile.isNotEmpty &&
                                  _.signature.isNotEmpty)
                              ? TextButton(
                                  onPressed: () {
                                    _.onNext();
                                  },
                                  style: TextButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25),
                                    ),
                                    elevation: 10.0,
                                    backgroundColor: Colors.green
                                  ),
                                  child: const Text(
                                    'Submit',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                  ),
                                )
                              : const SizedBox(
                                  height: 0,
                                ),
                        ],
                      ),
                    ),
                  ],
                )),
              ),
            ),
          ),
        );
      },
    );
  }

}

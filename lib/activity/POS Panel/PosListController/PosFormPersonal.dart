import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/activity/POS%20Panel/PosListController/PosFormAddressUI.dart';
import 'package:pi/network/ApiClient.dart';
import '../../../Model/StateData.dart';
import '../../../Model/StateList.dart';
import '../../../utils/AppUtils.dart';

class PosFormPersonal extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;
  String selectedGender = 'Male';
  String selectedGraduation = '10th';
  String version = '',
      userId = '',
      userType = '',
      token = '',
      selectedState = '',
      selectedUser = '',
      selectedUserType = '',
      selectedDealer = '',
      category = 'Individual';
  String isGst = 'yes';
  List<StateData> franchiseeList = [];
  List<StateData> userList = [];
  List<StateData> userTypeList = [];
  List<StateData> dealerList = [];
  var panNo,
      aadhar,
      salutation = 'Mr',
      name,
      mobile,
      altMobile,
      email,
      dob,
      gender,
      fatherName,
      agentType = "sp",
      gstStatus,
      gstNo,
      insName,
      insEmail,
      insMobile,
      contactPerson,
      contactPersonMobile,
      contactPersonEmail;

  bool no_selected = false;
  bool yes_selected = true;


  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    getFranchisee();
    getUserList();
  }

  void onNext() {
    Map<String, String> mData = {
      'Login_User_Id': userId,
      'Login_User_Type': userType,
      'RM_Id': userId,
      'Agent_Type': selectedUser,
      'partner_Category': selectedUserType,
      'partner_Sub_Catgory': selectedDealer,
      'Agent_Category': category,
      'Salutation_Type': salutation,
      'Agent_Name': name,
      'Father_Name': fatherName,
      'Gender': selectedGender,
      'DOB': dob,
      'Mobile': mobile,
      'Email': email,
      'ContactPerson_Name': contactPerson,
      'ContactPerson_Email': contactPersonEmail,
      'ContactPerson_Mobile': contactPersonMobile,
      'AadharCard_No': aadhar,
      'PanCard_No': panNo,
      'GST_Status': isGst,
      'GST_No': gstNo,
      'platForm': AppClient.platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };
    Get.put(mData, tag: "mData");
    Get.to(() => const PosFormAddressUI());
  }

  void getFranchisee() async {
    try {
      var url = Uri.parse('${AppUtils.baseUrl}${AppUtils.getFranchisee}?Partner_Type=SP&Login_User_Id=$userId');
      var response = await http.get(url);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        StateList bank = StateList.fromJson(jsonDecode(response.body));
        if (bank.data != null) {
          franchiseeList.addAll(bank.data as Iterable<StateData>);
          print(franchiseeList);
          update();
        }
      }
    } catch (e) {
      print(e);
    }
  }

  void getUserList() async {
    try {
      Map<String, String> mData = {
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.selectUsers);
      var response = await http.post(url, body: mData);
      print(url);
      print(mData);
      print(response.body);
      if (response.statusCode == 200) {
        StateList bank = StateList.fromJson(jsonDecode(response.body));
        if (bank.data != null) {
          userList.addAll(bank.data as Iterable<StateData>);
          print(userList);
          update();
        }
      }
    } catch (e) {
      print(e);
    }
  }

  void getUserType() async {
    try {
      Map<String, String> mData = {
        'Agent_Type': selectedUser,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.selectUserType);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        StateList bank = StateList.fromJson(jsonDecode(response.body));
        userTypeList.addAll(bank.data as Iterable<StateData>);
        print(userTypeList);
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void getDealer() async {
    try {
      Map<String, String> mData = {
        'Select_Type': selectedUserType,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.selectDealer);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        StateList bank = StateList.fromJson(jsonDecode(response.body));
        dealerList.addAll(bank.data as Iterable<StateData>);
        print(dealerList);
        update();
      }
    } catch (e) {
      print(e);
    }
  }
}


import 'package:custom_searchable_dropdown/custom_searchable_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


import 'PosFormAddress.dart';

class PosFormAddressUI extends StatefulWidget {
  const PosFormAddressUI({super.key});

  @override
  _PosFormAddressUI createState() => _PosFormAddressUI();
}

class _PosFormAddressUI extends State<PosFormAddressUI> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController address1Controller = TextEditingController();
  TextEditingController address2Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PosFormAddress>(
      init: PosFormAddress(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Address Detail"),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            const SizedBox(height: 16.0),
                            TextFormField(
                              controller: address1Controller,
                              decoration: const InputDecoration(
                                labelText: 'Address 1',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.lightBlue,
                                  ),
                                ),
                                labelStyle: TextStyle(color: Colors.black54),
                              ),
                              keyboardType: TextInputType.name,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Enter Address';
                                }
                                return null;
                              },
                            ),
                            const SizedBox(height: 16.0),
                            TextFormField(
                              controller: address2Controller,
                              decoration: const InputDecoration(
                                labelText: 'Address 2',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.lightBlue,
                                  ),
                                ),
                                labelStyle: TextStyle(color: Colors.black54),
                              ),
                              keyboardType: TextInputType.name,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Enter Address';
                                }
                                return null;
                              },
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            CustomSearchableDropDown(
                              items: _.stateList,
                              label: 'Select State',
                              decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.grey,
                                  ),
                                  borderRadius: BorderRadius.circular(5)),
                              dropDownMenuItems: _.stateList.map((item) {
                                return item.name ?? "";
                              }).toList(),
                              onChanged: (value) {
                                if (value != null) {
                                  setState(() {
                                    _.selectedState = value?.id ?? "";
                                    if (_.districtList.isNotEmpty) {
                                      _.districtList = [];
                                      _.cityList = [];
                                      _.pincodeList = [];
                                    }
                                    _.getDistrict();
                                  });
                                } else {}
                              },
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            _.districtList.isNotEmpty
                                ? CustomSearchableDropDown(
                                    items: _.districtList,
                                    label: 'Select District',
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Colors.grey,
                                        ),
                                        borderRadius: BorderRadius.circular(5)),
                                    dropDownMenuItems:
                                        _.districtList.map((item) {
                                      return item.name ?? "";
                                    }).toList(),
                                    onChanged: (value) {
                                      if (value != null) {
                                        setState(() {
                                          _.selectAccDistrict = value?.id ?? "";
                                          if (_.cityList.isNotEmpty) {
                                            _.cityList = [];
                                            _.pincodeList = [];
                                          }
                                          _.getCityList();
                                        });
                                      } else {}
                                    },
                                  )
                                : const SizedBox(
                                    height: 0,
                                  ),
                            const SizedBox(height: 16.0),
                            _.cityList.isNotEmpty
                                ? CustomSearchableDropDown(
                                    items: _.cityList,
                                    label: 'Select City',
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Colors.grey,
                                        ),
                                        borderRadius: BorderRadius.circular(5)),
                                    dropDownMenuItems: _.cityList.map((item) {
                                      return item.name ?? "";
                                    }).toList(),
                                    onChanged: (value) {
                                      if (value != null) {
                                        setState(() {
                                          _.selectAccCity = value?.id ?? "";
                                          if (_.pincodeList.isNotEmpty) {
                                            _.pincodeList = [];
                                          }
                                          _.getPincode();
                                        });
                                      } else {}
                                    },
                                  )
                                : const SizedBox(
                                    height: 0,
                                  ),
                            const SizedBox(height: 16.0),
                            _.pincodeList.isNotEmpty
                                ? CustomSearchableDropDown(
                                    items: _.pincodeList,
                                    label: 'Select Pincode',
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Colors.grey,
                                        ),
                                        borderRadius: BorderRadius.circular(5)),
                                    dropDownMenuItems:
                                        _.pincodeList.map((item) {
                                      return item.name ?? "";
                                    }).toList(),
                                    onChanged: (value) {
                                      if (value != null) {
                                        setState(() {
                                          _.selectedPin = value?.id ?? "";
                                        });
                                      } else {}
                                    },
                                  )
                                : const SizedBox(
                                    height: 0,
                                  ),
                            const SizedBox(height: 20.0),
                            ElevatedButton(
                              onPressed: () {
                                _.address1 = address1Controller.text ?? "";
                                _.address2 = address2Controller.text ?? "";

                                if (_formKey.currentState!.validate()) {
                                  _.onNext();
                                }
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    WidgetStateProperty.all<Color>(
                                        Colors.lightBlue),
                                // Set the background color to blue
                                minimumSize: WidgetStateProperty.all<Size>(
                                    const Size(double.infinity, 50)),
                                shape: WidgetStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        10.0), // Set the border radius to 10.0 for rounded corners
                                  ),
                                ), // Set the button width to full and height to 50
                              ),
                              child: const Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Text(
                                  'Next',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        );
      },
    );
  }
}

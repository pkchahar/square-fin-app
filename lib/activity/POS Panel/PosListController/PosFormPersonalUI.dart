
import 'package:custom_searchable_dropdown/custom_searchable_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pi/activity/POS%20Panel/PosListController/PosFormPersonal.dart';


class PosFormPersonalUI extends StatefulWidget {
  const PosFormPersonalUI({super.key});

  @override
  _PosFormPersonalUI createState() => _PosFormPersonalUI();
}

class _PosFormPersonalUI extends State<PosFormPersonalUI> {
  final _formKey = GlobalKey<FormState>();
  DateTime selectedDate = DateTime(
      DateTime.now().year - 18, DateTime.now().month, DateTime.now().day);

  TextEditingController nameController = TextEditingController();
  TextEditingController fatherController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController altMobileController = TextEditingController();
  TextEditingController aadharController = TextEditingController();
  TextEditingController panController = TextEditingController();
  TextEditingController gstController = TextEditingController();

  TextEditingController insNameController = TextEditingController();
  TextEditingController cpNameController = TextEditingController();
  TextEditingController cpMobileController = TextEditingController();
  TextEditingController insMobileController = TextEditingController();
  TextEditingController cpEmailController = TextEditingController();
  TextEditingController insEmailController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PosFormPersonal>(
      init: PosFormPersonal(),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Partner Personal Detail"),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            _.franchiseeList.isNotEmpty
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 16),
                                    child: CustomSearchableDropDown(
                                      items: _.franchiseeList,
                                      label: 'Select Franchisee',
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.grey,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      dropDownMenuItems:
                                          _.franchiseeList.map((item) {
                                        return item.name ?? "";
                                      }).toList(),
                                      onChanged: (value) {
                                        if (value != null) {
                                          setState(() {
                                            _.selectedState = value?.id ?? "";
                                          });
                                        } else {}
                                      },
                                    ),
                                  )
                                : const SizedBox(
                                    height: 0,
                                  ),
                            _.userList.isNotEmpty
                                ? CustomSearchableDropDown(
                                    items: _.userList,
                                    label: 'Select User',
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Colors.grey,
                                        ),
                                        borderRadius: BorderRadius.circular(5)),
                                    dropDownMenuItems: _.userList.map((item) {
                                      return item.name ?? "";
                                    }).toList(),
                                    onChanged: (value) {
                                      if (value != null) {
                                        setState(() {
                                          _.selectedUser = value?.name ?? "";
                                          if (_.userTypeList.isNotEmpty) {
                                            _.userTypeList = [];
                                            _.dealerList = [];
                                          }
                                          _.getUserType();
                                        });
                                      } else {}
                                    },
                                  )
                                : const SizedBox(
                                    height: 0,
                                  ),
                            _.userTypeList.isNotEmpty
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 16),
                                    child: CustomSearchableDropDown(
                                      items: _.userTypeList,
                                      label: 'Select User Type',
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.grey,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      dropDownMenuItems:
                                          _.userTypeList.map((item) {
                                        return item.name ?? "";
                                      }).toList(),
                                      onChanged: (value) {
                                        if (value != null) {
                                          setState(() {
                                            _.selectedUserType =
                                                value?.name ?? "";
                                            if (_.dealerList.isNotEmpty) {
                                              _.dealerList = [];
                                            }
                                            _.getDealer();
                                          });
                                        } else {}
                                      },
                                    ),
                                  )
                                : const SizedBox(
                                    height: 0,
                                  ),
                            _.dealerList.isNotEmpty
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 16),
                                    child: CustomSearchableDropDown(
                                      items: _.dealerList,
                                      label: 'Select Dealer',
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                            color: Colors.grey,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      dropDownMenuItems:
                                          _.dealerList.map((item) {
                                        return item.name ?? "";
                                      }).toList(),
                                      onChanged: (value) {
                                        if (value != null) {
                                          setState(() {
                                            _.selectedDealer =
                                                value?.name ?? "";
                                          });
                                        } else {}
                                      },
                                    ),
                                  )
                                : const SizedBox(
                                    height: 0,
                                  ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: DropdownButtonFormField<String>(
                                    value: _.category,
                                    items: const [
                                      DropdownMenuItem(
                                          value: 'Individual',
                                          child: Text('Individual')),
                                      DropdownMenuItem(
                                          value: 'Institution',
                                          child: Text('Institution')),
                                    ],
                                    onChanged: (value) {
                                      _.category = value!.toString();
                                      setState(() {});
                                    },
                                    decoration: const InputDecoration(
                                      labelText: 'Category',
                                      border: OutlineInputBorder(),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.lightBlue,
                                        ),
                                      ),
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 7,
                                ),
                                _.category == "Individual"
                                    ? Expanded(
                                        flex: 1,
                                        child: DropdownButtonFormField<String>(
                                          value: _.salutation,
                                          items: const [
                                            DropdownMenuItem(
                                                value: 'Mr', child: Text('Mr')),
                                            DropdownMenuItem(
                                                value: 'Miss',
                                                child: Text('Miss')),
                                            DropdownMenuItem(
                                                value: 'Mrs',
                                                child: Text('Mrs')),
                                          ],
                                          onChanged: (value) {
                                            _.salutation = value!.toString();
                                            setState(() {});
                                          },
                                          decoration: const InputDecoration(
                                            labelText: 'Salutation',
                                            border: OutlineInputBorder(),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                color: Colors.lightBlue,
                                              ),
                                            ),
                                            labelStyle: TextStyle(
                                                color: Colors.black54),
                                          ),
                                        ),
                                      )
                                    : const SizedBox(
                                        width: 0,
                                      ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            _.category == "Individual"
                                ? Column(
                                    children: [
                                      TextFormField(
                                        controller: nameController,
                                        decoration: const InputDecoration(
                                          labelText: 'Name',
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType: TextInputType.name,
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return 'Please enter your name';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(height: 16.0),
                                      Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child:
                                                DropdownButtonFormField<String>(
                                              value: _.selectedGender,
                                              items: const [
                                                DropdownMenuItem(
                                                    value: 'Male',
                                                    child: Text('Male')),
                                                DropdownMenuItem(
                                                    value: 'Female',
                                                    child: Text('Female')),
                                                DropdownMenuItem(
                                                    value: 'Other',
                                                    child: Text('Other')),
                                              ],
                                              onChanged: (value) {
                                                setState(() {
                                                  _.selectedGender = value!;
                                                });
                                              },
                                              decoration: const InputDecoration(
                                                labelText: 'Gender',
                                                border: OutlineInputBorder(),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                    color: Colors.lightBlue,
                                                  ),
                                                ),
                                                labelStyle: TextStyle(
                                                    color: Colors.black54),
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 7,
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: InkWell(
                                              onTap: () {
                                                showDatePicker(
                                                  context: context,
                                                  initialDate: DateTime.now(),
                                                  firstDate: DateTime(2019, 1),
                                                  lastDate: DateTime(2021, 12),
                                                ).then((pickedDate) {
                                                  //do whatever you want
                                                });
                                              },
                                              child: TextFormField(
                                                decoration:
                                                    const InputDecoration(
                                                  labelText: 'Date Of Birth',
                                                  border: OutlineInputBorder(),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderSide: BorderSide(
                                                      color: Colors.lightBlue,
                                                    ),
                                                  ),
                                                  labelStyle: TextStyle(
                                                      color: Colors.black54),
                                                ),
                                                controller: TextEditingController(
                                                    text:
                                                        '${selectedDate.day}/${selectedDate.month}/${selectedDate.year}'),
                                                onTap: () =>
                                                    _selectDate(context),
                                                readOnly: true,
                                                validator: (value) {
                                                  if (value == null ||
                                                      value.isEmpty) {
                                                    return 'Please select a date';
                                                  }
                                                  return null;
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 16,
                                      ),
                                      TextFormField(
                                        controller: fatherController,
                                        decoration: const InputDecoration(
                                          labelText: "Father/Husband Name",
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType: TextInputType.name,
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return 'Father/Husband name';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(
                                        height: 16,
                                      ),
                                      TextFormField(
                                        controller: emailController,
                                        decoration: const InputDecoration(
                                          labelText: 'Email',
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Please enter your email address';
                                          } else if (!RegExp(r'\S+@\S+\.\S+')
                                              .hasMatch(value)) {
                                            return 'Please enter a valid email address';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(height: 16.0),
                                      Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: TextFormField(
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    10),
                                              ],
                                              controller: mobileController,
                                              decoration: const InputDecoration(
                                                labelText: 'Mobile',
                                                border: OutlineInputBorder(),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                    color: Colors.lightBlue,
                                                  ),
                                                ),
                                                labelStyle: TextStyle(
                                                    color: Colors.black54),
                                              ),
                                              keyboardType:
                                                  TextInputType.number,
                                              validator: (value) {
                                                if (value!.isEmpty ||
                                                    value.length != 10) {
                                                  return 'Mobile Number';
                                                }
                                                return null;
                                              },
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 7,
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: TextFormField(
                                              inputFormatters: [
                                                LengthLimitingTextInputFormatter(
                                                    10),
                                              ],
                                              controller: altMobileController,
                                              decoration: const InputDecoration(
                                                labelText: 'Alternate Mobile',
                                                border: OutlineInputBorder(),
                                                focusedBorder:
                                                    OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                    color: Colors.lightBlue,
                                                  ),
                                                ),
                                                labelStyle: TextStyle(
                                                    color: Colors.black54),
                                              ),
                                              keyboardType:
                                                  TextInputType.number,
                                              validator: (value) {
                                                if (value!.isNotEmpty &&
                                                    value.length != 10) {
                                                  return 'Mobile Number';
                                                }
                                                return null;
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 20.0),
                                      DropdownButtonFormField<String>(
                                        value: _.selectedGraduation,
                                        items: const [
                                          DropdownMenuItem(
                                              value: '10th',
                                              child: Text('10 Pass')),
                                          DropdownMenuItem(
                                              value: '12th',
                                              child: Text('12 Pass')),
                                          DropdownMenuItem(
                                              value: 'Graduation',
                                              child: Text('Graduation')),
                                          DropdownMenuItem(
                                              value: 'Post Graduation',
                                              child: Text('Post Graduation')),
                                        ],
                                        onChanged: (value) {
                                          setState(() {
                                            _.selectedGraduation = value!;
                                          });
                                        },
                                        decoration: const InputDecoration(
                                          labelText: 'Qualification',
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                      ),
                                    ],
                                  )
                                : Column(
                                    children: [
                                      TextFormField(
                                        controller: insNameController,
                                        decoration: const InputDecoration(
                                          labelText: 'Institution Name',
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType: TextInputType.name,
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return 'Enter Institution Name';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(
                                        height: 16,
                                      ),
                                      TextFormField(
                                        controller: cpNameController,
                                        decoration: const InputDecoration(
                                          labelText: "Contact Person Name",
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType: TextInputType.name,
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return 'Contact Person Name';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(
                                        height: 16,
                                      ),
                                      TextFormField(
                                        controller: cpEmailController,
                                        decoration: const InputDecoration(
                                          labelText: 'Contact Person Email',
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Invalid Email Address';
                                          } else if (!RegExp(r'\S+@\S+\.\S+')
                                              .hasMatch(value)) {
                                            return 'Invalid Email Address';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(height: 16.0),
                                      TextFormField(
                                        inputFormatters: [
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller: cpMobileController,
                                        decoration: const InputDecoration(
                                          labelText: 'Contact Person Mobile',
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType: TextInputType.number,
                                        validator: (value) {
                                          if (value!.isEmpty ||
                                              value.length != 10) {
                                            return 'Contact Person Number';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(height: 16.0),
                                      TextFormField(
                                        controller: insEmailController,
                                        decoration: const InputDecoration(
                                          labelText: 'Email Address',
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Invalid Email Address';
                                          } else if (!RegExp(r'\S+@\S+\.\S+')
                                              .hasMatch(value)) {
                                            return 'Invalid Email Address';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(height: 16.0),
                                      TextFormField(
                                        inputFormatters: [
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller: insMobileController,
                                        decoration: const InputDecoration(
                                          labelText: 'Contact Number',
                                          border: OutlineInputBorder(),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              color: Colors.lightBlue,
                                            ),
                                          ),
                                          labelStyle:
                                              TextStyle(color: Colors.black54),
                                        ),
                                        keyboardType: TextInputType.number,
                                        validator: (value) {
                                          if (value!.isEmpty ||
                                              value.length != 10) {
                                            return 'Invalid Contact Number';
                                          }
                                          return null;
                                        },
                                      ),
                                      const SizedBox(height: 16.0),
                                    ],
                                  ),
                            const SizedBox(height: 16.0),
                            Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const Text('Do you have GST:'),
                                  ChoiceChip(
                                      selected: _.yes_selected,
                                      label: const Text('Yes'),
                                      onSelected: (bool selected) {
                                        setState(() {
                                          _.yes_selected = !_.yes_selected;
                                          _.no_selected = !_.no_selected;
                                          if (_.yes_selected) {
                                            _.isGst = "yes";
                                          }
                                        });
                                      }),
                                  ChoiceChip(
                                      selected: _.no_selected,
                                      label: const Text('No'),
                                      onSelected: (bool selected) {
                                        setState(() {
                                          _.no_selected = !_.no_selected;
                                          _.yes_selected = !_.yes_selected;
                                          if (_.no_selected) {
                                            _.isGst = "no";
                                          }
                                        });
                                      }),
                                ]),
                            _.isGst == 'yes'
                                ? const SizedBox(height: 16.0)
                                : const SizedBox(height: 0),
                            _.isGst == 'yes'
                                ? TextFormField(
                                    controller: gstController,
                                    decoration: const InputDecoration(
                                      labelText: 'GST Number',
                                      border: OutlineInputBorder(),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.lightBlue,
                                        ),
                                      ),
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                    ),
                                    keyboardType: TextInputType.text,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Invalid GST Number';
                                      }
                                      return null;
                                    },
                                  )
                                : const SizedBox(
                                    height: 0,
                                  ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: TextFormField(
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(12),
                                    ],
                                    controller: aadharController,
                                    decoration: const InputDecoration(
                                      labelText: 'Addhar',
                                      border: OutlineInputBorder(),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.lightBlue,
                                        ),
                                      ),
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                    ),
                                    keyboardType: TextInputType.number,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Enter Adhar Number';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                                const SizedBox(
                                  width: 7,
                                ),
                                Expanded(
                                  flex: 1,
                                  child: TextFormField(
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(11),
                                    ],
                                    controller: panController,
                                    decoration: const InputDecoration(
                                      labelText: 'Pan Number',
                                      border: OutlineInputBorder(),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.lightBlue,
                                        ),
                                      ),
                                      labelStyle:
                                          TextStyle(color: Colors.black54),
                                    ),
                                    keyboardType: TextInputType.text,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Enter PAN Number';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 20.0),
                            ElevatedButton(
                              onPressed: () {
                                _.name = nameController.text ?? "";
                                _.mobile = mobileController.text ?? "";
                                _.email = emailController.text ?? "";
                                _.altMobile = altMobileController.text ?? "";
                                _.fatherName = fatherController.text ?? "";
                                _.aadhar = aadharController.text ?? "";
                                _.panNo = panController.text ?? "";
                                _.gstNo = gstController.text ?? "";
                                _.insName = insNameController.text ?? "";
                                _.contactPerson = cpNameController.text ?? "";
                                _.contactPersonEmail =
                                    cpEmailController.text ?? "";
                                _.contactPersonMobile =
                                    cpMobileController.text ?? "";
                                _.insEmail = insEmailController.text ?? "";
                                _.insMobile = insMobileController.text ?? "";
                                _.dob =
                                    '${selectedDate.day}/${selectedDate.month}/${selectedDate.year}';

                                if (_formKey.currentState!.validate()) {
                                  if (_.salutation.isEmpty) {
                                    Get.snackbar("Required Salutation",
                                        "Kindly Select Salutation");
                                  } else {
                                    _.onNext();
                                  }
                                }
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    WidgetStateProperty.all<Color>(
                                        Colors.lightBlue),
                                // Set the background color to blue
                                minimumSize: WidgetStateProperty.all<Size>(
                                    const Size(double.infinity, 50)),
                                shape: WidgetStateProperty.all<
                                    RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        10.0), // Set the border radius to 10.0 for rounded corners
                                  ),
                                ), // Set the button width to full and height to 50
                              ),
                              child: const Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Text(
                                  'Next',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        );
      },
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    DateTime now = DateTime.now();
    selectedDate = DateTime(now.year - 18, now.month, now.day);
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1940, 1),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'PosFormPersonalController1.dart';

class PosFormPersonalUIController extends StatefulWidget {
  const PosFormPersonalUIController({super.key});

  @override
  State<PosFormPersonalUIController> createState() =>
      _PosFormPersonalUIControllerState();
}
final formKey = GlobalKey<FormState>();
class _PosFormPersonalUIControllerState
    extends State<PosFormPersonalUIController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PosFormPersonalController>(
        init: PosFormPersonalController(),
        builder: (_) {
          return Scaffold(
            resizeToAvoidBottomInset: true,
              appBar: AppBar(
                title: const Text('Partner Personal Detail'),
              ),
              body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Stack(
                  children:[
                    SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width:MediaQuery.of(context).size.height,
                    ),
                    SingleChildScrollView(
                      child: Form(
                        key: formKey,
                        child: Column(
                        children: [
                          TextFormField(
                            controller: _.nameController,
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(RegExp(r'^[0-9]*$'))
                            ],
                            decoration: const InputDecoration(
                              labelText: 'Name',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(),
                                labelStyle:  TextStyle(color: Colors.grey)
                            ),
                          ),
                          const SizedBox(height: 16),
                          TextFormField(
                            controller: _.emailController,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9@._]')),
                            ],
                            decoration: const InputDecoration(
                              labelText: 'Email',
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(),
                              labelStyle: TextStyle(color: Colors.grey),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter an email';
                              } else if (!RegExp(r'^[^@]+@[^@]+\.[^@]+').hasMatch(value)) {
                                return 'Please enter a valid email';
                              } else if (!value.contains('@')) {
                                return 'Email must contain "@" ';
                              }
                              return null;
                            },
                          ),

                          const SizedBox(height: 16),
                          TextFormField(
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly
                            ],
                            maxLength: 10,
                            controller: _.mobileController,
                            decoration: const InputDecoration(
                              labelText: 'Phone Number',
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(),
                              labelStyle: TextStyle(color: Colors.black54),
                            ),
                            keyboardType: TextInputType.phone,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter your Phone Number';
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 16),
                        ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: ElevatedButton(onPressed: (){
                        if (formKey.currentState!.validate()) {
                        _.addPartner();
                        }
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green.shade300,
                            shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
                        ), child: const Text('Add Partner',style: TextStyle(fontSize: 18),),
                      ),
                    )
                ]),
              ));
        });
  }
}

import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/Model/Pos/UploadFile.dart';
import 'package:pi/utils/BottomBarScreen.dart';
import '../../../Model/StatusMessageRes.dart';
import '../../../utils/AppUtils.dart';
import 'package:file_picker/file_picker.dart';

class PosFormDoc extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;
  String version = '', userId = '', userType = '', token = '';
  late String pan = "",
      aadharFront= "",
      aadharBack = "",
      qualification= "",
      cheque= "",
      profile= "",
      signature= "",
      other= "";
  Map<String, String> allData = {};

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    allData = Get.find(tag: "mData");
}

  void onNext() async {
    setLoading(true);
    update();
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.addPartner);
      var response = await http.post(url, body: allData);

      print(url);
      print(allData);
      print(response.body);
      if (response.statusCode == 200) {
        StatusMessageRes loginResponse =
        StatusMessageRes.fromJson(jsonDecode(response.body));

        String msg = loginResponse.message ?? "Something went wrong";
        if (loginResponse.status!) {
          Get.snackbar( "Partner Create", msg);
          Get.offAll(const BottomBarScreen());
        } else {
          Get.snackbar( "Partner Create Failed!", msg);
        }
      } else {
        Get.snackbar( "Partner Create Failed!", "Something went wrong...");

      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
    update();
  }


  Future<void> uploadFile(PlatformFile file, String key) async {
    var request = http.MultipartRequest(
        'POST',
        Uri.parse(
            '${AppUtils.baseUrl}PartnerReport/UploadDocumentsSquarefin'
          //'http://13.127.142.101/sanity/ci/tarun/API'
        ));
    request.fields.addAll({'Login_User_Id': userId, 'Document_Type': key});
    request.files
        .add(await http.MultipartFile.fromPath(key, file.path.toString()));

    http.StreamedResponse response = await request.send();
    var res = await http.Response.fromStream(response);
    print(res.body);
    if (res.statusCode == 200) {
      UploadFile fu = UploadFile.fromJson(jsonDecode(res.body));
      allData.addEntries({
        key: fu.fileName ?? ""
      }.entries);

      print(allData);
      Get.snackbar(key, "Uploaded Successfully");
    } else {
      print(response.reasonPhrase);
    }
  }
}

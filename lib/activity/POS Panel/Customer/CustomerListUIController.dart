import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pi/activity/POS%20Panel/Customer/AddCustomerController.dart';
import 'package:pi/activity/POS%20Panel/Customer/AddCustomerUIController.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../utils/AppUtils.dart';
import 'CustomerListController.dart';

class CustomerListUIController extends StatefulWidget {
  const CustomerListUIController({super.key});

  @override
  State<CustomerListUIController> createState() => _CustomerListUIControllerState();
}

  final formKey = GlobalKey<FormState>();
class _CustomerListUIControllerState extends State<CustomerListUIController> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CustomerListController>(
        init: CustomerListController(),
        builder: (_){
          return Scaffold(
            appBar: AppBar(title: Text('My Customer' ' (${_.myPartner.length})'),actions: [
                IconButton(
                onPressed: () {
                    Get.to(() => const AddCustomerUIController())?.then((value) {
                      if (value == true) {
                        _.fetchPartner();
                      }
                    });
                  },
                  icon: const Icon(Icons.add),
                ),
                IconButton(onPressed: (){
                setState(() {
                  if(_.searchBarBool){
                    _.reloadList();
                    _.update();
                  }
              _.searchBarBool = !_.searchBarBool;
                });
            }, icon: _.searchBarBool?const Icon(Icons.close):const Icon(Icons.search))
            ],),
            body:
              Stack(
                children: [
                  Container(
                    height: double.infinity,
                    width: double.infinity,
                    color: AppUtils.bgColor
                  ),
                  Column(children: [
                    _.searchBarBool?Padding(
                      padding: const EdgeInsets.only(
                          left: 13, right: 13, top: 8),
                      child: Container(
                        height:
                        MediaQuery.of(context).size.height * 0.04,
                        decoration: BoxDecoration(
                            color:
                            Colors.blue.shade100.withOpacity(0.3),
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(width: 0.3)),
                        child: Center(
                          child: Row(
                            children: [
                              Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 14, left: 15, right: 15),
                                    child: TextFormField(
                                      autofocus: true,
                                      decoration: const InputDecoration(
                                        hintText: 'Search',
                                        border: InputBorder.none,
                                      ),
                                      onChanged: (value) {
                                        _.searchData(value);
                                      },
                                    ),
                                  )),
                              const Icon(
                                Icons.search_outlined,
                                size: 18,
                                color: Colors.blue,
                              ),
                              const SizedBox(
                                width: 10,
                              )
                            ],
                          ),
                        ),
                      ),
                    ):const SizedBox(),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: _.showIndigator
                            ? const Center(
                          child: CircularProgressIndicator(color: Colors.blue),
                        )
                            : _.myPartner.isEmpty
                            ? Center(
                          child: SvgPicture.asset('res/images/ic_empty.svg'),
                        )
                            : ListView.builder(
                          itemCount: _.myPartner.length,
                          itemBuilder: (context, index) {
                            var use = _.myPartner[_.myPartner.length - 1 - index];
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 2),
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    _.searchBarBool = false;
                                  });
                                },
                                child: Stack(
                                  children: [
                                    Container(
                                      width: double.infinity, // Full width
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(5),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            blurRadius: 5,
                                            spreadRadius: 0,
                                            offset: const Offset(0, 2),
                                          ),
                                        ],
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0), // Consistent padding
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              use.name,
                                              style: const TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15,
                                              ),
                                              overflow: TextOverflow.ellipsis, // Truncate if too long
                                            ),
                                            const SizedBox(height: 10),
                                            if (use.email.isNotEmpty)
                                              InkWell(
                                                onTap: () async {
                                                  final emailUri = Uri(
                                                    scheme: 'mailto',
                                                    path: use.email,
                                                  );
                                                  if (await launchUrl(emailUri, mode: LaunchMode.externalApplication)) {
                                                    // Email opened
                                                  } else {
                                                    ScaffoldMessenger.of(context).showSnackBar(
                                                      const SnackBar(
                                                        content: Text('Could not launch email client'),
                                                      ),
                                                    );
                                                  }
                                                },
                                                child: Row(
                                                  children: [
                                                    Image.asset('res/images/contactUs/email.png'),
                                                    const SizedBox(width: 8),
                                                    Expanded(
                                                      child: Text(
                                                        use.email,
                                                        style: const TextStyle(fontSize: 12),
                                                        overflow: TextOverflow.ellipsis,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            if (use.email.isNotEmpty)
                                              Divider(thickness: 1, color: Colors.blue.shade100),
                                            if (use.mobile.isNotEmpty)
                                              Row(
                                                children: [
                                                  Image.asset('res/images/contactUs/phone-call.png'),
                                                  const SizedBox(width: 8),
                                                  InkWell(
                                                    onTap: () async {
                                                      final phoneNo = Uri.parse('tel:+91${use.mobile}');
                                                      if (await launchUrl(phoneNo)) {
                                                        // Call initiated
                                                      } else {
                                                        ScaffoldMessenger.of(context).showSnackBar(
                                                          const SnackBar(
                                                            content: Text('Could not launch Dialer pad'),
                                                          ),
                                                        );
                                                      }
                                                    },
                                                    child: Text(
                                                      '+91 ${use.mobile}',
                                                      style: const TextStyle(fontSize: 12),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            if (use.mobile.isNotEmpty)
                                              Divider(thickness: 1, color: Colors.blue.shade100),
                                            if (use.pan.isNotEmpty)
                                              Row(
                                                children: [
                                                  Image.asset('res/images/contactUs/calendar.png'),
                                                  const SizedBox(width: 8),
                                                  const Text(
                                                    'Pan: ',
                                                    style: TextStyle(fontSize: 10),
                                                  ),
                                                  Text(
                                                    use.pan,
                                                    style: const TextStyle(fontSize: 12),
                                                  ),
                                                ],
                                              ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    )

                  ],),
                ],
              )
          );
        });
  }
}

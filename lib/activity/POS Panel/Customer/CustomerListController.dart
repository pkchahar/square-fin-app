import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:pi/activity/POS%20Panel/Customer/CustomerList.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../Model/Pos/AddPartnerModel.dart';
import '../../../Model/Pos/PartnerList.dart';
import '../../../Model/UtilityModel/BoolResponse.dart';
class CustomerListController extends GetxController
{

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController panController = TextEditingController();
  TextEditingController otpController = TextEditingController();
  var status;
  var msg;
  bool searchBarBool = false;
  List<CustomerRes>  myPartner = [];
  List<CustomerRes> searchList =[];

  bool showIndigator = false;

  @override
  onInit() async {
    await fetchPartner();
    super.onInit();
  }

  Future<void> fetchPartner()async{
    showIndigator = true;
    update();
    SharedPreferences pref = await SharedPreferences.getInstance();
    print(pref.getString(AppUtils.userId));
    try{
      Map<String, dynamic> mData ={
        'user_id' : pref.getString(AppUtils.userId)
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.customerList);
      var response = await http.post(url,body: mData);

      print(url);
      print(mData);
      print(response.body);
      if(response.statusCode ==200){
        Map<String, dynamic> responseJson = jsonDecode(response.body);
        CustomerList myResponse = CustomerList.fromJson(responseJson);
        if(myPartner.isNotEmpty){
          myPartner =[];
          searchList = [];
        }
        myPartner.addAll(myResponse.data!);
        searchList.addAll(myResponse.data!);
        showIndigator = false;
        update();
        print('this is res');
        print(myPartner[0].name);
      }
    }catch(e){
      print(e);
    }
  }


  void launchEmail(String email) async {
    final Uri url = Uri(
      scheme: 'mailto',
      path: email,
    );

    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  void reloadList() {
    print('executing this');
    myPartner = [];
    myPartner.addAll(searchList);
    update();
  }
  void searchData(String ask) {
    myPartner = [];
    if (ask.isNotEmpty) {
      myPartner = searchList.where((item) {
        return ((item.pan.toLowerCase().contains(ask.toLowerCase()) ||
            item.name.toLowerCase().contains(ask.toLowerCase()) ||
            item.email.toLowerCase().contains(ask.toLowerCase()) ||
            item.mobile.toLowerCase().contains(ask.toLowerCase()) ||
            item.id.toLowerCase().contains(ask.toLowerCase())));
      }).toList();
    } else {
      myPartner.addAll(searchList);
    }
    update();
  }


}
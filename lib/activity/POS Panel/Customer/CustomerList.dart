class CustomerList {
  List<CustomerRes> data;
  bool status;

  CustomerList({required this.data, required this.status});

  // Factory constructor to create a CustomerList object from JSON
  factory CustomerList.fromJson(Map<String, dynamic> json) {
    return CustomerList(
      data: json['Data'] != null
          ? List<CustomerRes>.from(json['Data'].map((x) => CustomerRes.fromJson(x)))
          : [],
      status: json['status'] ?? false,
    );
  }

  // Method to convert a CustomerList object to JSON
  Map<String, dynamic> toJson() {
    return {
      'Data': data.map((x) => x.toJson()).toList(),
      'status': status,
    };
  }
}

class CustomerRes {
  String id;
  String name;
  String email;
  String mobile;
  String pan;

  CustomerRes({
    required this.id,
    required this.name,
    required this.email,
    required this.mobile,
    required this.pan,
  });

  // Factory constructor to create a CustomerRes object from JSON
  factory CustomerRes.fromJson(Map<String, dynamic> json) {
    return CustomerRes(
      id: json['Id'] ?? "",
      name: json['Name'] ?? "",
      email: json['Email'] ?? "",
      mobile: json['Mobile'] ?? "",
      pan: json['Pan'] ?? "",
    );
  }

  // Method to convert a CustomerRes object to JSON
  Map<String, dynamic> toJson() {
    return {
      'Id': id,
      'Name': name,
      'Email': email,
      'Mobile': mobile,
      'Pan': pan,
    };
  }
}

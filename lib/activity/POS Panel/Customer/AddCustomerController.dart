import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:pi/utils/AppUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../Model/UtilityModel/BoolResponse.dart';
import 'package:get/get.dart';

class AddCustomerController extends GetxController {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController panController = TextEditingController();
  TextEditingController otpController = TextEditingController();
  final GlobalKey buttonKey = GlobalKey();

  var status;
  String mobile = "";
  bool otpSend = false;
  bool showIndigator = false;
  bool _isLoading = false;
  late Timer _timer;

  bool get isLoading => _isLoading;
  late ValueNotifier<int> secondsNotifier;

  @override
  onInit() async {
    super.onInit();
    secondsNotifier = ValueNotifier<int>(0);
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  void startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      if (secondsNotifier.value > 0) {
        secondsNotifier.value--;
      } else {
        timer.cancel();
      }
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    secondsNotifier.dispose();
    super.dispose();
  }

  Future<void> addCustomer() async {
    setLoading(true);
    try {
      if (nameController.text.isEmpty || nameController.text.length < 2) {
        AppUtils.showSnackBar('Error', 'Please Insert Name', false);
        return;
      }
      if (emailController.text.isEmpty) {
        AppUtils.showSnackBar('Error', 'Please Insert Email', false);
        return;
      }
      if (mobileController.text.isEmpty || mobileController.text.length < 10) {
        AppUtils.showSnackBar(
            'Error', 'Please Insert Valid Phone Number', false);
        return;
      }

      SharedPreferences pref = await SharedPreferences.getInstance();
      Map<String, dynamic> mData = {
        'user_id': pref.getString(AppUtils.userId).toString(),
        'name': nameController.text,
        'email': emailController.text,
        'mobile': mobileController.text,
        'pannumber': panController.text,
        'otp': otpController.text
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.addCustomer);
      var response = await http.post(url, body: mData);

      print(url);
      print(mData);
      print(response.body);

      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = jsonDecode(response.body);
        BoolResponse myResponse = BoolResponse.fromJson(responseJson);
        status = myResponse.status;

        String msg = myResponse.msg;
        AppUtils.showSnackBar('Add Customer', msg, status);
        if(status) {
          nameController.text = "";
          emailController.text = "";
          mobileController.text = "";
          panController.text = "";
          otpController.text = "";
          otpSend = false;
        }
      } else {
        print('Failed to add partner: ${response.statusCode}');
        AppUtils.showSnackBar('Error', 'Failed to add partner', false);
      }
      update();
    } catch (e) {
      print('Exception occurred: $e');
      AppUtils.showSnackBar('Error', 'An error occurred', false);
    }
    setLoading(false);
  }

  Future<void> resendOtp() async {
    setLoading(true);
    SharedPreferences pref = await SharedPreferences.getInstance();
    mobile = mobileController.text.toString();

    if (mobile.isEmpty || mobile.length != 10) {
      AppUtils.showSnackBar(
          "Invalid Mobile", "Enter a valid mobile number", false);
      return;
    }
    try {
      Map<String, dynamic> mData = {
        'user_id': pref.getString(AppUtils.userId),
        'mobile': mobile
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.sendCustomerOtp);
      var response = await http.post(url, body: mData);

      print(url);
      print(mData);
      print(response.statusCode);
      print(response.body);

      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = jsonDecode(response.body);
        BoolResponse myResponse = BoolResponse.fromJson(responseJson);

        if (myResponse.status) {
          otpSend = true;
          startTimer();
        } else {
          secondsNotifier.value = 0;
          otpSend = false;
        }
        setLoading(false);
        AppUtils.showSnackBar(
            "One Time Passcode", myResponse.msg, myResponse.status);
        update();
      }
    } catch (e) {
      print(e);
    }
  }
}

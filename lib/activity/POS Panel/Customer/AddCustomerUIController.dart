import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pi/activity/POS%20Panel/Customer/AddCustomerController.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../utils/AppUtils.dart';
import 'CustomerListController.dart';

class AddCustomerUIController extends StatefulWidget {
  const AddCustomerUIController({super.key});

  @override
  State<AddCustomerUIController> createState() =>
      _AddCustomerUIControllerState();
}

final formKey = GlobalKey<FormState>();

class _AddCustomerUIControllerState extends State<AddCustomerUIController> {
  String otp = '';

  TextInputFormatter upperCaseFormatter() {
    return TextInputFormatter.withFunction((oldValue, newValue) {
      return newValue.copyWith(
        text: newValue.text.toUpperCase(),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddCustomerController>(
        init: AddCustomerController(),
        builder: (_) {
          return Scaffold(
              appBar: AppBar(
                title: const Text('Add Customer'),
              ),
              body: Stack(
                children: [
                  Container(
                      height: double.infinity,
                      width: double.infinity,
                      color: AppUtils.bgColor),
                  SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Form(
                        key: formKey,
                        child: Column(
                          children: [
                            TextFormField(
                              controller: _.nameController,
                              inputFormatters: [
                                FilteringTextInputFormatter.deny(
                                    RegExp(r'[0-9]'))
                              ],
                              decoration: const InputDecoration(
                                labelText: 'Enter Name',
                                border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Color(0xFFD4E4F1))),
                                focusedBorder: OutlineInputBorder(),
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please Enter Name';
                                }
                                return null;
                              },
                            ),
                            const SizedBox(height: 16),
                            TextFormField(
                              controller: _.emailController,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                    RegExp(r'[a-zA-Z0-9@._]')),
                              ],
                              decoration: const InputDecoration(
                                labelText: 'Enter Email Id',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(),
                                labelStyle: TextStyle(color: Colors.grey),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter an email';
                                } else if (!RegExp(r'^[^@]+@[^@]+\.[^@]+')
                                    .hasMatch(value)) {
                                  return 'Please enter a valid email';
                                } else if (!value.contains('@')) {
                                  return 'Email must contain "@"';
                                }
                                return null;
                              },
                            ),
                            const SizedBox(height: 16),
                            TextFormField(
                              maxLength: 10,
                              controller: _.panController,
                              decoration: const InputDecoration(
                                labelText: 'Pan Number',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(),
                                labelStyle: TextStyle(color: Colors.black54),
                                counterText: '',
                              ),
                              keyboardType: TextInputType.text,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                    RegExp(r'[A-Z0-9]')),
                                upperCaseFormatter(),
                              ],
                              validator: (value) {
                                if (value != null &&
                                    value.length > 1 &&
                                    value.length != 10) {
                                  return 'Please enter valid pan number';
                                }
                                return null;
                              },
                            ),
                            const SizedBox(height: 16),
                            TextFormField(
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly
                              ],
                              maxLength: 10,
                              controller: _.mobileController,
                              decoration: const InputDecoration(
                                labelText: 'Phone Number',
                                border: OutlineInputBorder(),
                                focusedBorder: OutlineInputBorder(),
                                labelStyle: TextStyle(color: Colors.black54),
                                counterText: '',
                              ),
                              keyboardType: TextInputType.phone,
                              validator: (value) {
                                if (value == null ||
                                    value.isEmpty ||
                                    value.length != 10) {
                                  return 'Please enter valid phone number';
                                } else {
                                  _.mobile = value;
                                }
                                return null;
                              },
                            ),
                            const SizedBox(height: 5),
                            GestureDetector(
                              onTap: () {
                                if (_.secondsNotifier.value == 0) {
                                  _.secondsNotifier.value = 45;
                                  _.resendOtp();
                                }
                              },
                              child: Container(
                                alignment: Alignment.centerRight,
                                padding: const EdgeInsets.only(bottom: 1.0),
                                child: ValueListenableBuilder<int>(
                                  valueListenable: _.secondsNotifier,
                                  builder: (context, secondsRemaining, _) {
                                    return Text(
                                      secondsRemaining > 0
                                          ? 'Resend OTP in $secondsRemaining seconds'
                                          : 'Send OTP',
                                      style: TextStyle(
                                          color: secondsRemaining > 0
                                              ? Colors.grey
                                              : const Color(0xFF0067FF)),
                                    );
                                  },
                                ),
                              ),
                            ),
                            const SizedBox(height: 16),
                            if (_.otpSend)
                              TextFormField(
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                maxLength: 6,
                                controller: _.otpController,
                                decoration: const InputDecoration(
                                  labelText: 'OTP',
                                  border: OutlineInputBorder(),
                                  focusedBorder: OutlineInputBorder(),
                                  labelStyle: TextStyle(color: Colors.black54),
                                  counterText: '',
                                ),
                                keyboardType: TextInputType.phone,
                                validator: (value) {
                                  if (value == null ||
                                      value.isEmpty ||
                                      value.length != 6) {
                                    return 'Invalid OTP';
                                  }
                                  return null;
                                },
                              ),
                            const SizedBox(height: 30),
                            if (_.otpSend)
                              GetBuilder<AddCustomerController>(
                                  builder: (loginController) {
                                return ElevatedButton(
                                  key: _.buttonKey,
                                  onPressed: () {
                                    if (formKey.currentState!.validate()) {
                                      if (!loginController.isLoading) {
                                        _.addCustomer();
                                      }
                                    }
                                  },
                                  style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                            const Color(0xFF0067FF)),
                                    shadowColor: WidgetStateProperty.all<Color>(
                                        Colors.transparent),
                                    minimumSize: WidgetStateProperty.all<Size>(
                                        const Size(double.infinity, 45)),
                                    shape: WidgetStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                  ),
                                  child: loginController.isLoading
                                      ? const CircularProgressIndicator(
                                          color: Colors.white)
                                      : const Text(
                                          'Submit',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18),
                                        ),
                                );
                              })
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ));
        });
  }
}

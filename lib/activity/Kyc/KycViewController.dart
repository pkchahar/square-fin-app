// import 'dart:convert';

// import 'package:custom_searchable_dropdown/custom_searchable_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'package:image_picker/image_picker.dart';
import 'package:pi/activity/Kyc/KycAdharViewController.dart';
import 'package:pi/utils/AppUtils.dart';
import '../Kyc/KycController.dart';
import 'dart:io';

class KycViewController extends StatefulWidget {
  String name, email, pan;

  KycViewController(
      {super.key, required this.name, required this.email, required this.pan});

  @override
  _KycViewController createState() => _KycViewController();
}

class _KycViewController extends State<KycViewController> {
  String panPath = "", adharFrontPath = '', adharBackPath = '', bankPath = '';
  final formKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime(
      DateTime.now().year - 18, DateTime.now().month, DateTime.now().day);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<KycController>(
      init: KycController(context: context),
      builder: (_) {
        _.panController.text = widget.pan;
        return Scaffold(
          appBar: AppBar(
            title: const Text("User KYC"),
          ),
          backgroundColor: AppUtils.backgroundColor,
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Center(
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      const SizedBox(height: 16.0),
                      Column(
                        children: [
                          Container(
                            alignment: Alignment.topLeft,
                            child: const Row(
                              children: [
                                Icon(
                                  Icons.perm_identity,
                                  color: Colors.orange,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "PAN Card Detail",
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const Divider(),
                          const SizedBox(height: 15.0),
                          TextFormField(
                            controller: _.panController,
                            decoration: const InputDecoration(
                              labelText: 'PAN Number',
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.lightBlue,
                                ),
                              ),
                              labelStyle: TextStyle(color: Colors.black54),
                            ),
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp(r'[A-Z0-9]')),
                            ],
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter PAN Number';
                              }
                              RegExp panRegex =
                                  RegExp(r'^[A-Z]{5}[0-9]{4}[A-Z]{1}$');
                              if (!panRegex.hasMatch(value)) {
                                return 'Please enter a valid PAN Number';
                              }
                              _.panNumber = value;
                              return null;
                            },
                          ),
                          const SizedBox(height: 15.0),
                          TextFormField(
                            decoration: const InputDecoration(
                              labelText: 'Date Of Birth',
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.lightBlue,
                                ),
                              ),
                              labelStyle: TextStyle(color: Colors.black54),
                            ),
                            controller: TextEditingController(
                                text:
                                    '${selectedDate.day}/${selectedDate.month}/${selectedDate.year}'),
                            onTap: () => _selectDate(context),
                            readOnly: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please select a date';
                              }
                              _.dob = value;
                              return null;
                            },
                          ),
                          const SizedBox(height: 15.0),
                          Container(
                            alignment: Alignment.topLeft,
                            child: const Text(
                              "Pan Card Image",
                              textAlign: TextAlign.start,
                              style:
                                  TextStyle(fontSize: 12.0, color: Colors.blue),
                            ),
                          ),
                          const SizedBox(height: 10.0),
                          InkWell(
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.blueGrey,
                                  style: BorderStyle.solid,
                                  width: 0.5,
                                ),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Icon(
                                      Icons.attachment_rounded,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: (_.panImg.isNotEmpty)
                                        ? const Text(
                                            "Pan Image Selected",
                                            style: TextStyle(
                                                color: Colors.green,
                                                fontWeight: FontWeight.bold),
                                          )
                                        : const Padding(
                                            padding: EdgeInsets.only(
                                                top: 20, bottom: 20),
                                            child: Text(
                                              'Select Pan image',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                  ),
                                  Expanded(
                                    child: Container(),
                                  ),
                                  panPath.isNotEmpty
                                      ? Row(
                                          children: [
                                            IconButton(
                                              onPressed: () {
                                                showImageDialog(context,
                                                    "PAN Image", _.panImg);
                                              },
                                              icon: const Icon(
                                                Icons.image_outlined,
                                                color: Colors.grey,
                                              ),
                                            ),
                                            InkWell(
                                              child: const Padding(
                                                padding: EdgeInsets.all(8.0),
                                                child: Icon(
                                                  Icons.clear,
                                                  color: Colors.red,
                                                ),
                                              ),
                                              onTap: () {
                                                _.panImg = "";
                                                panPath = "";
                                                setState(() {});
                                              },
                                            ),
                                          ],
                                        )
                                      : const SizedBox(),
                                ],
                              ),
                            ),
                            onTap: () async {
                              widget.pan = _.panController.text;
                              _showDialog(_);
                            },
                          ),
                        ],
                      ),
                      const SizedBox(height: 30.0),
                      ElevatedButton(
                        onPressed: () {
                          if (formKey.currentState!.validate()) {
                            if (panPath.isEmpty) {
                              AppUtils.showSnackBar(
                                  "Pan Card Document", "is mandatory!!", false);
                            } else {
                              Get.to(const KycAdharViewController());
                            }
                          }
                        },
                        style: ButtonStyle(
                          backgroundColor: WidgetStateProperty.all<Color>(
                              Colors.lightBlue),
                          shape:
                              WidgetStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(7.0),
                            ),
                          ),
                        ),
                        child: const Text(
                          'Next',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> _showDialog(KycController obj) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Choose an option'),
          children: [
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                _pickImage(obj, ImageSource.camera);
              },
              child: const Row(
                children: [
                  Icon(Icons.camera),
                  SizedBox(width: 10),
                  Text('Camera'),
                ],
              ),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                _pickImage(obj, ImageSource.gallery);
              },
              child: const Row(
                children: [
                  Icon(Icons.image),
                  SizedBox(width: 10),
                  Text('Gallery'),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> _pickImage(KycController obj, ImageSource source) async {
    final imagePicker = ImagePicker();
    final XFile? image = await imagePicker.pickImage(source: source);
    if (image == null) {
      // User canceled image capture
      return;
    } else {
      final List<String> pathParts = image.path.split('/');
      final String imageName = pathParts.last;
      panPath = imageName;
      obj.panImg = image.path;
    }
    setState(() {});
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime currentDate = DateTime.now();
    final DateTime lastDate =
        currentDate.subtract(const Duration(days: 18 * 365));
    final DateTime firstDate =
        currentDate.subtract(const Duration(days: 80 * 365));

    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: firstDate,
      lastDate: lastDate,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: Colors.blue,
            colorScheme: const ColorScheme.light(primary: Colors.blue)
                .copyWith(secondary: Colors.blue),
          ),
          child: child!,
        );
      },
    );

    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  void showImageDialog(BuildContext context, String title, String imagePath) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.file(File(imagePath), fit: BoxFit.contain),
                const SizedBox(height: 20),
                Text(title), // You can add a text description or title here
              ],
            ),
          ),
        );
      },
    );
  }
}

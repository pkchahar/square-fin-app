import 'dart:convert';
// import 'dart:developer';

// import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
// import 'package:pi/activity/Kyc/KycAdharViewController.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:pi/Model/BasicResponse.dart';
import 'package:pi/network/ApiClient.dart';
import '../../../Model/Bank.dart';
import '../../../utils/AppUtils.dart';
import '../../Model/BankList.dart';
// import '../../Model/LoginResponse.dart';
// import '../../Model/UtilityModel/ContactUs.dart';
import '../../utils/BottomBarScreen.dart';

class KycController extends GetxController {
  BuildContext context;

  KycController({required this.context});

  bool _isLoading = false;

  bool get isLoading => _isLoading;
  String version = '', userId = '', userType = '', token = '';
  late String branch,
      accountNo,
      bankName,
      ifscCode,
      adhaarNumber,
      panNumber,
      dob,
      name = '',
      mobile = '',
      email = '',
      address = '',
      panImg = '',
      adharFrontImg = '',
      adharBackImg = '',
      bankImg = '';
  List<BankList> bankList = [];
  late String selectedBank = 'SBI';
  late String selectAccType = 'Saving';

  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final panController = TextEditingController();
  final adharController = TextEditingController();
  final accController = TextEditingController();
  final ifscController = TextEditingController();
  final branchController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    getBankList();
  }

  void submitKyc() async {
    setLoading(true);
    var request = http.MultipartRequest('POST',
        Uri.parse('https://www.squarefin.in/api/api/SignUp/Createkycrequest'));

    request.fields['Login_User_Id'] = userId;
    request.fields['bank_name'] = selectedBank;
    request.fields['branch_name'] = branch;
    request.fields['account_type'] = selectAccType;
    request.fields['account_number'] = accountNo;
    request.fields['ifsc_code'] = ifscCode;
    request.fields['adhaar_number'] = adhaarNumber;
    request.fields['pan_number'] = panNumber;
    request.fields['dob'] = dob;

    request.files.add(await http.MultipartFile.fromPath('pan_proof', panImg));
    request.files.add(await http.MultipartFile.fromPath('bank_proof', bankImg));
    request.files
        .add(await http.MultipartFile.fromPath('adhaar_front', adharFrontImg));
    request.files
        .add(await http.MultipartFile.fromPath('adhaar_back', adharBackImg));

    http.StreamedResponse response = await request.send();
    try {
      String userMessage = 'Something went wrong!';
      if (response.statusCode == 200) {
        final Map<String, dynamic> jsonData =
            json.decode(await response.stream.bytesToString());
        if (jsonData['message'] != null) {
          userMessage = jsonData['message'];
        }
        print(jsonData);
        AppUtils.showSnackBar("KYC Submit", userMessage, true);
        openDashboard();
      } else {
        AppUtils.showSnackBar("KYC Submit", userMessage, false);
      }
    } catch (e) {
      print('Error parsing JSON: $e');
    }
    setLoading(false);
    update();
  }

  openDashboard() async {
    BuildContext currentContext = context;
    Navigator.pushAndRemoveUntil(
      currentContext,
      MaterialPageRoute(
        builder: (context) => const BottomBarScreen(),
      ),
          (route) => false,
    );
  }

  showMyDialog(String title, String msg, bool pFlag) async {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(
          title,
          style: const TextStyle(color: Colors.lightBlue),
        ),
        content: Text(
          msg,
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              if (pFlag) {
                Get.to(const BottomBarScreen());
              } else {
                Navigator.pop(context);
              }
            },
            child: const Text('OK', style: TextStyle(color: Colors.black38)),
          ),
        ],
      ),
    );
  }

  void getBankList() async {
    try {
      Map<String, String> mData = {
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.bankList);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        Bank bank = Bank.fromJson(jsonDecode(response.body));
        bankList.addAll(bank.bankList as Iterable<BankList>);
        print(bankList);
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void handleBackNavigation() {}
}

// import 'dart:convert';

// import 'package:custom_searchable_dropdown/custom_searchable_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:image_picker/image_picker.dart';
import 'package:pi/activity/Kyc/KycBankViewController.dart';
import '../../utils/AppUtils.dart';
import 'KycController.dart';
import 'dart:io';

class KycAdharViewController extends StatefulWidget {
  const KycAdharViewController({super.key});

  @override
  _KycAdharViewController createState() => _KycAdharViewController();
}

class _KycAdharViewController extends State<KycAdharViewController> {
  String panPath = "", adharFrontPath = '', adharBackPath = '', bankPath = '';
  final formKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime(
      DateTime.now().year - 18, DateTime.now().month, DateTime.now().day);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<KycController>(
      init: KycController(context: context),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("User KYC"),
          ),
          backgroundColor: AppUtils.backgroundColor,
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Center(
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      const SizedBox(height: 30.0),
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Row(
                          children: [
                            Icon(
                              Icons.person_pin_outlined,
                              color: Colors.green,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Aadhaar Card Detail",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        controller: _.adharController,
                        decoration: const InputDecoration(
                          labelText: 'Aadhaar Number',
                          border: OutlineInputBorder(),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                            ),
                          ),
                          labelStyle: TextStyle(color: Colors.black54),
                        ),
                        keyboardType: TextInputType.number,
                        maxLength: 12,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter Aadhaar Number';
                          }
                          RegExp adharRegex = RegExp(r'^[0-9]{12}$');
                          if (!adharRegex.hasMatch(value)) {
                            return 'Please enter a valid Aadhaar Number';
                          }
                          _.adhaarNumber = value;
                          return null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Text(
                          "Aadhaar Front Image",
                          textAlign: TextAlign.start,
                          style: TextStyle(fontSize: 12.0, color: Colors.blue),
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      InkWell(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.blueGrey,
                              style: BorderStyle.solid,
                              width: 0.5,
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Icon(
                                  Icons.attachment_rounded,
                                  color: Colors.grey,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: (_.adharFrontImg.isNotEmpty)
                                    ? const Text(
                                        "Aadhaar Front Selected",
                                        style: TextStyle(
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold),
                                      )
                                    : const Padding(
                                        padding: EdgeInsets.only(
                                            top: 20, bottom: 20),
                                        child: Text(
                                          'Select Aadhaar Front Image',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                              ),
                              Expanded(
                                child: Container(),
                              ),
                              adharFrontPath.isNotEmpty
                                  ? Row(
                                      children: [
                                        IconButton(
                                          onPressed: () {
                                            showImageDialog(
                                                context,
                                                "Aadhaar Front Image",
                                                _.adharFrontImg);
                                          },
                                          icon: const Icon(
                                            Icons.image_outlined,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        InkWell(
                                          child: const Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: Icon(
                                              Icons.clear,
                                              color: Colors.red,
                                            ),
                                          ),
                                          onTap: () {
                                            _.adharFrontImg = "";
                                            adharFrontPath = "";
                                            setState(() {});
                                          },
                                        ),
                                      ],
                                    )
                                  : const SizedBox(),
                            ],
                          ),
                        ),
                        onTap: () async {
                          _showDialog(_, true);
                        },
                      ),
                      const SizedBox(height: 10.0),
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Text(
                          "Aadhaar Back Image",
                          textAlign: TextAlign.start,
                          style: TextStyle(fontSize: 12.0, color: Colors.blue),
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      InkWell(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.blueGrey,
                              style: BorderStyle.solid,
                              width: 0.5,
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Icon(
                                  Icons.attachment_rounded,
                                  color: Colors.grey,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: (_.adharBackImg.isNotEmpty)
                                    ? const Text(
                                        "Aadhaar Back Selected",
                                        style: TextStyle(
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold),
                                      )
                                    : const Padding(
                                        padding: EdgeInsets.only(
                                            top: 20, bottom: 20),
                                        child: Text(
                                          'Select Aadhaar Back Image',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                              ),
                              Expanded(
                                child: Container(),
                              ),
                              adharBackPath.isNotEmpty
                                  ? Row(
                                      children: [
                                        IconButton(
                                          onPressed: () {
                                            showImageDialog(
                                                context,
                                                "Aadhaar Back Image",
                                                _.adharBackImg);
                                          },
                                          icon: const Icon(
                                            Icons.image_outlined,
                                            color: Colors.grey,
                                          ),
                                        ),
                                        InkWell(
                                          child: const Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: Icon(
                                              Icons.clear,
                                              color: Colors.red,
                                            ),
                                          ),
                                          onTap: () {
                                            _.adharBackImg = "";
                                            adharBackPath = "";
                                            setState(() {});
                                          },
                                        ),
                                      ],
                                    )
                                  : const SizedBox(),
                            ],
                          ),
                        ),
                        onTap: () async {
                          _showDialog(_, false);
                        },
                      ),
                      const SizedBox(height: 30.0),
                      ElevatedButton(
                        onPressed: () {
                          if (formKey.currentState!.validate()) {
                            if (adharFrontPath.isEmpty) {
                              AppUtils.showSnackBar("Adhar Card Front Document",
                                  "is mandatory!!", false);
                            } else if (adharBackPath.isEmpty) {
                              AppUtils.showSnackBar("Adhar Card Back Document",
                                  "is mandatory!!", false);
                            } else {
                              Get.to(const KycBankViewController());
                            }
                          }
                        },
                        style: ButtonStyle(
                          backgroundColor: WidgetStateProperty.all<Color>(
                              Colors.lightBlue),
                          shape:
                              WidgetStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(7.0),
                            ),
                          ),
                        ),
                        child: const Text(
                          'Next',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> _showDialog(KycController obj, bool isFront) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Choose an option'),
          children: [
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                _pickImage(obj, isFront, ImageSource.camera);
              },
              child: const Row(
                children: [
                  Icon(Icons.camera),
                  SizedBox(width: 10),
                  Text('Camera'),
                ],
              ),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                _pickImage(obj, isFront, ImageSource.gallery);
              },
              child: const Row(
                children: [
                  Icon(Icons.image),
                  SizedBox(width: 10),
                  Text('Gallery'),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> _pickImage(
      KycController obj, bool isFront, ImageSource source) async {
    final imagePicker = ImagePicker();
    final XFile? image = await imagePicker.pickImage(source: source);
    if (image == null) {
      // User canceled image capture
      return;
    } else {
      final List<String> pathParts = image.path.split('/');
      final String imageName = pathParts.last;

      if (isFront) {
        adharFrontPath = imageName;
        obj.adharFrontImg = image.path;
      } else {
        adharBackPath = imageName;
        obj.adharBackImg = image.path;
      }
    }
    setState(() {});
  }

  void showImageDialog(BuildContext context, String title, String imagePath) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.file(File(imagePath), fit: BoxFit.contain),
                const SizedBox(height: 20),
                Text(title), // You can add a text description or title here
              ],
            ),
          ),
        );
      },
    );
  }
}

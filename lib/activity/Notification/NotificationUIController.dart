import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../utils/AppUtils.dart';
import 'NotificationController.dart';

class NotificationUIController extends StatelessWidget {
  const NotificationUIController({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NotificationController>(
      init: NotificationController(),
      builder: (_) {
        return Scaffold(
          backgroundColor: AppUtils.backgroundColor,
          appBar: AppBar(
            title: const Text("Notification"),
          ),
          body:Container(

            child:
          
          _.isLoading
              ? const Center(child: CircularProgressIndicator())
              : _.productsData.data == null || _.productsData.data!.isEmpty
                  ? Center(
                      child: SvgPicture.asset(AppUtils.blankImg,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * 0.33,
                          fit: BoxFit.fill),
                    )
                  : ListView.builder(
                      itemCount: _.productsData.data!.length,
                      itemBuilder: (context, index) {
                        return  (_.productsData.data![index].title=='')||(_.productsData.data![index].title=='')?const SizedBox():Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Colors.blue.shade50.withOpacity(0.6),
                            border: Border.all( width: 0.1)),
                        //color: const Color(0xffF5F5F5)),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 10, bottom: 10),
                              child: ListTile(
                                leading: const CircleAvatar(
                                  child: Icon(
                                    Icons.circle_notifications_outlined,
                                    color: Colors.indigo,
                                  ),
                                ),
                                title: Text(
                                  _.productsData.data![index].title!,
                                  style: const TextStyle(
                                     // color: Color(0xff1FC4F4),
                                    color:Colors.blueAccent,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                                subtitle: Text(
                                  _.productsData.data![index].message!,
                                ),
                              )
                            ),
                          ),
                        );
                      },
          ),
          )
        );
      },
    );
  }
}
//Changed by Rohit «
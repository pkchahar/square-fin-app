import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../../utils/AppUtils.dart';
import '../../widgets/seperator.dart';
import '../Product/ProductUIController.dart';
import '../../Model/Wallet/NewWalletResponse.dart';
import 'WalletController.dart';

class WalletUIController extends StatelessWidget {
  String walletBalance = "0", totalEarning = "0", withdrawableBal = "0";

  WalletUIController(
      {super.key,
      required this.walletBalance,
      required this.totalEarning,
      required this.withdrawableBal});

  TextEditingController amtController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<WalletController>(
      init: WalletController(),
      builder: (_) {
        if (_.walletBalance.isEmpty) {
          _.walletBalance = walletBalance;
          _.totalEarning = totalEarning;
          _.withdrawableBal = withdrawableBal;
        } else {
          walletBalance = _.walletBalance;
          totalEarning = _.totalEarning;
          withdrawableBal = _.withdrawableBal;
        }
        return Scaffold(
            // backgroundColor: AppUtils.backgroundColor,
            appBar: AppBar(
              title: const Text("Earnings"),
            ),
            body: Stack(
              children:[
                Container(
                  height: double.infinity,
                  width:double.infinity,
                  color: AppUtils.bgColor
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Card(
                        color: const Color(0xFF0067FF),
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                        child: Container(
                          // width: MediaQuery.of(context).size.width * 0.9,
                          child: Padding(
                            padding: const EdgeInsets.all(8),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Center(
                                          child: SvgPicture.asset(
                                            'res/images/wallet.svg',
                                            height: 35,
                                            width: 35,
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const Text("Balance",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.w500)),
                                            Container(
                                              constraints: const BoxConstraints(minWidth:115 ),
                                              child: Text("₹ ${_.walletBalance}",
                                                  style: const TextStyle(
                                                    overflow: TextOverflow.visible,
                                                      color: Colors.white,
                                                      fontSize: 19,
                                                      fontWeight: FontWeight.w700)),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        InkWell(
                                            onTap: () {
                                              showModalBottomSheet<void>(
                                                context: context,
                                                builder: (BuildContext
                                                context) {
                                                  return Container(

                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                        BorderRadius
                                                            .circular(
                                                            20),
                                                        color:
                                                        Colors.white),
                                                    child: Padding(
                                                      padding:
                                                      const EdgeInsets
                                                          .all(10.0),
                                                      child: Container(
                                                        child: Column(
                                                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                          children: <Widget>[
                                                            const Text(
                                                              'Withdraw',
                                                              style: TextStyle(
                                                                  fontSize:
                                                                  15,
                                                                  color: Colors
                                                                      .lightBlue,
                                                                  fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                                  fontFamily:
                                                                  'Radio Canada'),
                                                            ),
                                                            sb(5, 0),
                                                            Text(
                                                                '₹ ${_.withdrawableBal} Available'),
                                                            sb(20, 0),
                                                            SizedBox(
                                                              width: 200,
                                                              child:
                                                              TextField(
                                                                autofocus:
                                                                true,
                                                                controller:
                                                                amtController,
                                                                keyboardType:
                                                                TextInputType
                                                                    .number,
                                                                textAlign:
                                                                TextAlign
                                                                    .center,
                                                                style: const TextStyle(
                                                                    fontSize:
                                                                    35,
                                                                    fontWeight:
                                                                    FontWeight.bold),
                                                                decoration:
                                                                InputDecoration(
                                                                  hintText:
                                                                  'Amount',
                                                                  hintStyle: TextStyle(
                                                                      color: Colors
                                                                          .grey
                                                                          .shade400),
                                                                ),
                                                              ),
                                                            ),
                                                            ElevatedButton(
                                                              onPressed:
                                                                  () {
                                                                var amt =
                                                                    amtController
                                                                        .text;

                                                                if (_.kycStatus ==
                                                                    "0") {
                                                                  AppUtils.showSnackBar(
                                                                      "KYC Status",
                                                                      "KYC mandatory for the withdrawn",
                                                                      false);
                                                                } else if (amt
                                                                    .isNotEmpty) {
                                                                  int intAmt =
                                                                  int.parse(
                                                                      amt);
                                                                  int intW =
                                                                  int.parse(
                                                                      withdrawableBal);

                                                                  if (intAmt <=
                                                                      intW) {
                                                                    amtController.text =
                                                                    "";
                                                                    _.withdrawalRequest(
                                                                        amt);
                                                                    Navigator.pop(
                                                                        context);
                                                                  } else {
                                                                    AppUtils.showSnackBar(
                                                                        "Withdrawal Amount",
                                                                        "Should be less than total wallet balance",
                                                                        false);
                                                                  }
                                                                } else {
                                                                  AppUtils.showSnackBar(
                                                                      "Withdrawal Amount",
                                                                      "Could Not be empty",
                                                                      false);
                                                                }
                                                              },
                                                              style:
                                                              ButtonStyle(
                                                                backgroundColor: WidgetStateProperty.all<
                                                                    Color>(
                                                                    Colors.green),
                                                                shape: WidgetStateProperty
                                                                    .all<
                                                                    RoundedRectangleBorder>(
                                                                  RoundedRectangleBorder(
                                                                    borderRadius:
                                                                    BorderRadius.circular(8),
                                                                  ),
                                                                ), // Set the button width to full and height to 50
                                                              ),
                                                              child:
                                                              const Text(
                                                                'Withdraw',
                                                                style:
                                                                TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                },
                                              );
                                              //adding keyboard
                                            },
                                            child: Container(margin: const EdgeInsets.only(right:5),decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(3),
                                                color:const Color(0xFF00A389)
                                            ),padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 7),child: const Center(child: Text("Withdraw",style: TextStyle(color: Colors.white),),),),),
                                        InkWell(
                                          onTap:(){
                                            Get.to(() => ProductUIController(
                                              productId: _.productId,
                                              subProductId: "",
                                            ));
                                          },
                                          child: Container(margin: const EdgeInsets.only(right:5),decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(3),
                                              color:const Color(0xFFFF5B5B)
                                          ),padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 7),child: const Center(child: Text("Earn More",style: TextStyle(color: Colors.white),),),),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                const MySeparator(
                                  color: Colors.lightBlueAccent,
                                ),
                                sb(10, 0),
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 4),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text(
                                        "Total Earnings",
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.white,fontWeight: FontWeight.w600),
                                      ),
                                      Text(
                                        '₹ ${_.totalEarning}',
                                        style: const TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                // sb(10, 0),
                                // Container(
                                //   height: 45,
                                //     // color: Colors.red,
                                //     width:
                                //         MediaQuery.of(context).size.width * 0.62,
                                //     child: Padding(
                                //         padding: const EdgeInsets.only(
                                //           left: 8.0,
                                //           right: 8,
                                //           top: 12,
                                //         ),
                                //         child: int.parse(_.withdrawableBal) != 0
                                //             ?
                                //         InkWell(
                                //               child: Container(
                                //                decoration: BoxDecoration(
                                //                  color: Colors.green,
                                //                  borderRadius: BorderRadius.only(topLeft: Radius.circular(11),topRight: Radius.circular(11))
                                //                ),
                                //                 child: Center(
                                //                   child: Text(
                                //                       // "Withdraw $withdrawableBal",
                                //                       "Withdraw",
                                //                           style:
                                //                               const TextStyle(
                                //                             color: Colors.white,fontWeight: FontWeight.w600)),
                                //                 ),
                                //               ),
                                //               onTap: () {
                                //                 showModalBottomSheet<void>(
                                //                   context: context,
                                //                   builder: (BuildContext
                                //                       context) {
                                //                     return Container(
                                //
                                //                       decoration: BoxDecoration(
                                //                           borderRadius:
                                //                               BorderRadius
                                //                                   .circular(
                                //                                       20),
                                //                           color:
                                //                               Colors.white),
                                //                       child: Padding(
                                //                         padding:
                                //                             const EdgeInsets
                                //                                 .all(10.0),
                                //                         child: Container(
                                //                           child: Column(
                                //                             // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //                             children: <Widget>[
                                //                               const Text(
                                //                                 'Withdraw',
                                //                                 style: TextStyle(
                                //                                     fontSize:
                                //                                         15,
                                //                                     color: Colors
                                //                                         .lightBlue,
                                //                                     fontWeight:
                                //                                         FontWeight
                                //                                             .w500,
                                //                                     fontFamily:
                                //                                         'Radio Canada'),
                                //                               ),
                                //                               sb(5, 0),
                                //                               Text(
                                //                                   '₹ ${_.withdrawableBal} Available'),
                                //                               sb(20, 0),
                                //                               Container(
                                //                                 width: 200,
                                //                                 child:
                                //                                     TextField(
                                //                                   autofocus:
                                //                                       true,
                                //                                   controller:
                                //                                       amtController,
                                //                                   keyboardType:
                                //                                       TextInputType
                                //                                           .number,
                                //                                   textAlign:
                                //                                       TextAlign
                                //                                           .center,
                                //                                   style: const TextStyle(
                                //                                       fontSize:
                                //                                           35,
                                //                                       fontWeight:
                                //                                           FontWeight.bold),
                                //                                   decoration:
                                //                                       InputDecoration(
                                //                                     hintText:
                                //                                         'Amount',
                                //                                     hintStyle: TextStyle(
                                //                                         color: Colors
                                //                                             .grey
                                //                                             .shade400),
                                //                                   ),
                                //                                 ),
                                //                               ),
                                //                               ElevatedButton(
                                //                                 onPressed:
                                //                                     () {
                                //                                   var amt =
                                //                                       amtController
                                //                                           .text;
                                //
                                //                                   if (_.kycStatus ==
                                //                                       "0") {
                                //                                     AppUtils.showSnackBar(
                                //                                         "KYC Status",
                                //                                         "KYC mandatory for the withdrawn",
                                //                                         false);
                                //                                   } else if (amt
                                //                                       .isNotEmpty) {
                                //                                     int intAmt =
                                //                                         int.parse(
                                //                                             amt);
                                //                                     int intW =
                                //                                         int.parse(
                                //                                             withdrawableBal);
                                //
                                //                                     if (intAmt <=
                                //                                         intW) {
                                //                                       amtController.text =
                                //                                           "";
                                //                                       _.withdrawalRequest(
                                //                                           amt);
                                //                                       Navigator.pop(
                                //                                           context);
                                //                                     } else {
                                //                                       AppUtils.showSnackBar(
                                //                                           "Withdrawal Amount",
                                //                                           "Should be less than total wallet balance",
                                //                                           false);
                                //                                     }
                                //                                   } else {
                                //                                     AppUtils.showSnackBar(
                                //                                         "Withdrawal Amount",
                                //                                         "Could Not be empty",
                                //                                         false);
                                //                                   }
                                //                                 },
                                //                                 style:
                                //                                     ButtonStyle(
                                //                                   backgroundColor: MaterialStateProperty.all<
                                //                                           Color>(
                                //                                       Colors.green),
                                //                                   shape: MaterialStateProperty
                                //                                       .all<
                                //                                           RoundedRectangleBorder>(
                                //                                     RoundedRectangleBorder(
                                //                                       borderRadius:
                                //                                           BorderRadius.circular(8),
                                //                                     ),
                                //                                   ), // Set the button width to full and height to 50
                                //                                 ),
                                //                                 child:
                                //                                     const Text(
                                //                                   'Withdraw',
                                //                                   style:
                                //                                       TextStyle(
                                //                                     color: Colors
                                //                                         .white,
                                //                                   ),
                                //                                 ),
                                //                               )
                                //                             ],
                                //                           ),
                                //                         ),
                                //                       ),
                                //                     );
                                //                   },
                                //                 );
                                //             //adding keyboard
                                //               },
                                //             )
                                //             : sb(0, 0)))
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    _.isLoading
                        ? const Expanded(
                            child: Center(child: CircularProgressIndicator()))
                        : _.monthDataList.isEmpty
                            ? Expanded(
                                child: Center(
                                  child: SvgPicture.asset(AppUtils.blankImg,
                                      width: MediaQuery.of(context).size.width,
                                      height: MediaQuery.of(context).size.height *
                                          0.33,
                                      fit: BoxFit.fill),
                                ),
                              )
                            : Expanded(
                                child: ListView.builder(
                                itemCount: _.monthDataList[0].monthData?.length,
                                itemBuilder: (context, index) {
                                  String? month = _
                                      .monthDataList[0].monthData?.keys
                                      .toList()[index];
                                  List<Transaction> transactions = _
                                      .monthDataList[0].monthData!.values
                                      .toList()[index];
                                  return Padding(
                                    padding:
                                    const EdgeInsets.only(top: 10.0, left: 14, right: 14 ),
                                    child: Container(
                                      decoration:  BoxDecoration(
                                        color:Colors.white,
                                        borderRadius: BorderRadius.circular(5)
                                      ),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(left:10,top:10,bottom: 20),
                                            child: Text(
                                              month!,
                                              style: const TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          ListView.builder(
                                            shrinkWrap: true,
                                            physics: const NeverScrollableScrollPhysics(),
                                            itemCount: transactions.length,
                                            itemBuilder: (context, index) {
                                              var transaction = transactions[index];
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10, right: 10, bottom: 5),
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Container(
                                                          height: 25,
                                                          width: 25,
                                                          decoration: BoxDecoration(
                                                            color:transaction.withdrawalStatus=="pending"?const Color(0xFFF7CB73):transaction.transactionType == "cr"?const Color(0xFF00A389):transaction.transactionType == "dr"?const Color(0xFFFF5B5B):Colors.amberAccent,
                                                            borderRadius: BorderRadius.circular(4)
                                                          ),
                                                          child: Icon(transaction.transactionType == "cr"? Icons.arrow_forward: Icons.arrow_back_outlined,color: Colors.white,size: 20,),
                                                        ),
                                                        const SizedBox(width: 5),
                                                        Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: [
                                                            Text(transaction.transactionReason?.toLowerCase().capitalizeFirst??'',style: const TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                                                            Text(transaction.date ?? '', style: const TextStyle(fontSize: 9)),
                                                          ],
                                                        ),
                                                        const Spacer(),
                                                        Text("₹ -${transaction.amount}",style: TextStyle(fontSize: 13,color: transaction.withdrawalStatus=="pending"?const Color(0xFFF7CB73):transaction.transactionType == "cr"?const Color(0xFF00A389):transaction.transactionType == "dr"?const Color(0xFFFF5B5B):Colors.amberAccent,fontWeight: FontWeight.bold)),
                                                      ],
                                                    ),
                                                    const SizedBox(height: 7),
                                                    Row(
                                                      children: [
                                                        Container(
                                                          // color:Colors.red,
                                                            constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width*0.65),
                                                            child: Text("Remark: ${transaction.remark}",style: const TextStyle(fontSize: 13),)),
                                                          const Spacer(),
                                                          Text(transaction.withdrawalStatus!,style: TextStyle(fontSize: 13,color: transaction.withdrawalStatus=="pending"?const Color(0xFFF7CB73):transaction.transactionType == "cr"?const Color(0xFF00A389):transaction.transactionType == "dr"?const Color(0xFFFF5B5B):Colors.amberAccent,decoration: TextDecoration.underline),)

                                                      ],
                                                    ),
                                                    // ListTile(
                                                    //   leading: transaction
                                                    //               .transactionType ==
                                                    //           "cr"
                                                    //       ?
                                                    //   Container(
                                                    //           width: 45,
                                                    //           height: 45,
                                                    //           decoration:
                                                    //               BoxDecoration(
                                                    //             shape: BoxShape
                                                    //                 .circle,
                                                    //             color: Colors
                                                    //                 .green
                                                    //                 .shade300,
                                                    //           ),
                                                    //           child: const Center(
                                                    //             child: Icon(
                                                    //               Icons
                                                    //                   .data_saver_on_outlined,
                                                    //               color: Colors
                                                    //                   .white,
                                                    //               size: 35,
                                                    //             ),
                                                    //           ),
                                                    //         )
                                                    //       : Container(
                                                    //           width: 45,
                                                    //           height: 45,
                                                    //           decoration: BoxDecoration(
                                                    //               shape: BoxShape
                                                    //                   .circle,
                                                    //               color: Colors
                                                    //                   .red
                                                    //                   .shade300),
                                                    //           child: const Center(
                                                    //             child: Icon(
                                                    //               Icons
                                                    //                   .data_usage_outlined,
                                                    //               color: Colors
                                                    //                   .white,
                                                    //               size: 35,
                                                    //             ),
                                                    //           ),
                                                    //         ),
                                                    //   title: Text(transaction
                                                    //           .transactionReason
                                                    //           ?.capitalize ??
                                                    //       ''),
                                                    //   subtitle: Text(
                                                    //       '${transaction.remark}'),
                                                    //   trailing: Column(
                                                    //     mainAxisAlignment:
                                                    //         MainAxisAlignment
                                                    //             .spaceAround,
                                                    //     children: [
                                                    //       transaction.transactionType ==
                                                    //               "cr"
                                                    //           ? Text(
                                                    //               '+ ₹${transaction.amount}',
                                                    //               style: TextStyle(
                                                    //                   color: Colors
                                                    //                       .green),
                                                    //             )
                                                    //           : Text(
                                                    //               '- ₹${transaction.amount}',
                                                    //               style: TextStyle(
                                                    //                   color: Colors
                                                    //                       .black)),
                                                    //       transaction.transactionType ==
                                                    //               'dr'
                                                    //           ? Flexible(
                                                    //         flex: 1,
                                                    //             child: const Text(
                                                    //                 'Debit',
                                                    //                 style: TextStyle(
                                                    //                     fontSize:
                                                    //                         10,
                                                    //                     color: Colors
                                                    //                         .grey),
                                                    //               ),
                                                    //           )
                                                    //           : Flexible(
                                                    //         flex: 1,
                                                    //             child: const Text(
                                                    //                 'Credit',
                                                    //                 style: TextStyle(
                                                    //                     fontSize:
                                                    //                         12 ,
                                                    //                     color: Colors
                                                    //                         .grey)),
                                                    //           ),
                                                    //       transaction.withdrawalStatus ==
                                                    //               'pending'
                                                    //           ? Flexible(
                                                    //         flex: 1,
                                                    //             child: Text(
                                                    //                 '${transaction.withdrawalStatus?.capitalize}',
                                                    //                 style: TextStyle(
                                                    //                   fontSize: 12,
                                                    //                     color: Colors
                                                    //                         .red),
                                                    //               ),
                                                    //           )
                                                    //           : Flexible(
                                                    //         flex: 1,
                                                    //             child: Text(
                                                    //                 '${transaction.withdrawalStatus?.capitalize}'),
                                                    //           )
                                                    //     ],
                                                    //   ),
                                                    // ),
                                                    const SizedBox(height: 10,),
                                                    index != transactions.length-1? Divider(color: Colors.blue.shade100):const SizedBox(),
                                                    // Container(
                                                    //     alignment:
                                                    //         Alignment.centerLeft,
                                                    //     child: Padding(
                                                    //       padding:
                                                    //           const EdgeInsets
                                                    //               .only(
                                                    //               top: 8.0,
                                                    //               left: 15,
                                                    //               right: 8,
                                                    //               bottom: 8),
                                                    //       child: Text(transaction
                                                    //           .date
                                                    //           .toString()),
                                                    //     ))
                                                  ],
                                                ),
                                              );
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              )),
                  ])],
            ));
      },
    );
  }

  void navigateBackAndExecuteFunction() {
    Get.back(result: "Okay");
  }
}
//
// onPressed: () {

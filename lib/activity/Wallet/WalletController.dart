import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../../../utils/AppUtils.dart';
import '../../Model/NotificationList.dart';
import '../../Model/UtilityModel/BoolResponse.dart';
import '../../Model/Wallet/Wallet.dart';
import '../../Model/Wallet/NewWalletResponse.dart';

class WalletController extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;
  NotificationList productsData = NotificationList();
  String userId = "", token = "", userType = "", version = "";
  String kycStatus = "0",
      loginFiles = "0",
      totalLead = "0",
      revenue = "0",
      sanctioned = "0",
      disbursed = "0",
      business = "0",
      walletBalance = "",
      totalEarning = "0",
      productId = "",
      withdrawableBal = "0";

  // List<TransactionData> productList = [];
  List<MonthData> monthDataList = [];
  List<MonthData> monthDataListDouble = [];

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void getWalletTransaction(String userId) async {
    setLoading(true);
    try {
      Map<String, String> mData = {
        'Login_User_Id': userId,
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.walletTransaction1);
      var response = await http.post(url, body: mData);
      print(url);
      print(mData);
      print(response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        NewWalletResponse myResponse = NewWalletResponse.fromJson(responseJson);
        if (myResponse.data != null) {
          monthDataList = myResponse.data!;
          print(monthDataList.length);
          print('elephant');
          // print(monthDataList[0]);
          // print(monthDataList[1]);

          // for (int i = 0; i < monthDataList.length; i++) {
          //   for (var entry in monthDataList) {
          //     var monthName = entry.monthData?.entries.first;
          //     var monthName1 = entry.monthData?.entries.last;
          //     print('2222222');
          //     print(monthName);
          //     print(monthName1);
          //   }
          //
          //   var monthData = monthDataList[i].monthData;
          //   // print(monthData);
          //   // print("month2222222");
          // }
          // for (var entry in monthDataList) {
          //   var monthEntries = entry.monthData?.entries;
          //   if (monthEntries != null) {
          //     for (var monthEntry in monthEntries) {
          //       var monthName = monthEntry.key;
          //       var transactions = monthEntry.value;
          //       print('33333333');
          //       print('Month: $monthName, Transactions: $transactions');
          //     }
          //   }
          // }

          monthDataListDouble = myResponse.data!;
          print('********');
          print(monthDataList[0].monthData?.length);


        }
        // update();
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
    // Now you can use monthDataList to populate your ListView.builder
  }

  void withdrawalRequest(String amt) async {
    setLoading(true);
    try {
      Map<String, String> mData = {
        'Login_User_Id': userId,
        'amount': amt,
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.withdrawal);
      var response = await http.post(url, body: mData);
      print(url);
      print(mData);
      print(response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        BoolResponse myResponse = BoolResponse.fromJson(responseJson);
        AppUtils.showSnackBar(
            "Withdrawal Request", myResponse.message, myResponse.status);
        getWalletBala();
        update();
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
  }

  void getWalletBala() async {
    try {
      Map<String, String> mData = {
        'Login_User_Id': userId,
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.wallet);
      var response = await http.post(url, body: mData);

      print(url);
      print(mData);
      print(response.body);

      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        Wallet myResponse = Wallet.fromJson(responseJson);
        var status = myResponse.status ?? false;
        if (status) {
          totalEarning = myResponse.data?.totalEarning as String;
          withdrawableBal = myResponse.data?.withdrawableBalance as String;
          walletBalance = myResponse.data?.walletBalance as String;
          print(totalEarning);
          print(withdrawableBal);
          print(walletBalance);
        }
        getWalletTransaction(userId);
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    kycStatus = preferences.getString(AppUtils.kycStatus) ?? "0";
    productId = preferences.getString(AppUtils.productId) ?? "0";
    print(token);
    version = preferences.getString(AppUtils.appVersion) ?? "";
    getWalletTransaction(userId);
  }

  void navigateBackAndExecuteFunction() {
    Get.back(result: "Okay");
  }
}

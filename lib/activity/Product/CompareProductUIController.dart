import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:printing/printing.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../Model/Product/ListingProduct.dart';
import 'CompareProductController.dart';
import 'package:pdf/widgets.dart' as pw;

class CompareProductUIController extends StatelessWidget {
  List<ListingProduct> selectedList = [];

  CompareProductUIController({super.key, required this.selectedList});

  Future<void> convertAndSharePDF(List<ListingProduct> data) async {
    final fallbackFont =
        pw.Font.ttf(await rootBundle.load("res/fonts/RobotoMono-Light.ttf"));
    double globalRemainingHeight = 200;
    final netImage = await networkImage(selectedList[0].icon);
    final netImage1 = await networkImage(selectedList[1].icon);
    final pdf = pw.Document();
    pdf.addPage(pw.Page(
        build: (pw.Context context) => pw.ListView(children: [
              pw.Column(children: [
                pw.Row(children: [
                  pw.Image(netImage, width: 400, height: 200),
                  pw.SizedBox(width: 50),
                  pw.Image(netImage1, width: 400, height: 200)
                ]),
                pw.Divider(thickness: 1, color: PdfColors.black),
                pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.spaceEvenly,
                    children: [
                      pw.Container(
                          width: 200,
                          child: pw.Flexible(
                            child: pw.Text(
                              selectedList[0].name.replaceAll('\\n ', '\n'),
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                            ),
                          )),
                      pw.SizedBox(width: 100),
                      pw.Container(
                          width: 200,
                          child: pw.Flexible(
                            child: pw.Text(
                              selectedList[1].name.replaceAll('\\n ', '\n'),
                              style: pw.TextStyle(
                                fontSize: 15,
                                fontWeight: pw.FontWeight.bold,
                              ),
                            ),
                          ))
                    ]),
                pw.Divider(thickness: 1, color: PdfColors.black),
                pw.SizedBox(height: 50),
                pw.Container(
                  decoration: pw.BoxDecoration(
                      border: pw.Border.all(width: 0.5),
                      borderRadius: pw.BorderRadius.circular(11)),
                  child: pw.Column(children: [
                    pw.Row(
                      children: [
                        pw.Expanded(
                          child: pw.Container(
                            decoration: const pw.BoxDecoration(
                              color: PdfColors.blue200,
                              borderRadius: pw.BorderRadius.only(
                                  topLeft: pw.Radius.circular(11),
                                  topRight: pw.Radius.circular(11)),
                            ),
                            padding: const pw.EdgeInsets.all(8.0),
                            child: pw.Text(
                              "Joining Fees",
                              textAlign: pw.TextAlign.center,
                              style: pw.TextStyle(
                                fontWeight: pw.FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ]),
                ),
                pw.SizedBox(height: 20),
                pw.Container(
                    decoration: const pw.BoxDecoration(
                        color: PdfColors.white,
                        borderRadius: pw.BorderRadius.only(
                          bottomLeft: pw.Radius.circular(11),
                          bottomRight: pw.Radius.circular(11),
                        )),
                    child: pw.Column(children: [
                      pw.SizedBox(height: 10),
                      pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.spaceEvenly,
                        children: [
                          selectedList[0].joiningFee.isNotEmpty
                              ? pw.Text('N/A')
                              : pw.Flexible(
                                  child: pw.Text(
                                    selectedList[0]
                                        .joiningFee
                                        .replaceAll('\\n ', '\n'),
                                  ),
                                ),
                          pw.SizedBox(width: 7),
                          pw.Flexible(
                            child: pw.Text(
                              selectedList[1]
                                  .joiningFee
                                  .replaceAll('\\n ', '\n'),
                            ),
                          ),
                        ],
                      ),
                    ])),
                pw.SizedBox(height: 30),
                pw.Container(
                  decoration: pw.BoxDecoration(
                      border: pw.Border.all(width: 0.5),
                      borderRadius: pw.BorderRadius.circular(11)),
                  child: pw.Column(
                    children: [
                      pw.Row(
                        children: [
                          pw.Expanded(
                            child: pw.Container(
                              decoration: const pw.BoxDecoration(
                                color: PdfColors.blue200,
                                borderRadius: pw.BorderRadius.only(
                                    topRight: pw.Radius.circular(11),
                                    topLeft: pw.Radius.circular(11)),
                              ),
                              padding: const pw.EdgeInsets.all(8.0),
                              child: pw.Text(
                                "Annual Fees",
                                textAlign: pw.TextAlign.center,
                                style: pw.TextStyle(
                                  fontWeight: pw.FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      pw.Container(
                        decoration: const pw.BoxDecoration(
                            borderRadius: pw.BorderRadius.only(
                                bottomRight: pw.Radius.circular(11),
                                bottomLeft: pw.Radius.circular(11)),
                            color: PdfColors.white),
                        child: pw.Column(
                          children: [
                            pw.SizedBox(height: 10, width: 10),
                            pw.Row(
                              mainAxisAlignment:
                                  pw.MainAxisAlignment.spaceEvenly,
                              children: [
                                pw.Text(
                                  selectedList[0].annualFee,
                                  style: const pw.TextStyle(),
                                ),
                                pw.SizedBox(width: 7),
                                pw.Text(
                                  selectedList[1].annualFee,
                                  style: const pw.TextStyle(),
                                ),
                              ],
                            ),
                            pw.SizedBox(height: 30)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                pw.SizedBox(height: 30),
                pw.SizedBox(
                  height: 30,
                ),
              ]),
            ])));
    pdf.addPage(pw.Page(build: (pw.Context context) {
      double remainingHeight = context.page.pageFormat.height;
      globalRemainingHeight = remainingHeight;
      print('elephantxx');
      print(globalRemainingHeight);
      return pw.ListView(children: [
        pw.Container(
          decoration: pw.BoxDecoration(
              border: pw.Border.all(width: 0.5),
              borderRadius: pw.BorderRadius.circular(11)),
          child: pw.Column(
            children: [
              pw.Row(
                children: [
                  pw.Expanded(
                    child: pw.Container(
                      decoration: const pw.BoxDecoration(
                        color: PdfColors.blue200,
                        borderRadius: pw.BorderRadius.only(
                            topLeft: pw.Radius.circular(11),
                            topRight: pw.Radius.circular(11)),
                      ),
                      padding: const pw.EdgeInsets.all(8.0),
                      child: pw.Text(
                        "Fees and Charges",
                        textAlign: pw.TextAlign.center,
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              pw.Container(
                decoration: const pw.BoxDecoration(
                    borderRadius: pw.BorderRadius.only(
                        bottomRight: pw.Radius.circular(11),
                        bottomLeft: pw.Radius.circular(11)),
                    color: PdfColors.white),
                child: pw.Column(
                  children: [
                    pw.SizedBox(height: 10),
                    pw.Container(
                      child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: pw.CrossAxisAlignment.start,
                        children: [
                          pw.Flexible(
                            child: pw.Padding(
                              padding: const pw.EdgeInsets.only(
                                  top: 8.0, left: 8, right: 4),
                              child: pw.Text(
                                selectedList[0]
                                    .feesCharges
                                    .replaceAll('\\n ', '\n')
                                    .replaceAll('\n', '\n\n'),
                                textAlign: pw.TextAlign.start,
                                style: const pw.TextStyle(),
                              ),
                            ),
                          ),
                          pw.SizedBox(height: 10),
                          pw.Flexible(
                            child: pw.Padding(
                              padding: const pw.EdgeInsets.only(
                                  top: 8.0, left: 4, right: 8),
                              child: pw.Text(
                                selectedList[1]
                                    .feesCharges
                                    .replaceAll('\\n ', '\n')
                                    .replaceAll('\n', '\n\n')
                                    .replaceAll('₹', 'Rs.')
                                    .replaceAll('✔', '*'),
                                textAlign: pw.TextAlign.start,
                                style: const pw.TextStyle(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    pw.SizedBox(height: 30)
                  ],
                ),
              )
            ],
          ),
        ),
        pw.SizedBox(height: 50),
      ]);
    }));
    //if(globalRemainingHeight < 20)
    pdf.addPage(pw.Page(build: (pw.Context context) {
      return pw.ListView(children: [
        pw.SizedBox(height: 50),
        pw.Container(
          decoration: pw.BoxDecoration(
              border: pw.Border.all(width: 0.5),
              borderRadius: pw.BorderRadius.circular(11)),
          child: pw.Column(
            children: [
              pw.Row(
                children: [
                  pw.Expanded(
                    child: pw.Container(
                      decoration: const pw.BoxDecoration(
                        color: PdfColors.blue200,
                        borderRadius: pw.BorderRadius.only(
                            topRight: pw.Radius.circular(11),
                            topLeft: pw.Radius.circular(11)),
                      ),
                      padding: const pw.EdgeInsets.all(8.0),
                      child: pw.Text(
                        "Features",
                        textAlign: pw.TextAlign.center,
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              pw.Container(
                decoration: const pw.BoxDecoration(
                    color: PdfColors.white,
                    borderRadius: pw.BorderRadius.only(
                        bottomLeft: pw.Radius.circular(11),
                        bottomRight: pw.Radius.circular(11))),
                child: pw.Column(
                  children: [
                    pw.SizedBox(height: 5),
                    pw.Container(
                      child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: pw.CrossAxisAlignment.start,
                        children: [
                          pw.Flexible(
                            child: pw.Column(
                              children: [
                                pw.Padding(
                                  padding: const pw.EdgeInsets.only(
                                      top: 4, left: 3, right: 2),
                                  child: pw.Text(
                                    selectedList[0]
                                        .features
                                        .replaceAll('\\n ', '\n')
                                        .replaceAll('\n ', '\n\n')
                                        .replaceAll('₹', 'Rs. ')
                                        .replaceAll('✔', '*'),
                                    style: pw.TextStyle(
                                      fontSize: 7,
                                      fontFallback: [fallbackFont],
                                    ),
                                  ),
                                ),
                                pw.SizedBox(height: 5)
                              ],
                            ),
                          ),
                          pw.SizedBox(width: 7),
                          pw.Flexible(
                            child: pw.Padding(
                              padding: const pw.EdgeInsets.only(
                                  top: 8.0, right: 8, left: 4),
                              child: pw.Text(
                                selectedList[1]
                                    .features
                                    .replaceAll('\\n ', '\n')
                                    .replaceAll('\n', '\n\n'),
                                style: const pw.TextStyle(fontSize: 10),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    pw.SizedBox(height: 30)
                  ],
                ),
              ),
            ],
          ),
        ),
      ]);
    }));
    final output = await getTemporaryDirectory();
    final filePath = '${output.path}/compare.pdf';
    final file = File(filePath);
    await file.writeAsBytes(await pdf.save());
    sharePdf(filePath, pdf);
    print('File path: $filePath');
  }

  Future<void> sharePdf(String pdfPath, pw.Document pdf) async {
    final file = File(pdfPath);
    await file.writeAsBytes(await pdf.save());

    if (file.existsSync()) {
      // Share the PDF using share_plus
      await Share.shareXFiles([XFile(pdfPath)], text: 'Sharing PDF file');
    } else {
      print('File does not exist');
    }
  }


  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompareProductController>(
      init: CompareProductController(),
      builder: (_) {
        return Scaffold(
          backgroundColor: Colors.grey.shade200,
          appBar: AppBar(
            title: const Text("Compare Products"),
            actions: [
              IconButton(
                  onPressed: () {
                    convertAndSharePDF(selectedList);
                  },
                  icon: const Icon(Icons.arrow_circle_down_rounded))
            ],
          ),
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Card(
                            elevation: 1,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(11),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: Image.network(
                                selectedList[0].icon,
                                fit: BoxFit.fill,
                                height: 100,
                                errorBuilder: (context, error, stackTrace) {
                                  return Image.asset('res/images/no_image.png');
                                },
                              ),
                            ),
                          ),
                          Card(
                            elevation: 1,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(11),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: Image.network(
                                selectedList[1].icon,
                                fit: BoxFit.fill,
                                height: 100,
                                errorBuilder: (context, error, stackTrace) {
                                  return Image.asset('res/images/no_image.png');
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      //  sb(10, 0),
                      const Divider(
                        height: 30,
                        color: Colors.blueGrey,
                        thickness: 2,
                      ),
                      //sb(10, 0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Flexible(
                            child: Text(
                              selectedList[0].name.replaceAll('\\n ', '\n'),
                              style: const TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          //sb(0, 3.5),
                          //  Container(height: 40,width: 1,color: Colors.red,),
                          // sb(0, 3.5),
                          Flexible(
                            child: Text(
                              selectedList[1].name.replaceAll('\\n ', '\n'),
                              style: const TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const Divider(
                        height: 30,
                        color: Colors.blueGrey,
                        thickness: 2,
                      ),
                      sb(10, 0),
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(width: 0.5),
                            borderRadius: BorderRadius.circular(11)),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.blue.shade200,
                                      borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(11),
                                          topRight: Radius.circular(11)),
                                    ),
                                    padding: const EdgeInsets.all(8.0),
                                    child: const Text(
                                      "Joining Fees",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(11),
                                    bottomRight: Radius.circular(11),
                                  )),
                              child: Column(
                                children: [
                                  sb(10, 0),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Flexible(
                                        child: Text(
                                          selectedList[0]
                                              .joiningFee
                                              .replaceAll('\\n ', '\n'),
                                        ),
                                      ),
                                      sb(0, 7),
                                      Flexible(
                                        child: Text(
                                          selectedList[1]
                                              .joiningFee
                                              .replaceAll('\\n ', '\n'),
                                        ),
                                      ),
                                    ],
                                  ),
                                  sb(30, 0),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      sb(30, 0),

                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(width: 0.5),
                            borderRadius: BorderRadius.circular(11)),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.blue.shade200,
                                      borderRadius: const BorderRadius.only(
                                          topRight: Radius.circular(11),
                                          topLeft: Radius.circular(11)),
                                    ),
                                    padding: const EdgeInsets.all(8.0),
                                    child: const Text(
                                      "Annual Fees",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(11),
                                      bottomLeft: Radius.circular(11)),
                                  color: Colors.white),
                              child: Column(
                                children: [
                                  sb(10, 10),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      selectedList[0].annualFee.isEmpty
                                          ? const Text('N/A')
                                          : Text(
                                              selectedList[0].annualFee,
                                              style: const TextStyle(),
                                            ),
                                      sb(0, 7),
                                      Text(
                                        selectedList[1].annualFee,
                                        style: const TextStyle(),
                                      ),
                                    ],
                                  ),
                                  sb(30, 0),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      sb(30, 0),

                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(width: 0.5),
                            borderRadius: BorderRadius.circular(11)),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.blue.shade200,
                                      borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(11),
                                          topRight: Radius.circular(11)),
                                    ),
                                    padding: const EdgeInsets.all(8.0),
                                    child: const Text(
                                      "Fees and Charges",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(11),
                                      bottomLeft: Radius.circular(11)),
                                  color: Colors.white),
                              child: Column(
                                children: [
                                  sb(10, 0),
                                  Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Flexible(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8.0, left: 8, right: 4),
                                            child: Text(
                                              selectedList[0]
                                                  .feesCharges
                                                  .replaceAll('\\n ', '\n')
                                                  .replaceAll('\n', '\n\n'),
                                              textAlign: TextAlign.start,
                                              style: const TextStyle(),
                                            ),
                                          ),
                                        ),
                                        sb(0, 7),
                                        Flexible(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8.0, left: 4, right: 8),
                                            child: Text(
                                              selectedList[1]
                                                  .feesCharges
                                                  .replaceAll('\\n ', '\n')
                                                  .replaceAll('\n', '\n\n'),
                                              textAlign: TextAlign.start,
                                              style: const TextStyle(),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  sb(30, 0),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      sb(30, 0),
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(width: 0.5),
                            borderRadius: BorderRadius.circular(11)),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.blue.shade200,
                                      borderRadius: const BorderRadius.only(
                                          topRight: Radius.circular(11),
                                          topLeft: Radius.circular(11)),
                                    ),
                                    padding: const EdgeInsets.all(8.0),
                                    child: const Text(
                                      "Features",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(11),
                                      bottomRight: Radius.circular(11))),
                              child: Column(
                                children: [
                                  sb(10, 0),
                                  Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Flexible(
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8.0,
                                                    left: 8,
                                                    right: 4),
                                                child: Text(
                                                  selectedList[0]
                                                      .features
                                                      .replaceAll('\\n ', '\n')
                                                      .replaceAll(
                                                          '\n ', '\n\n'),
                                                  style: const TextStyle(),
                                                ),
                                              ),
                                              sb(5, 0),
                                            ],
                                          ),
                                        ),
                                        sb(0, 7),
                                        Flexible(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8.0, right: 8, left: 4),
                                            child: Text(
                                              selectedList[1]
                                                  .features
                                                  .replaceAll('\\n ', '\n')
                                                  .replaceAll('\n', '\n\n'),
                                              style: const TextStyle(),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  sb(30, 0),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      sb(30, 0),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _openYoutubeApp(String youtubeLink) async {
    final Uri url = Uri.parse(youtubeLink);

    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  void showImageDialog(BuildContext context, String imagePath) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.network(
                  imagePath,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      'res/images/no_image.png',
                      fit: BoxFit.contain,
                    );
                  },
                ), // You can add a text description or title here
              ],
            ),
          ),
        );
      },
    );
  }

  String? extractVideoIdFromUrl(String youtubeUrl) {
    Uri uri = Uri.parse(youtubeUrl);
    String? videoId = '';

    if (uri.queryParameters.containsKey('v')) {
      videoId = uri.queryParameters['v'];
    } else {
      List<String> pathSegments = uri.pathSegments;
      if (pathSegments.contains('watch') && pathSegments.length >= 2) {
        int index = pathSegments.indexOf('watch');
        if (index < pathSegments.length - 1) {
          videoId = pathSegments[index + 1];
        }
      }
    }
    return videoId;
  }
}

class HorizontalList1 extends StatelessWidget {
  CompareProductController obj;

  HorizontalList1({super.key, required this.obj});

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    const borderSide = BorderSide(
      width: 2,
      color: Colors.deepPurple,
    );
    const borderGSide = BorderSide(
      width: 1,
      color: Colors.grey,
    );
    return ListView(
      controller: _scrollController,
      scrollDirection: Axis.horizontal,
      children:
          obj.myHeaderList.asMap().entries.map((MapEntry<int, String> entry) {
        String item = entry.value;
        int index = entry.key;

        return GestureDetector(
          onTap: () {
            if (obj.selectedIdx != index) {
              obj.selectedIdx = index;
              double offset = index * 200.0 -
                  (_scrollController.position.viewportDimension / 2) +
                  100;

              _scrollController.animateTo(
                offset,
                duration: const Duration(milliseconds: 600),
                curve: Curves.ease,
              );
            }
            obj.update();
          },
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin:
                  const EdgeInsets.only(top: 0, right: 0, bottom: 0, left: 0),
              decoration: BoxDecoration(
                border: Border(
                  bottom: obj.selectedIdx == index ? borderSide : borderGSide,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, bottom: 0),
                child: Center(
                  child: Text(
                    item.toString(),
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey.shade800,
                        fontWeight: obj.selectedIdx == index
                            ? FontWeight.w500
                            : FontWeight.normal),
                  ),
                ),
              ),
            ),
          ),
        );
      }).toList(),
    );
  }
}

// Future<void> convertAndSharePDF(List<ListingProduct> data) async {
//   final pdf = pw.Document();
//   pdf.addPage(
//     pw.Page(
//       build: (context) => pw.ListView(
//         // children: data.map((item) => pw.Text(item.name)).toList(),
//           children: data.map((item) => pw.Text(item.name)).toList()+
//               data.map((item) => pw.Text(item.companyName)).toList()+
//               data.map((item) => pw.Text(item.status)).toList()+
//               data.map((item) => pw.Text(item.annualFee)).toList()
//       ),
//     ),
//   );
//
//   final output = await getTemporaryDirectory();
//   final filePath = '${output.path}/example.pdf';
//   final file = File(filePath);
//   await file.writeAsBytes(await pdf.save());
//   sharePdf(filePath, pdf);
//   //sharePdf(filePath);
//   //Share.shareFiles([filePath], text: 'Sharing PDF');
//   print('File path: $filePath');
// }

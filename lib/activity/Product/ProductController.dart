import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../utils/AppUtils.dart';
import '../../Model/NotificationList.dart';
import '../../Model/Product/ListingProduct.dart';
import '../../Model/Product/ProductResponse.dart';

class ProductController extends GetxController {
  bool _isLoading = true;

  bool get isLoading => _isLoading;
  NotificationList productsData = NotificationList();
  String userId = "",
      token = "",
      userType = "",
      version = "",
      productName = "",
      productId = "",
      subProductId = "",
      selectedItemName = "";
  List<ListingProduct> leadProducts = [];
  List<ListingProduct> selectedList = [];
  List<ProductData> subProductList = [];
  late ListingProduct item;

  int selectedIdx = 0;

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void getProducts() async {
    try {
      setLoading(true);
      Map<String, String> mData = {
        'userId': userId,
        'category': subProductId,
      };
      var url = Uri.parse(AppUtils.baseUrlNew + AppUtils.listingProduct);
      var response = await http.post(url, body: mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        if (leadProducts.isNotEmpty) {
          leadProducts.clear();
        }

        Map<String, dynamic> responseJson = json.decode(response.body);
        ListingProductResponse myResponse =
            ListingProductResponse.fromJson(responseJson);
        if (myResponse.data.isNotEmpty) {
          leadProducts.addAll(myResponse.data);
          if (leadProducts.isNotEmpty) {
            item = leadProducts[0];
          }
        }
      }
      setLoading(false);
      update();
    } catch (e) {
      print(e);
    }
  }

  void getSub() async {
    try {
      var url =
          Uri.parse('${AppUtils.baseUrlNew}${AppUtils.getSubCategory}$productId/$userId');
          // Uri.parse(AppUtils.baseUrl + AppUtils.getSubCategory + productId + userId);
      var response = await http.get(url);
      print(productId);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        ProductResponse myResponse = ProductResponse.fromJson(responseJson);
        subProductList.addAll(myResponse.data);

        if (subProductList.isNotEmpty) {
          if (subProductId.isEmpty) {
            subProductId = subProductList[0].id;
          }else{
            selectedIdx = getIndex1();
          }
          getProducts();
        }
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  int getIndex1() {
    for (var index = 0; index < subProductList.length; index++){
      print(subProductList[index].id);
      if(subProductId == subProductList[index].id){
        selectedIdx = index;
        print('selected $selectedIdx' );
        return index;
      }
    }
    return 0;
  }
  void setScroller(ScrollController scrollController, ProductController obj) {
    obj.subProductId = item.id;
    obj.getProducts();
    double offset = selectedIdx * 165.0 -
        (scrollController.position.viewportDimension / 4) +
        100;

    scrollController.animateTo(
      offset,
      duration: const Duration(milliseconds: 500),
      curve: Curves.ease,
    );
  }

  void updateData() {
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    productId = preferences.getString(AppUtils.productId) ?? "";
    subProductId = preferences.getString(AppUtils.subProductId) ?? "";
    productName = preferences.getString(AppUtils.productName) ?? "Products";
    getSub();
  }
}

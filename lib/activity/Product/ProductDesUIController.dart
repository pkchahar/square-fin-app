import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:pi/Model/Product/ProductResponse.dart';
import 'package:pi/utils/WebviewController.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:youtube/youtube_thumbnail.dart';

import '../../Model/Product/ListingProduct.dart';
import '../../utils/AppUtils.dart';
import 'ProductController.dart';
import 'ProductDesController.dart';

class ProductDesUIController extends StatelessWidget {
  ListingProduct item;

  ProductDesUIController({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductDesController>(
      init: ProductDesController(),
      builder: (_) {
        var youtubeVideoId = extractVideoIdFromUrl(item.trainingLink ?? '');
        return Scaffold(
          backgroundColor: AppUtils.backgroundColor,
          appBar: AppBar(
            title: Text(item.name),
          ),
          body: Stack(
            children: [
              Container(color: AppUtils.bgColor,
                height:double.infinity,
                width: double.infinity,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SingleChildScrollView(
                  child: Card(
                    elevation: 0,
                    child: Column(
                      children: <Widget>[
                        Card(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Container(
                              child:
                              // ListTile(
                              //   contentPadding: EdgeInsets.zero,
                              //   leading:
                              //   Image.network(
                              //     item.icon,
                              //     fit: BoxFit.fill,
                              //     height: 200,
                              //     width: 80,
                              //     errorBuilder: (context, error, stackTrace) {
                              //       return Image.asset('res/images/no_image.png');
                              //     },
                              //   ),
                              //   title:
                              //   Text(
                              //     item.name,
                              //     style: const TextStyle(
                              //       fontSize: 15,
                              //       fontWeight: FontWeight.bold,
                              //     ),
                              //   ),
                              //   subtitle: Column(
                              //     crossAxisAlignment: CrossAxisAlignment.start,
                              //     children: [
                              //       Text(
                              //         item.companyName,
                              //         style: const TextStyle(
                              //           fontSize: 12,
                              //         ),
                              //       ),
                              //       // const SizedBox(height: 5),
                              //       Text(
                              //         item.shortDescription,
                              //         style: const TextStyle(
                              //           fontSize: 10,
                              //         ),
                              //         maxLines: 3,
                              //         overflow: TextOverflow.ellipsis,
                              //       ),
                              //     ],
                              //   ),
                              // ),
                              Row(children: [
                                Image.network(
                                  item.icon,
                                  fit: BoxFit.fill,
                                  height: 80,
                                  width: 80,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Image.asset('res/images/no_image.png');
                                  },
                                ),
                                Container(
                                  margin: const EdgeInsets.symmetric(horizontal: 15),
                                  color:Colors.blue.shade100,
                                  height: 50,
                                  width: 1,
                                ),
                                Flexible(
                                  child: Text(
                                    item.name,
                                    style: const TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],)
                          ),
                        ),
                        SizedBox(
                          height: 40,
                          child: HorizontalList1(
                            obj: _,
                          ),
                        ),
                        // const SizedBox(
                        //   height: 12,
                        // ),
                        _.selectedIdx == 0
                            ? Column(
                          children: [
                            Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.topLeft,
                                      child: const Text(
                                        "Features",
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    item.features.isNotEmpty
                                        ? Text(
                                      " ${item.features.replaceAll(
                                              '\\n', '\n')}",
                                    )
                                        : const Text(
                                      "NA",
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            // const SizedBox(
                            //   height: 12,
                            // ),
                            goodDiv(),
                            Container(
                              // elevation: 0,
                              // shape: RoundedRectangleBorder(
                              //   borderRadius: BorderRadius.circular(10.0),
                              // ),
                              child: Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.topLeft,
                                      child: const Text(
                                        "Instructions",
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    item.instructions.isNotEmpty
                                        ? Text(
                                      " ${item.instructions.replaceAll('\\n', '\n')}",
                                    )
                                        : Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(
                                            20.0),
                                        child: SvgPicture.asset(
                                            AppUtils.blankImg,
                                            width:
                                            MediaQuery.of(context)
                                                .size
                                                .width,
                                            height:
                                            MediaQuery.of(context)
                                                .size
                                                .height *
                                                0.33,
                                            fit: BoxFit.fill),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            goodDiv(),
                            Container(
                              // elevation: 0,
                              // shape: RoundedRectangleBorder(
                              //   borderRadius: BorderRadius.circular(10.0),
                              // ),
                              child: Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.topLeft,
                                      child: const Row(
                                        children: [
                                          Icon(
                                            Icons.currency_rupee_rounded,
                                            color: Colors.red,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            "Fees Charges",
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Container(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        item.feesCharges
                                            .replaceAll('\\n ', '\n'),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                            : _.selectedIdx == 1
                            ? Column(
                          children: [
                            Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.circular(10.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.topLeft,
                                      child: const Text(
                                        "YouTube Videos",
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight:
                                          FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    item.trainingLink.isNotEmpty
                                        ? InkWell(
                                        onTap: () {
                                          _openYoutubeApp(
                                              item.trainingLink ??
                                                  '');
                                        },
                                        child: Stack(
                                          alignment:
                                          Alignment.center,
                                          children: [
                                            Image.network(
                                              YoutubeThumbnail(
                                                  youtubeId:
                                                  '$youtubeVideoId')
                                                  .standard(),
                                              errorBuilder:
                                                  (context, error,
                                                  stackTrace) {
                                                return Image.asset(
                                                  'res/images/no_image.png',
                                                  fit: BoxFit
                                                      .contain,
                                                );
                                              },
                                            ),
                                            item.trainingLink
                                                .isNotEmpty
                                                ? const Icon(
                                              Icons
                                                  .play_circle_filled,
                                              size: 60,
                                              color:
                                              Colors.red,
                                            )
                                                : sb(0, 0),
                                          ],
                                        ))
                                        : Center(
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.all(
                                            20.0),
                                        child: SvgPicture.asset(
                                            AppUtils.blankImg,
                                            width: MediaQuery.of(
                                                context)
                                                .size
                                                .width,
                                            height: MediaQuery.of(
                                                context)
                                                .size
                                                .height *
                                                0.33,
                                            fit: BoxFit.fill),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                            : _.selectedIdx == 2
                            ? Column(
                          children: [
                            Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.circular(10.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(18.0),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.topLeft,
                                      child: const Text(
                                        "Banner",
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight:
                                          FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    item.multipleBanar.isNotEmpty
                                        ? SizedBox(
                                      height: 380,
                                      child: ListView.builder(
                                        itemCount: item
                                            .multipleBanar
                                            .length,
                                        itemBuilder:
                                            (BuildContext
                                        context,
                                            int index) {
                                          return GestureDetector(
                                            onTap: () {
                                              String path =
                                                  'https://www.squarefin.in/api/upload/icon/${item.multipleBanar[index]}';
                                              // showImageDialog(
                                              //     context, path);

                                              showModalBottomSheet<
                                                  void>(
                                                context:
                                                context,
                                                builder:
                                                    (BuildContext
                                                context) {
                                                  return Container(
                                                    decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(
                                                            20),
                                                        color:
                                                        Colors.white),
                                                    child:
                                                    Padding(
                                                      padding: const EdgeInsets
                                                          .all(
                                                          0.0),
                                                      child:
                                                      Column(
                                                        children: <Widget>[
                                                          Image.network(
                                                            path,
                                                            fit: BoxFit.cover,
                                                            errorBuilder: (context, error, stackTrace) {
                                                              return Image.asset(
                                                                'res/images/no_image.png',
                                                                fit: BoxFit.contain,
                                                              );
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                            child: Padding(
                                              padding:
                                              const EdgeInsets
                                                  .all(
                                                  8.0),
                                              child:
                                              Container(
                                                decoration:
                                                BoxDecoration(
                                                  border:
                                                  Border
                                                      .all(
                                                    color: Colors
                                                        .grey[
                                                    300]!,
                                                    width:
                                                    1.0,
                                                  ),
                                                ),
                                                child:
                                                ClipRRect(
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      8.0),
                                                  child: Image
                                                      .network(
                                                    'https://www.squarefin.in/api/upload/icon/${item.multipleBanar[index]}',
                                                    fit: BoxFit
                                                        .none,
                                                    errorBuilder: (context,
                                                        error,
                                                        stackTrace) {
                                                      return Image
                                                          .asset(
                                                        'res/images/no_image.png',
                                                        fit: BoxFit
                                                            .contain,
                                                      );
                                                    },
                                                  ),
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                        : Center(
                                      child: Padding(
                                        padding:
                                        const EdgeInsets
                                            .all(20.0),
                                        child: SvgPicture.asset(
                                            AppUtils.blankImg,
                                            width:
                                            MediaQuery.of(
                                                context)
                                                .size
                                                .width,
                                            height: MediaQuery.of(
                                                context)
                                                .size
                                                .height *
                                                0.33,
                                            fit: BoxFit.fill),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                            : _.selectedIdx == 3
                            ? Column(
                          children: [
                            Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.circular(10.0),
                              ),
                              child: Padding(
                                padding:
                                const EdgeInsets.all(18.0),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment:
                                      Alignment.topLeft,
                                      child: const Row(
                                        children: [
                                          Image(image: AssetImage('res/images/profile/credit-card.png')),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            "Credit Score",
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight:
                                              FontWeight
                                                  .bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      children: [
                                        const IntrinsicWidth(
                                          stepWidth: 160,
                                          child: Text(
                                              'Minimum Credit Score:',
                                              style: TextStyle(
                                                  fontWeight:
                                                  FontWeight
                                                      .bold)),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: Text(
                                            item.minCreditScore,
                                            style: const TextStyle(color: Color(0xffd0067ff),fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        const Text(
                                            'Maximum Credit Score:',
                                            style: TextStyle(
                                                fontWeight:
                                                FontWeight
                                                    .bold)),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          item.maxCreditScore,
                                          style: const TextStyle(color: Color(0xffd0067ff),fontWeight: FontWeight.bold),),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        const IntrinsicWidth(
                                          stepWidth: 160,
                                          child: Text('ITR:',
                                              style: TextStyle(
                                                  fontWeight:
                                                  FontWeight
                                                      .bold)),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: Text(
                                            item.itr,
                                            style: const TextStyle(color: Color(0xffd0067ff),fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            goodDiv(),
                            Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.circular(10.0),
                              ),
                              child: Padding(
                                padding:
                                const EdgeInsets.only(left: 18,right: 18,bottom: 18),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment:
                                      Alignment.topLeft,
                                      child: const Row(
                                        children: [
                                          Icon(
                                            Icons
                                                .currency_rupee_outlined,
                                            color: Colors.green,
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            "Salary Criteria",
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight:
                                              FontWeight
                                                  .bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      children: [
                                        const IntrinsicWidth(
                                          stepWidth: 160,
                                          child: Text(
                                              'Minimum Salary:',
                                              style: TextStyle(
                                                  fontWeight:
                                                  FontWeight
                                                      .bold)),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: Text(
                                            item.minSalary, style: const TextStyle(color: Color(0xffd0067ff),fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        const IntrinsicWidth(
                                          stepWidth: 160,
                                          child: Text(
                                              'Maximum Salary:',
                                              style: TextStyle(
                                                  fontWeight:
                                                  FontWeight
                                                      .bold)),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: Text(
                                            item.maxSalary, style: const TextStyle(color: Color(0xffd0067ff),fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                            : _.selectedIdx == 4
                            ? Column(
                          children: [
                            Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.circular(
                                    10.0),
                              ),
                              child: Padding(
                                padding:
                                const EdgeInsets.all(
                                    18.0),
                                child: Column(
                                  children: [
                                    Container(
                                      alignment:
                                      Alignment.topLeft,
                                      child: const Text(
                                        "Terms & Conditions",
                                        style:
                                        TextStyle(
                                          fontSize: 18,
                                          fontWeight:
                                          FontWeight
                                              .bold,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    item.termsAndConditions
                                        .isNotEmpty
                                        ? Container(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        item.termsAndConditions.replaceAll('\\n ', '\n'),
                                      ),
                                    )
                                        : Center(
                                      child: Padding(
                                        padding:
                                        const EdgeInsets
                                            .all(
                                            20.0),
                                        child: SvgPicture.asset(
                                            AppUtils
                                                .blankImg,
                                            width: MediaQuery.of(
                                                context)
                                                .size
                                                .width,
                                            height: MediaQuery.of(context)
                                                .size
                                                .height *
                                                0.33,
                                            fit: BoxFit
                                                .fill),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                            : const SizedBox(
                          width: 0,
                        ),
                        const SizedBox(
                          height: 80,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(top: BorderSide(color: Colors.blue.shade100))),
                  height: MediaQuery.of(context).size.height*0.1,
                  // color: Colors.white,
                  alignment: Alignment.bottomCenter,
                  width: double.infinity,
                  padding: const EdgeInsets.all(16),
                  child:
                  Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () {
                            print(item.shareUrl);
                            Get.to(WebviewController(
                                url: item.shareUrl, title: item.name));
                          },
                          style: ButtonStyle(
                            backgroundColor: WidgetStateProperty.all<Color>(
                                const Color(0xFFAB54DB)),
                            minimumSize: WidgetStateProperty.all<Size>(
                                const Size(double.infinity, 40)),
                            shape: WidgetStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                              ),
                            ),
                          ),
                          child: const Text(
                            'Process',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 16),
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () {
                            Share.share(item.shareUrl ?? 'NA',
                                subject: item.name);
                          },
                          style: ButtonStyle(
                            backgroundColor: WidgetStateProperty.all<Color>(
                                const Color(0xFF00A389)),
                            minimumSize: WidgetStateProperty.all<Size>(
                                const Size(double.infinity, 40)),
                            shape: WidgetStateProperty.all<
                                RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                              ),
                            ),
                          ),
                          child: const Text(
                            'Share',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              // fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _openYoutubeApp(String youtubeLink) async {
    final Uri url = Uri.parse(youtubeLink);

    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  void showImageDialog(BuildContext context, String imagePath) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.network(
                  imagePath,
                  fit: BoxFit.cover,
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      'res/images/no_image.png',
                      fit: BoxFit.contain,
                    );
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  String? extractVideoIdFromUrl(String youtubeUrl) {
    Uri uri = Uri.parse(youtubeUrl);
    String? videoId = '';

    if (uri.queryParameters.containsKey('v')) {
      videoId = uri.queryParameters['v'];
    } else {
      List<String> pathSegments = uri.pathSegments;
      if (pathSegments.contains('watch') && pathSegments.length >= 2) {
        int index = pathSegments.indexOf('watch');
        if (index < pathSegments.length - 1) {
          videoId = pathSegments[index + 1];
        }
      }
    }
    return videoId;
  }

  goodDiv() {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Divider(thickness: 1,color: Colors.blue.shade100,));
  }
}

class HorizontalList1 extends StatelessWidget {
  ProductDesController obj;

  HorizontalList1({super.key, required this.obj});

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    // const borderSide = BorderSide(
    //   width: 2,
    //   color: Color(0xFF9DBCE8)
    // );
    // const borderGSide = BorderSide(
    //   width: 1,
    //   color: Colors.grey,
    // );
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color:const Color(0xFF9DBCE8),
          borderRadius: BorderRadius.circular(5)

      ),
      child: ListView(
        controller: _scrollController,
        scrollDirection: Axis.horizontal,
        children:
        obj.myHeaderList.asMap().entries.map((MapEntry<int, String> entry) {
          String item = entry.value;
          int index = entry.key;

          return GestureDetector(
            onTap: () {
              if (obj.selectedIdx != index) {
                obj.selectedIdx = index;
                double offset = index * 227.5 -
                    (_scrollController.position.viewportDimension) +
                    100;
                _scrollController.animateTo(
                  offset,
                  duration: const Duration(milliseconds: 600),
                  curve: Curves.ease,
                );
              }
              obj.update();
            },
            child: Align(
              alignment: Alignment.centerLeft,
              child: Container(
                // height: 30,
                decoration: BoxDecoration(
                    color: obj.selectedIdx==index?const Color(0xFF0067FF):const Color(0x000067ff),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Center(
                    child: Text(
                      item.toString(),
                      style: TextStyle(
                          fontSize: obj.selectedIdx==index?16:14,
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        }).toList(),
      ),
    );
  }


// this is productDesUIController
}
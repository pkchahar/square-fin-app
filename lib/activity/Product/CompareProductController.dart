import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../../../utils/AppUtils.dart';
import '../../Model/NotificationList.dart';
import '../../Model/Product/ListingProduct.dart';
import '../../Model/Product/ProductResponse.dart';

class CompareProductController extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;
  NotificationList productsData = NotificationList();
  String userId = "",
      token = "",
      userType = "",
      version = "",
      productId = "",
      subProductId = "";
  List<ListingProduct> leadProducts = [];
  List<ProductData> subProductList = [];
  List<String> myHeaderList = [
    'Details',
    'Training',
    'Marketing',
    'Eligibility',
    'Terms and Conditions',
  ];
  int selectedIdx = 0;

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void getCategory() async {
    try {
      var url =
          Uri.parse(AppUtils.baseUrlNew + AppUtils.listingProduct + subProductId);
      var response = await http.get(url);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        if (leadProducts.isNotEmpty) {
          leadProducts.clear();
        }
        Map<String, dynamic> responseJson = json.decode(response.body);
        ListingProductResponse myResponse =
            ListingProductResponse.fromJson(responseJson);
        leadProducts.addAll(myResponse.data);

        AppUtils.showSnackBar(
            "Products Fetch", myResponse.message, leadProducts.isNotEmpty);
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void getSub() async {
    try {
      var url =
          Uri.parse(AppUtils.baseUrlNew + AppUtils.getSubCategory + productId);
      var response = await http.get(url);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        ProductResponse myResponse = ProductResponse.fromJson(responseJson);
        subProductList.addAll(myResponse.data);
        if (subProductList.isNotEmpty) {
          subProductId = subProductList[0].id;
          getCategory();
        }
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    productId = preferences.getString(AppUtils.productId) ?? "";
    // getSub();
  }




}

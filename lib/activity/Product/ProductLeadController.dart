import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../utils/AppUtils.dart';
import '../../Model/Product/MyCreditLeads.dart';
import '../../Model/Ticket/CreateTicket.dart';
import '../../Model/Ticket/FetchTicketList.dart';

class ProductLeadController extends GetxController {
  bool _isLoading = true;


  TextEditingController subject = TextEditingController();
  TextEditingController issue = TextEditingController();

  bool get isLoading => _isLoading;
  List<LeadData> productList = [];
  List<LeadData> productList1 = [];
  List<LeadData> mainList = [];
  List<String> filterProductList = [];
  List<String> filterStatusList = [];

  String userId = "",
      userType = "",
      token = "",
      version = "",
      startDate = "",
      endDate = "",
      pName = "",
      status = "";

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  // Future<void> showTicketList() async {
  //   try {
  //     SharedPreferences pref = await SharedPreferences.getInstance();
  //     Map<String, dynamic> mData = {'Login_User_Id': pref.getString(AppUtils.userId)};
  //     var url = Uri.parse(AppUtils.baseUrl + AppUtils.getAllTickets);
  //     var response = await http.post(url, body: mData);
  //     print(url);
  //     print(response.statusCode);
  //     print(mData);
  //     if (response.statusCode == 200) {
  //       Map<String, dynamic> responseJson = json.decode(response.body);
  //       FetchTicketList myResponse = FetchTicketList.fromJson(responseJson);
  //       data = myResponse.data!;
  //       print(response.body);
  //     }
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    getLeadMaster();
  }

  void getLeadMaster() async {
    setLoading(true);

    if (startDate.isNotEmpty) {
      startDate = AppUtils.getYMD(startDate).toString();
    }

    if (endDate.isNotEmpty) {
      endDate = AppUtils.getYMD(endDate).toString();
    }

    try {
      Map<String, String> mData = {
        'Login_User_Id': userId,
        'start_date': startDate,
        'end_date': endDate,
      };
      print(mData);
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.generatedLeads);
      print(url);
      var response = await http.post(url, body: mData);
      print(response.body);
      mainList.clear();
      productList.clear();
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        MyCreditLeads myResponse = MyCreditLeads.fromJson(responseJson);

        productList.addAll(myResponse.data);
        mainList.addAll(myResponse.data);

        if (mainList.isNotEmpty && (pName.isNotEmpty || status.isNotEmpty)) {
          customFilter(pName, status);
        }

        filterProductList.clear();
        filterStatusList.clear();
        if (mainList.isNotEmpty) {
          for (int i = 0; i < mainList.length; i++) {
            String name = mainList[i].productName;

            if (filterProductList.isNotEmpty) {
              for (int i = 0; i < filterProductList.length; i++) {
                String value = filterProductList[i];
                if (value != name) {
                  filterProductList.add(name);
                }
              }
            } else {
              filterProductList.add(name);
            }
          //Set<int> uniqueNumbersSet = numbers.toSet();
            //
            //   // Convert the set back to a list
            //   List<int> uniqueNumbersList = uniqueNumbersSet.toList();

            Set<String> uniqueFilterProductSet = filterProductList.toSet();
            filterProductList = uniqueFilterProductSet.toList();

            if (filterStatusList.isNotEmpty) {
              for (int j = 0; j < filterStatusList.length; j++) {
                String statusValue = filterStatusList[j];
                String mStatus = mainList[i].status;

                if (statusValue != mStatus) {
                  filterStatusList.add(mStatus);
                }else{}

              }
            } else {
              String mStatus = mainList[i].status;
              filterStatusList.add(mStatus);
            }

          }
          Set<String> uniqueFilterStatusLSwt = filterStatusList.toSet();
          filterStatusList = uniqueFilterStatusLSwt.toList();
          // if (filterStatusList.isNotEmpty) {
          //   Set<String> v1 = filterStatusList.toSet();
          //   filterStatusList = v1.toList();
          // }
        }
        update();
      }
      setLoading(false);
    } catch (e) {
      print(e);
    }
  }

  void reloadList() {
    productList = [];
    productList.addAll(mainList);
    print('${productList.length}     --------');
    update();
  }

  void searchData(String ask) {
    productList = [];
    if (ask.isNotEmpty) {
      productList = mainList.where((item) {
        return ((item.productId.toLowerCase().contains(ask.toLowerCase()) ||
            item.name.toLowerCase().contains(ask.toLowerCase()) ||
            item.email.toLowerCase().contains(ask.toLowerCase()) ||
            item.mobile.toLowerCase().contains(ask.toLowerCase()) ||
            item.productName.toLowerCase().contains(ask.toLowerCase()) ||
            item.remark.toLowerCase().contains(ask.toLowerCase())));
      }).toList();
    } else {
      productList.addAll(mainList);
    }
    update();
  }

  void searchFilterData(String pName, String status) {
    // if(pName == ''&& status ==''){productList.addAll(mainList);}
    if (startDate.isNotEmpty && endDate.isNotEmpty) {
      getLeadMaster();
    } else {
      customFilter(pName, status);
    }
    update();
  }

  void customFilter(String pName, String status) {
    productList = [];
    if (pName.isNotEmpty && status.isNotEmpty) {
      productList = mainList.where((item) {
        return (item.productName.toLowerCase().contains(pName.toLowerCase()) &&
            item.status.toLowerCase().contains(status.toLowerCase()));
      }).toList();
    } else if (pName.isNotEmpty) {
      productList = mainList.where((item) {
        return ((item.productName.toLowerCase().contains(pName.toLowerCase())));
      }).toList();
    } else if (status.isNotEmpty) {
      productList = mainList.where((item) {
        return (item.status.toLowerCase().contains(status.toLowerCase()));
      }).toList();
    } else {
      productList.addAll(mainList);
    }
  }

  Future<void> refreshPage() async {
    try {
      Map<String, String> mData = {
        'Login_User_Id': userId,
      };
      print(mData);
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.generatedLeads);
      print(url);
      var response = await http.post(url, body: mData);
      print(response.body);
      productList.clear();
      mainList.clear();
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        MyCreditLeads myResponse = MyCreditLeads.fromJson(responseJson);
        productList.addAll(myResponse.data);
        mainList.addAll(myResponse.data);
        filterProductList.clear();
        filterStatusList.clear();

        if (mainList.isNotEmpty) {
          for (int i = 0; i < mainList.length; i++) {
            String name = mainList[i].productName;
            String status = mainList[i].status;

            if (filterProductList.isNotEmpty) {
              for (int i = 0; i < filterProductList.length; i++) {
                String value = filterProductList[i];
                if (value != name) {
                  filterProductList.add(name);
                }
              }
            } else {
              filterProductList.add(name);
            }
            Set<String> uniqueFilterProductSet = filterProductList.toSet();
            filterProductList = uniqueFilterProductSet.toList();
            if (filterStatusList.isNotEmpty) {
              for (int j = 0; j < filterStatusList.length; j++) {
                String statusValue = filterStatusList[j];
                if (statusValue != status) {
                  filterStatusList.add(status);
                }
              }
            } else {
              filterStatusList.add(status);
            }
            Set<String> uniqueFilterStatusLSwt = filterStatusList.toSet();
            filterStatusList = uniqueFilterStatusLSwt.toList();
          }
        }
        update();
        update();
      }
    } catch (e) {
      print(e);
    }
  }
  bool ticketListPath=  false;
  // List <CreateTicket> queryResponse = [];

  submitQuery(leadId, TextEditingController subject, TextEditingController issue) async {
    try{
      SharedPreferences pref = await SharedPreferences.getInstance();
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.createTicket);
      Map<String, dynamic> mData = {
        'subject' : subject.text.toString(),
        'message' : issue.text.toString(),
        'Lead_Id' : '$leadId',
        'Login_User_Id' : pref.getString(AppUtils.userId)
      };

      var response = await http.post(url,body: mData);
      print(mData);
      print(response.statusCode);
      print(url);
      if(response.statusCode == 200){
        Map<String, dynamic> responseJson = jsonDecode(response.body);
        CreateTicket myResponse = CreateTicket.fromJson(responseJson);
        if(myResponse.status){
          AppUtils.showSnackBar('Ticket raised!', 'Our Team will answer your query soon!', true);
          ticketListPath = true;
          update();
        }else{
          AppUtils.showSnackBar('Something Went Wrong!', 'Some issue occurred, Please Try again! ', false);
        }
      }
    }catch(e)
    {
      print(e);
    }
  }

  List <DataForList> data =[];


}

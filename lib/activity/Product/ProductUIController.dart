import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pi/Model/Product/ProductResponse.dart';
import 'package:pi/activity/Product/ProductDesUIController.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:share_plus/share_plus.dart';

// import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

// import 'package:url_launcher/url_launcher.dart';
import '../../Model/Product/ListingProduct.dart';
import '../Utility Controllers/Faqs/FaqsUIController.dart';
import 'CompareProductUIController.dart';
import 'ProductController.dart';

class ProductUIController extends StatelessWidget {
  String productId = "";
  String subProductId = "";

  ProductUIController(
      {super.key, required this.productId, required this.subProductId});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductController>(
      init: ProductController(),
      builder: (_) {
        return Scaffold(
          // backgroundColor: const Color(0xF8F8F6F6),
          backgroundColor: AppUtils.backgroundColor,
          appBar: AppBar(
            title: Text(_.productName),
            actions: [
              IconButton(
                icon: const Icon(
                  Icons.support_agent_outlined,
                  size: 30,
                  color: Colors.black,
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return const ContactUsDialog();
                    },
                  );
                },
              ),
            ],
          ),
          body: Stack(
            children: [
              Container(
                  height: double.infinity,
                  width: double.infinity,
                  color: AppUtils.bgColor),
              Column(
                children: <Widget>[
                  SizedBox(
                    height: 80,
                    child: HorizontalList1(
                      obj: _,
                    ),
                  ),
                  _.isLoading
                      ? const Center(
                          child: Padding(
                          padding: EdgeInsets.only(top: 300),
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.blue),
                          ),
                        ))
                      : Expanded(
                          child: HorizontalList2(
                            obj: _,
                          ),
                        ),
                  _.selectedList.isNotEmpty
                      ? Container(
                          alignment: Alignment.bottomCenter,
                          width: double.infinity,
                          padding: const EdgeInsets.all(16),
                          color: Colors.white,
                          child: _.selectedList.length == 2
                              ? ElevatedButton(
                                  onPressed: () {
                                    // AppUtils.showSnackBar('Product Compare',
                                    //     'Coming Soon!!', false);
                                    Get.to(() => CompareProductUIController(
                                        selectedList: _.selectedList));
                                  },
                                  style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                            Colors.lightBlue),
                                    minimumSize: WidgetStateProperty.all<Size>(
                                        const Size(double.infinity, 50)),
                                    shape: WidgetStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                    ),
                                  ),
                                  child: const Text(
                                    'Compare Selected Products',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )
                              : const Text(
                                  'Select 1 more product to compare',
                                  style: TextStyle(fontSize: 18),
                                ))
                      : const SizedBox(
                          width: 0,
                        )
                ],
              )
            ],
          ),
        );
      },
    );
  }
}

class ContactUsDialog extends StatelessWidget {
  final String phoneNumber = '+91 8306300415';
  final String mMail = 'info@squarefin.in';

  const ContactUsDialog({super.key});

  _launchEmail() async {
    String email = Uri.encodeComponent(mMail);
    String subject = Uri.encodeComponent("Square fin App Support");
    String body = Uri.encodeComponent("");
    Uri mail = Uri.parse("mailto:$email?subject=$subject&body=$body");
    if (await launchUrl(mail)) {
      print(mMail);
    }
  }

  _launchPhone() async {
    final Uri phoneLaunchUri = Uri(
      scheme: 'tel',
      path: phoneNumber,
    );
    if (await canLaunch(phoneLaunchUri.toString())) {
      await launch(phoneLaunchUri.toString());
    } else {
      throw 'Could not launch phone';
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      // title: Text('Customer Support'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Container(
              //color: Colors.blue,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: //Text('X',style: TextStyle(color: Colors.red),))
                          Icon(
                        Icons.cancel_outlined,
                        color: Colors.red.shade200,
                      )),
                ],
              ),
            ),
          ),
          SizedBox(
              height: 150,
              child: SvgPicture.asset(
                'res/images/support.svg',
                fit: BoxFit.fitHeight,
              )),
          const Text(
            '\nCustomer Support',
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline),
          ),
          const SizedBox(
            height: 4,
          ),
          const Text(
            '24x7 Customer Support: Quick, reliable assistance at your fingertips. Reach out anytime for technical help.',
            style: TextStyle(fontSize: 10),
          ),
          const SizedBox(
            height: 5,
          ),
          const Text('Connect to us:'),
          const SizedBox(
            height: 15,
          ),
          Container(
            height: 35,
            decoration: BoxDecoration(
              border: Border.all(width: 0.3),
              color: Colors.blue.shade50,
              borderRadius: BorderRadius.circular(11),
            ),
            child: Row(
              children: [
                Container(
                    constraints: const BoxConstraints(
                      minWidth: 50,
                    ),
                    child: const Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text('Mail Us: '),
                    )),
                InkWell(
                    onTap: () {
                      _launchEmail();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        mMail,
                        style: const TextStyle(color: Colors.indigo),
                      ),
                    )),
              ],
            ),
          ),
          const SizedBox(height: 10),
          Container(
            height: 35,
            decoration: BoxDecoration(
              border: Border.all(width: 0.3),
              color: Colors.blue.shade50,
              borderRadius: BorderRadius.circular(11),
            ),
            child: Row(
              children: [
                Container(
                    constraints: const BoxConstraints(
                      minWidth: 50, // Set the minimum width
                    ),
                    child: const Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text('Call Us: '),
                    )),
                InkWell(
                    onTap: () {
                      _launchPhone();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(phoneNumber,
                          style: const TextStyle(color: Colors.indigo)),
                    )),
              ],
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          TextButton(
            onPressed: () {
              Get.back();
              Get.to(const FaqsUIController());
            },
            child: const Center(
              child: Text(
                'FAQ\'s',
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black,
                  decoration: TextDecoration.underline,
                ),
              ),
            ),
          ),
        ],
      ),
      // actions: [
      //   TextButton(
      //     onPressed: () {
      //       Navigator.of(context).pop();
      //     },
      //     child: Text(
      //       'Close',
      //       style: TextStyle(color: Colors.black),
      //     ),
      //   ),
      //
      //   TextButton(
      //     onPressed: () {
      //      Get.to(FaqsUIController());
      //     },
      //     child: Text(
      //       'FAQs',
      //       style: TextStyle(color: Colors.black),
      //     ),
      //   ),
      // ],
    );
  }
}

class HorizontalList1 extends StatefulWidget {
  final ProductController obj;

  const HorizontalList1({Key? key, required this.obj}) : super(key: key);

  @override
  _HorizontalList1State createState() => _HorizontalList1State();
}

class _HorizontalList1State extends State<HorizontalList1> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.obj.subProductList.isNotEmpty) {
      double offset = widget.obj.selectedIdx * 165.0 -
          (_scrollController.position.viewportDimension / 4) +
          100;

      _scrollController.animateTo(
        offset,
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );

      print("check condition.. ");
      print(widget.obj.subProductList.length);
      print(widget.obj.selectedIdx);
    }
    return ListView(
      controller: _scrollController,
      scrollDirection: Axis.horizontal,
      children: widget.obj.subProductList
          .asMap()
          .entries
          .map((MapEntry<int, ProductData> entry) {
        ProductData item = entry.value;
        int index = entry.key;

        return GestureDetector(
          onTap: () {
            if (widget.obj.selectedIdx != index) {
              widget.obj.selectedIdx = index;
              widget.obj.subProductId = item.id;
              widget.obj.selectedItemName = item.name;
              widget.obj.getProducts();
              double offset = index * 165.0 -
                  (_scrollController.position.viewportDimension / 4) +
                  500;
              _scrollController.animateTo(
                offset,
                duration: const Duration(milliseconds: 500),
                curve: Curves.ease,
              );
              widget.obj.selectedList = [];
              widget.obj.updateData();
            }
            widget.obj.update();
          },
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              alignment: Alignment.center,
              height: 55,
              margin:
                  const EdgeInsets.only(top: 0, right: 0, bottom: 0, left: 8),
              child: Center(
                child: Card(
                    elevation: 0,
                    color: widget.obj.selectedIdx == index
                        ? const Color(0xFF0067FF)
                        : Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(
                        color: widget.obj.selectedIdx == index
                            ? Colors.cyan
                            : Colors.cyan.shade200,
                        width: 0.3,
                      ),
                    ),
                    child: Align(
                      alignment: Alignment.center,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          sb(0, 10),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),
                                color: Colors.white,
                                border:
                                    Border.all(color: const Color(0xFFB5CEF3))),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 3, vertical: 2),
                            child: Image.network(
                              item.icon,
                              fit: BoxFit.contain,
                              width: 30,
                              height: 30,
                              errorBuilder: (context, error, stackTrace) {
                                return Image.asset(
                                  'res/images/no_image.png',
                                  fit: BoxFit.contain,
                                  width: 30,
                                  height: 30,
                                );
                              },
                            ),
                          ),
                          sb(0, 10),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                item.name,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: widget.obj.selectedIdx == index
                                        ? 14
                                        : 13,
                                    color: widget.obj.selectedIdx == index
                                        ? Colors.white
                                        : Colors.black,
                                    fontWeight: FontWeight.w600),
                              ),
                              Text(
                                item.tagline,
                                style: TextStyle(
                                    fontSize: widget.obj.selectedIdx == index
                                        ? 10
                                        : 11,
                                    color: widget.obj.selectedIdx == index
                                        ? Colors.white
                                        : Colors.grey),
                              ),
                            ],
                          ),
                          sb(0, 10),
                        ],
                      ),
                    )),
              ),
            ),
          ),
        );
      }).toList(),
    );
  }
}

class HorizontalList2 extends StatelessWidget {
  ProductController obj;

  HorizontalList2({super.key, required this.obj});

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.vertical,
      children: obj.leadProducts.isEmpty
          ? [
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 120.0),
                  child: SvgPicture.asset(AppUtils.blankImg,
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.33,
                      fit: BoxFit.fill),
                ),
              ),
            ]
          : obj.leadProducts.map((ListingProduct item) {
              return GestureDetector(
                onTap: () {
                  Get.to(() => ProductDesUIController(item: item));
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 3, bottom: 3, right: 10, left: 10),
                  child: Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Stack(children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(right: 10, left: 10, top: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Image.network(
                                  item.icon,
                                  fit: BoxFit.contain,
                                  width: 90,
                                  errorBuilder: (context, error, stackTrace) {
                                    return Image.asset(
                                      'res/images/no_image.png',
                                      fit: BoxFit.contain,
                                      width: 90,
                                    );
                                  },
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 8, right: 10),
                                  child: Container(
                                      color: Colors.blue.shade100,
                                      width: 1,
                                      height: 50),
                                ),
                                Container(
                                  constraints: BoxConstraints(
                                      maxWidth:
                                          MediaQuery.of(context).size.width *
                                              0.59),
                                  child: Text(
                                    item.name,
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        height: 1.3),
                                  ),
                                ),
                              ],
                            ),

                            // ListTile(
                            //   contentPadding: EdgeInsets.zero,
                            //   leading: Container(
                            //     decoration: BoxDecoration(
                            //       border: Border.all(
                            //         color: Colors.blueGrey,
                            //         // You can change the border color
                            //         width:
                            //             .1, // You can adjust the border width
                            //       ),
                            //       borderRadius: BorderRadius.circular(
                            //           8.0), // You can adjust the border radius
                            //     ),
                            //     child:
                            //     Image.network(
                            //       item.icon,
                            //       fit: BoxFit.contain,
                            //       width: 90,
                            //       errorBuilder: (context, error, stackTrace) {
                            //         return Image.asset(
                            //           'res/images/no_image.png',
                            //           fit: BoxFit.contain,
                            //           width: 90,
                            //         );
                            //       },
                            //     ),
                            //   ),
                            //   title:
                            //   Text(
                            //     item.name,
                            //     style: const TextStyle(
                            //       fontSize: 18,
                            //       fontWeight: FontWeight.bold,
                            //     ),
                            //   ),
                            //   subtitle: item.companyName.isNotEmpty
                            //       ? Text(
                            //           item.companyName,
                            //           style: const TextStyle(
                            //             fontSize: 15,
                            //           ),
                            //         )
                            //       : sb(0, 0),
                            // ),
                            const Divider(),
                            const SizedBox(height: 5),
                            Text(
                              item.features.replaceAll('\\n  ', '\n'),
                              style: const TextStyle(
                                fontSize: 12,
                              ),
                              maxLines: 7,
                              overflow: TextOverflow.ellipsis,
                            ),
                            const SizedBox(height: 10),
                            // (item.joiningFee == "0" &&
                            //     item.joiningFee == "0") ||
                            //     (item.joiningFee == "0.00" &&
                            //         item.joiningFee == "0.00")||
                            ((obj.selectedItemName != "Credit Cards") ||
                                    item.joiningFee.isEmpty)
                                ? sb(0, 0)
                                : Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          constraints: const BoxConstraints(
                                            maxWidth: 140.0,
                                          ),
                                          alignment: Alignment.centerLeft,
                                          decoration: BoxDecoration(
                                            color: const Color(0xFFAB54DB),
                                            // border: Border.all(
                                            //   color: Colors.grey.shade100,
                                            //   width: 1,
                                            // ),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 6),
                                            child: Center(
                                              child: Text(
                                                "Joining Fee ₹ ${item.joiningFee}",
                                                style: const TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 5),
                                      Expanded(
                                        child: Container(
                                          constraints: const BoxConstraints(
                                            minWidth: 140.0,
                                          ),
                                          alignment: Alignment.centerLeft,
                                          decoration: BoxDecoration(
                                            color: const Color(0xFF00A389),
                                            borderRadius:
                                                BorderRadius.circular(5),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 6),
                                            child: Center(
                                              child: Text(
                                                "Annual Fee ₹ ${item.annualFee}",
                                                style: const TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                            Divider(
                              color: Colors.blue.shade100,
                              thickness: 1,
                            ),
                            const SizedBox(height: 5),
                            Row(
                              children: [
                                obj.subProductId == "3"
                                    ? Transform.scale(
                                        scale: 1.22,
                                        child: Theme(
                                          data: ThemeData(
                                              unselectedWidgetColor:
                                                  const Color(0xFFFF5B5B)),
                                          child: Checkbox(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(4),
                                                side: const BorderSide(
                                                    color: Color(0xFFFF5B5B))),
                                            value: item.isChecked,
                                            activeColor: Colors.green,
                                            onChanged: (bool? value) {
                                              if (obj.selectedList.length ==
                                                      2 &&
                                                  !item.isChecked) {
                                                AppUtils.showSnackBar(
                                                    "Alert",
                                                    "Maximum 2 products can be compared",
                                                    false);
                                              } else {
                                                item.isChecked = value!;
                                                if (value) {
                                                  obj.selectedList.add(item);
                                                } else {
                                                  obj.selectedList.remove(item);
                                                }
                                              }
                                              obj.updateData();
                                            },
                                          ),
                                        ),
                                      )
                                    : sb(0, 0),
                                (item.amount.isNotEmpty && item.amount !="0")
                                    ? Container(
                                        alignment: Alignment.centerLeft,
                                        decoration: BoxDecoration(
                                          color: const Color(0xFFFF5B5B),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 5.0, horizontal: 15),
                                          child: Center(
                                            child: Row(
                                              children: [
                                                Text(
                                                  "Earn upto ${item.amtFlag} ",
                                                  style: const TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                Text(
                                                  item.amount,
                                                  style: const TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    : sb(0, 0),
                                Expanded(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          Share.share(item.shareUrl ?? 'NA',
                                              subject: item.name);
                                        },
                                        child: const Icon(
                                          Icons.share_outlined,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(width: 10),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 10),
                          ],
                        ),
                      ),
                    ]),
                  ),
                ),
              );
            }).toList(),
    );
  }
}

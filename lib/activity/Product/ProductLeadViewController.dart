
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:pi/activity/Utility%20Controllers/TicketDetailUIController.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../Model/Product/MyCreditLeads.dart';
import '../../utils/AppUtils.dart';
import 'ProductLeadController.dart';

class ProductLeadViewController extends StatefulWidget {
  const ProductLeadViewController({super.key});

  @override
  _LeadViewUI createState() => _LeadViewUI();
}

TextEditingController searchController = TextEditingController();

class _LeadViewUI extends State<ProductLeadViewController> {
  @override
  void initState() {
    super.initState();
  }

  var productValue = '';
  var statusValue = '';
  String startDate = '';
  String endDate = '';
  bool productBool = true;
  bool statusBool = false;
  bool dateBool = false;

  bool customButton = false;
  bool last7days = false;
  bool last30days = false;
  bool searchBarBool = false;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductLeadController>(
      init: ProductLeadController(),
      builder: (_) {
        return Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: AppUtils.backgroundColor,
          appBar: AppBar(
            title: const Text("My Leads"),
            actions: [
              searchBarBool
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          searchBarBool = !searchBarBool;
                          _.reloadList();
                        });
                      },
                      icon: const Icon(Icons.close))
                  : IconButton(
                      onPressed: () {
                        setState(() {
                          searchBarBool = !searchBarBool;
                        });
                      },
                      icon: const Icon(Icons.search)),
              _.productList.isNotEmpty
                  ? IconButton(
                      onPressed: () {
                        showModalBottomSheet(
                            useSafeArea: true,
                            context: context,
                            builder: (BuildContext mContext) {
                              return StatefulBuilder(builder:
                                  (BuildContext bContext,
                                      StateSetter setState) {
                                return Container(
                                  decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(22),
                                        topRight: Radius.circular(22)),
                                    color: Colors.grey.shade100,
                                  ),
                                  height: MediaQuery.of(bContext).size.height *
                                          0.4699 -
                                      3.7,
                                  // MediaQuery.of(bContext).size.height *
                                  //     0.3699 -
                                  // 4.7,
                                  width: double.infinity,
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          const SizedBox(
                                            width: 2,
                                          ),
                                          if (productValue != '' ||
                                              statusValue != '' ||
                                              startDate.isNotEmpty ||
                                              endDate.isNotEmpty)
                                            TextButton(
                                              onPressed: () {
                                                setState(() {
                                                  productValue = '';
                                                  statusValue = '';
                                                  startDate = '';
                                                  endDate = '';
                                                  customButton = false;
                                                  last7days = false;
                                                  last30days = false;
                                                });
                                              },
                                              child: Icon(
                                                Icons.sync,
                                                color: Colors.red.shade200,
                                              ),
                                            ),
                                          Expanded(
                                              child: SingleChildScrollView(
                                            scrollDirection: Axis.horizontal,
                                            child: Row(
                                              children: [
                                                Row(
                                                  children: [
                                                    productValue != ''
                                                        ? Container(
                                                            padding:
                                                                const EdgeInsets
                                                                    .only(
                                                                    top: 3,
                                                                    bottom: 3,
                                                                    right: 5,
                                                                    left: 5),
                                                            decoration: BoxDecoration(
                                                                color: Colors
                                                                    .white,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                                border:
                                                                    Border.all(
                                                                        width:
                                                                            0.3)),
                                                            child: Text(
                                                                productValue),
                                                          )
                                                        : const SizedBox(),
                                                    statusValue != ''
                                                        ? Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .only(
                                                              left: 8,
                                                            ),
                                                            child: Container(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .only(
                                                                        top: 3,
                                                                        bottom:
                                                                            3,
                                                                        right:
                                                                            5,
                                                                        left:
                                                                            5),
                                                                decoration: BoxDecoration(
                                                                    color: Colors
                                                                        .white,
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            5),
                                                                    border: Border.all(
                                                                        width:
                                                                            0.3)),
                                                                child: Text(
                                                                    statusValue)),
                                                          )
                                                        : const SizedBox(),
                                                    startDate.isNotEmpty
                                                        ? Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .only(
                                                                    left: 8.0),
                                                            child: Container(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .only(
                                                                        top: 3,
                                                                        bottom:
                                                                            3,
                                                                        right:
                                                                            5,
                                                                        left:
                                                                            5),
                                                                decoration: BoxDecoration(
                                                                    color: Colors
                                                                        .white,
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            5),
                                                                    border: Border.all(
                                                                        width:
                                                                            0.3)),
                                                                child: startDate !=
                                                                        endDate
                                                                    ? Text(
                                                                        '$startDate - $endDate')
                                                                    : Text(
                                                                        startDate)),
                                                          )
                                                        : const SizedBox(),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )),
                                          TextButton(
                                              onPressed: () {
                                                _.pName = productValue;
                                                _.status = statusValue;
                                                _.startDate = startDate;
                                                _.endDate = endDate;
                                                Navigator.pop(bContext, true);
                                                if (productValue.isNotEmpty ||
                                                    statusValue.isNotEmpty ||
                                                    startDate.isNotEmpty ||
                                                    endDate.isNotEmpty) {
                                                  _.searchFilterData(
                                                      productValue,
                                                      statusValue);
                                                } else {
                                                  _.refreshPage();
                                                  _.pName = '';
                                                  _.status = '';
                                                  _.startDate = '';
                                                  _.endDate = '';
                                                }
                                              },
                                              child: const Icon(
                                                Icons.search,
                                                color: Colors.black,
                                              ))
                                        ],
                                      ),
                                      const Divider(
                                        height: 0,
                                        color: Colors.black,
                                        thickness: 0.3,
                                      ),
                                      Container(
                                        color: Colors.grey.shade200,
                                        child: Row(
                                          children: [
                                            Expanded(
                                                // flex: 2,
                                                child: Container(
                                              color: Colors.grey.shade200,
                                              constraints: const BoxConstraints(
                                                  minWidth: 150),
                                              child: Column(
                                                children: [
                                                  Container(
                                                    width: double.infinity,
                                                    decoration: BoxDecoration(
                                                        color: productBool
                                                            ? Colors
                                                                .grey.shade100
                                                            : Colors
                                                                .transparent),
                                                    child: TextButton(
                                                        onPressed: () {
                                                          setState(() {
                                                            statusBool = false;
                                                            dateBool = false;
                                                            productBool = true;
                                                          });
                                                        },
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(
                                                            right: 42,
                                                          ),
                                                          child: Text(
                                                            'Product',
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 15,
                                                                color: productBool
                                                                    ? Colors
                                                                        .blue
                                                                        .shade700
                                                                    : Colors
                                                                        .black),
                                                          ),
                                                        )),
                                                  ),
                                                  Container(
                                                    width: double.infinity,
                                                    decoration: BoxDecoration(
                                                        color: statusBool
                                                            ? Colors
                                                                .grey.shade100
                                                            : Colors
                                                                .transparent),
                                                    child: TextButton(
                                                        onPressed: () {
                                                          setState(() {
                                                            dateBool = false;
                                                            productBool = false;
                                                            statusBool = true;
                                                          });
                                                        },
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(
                                                                  right: 55),
                                                          child: Text(
                                                            'Status',
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 15,
                                                                color: statusBool
                                                                    ? Colors
                                                                        .blue
                                                                        .shade700
                                                                    : Colors
                                                                        .black),
                                                          ),
                                                        )),
                                                  ),
                                                  Container(
                                                    width: double.infinity,
                                                    decoration: BoxDecoration(
                                                        color: dateBool
                                                            ? Colors
                                                                .grey.shade100
                                                            : Colors
                                                                .transparent),
                                                    child: TextButton(
                                                        onPressed: () {
                                                          setState(() {
                                                            productBool = false;
                                                            statusBool = false;
                                                            dateBool = true;
                                                          });
                                                        },
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(
                                                            right: 17,
                                                          ),
                                                          child: Text(
                                                            'Date-Range',
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 15,
                                                                color: dateBool
                                                                    ? Colors
                                                                        .blue
                                                                        .shade700
                                                                    : Colors
                                                                        .black),
                                                          ),
                                                        )),
                                                  ),
                                                  const SizedBox(
                                                    height: 70,
                                                  )
                                                ],
                                              ),
                                            )),
                                            Expanded(
                                                // flex: 4,
                                                child: Container(
                                              height: 315,
                                              color: Colors.grey.shade100,
                                              child: SingleChildScrollView(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    if (productBool)
                                                      for (int i = 0;
                                                          i <
                                                              _.filterProductList
                                                                  .length;
                                                          i++)
                                                        InkWell(
                                                            onTap: () {
                                                              setState(() {
                                                                _.filterProductList[
                                                                            i] !=
                                                                        productValue
                                                                    ? productValue =
                                                                        _.filterProductList[
                                                                            i]
                                                                    : productValue =
                                                                        '';
                                                                // productValue.isEmpty?productValue = _.filterProductList[i]:productValue='';
                                                              });
                                                            },
                                                            child: Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .only(
                                                                      top: 8,
                                                                      right: 8,
                                                                      left: 8),
                                                              child: Container(
                                                                decoration: BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                5),
                                                                    color: productValue ==
                                                                            _.filterProductList[
                                                                                i]
                                                                        ? Colors
                                                                            .teal
                                                                            .shade200
                                                                        : Colors
                                                                            .white),
                                                                child: Padding(
                                                                    padding: const EdgeInsets
                                                                        .only(
                                                                        left:
                                                                            12,
                                                                        right:
                                                                            12,
                                                                        top: 5,
                                                                        bottom:
                                                                            5),
                                                                    child: Text(
                                                                        _.filterProductList[i])),
                                                              ),
                                                            )),
                                                    if (statusBool)
                                                      for (int i = 0;
                                                          i <
                                                              _.filterStatusList
                                                                  .length;
                                                          i++)
                                                        InkWell(
                                                          onTap: () {
                                                            setState(() {
                                                              // _.filterProductList[i] != productValue
                                                              //  ?productValue = _.filterProductList[i]
                                                              //: productValue = '';
                                                              _.filterStatusList[
                                                                          i] !=
                                                                      statusValue
                                                                  ? statusValue =
                                                                      _.filterStatusList[
                                                                          i]
                                                                  : statusValue =
                                                                      '';

                                                              // statusValue = _.filterStatusList[i];
                                                            });
                                                          },
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .only(
                                                                    top: 8,
                                                                    left: 8,
                                                                    right: 8),
                                                            child: Container(
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              5),
                                                                  color: statusValue ==
                                                                          _.filterStatusList[
                                                                              i]
                                                                      ? Colors
                                                                          .teal
                                                                          .shade200
                                                                      : Colors
                                                                          .white),
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .only(
                                                                        left:
                                                                            12,
                                                                        right:
                                                                            12,
                                                                        top: 5,
                                                                        bottom:
                                                                            5),
                                                                child: Text(
                                                                    _.filterStatusList[i]),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                    if (dateBool)
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(
                                                                left: 0,
                                                                top: 2),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            InkWell(
                                                              onTap: () async {
                                                                setState(() {
                                                                  startDate =
                                                                      '';
                                                                  endDate = '';
                                                                  customButton =
                                                                      true;
                                                                  last7days =
                                                                      false;
                                                                  last30days =
                                                                      false;
                                                                });

                                                                final picked =
                                                                    await showDateRangePicker(
                                                                  context:
                                                                      bContext,
                                                                  firstDate:
                                                                      DateTime(
                                                                          2011),
                                                                  lastDate:
                                                                      DateTime
                                                                          .now(),
                                                                  builder: (BuildContext
                                                                          context,
                                                                      Widget?
                                                                          child) {
                                                                    return Builder(
                                                                      builder:
                                                                          (BuildContext
                                                                              context) {
                                                                        return Theme(
                                                                          data:
                                                                              ThemeData(
                                                                            primaryColor:
                                                                                Colors.blue,
                                                                          ),
                                                                          child:
                                                                              child!,
                                                                        );
                                                                      },
                                                                    );
                                                                  },
                                                                );
                                                                if (picked !=
                                                                    null) {
                                                                  setState(() {
                                                                    startDate = DateFormat(
                                                                            'dd-MM-yyyy')
                                                                        .format(
                                                                            picked.start);
                                                                    endDate = DateFormat(
                                                                            'dd-MM-yyyy')
                                                                        .format(
                                                                            picked.end);
                                                                  });
                                                                }
                                                              },
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .only(
                                                                        top: 8,
                                                                        right:
                                                                            8,
                                                                        left:
                                                                            8),
                                                                child:
                                                                    Container(
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(5),
                                                                    color: customButton &&
                                                                            startDate
                                                                                .isNotEmpty
                                                                        ? Colors
                                                                            .teal
                                                                            .shade200
                                                                        : Colors
                                                                            .white,
                                                                  ),
                                                                  child: const Padding(
                                                                      padding: EdgeInsets.only(
                                                                          left:
                                                                              12,
                                                                          right:
                                                                              12,
                                                                          top:
                                                                              5,
                                                                          bottom:
                                                                              5),
                                                                      child: Text(
                                                                          'Custom')),
                                                                ),
                                                              ),
                                                            ),
                                                            InkWell(
                                                              onTap: () {
                                                                setState(() {
                                                                  last7days =
                                                                      true;
                                                                  last30days =
                                                                      false;
                                                                  customButton =
                                                                      false;
                                                                  startDate = DateFormat(
                                                                          'dd-MM-yyyy')
                                                                      .format(DateTime
                                                                              .now()
                                                                          .subtract(
                                                                              const Duration(days: 7)));
                                                                  endDate = DateFormat(
                                                                          'dd-MM-yyyy')
                                                                      .format(DateTime
                                                                          .now());
                                                                });
                                                              },
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .only(
                                                                        top: 8,
                                                                        right:
                                                                            8,
                                                                        left:
                                                                            8),
                                                                child:
                                                                    Container(
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(5),
                                                                    color: last7days
                                                                        ? Colors
                                                                            .teal
                                                                            .shade200
                                                                        : Colors
                                                                            .white,
                                                                  ),
                                                                  child:
                                                                      const Padding(
                                                                    padding: EdgeInsets.only(
                                                                        left:
                                                                            12,
                                                                        right:
                                                                            12,
                                                                        top: 5,
                                                                        bottom:
                                                                            5),
                                                                    child: Text(
                                                                        'Last 7 days'),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            InkWell(
                                                              onTap: () {
                                                                setState(() {
                                                                  last30days =
                                                                      true;
                                                                  last7days =
                                                                      false;
                                                                  customButton =
                                                                      false;
                                                                  startDate = DateFormat(
                                                                          'dd-MM-yyyy')
                                                                      .format(DateTime
                                                                              .now()
                                                                          .subtract(
                                                                              const Duration(days: 30)));
                                                                  endDate = DateFormat(
                                                                          'dd-MM-yyyy')
                                                                      .format(DateTime
                                                                          .now());
                                                                });
                                                              },
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .only(
                                                                        top: 8,
                                                                        right:
                                                                            8,
                                                                        left:
                                                                            8),
                                                                child:
                                                                    Container(
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(5),
                                                                    color: last30days
                                                                        ? Colors
                                                                            .teal
                                                                            .shade200
                                                                        : Colors
                                                                            .white,
                                                                  ),
                                                                  child:
                                                                      const Padding(
                                                                    padding: EdgeInsets.only(
                                                                        left:
                                                                            12,
                                                                        right:
                                                                            12,
                                                                        top: 5,
                                                                        bottom:
                                                                            5),
                                                                    child: Text(
                                                                        'Last 30 days'),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                  ],
                                                ),
                                              ),
                                            ))
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              });
                            });
                      },
                      icon: const Icon(Icons.filter_alt_outlined))
                  : _.isLoading
                      ? const SizedBox()
                      : IconButton(
                          onPressed: () {
                            print(_.productList.length);
                            _.refreshPage();
                            print(_.productList.length);
                          },
                          icon: Icon(Icons.restart_alt,
                              color: Colors.red.shade200))
            ],
          ),
          body: Stack(children: [
            Container(
                height: double.infinity,
                width: double.infinity,
                color: AppUtils.bgColor),
            _.isLoading
                ? const Center(
                    child: Padding(
                    padding: EdgeInsets.all(10),
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                    ),
                  ))
                : Column(
                    children: [
                      searchBarBool
                          ? Stack(
                              children: [
                                Padding(
                                    padding: const EdgeInsets.only(
                                        left: 14, right: 14, top: 8),
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.04,
                                      decoration: BoxDecoration(
                                          color: Colors.blue.shade100
                                              .withOpacity(0.3),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          border: Border.all(width: 0.3)),
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18, top: 0, right: 50),
                                  child: TextFormField(
                                    autofocus: true,
                                    decoration: const InputDecoration(
                                      prefixText: "Search: ",
                                      border: InputBorder.none,
                                    ),
                                    onChanged: (value) {
                                      _.searchData(value);
                                    },
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                              ],
                            )
                          : const SizedBox(),
                      Expanded(
                        child: ListView(
                          shrinkWrap: true,
                          children: _.productList.isEmpty
                              ? [
                                  Center(
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(top: 113.0),
                                      child: SvgPicture.asset(AppUtils.blankImg,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.33,
                                          fit: BoxFit.fill),
                                    ),
                                  )
                                ]
                              : _.productList.map((LeadData item) {
                                  return InkWell(
                                    onTap: () {
                                      setState(() {
                                        searchBarBool = false;
                                      });
                                    },
                                    child: Container(
                                      child: Stack(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 3,
                                                bottom: 3,
                                                right: 10,
                                                left: 10),
                                            child: Card(
                                              elevation: 0,
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                              ),
                                              child: Stack(children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8,
                                                          top: 10,
                                                          right: 8),
                                                  child: Container(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Row(
                                                          children: [
                                                            Container(
                                                              child: ClipOval(
                                                                child: SizedBox(
                                                                  height: 75,
                                                                  width: 100,
                                                                  child:
                                                                      Padding(
                                                                    padding:
                                                                        const EdgeInsets
                                                                            .all(
                                                                            1.0),
                                                                    child: Image
                                                                        .network(
                                                                      item.icon,
                                                                      fit: BoxFit
                                                                          .fill,
                                                                      errorBuilder: (context,
                                                                          error,
                                                                          stackTrace) {
                                                                        return SvgPicture.asset(
                                                                            'res/images/no_image_icon.svg');
                                                                      },
                                                                      height:
                                                                          35,
                                                                      width: 35,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                      .only(
                                                                      left: 10,
                                                                      right:
                                                                          10),
                                                              child: Container(
                                                                  width: 1,
                                                                  height: 65,
                                                                  color: Colors
                                                                      .blue
                                                                      .shade100),
                                                            ),
                                                            Expanded(
                                                              child: Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Padding(
                                                                    padding: const EdgeInsets
                                                                        .only(
                                                                        right:
                                                                            45),
                                                                    child: Text(
                                                                      item.productName,
                                                                      textAlign:
                                                                          TextAlign
                                                                              .start,
                                                                      style:
                                                                          const TextStyle(
                                                                        fontSize:
                                                                            15,
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      ),
                                                                      maxLines:
                                                                          2,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                    ),
                                                                  ),
                                                                  sb(5, 0),
                                                                  Text(
                                                                    item.createdAt,
                                                                    textAlign:
                                                                        TextAlign
                                                                            .start,
                                                                    style: const TextStyle(
                                                                        fontSize:
                                                                            12),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        const SizedBox(height: 10),
                                                        Row(
                                                          children: [
                                                            Container(
                                                              constraints:
                                                                  const BoxConstraints(
                                                                      minWidth:
                                                                          40),
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: const Icon(
                                                                Icons.person,
                                                                color:
                                                                    Colors.blue,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text(item.name
                                                                .capitalizeFirst!),
                                                            // SizedBox(width:MediaQuery.of(context).size.width*0.1),
                                                            const Spacer(),
                                                            Container(
                                                              constraints:
                                                                  const BoxConstraints(
                                                                      minWidth:
                                                                          40),
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: const Icon(
                                                                  Icons.phone,
                                                                  color: Colors
                                                                      .blue,
                                                                  size: 20),
                                                            ),
                                                            InkWell(
                                                                onTap: () {
                                                                  launch(
                                                                      "tel://${item.mobile}");
                                                                },
                                                                child: Text(item
                                                                    .mobile)),
                                                            const SizedBox(width: 10),
                                                          ],
                                                        ),
                                                        const SizedBox(height: 7),
                                                        Row(children: [
                                                          Container(
                                                            constraints:
                                                                const BoxConstraints(
                                                                    minWidth:
                                                                        40),
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            child: const Icon(
                                                                Icons
                                                                    .email_outlined,
                                                                color:
                                                                    Colors.blue,
                                                                size: 20),
                                                          ),
                                                          Text(item.email),
                                                        ]),
                                                        const SizedBox(height: 7),
                                                        // const Divider(),
                                                        Row(
                                                          children: [
                                                            Container(
                                                                constraints:
                                                                    const BoxConstraints(
                                                                        minWidth:
                                                                            30),
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                child: item
                                                                        .status
                                                                        .contains(
                                                                            "Reject")
                                                                    ? const Icon(
                                                                        Icons
                                                                            .close,
                                                                        color: Colors
                                                                            .red,
                                                                      )
                                                                    : item.status.contains(
                                                                            "Pending")
                                                                        ? const Icon(
                                                                            Icons.timelapse_outlined,
                                                                            color:
                                                                                Colors.orange,
                                                                          )
                                                                        : item.status.contains(
                                                                                "Progress")
                                                                            ? const Icon(
                                                                                Icons.pending_actions_rounded,
                                                                                color: Colors.orange,
                                                                              )
                                                                            : const Icon(
                                                                                Icons.check,
                                                                                color: Colors.green,
                                                                              )),
                                                            item.status.contains(
                                                                    "Reject")
                                                                ? const Text(
                                                                    "Rejected",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .red,
                                                                        decoration:
                                                                            TextDecoration
                                                                                .underline))
                                                                : item.status
                                                                        .contains(
                                                                            "Pending")
                                                                    ? const Text(
                                                                        "Pending",
                                                                        style: TextStyle(
                                                                            color: Colors
                                                                                .orange,
                                                                            decoration: TextDecoration
                                                                                .underline))
                                                                    : item.status.contains(
                                                                            "Progress")
                                                                        ? const Text(
                                                                            "In Progress",
                                                                            style:
                                                                                TextStyle(color: Colors.orange, decoration: TextDecoration.underline))
                                                                        : const Text("Approved", style: TextStyle(color: Colors.green, decoration: TextDecoration.underline)),
                                                            const Spacer(),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .end,
                                                              children: [
                                                                GestureDetector(
                                                                  onTap: () {
                                                                    item.is_ticket_rasied
                                                                        ? Navigator.push(
                                                                            context,
                                                                            MaterialPageRoute(
                                                                                builder: (context) => TicketDetailUIController(
                                                                                      ticketId: item.ticket_id.toString(),
                                                                                    )))
                                                                        : showDialog(
                                                                            context:
                                                                                context,
                                                                            builder:
                                                                                (BuildContext context) {
                                                                              return AlertDialog(
                                                                                title: Row(
                                                                                  children: [
                                                                                    const Text('Dear '),
                                                                                    Text(item.name.capitalizeFirst.toString(), style: const TextStyle(fontWeight: FontWeight.bold)),
                                                                                    const Text(','),
                                                                                  ],
                                                                                ),
                                                                                content: StatefulBuilder(
                                                                                  builder: (BuildContext context, StateSetter setState) {
                                                                                    return SingleChildScrollView(
                                                                                      child: Column(
                                                                                        children: [
                                                                                          const Text(
                                                                                            'Please describe the issue you are experiencing.',
                                                                                            style: TextStyle(fontSize: 14),
                                                                                          ),
                                                                                          const SizedBox(height: 35),
                                                                                          TextFormField(
                                                                                            autofocus: true,
                                                                                            controller: _.subject,
                                                                                            maxLines: null,
                                                                                            decoration: const InputDecoration(
                                                                                              label: Text('Subject', style: TextStyle(color: Colors.grey)),
                                                                                              border: OutlineInputBorder(),
                                                                                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
                                                                                            ),
                                                                                          ),
                                                                                          const SizedBox(height: 10),
                                                                                          TextFormField(
                                                                                            controller: _.issue,
                                                                                            maxLines: 4,
                                                                                            decoration: const InputDecoration(
                                                                                              label: Text('Issue', style: TextStyle(color: Colors.grey)),
                                                                                              border: OutlineInputBorder(),
                                                                                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
                                                                                            ),
                                                                                          ),
                                                                                        ],
                                                                                      ),
                                                                                    );
                                                                                  },
                                                                                ),
                                                                                actions: [
                                                                                  ElevatedButton(
                                                                                    onPressed: () {
                                                                                      setState(() {
                                                                                        _.subject.text = '';
                                                                                        _.issue.text = '';
                                                                                      });
                                                                                      Get.back();
                                                                                    },
                                                                                    style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
                                                                                    child: const Text(
                                                                                      'Cancel',
                                                                                      style: TextStyle(color: Colors.white),
                                                                                    ),
                                                                                  ),
                                                                                  ElevatedButton(
                                                                                    onPressed: () {
                                                                                      if (_.subject.text == '' || _.issue.text == '') {
                                                                                        AppUtils.showSnackBar('Try Again!', 'Required Details are Missing', false);
                                                                                      } else {
                                                                                        _.submitQuery(item.lead_id_primary, _.subject, _.issue);
                                                                                        // refreshing page here!
                                                                                        _.getLeadMaster();
                                                                                        // submission is done here !
                                                                                        Navigator.of(context).pop();
                                                                                      }
                                                                                      print(_.subject.text);
                                                                                      print(_.issue.text);
                                                                                    },
                                                                                    style: ElevatedButton.styleFrom(backgroundColor: Colors.green),
                                                                                    child: const Text(
                                                                                      'Submit',
                                                                                      style: TextStyle(color: Colors.white),
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              );
                                                                            },
                                                                          );
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    decoration: BoxDecoration(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                                5),
                                                                        color: Colors
                                                                            .blue,
                                                                        border: Border.all(
                                                                            color:
                                                                                Colors.blue.shade700)),
                                                                    child:
                                                                        Padding(
                                                                      padding: const EdgeInsets
                                                                          .symmetric(
                                                                          vertical:
                                                                              1,
                                                                          horizontal:
                                                                              7),
                                                                      child: !item
                                                                              .is_ticket_rasied
                                                                          ? const Text(
                                                                              'Raise Ticket',
                                                                              style: TextStyle(fontSize: 12, color: Colors.white),
                                                                            )
                                                                          : const Text(
                                                                              'Show Ticket',
                                                                              style: TextStyle(fontSize: 12, color: Colors.white),
                                                                            ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                const SizedBox(
                                                                    width: 5),
                                                                Container(
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              5),
                                                                      color: const Color(
                                                                          0xFFE2C6F6),
                                                                      border: Border.all(
                                                                          color:
                                                                              const Color(0xFFB742E0))),
                                                                  child:
                                                                      Padding(
                                                                          padding: const EdgeInsets
                                                                              .symmetric(
                                                                              vertical:
                                                                                  1,
                                                                              horizontal:
                                                                                  7),
                                                                          child:
                                                                              Text(
                                                                            item.leadId,
                                                                            style:
                                                                                const TextStyle(fontSize: 12, color: Colors.black),
                                                                          )),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                        Divider(
                                                          color: Colors
                                                              .blue.shade100,
                                                          thickness: 1,
                                                        ),
                                                        item.remark.isNotEmpty
                                                            ? Text(
                                                                "Remark: ${item.remark}",
                                                                style:
                                                                    const TextStyle(
                                                                  fontSize: 12,
                                                                ),
                                                                maxLines: 3,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              )
                                                            : sb(0, 0),

                                                        const SizedBox(
                                                            height: 8),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ]),
                                            ),
                                          ),
                                          if (item.payout.isNotEmpty &&
                                              item.payout != "0" &&
                                              item.status == "Reject")
                                            Align(
                                              alignment: Alignment.topRight,
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 5, right: 12),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color:
                                                        Colors.orange.shade100,
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    10),
                                                            topRight:
                                                                Radius.circular(
                                                                    10)),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            7.0),
                                                    child: Text(
                                                      "₹ ${item.payout}",
                                                      style: const TextStyle(
                                                        fontSize: 13,
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                  );
                                }).toList(),
                        ),
                      )
                    ],
                  )
          ]),
        );
      },
    );
  }

  Widget getStatusTextAndIcon(String status) {
    TextStyle textStyle;

    if (status.contains("Reject")) {
      textStyle = const TextStyle(
        color: Colors.red,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    } else if (status.contains("Pending")) {
      textStyle = const TextStyle(
        color: Colors.orangeAccent,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    } else if (status.contains("Progress")) {
      textStyle = TextStyle(
        color: Colors.green.shade400,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    } else if (status.contains("Completed")) {
      textStyle = const TextStyle(
        color: Colors.green,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    } else {
      textStyle = const TextStyle(
        color: Colors.orange,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    }

    return Text(status, style: textStyle);
  }

  Widget getStatusIcon(String status) {
    if (status.contains("Reject")) {
      return const Icon(Icons.close, color: Colors.red, size: 20);
    } else if (status.contains("Pending")) {
      return const Icon(Icons.info_outline, color: Colors.orange, size: 20);
    } else if (status.contains("Progress")) {
      return Icon(Icons.pending_actions_rounded,
          color: Colors.green.shade400, size: 20);
    } else {
      return const Icon(Icons.done_all_outlined, color: Colors.green, size: 20);
    }
  }
}

//Stack(
//         children: [
//          Container(
//         height: double.infinity,
//         width: double.infinity,
//         color: AppUtils.bgColor
//         ) ,
//             _.isLoading
//                 ? const Center(
//                 child: Padding(
//                   padding: EdgeInsets.all(10),
//                   child: CircularProgressIndicator(
//                     valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
//                   ),
//                 ))
//                 : Column(
//               children: [
//                 ///**********************************************************************
//                 searchBarBool?  Stack(children: [
//                   Padding(
//                       padding: const EdgeInsets.only(
//                           left: 14, right: 14, top: 8),
//                       child: Container(
//                         height:
//                         MediaQuery.of(context).size.height * 0.04,
//                         decoration: BoxDecoration(
//                             color:
//                             Colors.blue.shade100.withOpacity(0.3),
//                             borderRadius: BorderRadius.circular(10),
//                             border: Border.all(width: 0.3)),
//                       )),
//
//
//                   Padding(
//                     padding: const EdgeInsets.only(left: 18, top:0,right:50),
//                     child: TextFormField(
//                       autofocus: true,
//                       decoration: const InputDecoration(
//                         prefixText: "Search: ",
//                         // prefixIcon:Icon(Icons.search_outlined,color: Colors.black54,size: 20,),
//                         // suffixIcon:Icon(Icons.search_outlined,color: Colors.black54,size: 18,),
//                         border: InputBorder.none,
//                       ),
//                       onChanged: (value) {
//                         _.searchData(value);
//                       },
//
//                     ),
//                   ),
//                   // Icon(
//                   //   Icons.search_outlined,
//                   //   size: 18,
//                   //   color: Colors.grey.shade800,
//                   // ),
//                   const SizedBox(
//                     width: 10,
//                   ),
//                   ///***********************************************************************
//                 ],): const SizedBox(),
//                 // searchBarBool? Padding(
//                 //     padding: const EdgeInsets.only(
//                 //         left: 14, right: 14, top: 8),
//                 //     child: Container(
//                 //       height:
//                 //           MediaQuery.of(context).size.height * 0.04,
//                 //       decoration: BoxDecoration(
//                 //           color:
//                 //               Colors.blue.shade100.withOpacity(0.3),
//                 //           borderRadius: BorderRadius.circular(10),
//                 //           border: Border.all(width: 0.3)),
//                 //       child:
//                 //
//                 //       Row(
//                 //         children: [
//                 //           Expanded(
//                 //               child: Center(
//                 //                 child: TextFormField(
//                 //                   autofocus: true,
//                 //                   decoration: const InputDecoration(
//                 //                     prefixIcon:Icon(Icons.search_outlined,color: Colors.black54,size: 18,),
//                 //                     // suffixIcon:Icon(Icons.search_outlined,color: Colors.black54,size: 18,),
//                 //                     border: InputBorder.none,
//                 //                   ),
//                 //                   onChanged: (value) {
//                 //                     _.searchData(value);
//                 //                   },
//                 //
//                 //                 ),
//                 //               )),
//                 //           // Icon(
//                 //           //   Icons.search_outlined,
//                 //           //   size: 18,
//                 //           //   color: Colors.grey.shade800,
//                 //           // ),
//                 //           const SizedBox(
//                 //             width: 10,
//                 //           )
//                 //         ],
//                 //       ),
//                 //     ),
//                 //   )
//                 // : const SizedBox(),
//                 Expanded(
//                   child: ListView(
//                     // physics: NeverScrollableScrollPhysics(),
//                     shrinkWrap: true,
//                     // scrollDirection: Axis.vertical,
//                     children: _.productList.isEmpty
//                         ? [
//                       Center(
//                         child: Padding(
//                           padding:
//                           const EdgeInsets.only(top: 113.0),
//                           child: SvgPicture.asset(AppUtils.blankImg,
//                               width:
//                               MediaQuery.of(context).size.width,
//                               height: MediaQuery.of(context)
//                                   .size
//                                   .height *
//                                   0.33,
//                               fit: BoxFit.fill),
//                         ),
//                       )
//                     ]
//                         : _.productList.map((LeadData item) {
//                       return InkWell(
//                         onTap:(){
//                           setState(() {
//                             searchBarBool = false;
//                           });},
//                         child: Container(
//                           child: Stack(
//                             children: [
//                               Padding(
//                                 padding: const EdgeInsets.only(
//                                     top: 3,
//                                     bottom: 3,
//                                     right: 10,
//                                     left: 10),
//                                 child: Card(
//                                   elevation: 2,
//                                   shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.circular(
//                                         10.0), // Adjust the radius to change border roundness
//                                     // side: BorderSide(color: Colors.green, width: 0.3), // Define the border color and width
//                                   ),
//                                   child: Stack(children: [
//                                     Padding(
//                                       padding:
//                                       const EdgeInsets.only(
//                                           left: 8,
//                                           top: 10,
//                                           right: 8),
//                                       child: Container(
//                                         child: Column(
//                                           crossAxisAlignment:
//                                           CrossAxisAlignment
//                                               .start,
//                                           children: [
//                                             Row(
//                                               children: [
//                                                 Container(
//                                                   child: ClipOval(
//                                                     child: SizedBox(
//                                                       height: 48,
//                                                       width: 48,
//                                                       child:
//                                                       Padding(
//                                                         padding:
//                                                         const EdgeInsets
//                                                             .all(
//                                                             1.0),
//                                                         child: Image
//                                                             .network(
//                                                           item.icon,
//                                                           fit: BoxFit
//                                                               .fill,
//                                                           errorBuilder: (context,
//                                                               error,
//                                                               stackTrace) {
//                                                             return SvgPicture.asset(
//                                                                 'res/images/no_image_icon.svg');
//                                                           },
//                                                           height:
//                                                           35,
//                                                           width: 35,
//                                                         ),
//                                                       ),
//                                                     ),
//                                                   ),
//                                                 ),
//                                                 const SizedBox(
//                                                     width: 10),
//                                                 Expanded(
//                                                   child: Column(
//                                                     crossAxisAlignment:
//                                                     CrossAxisAlignment
//                                                         .start,
//                                                     children: [
//                                                       Padding(
//                                                         padding: const EdgeInsets
//                                                             .only(
//                                                             right:
//                                                             45),
//                                                         child: Text(
//                                                           item.productName,
//                                                           textAlign:
//                                                           TextAlign
//                                                               .start,
//                                                           style:
//                                                           const TextStyle(
//                                                             fontSize:
//                                                             15,
//                                                             fontWeight:
//                                                             FontWeight.bold,
//                                                           ),
//                                                           maxLines:
//                                                           2,
//                                                           overflow:
//                                                           TextOverflow
//                                                               .ellipsis,
//                                                         ),
//                                                       ),
//                                                       sb(5, 0),
//                                                       Text(
//                                                         item.createdAt,
//                                                         textAlign:
//                                                         TextAlign
//                                                             .start,
//                                                         style: const TextStyle(
//                                                             fontSize:
//                                                             12),
//                                                       ),
//                                                     ],
//                                                   ),
//                                                 ),
//                                               ],
//                                             ),
//                                             const SizedBox(
//                                                 height: 10),
//                                             Row(
//                                               children: [
//                                                 Container(
//                                                   alignment:
//                                                   Alignment
//                                                       .center,
//                                                   decoration:
//                                                   BoxDecoration(
//                                                     color: Colors
//                                                         .green
//                                                         .shade50,
//                                                     border:
//                                                     Border.all(
//                                                       color: Colors
//                                                           .green
//                                                           .shade100,
//                                                       width: 1,
//                                                     ),
//                                                     borderRadius:
//                                                     BorderRadius
//                                                         .circular(
//                                                         10),
//                                                   ),
//                                                   child: Padding(
//                                                     padding:
//                                                     const EdgeInsets
//                                                         .all(
//                                                         7.0),
//                                                     child: Text(
//                                                       item.name,
//                                                       style:
//                                                       const TextStyle(
//                                                         fontSize:
//                                                         12,
//                                                       ),
//                                                     ),
//                                                   ),
//                                                 ),
//                                                 const SizedBox(
//                                                     width: 8),
//                                                 Container(
//                                                   constraints:
//                                                   BoxConstraints(
//                                                     minWidth: MediaQuery.of(
//                                                         context)
//                                                         .size
//                                                         .width *
//                                                         0.15,
//                                                   ),
//                                                   alignment:
//                                                   Alignment
//                                                       .center,
//                                                   decoration:
//                                                   BoxDecoration(
//                                                     color: Colors
//                                                         .purple
//                                                         .shade50,
//                                                     border:
//                                                     Border.all(
//                                                       color: Colors
//                                                           .purple
//                                                           .shade100,
//                                                       width: 1,
//                                                     ),
//                                                     borderRadius:
//                                                     BorderRadius
//                                                         .circular(
//                                                         10),
//                                                   ),
//                                                   child:
//                                                   GestureDetector(
//                                                     onTap: () {
//                                                       launch(
//                                                           "tel://${item.mobile}");
//                                                     },
//                                                     child: Padding(
//                                                       padding:
//                                                       const EdgeInsets
//                                                           .all(
//                                                           7.0),
//                                                       child: Text(
//                                                         item.mobile,
//                                                         style:
//                                                         const TextStyle(
//                                                           fontSize:
//                                                           12,
//                                                         ),
//                                                       ),
//                                                     ),
//                                                   ),
//                                                 ),
//                                               ],
//                                             ),
//                                             const SizedBox(
//                                                 height: 8),
//                                             Row(
//                                               children: [
//                                                 Container(
//                                                   alignment:
//                                                   Alignment
//                                                       .center,
//                                                   decoration:
//                                                   BoxDecoration(
//                                                     color: Colors
//                                                         .blue
//                                                         .shade50,
//                                                     border:
//                                                     Border.all(
//                                                       color: Colors
//                                                           .blue
//                                                           .shade100,
//                                                       width: 1,
//                                                     ),
//                                                     borderRadius:
//                                                     BorderRadius
//                                                         .circular(
//                                                         10),
//                                                   ),
//                                                   child: InkWell(
//                                                     onTap:
//                                                         () async {
//                                                       if (item.email
//                                                           .isNotEmpty) {
//                                                         final Uri
//                                                         emailLaunchUri =
//                                                         Uri.dataFromString(
//                                                           'mailto:${Uri.encodeComponent(item.email)}',
//                                                           mimeType:
//                                                           'text/plain',
//                                                         );
//
//                                                         if (await canLaunch(
//                                                             emailLaunchUri
//                                                                 .toString())) {
//                                                           await launch(
//                                                               emailLaunchUri
//                                                                   .toString());
//                                                         } else {
//                                                           print(
//                                                               'Could not launch email app');
//                                                         }
//                                                       }
//                                                     },
//                                                     child: Padding(
//                                                       padding:
//                                                       const EdgeInsets
//                                                           .all(
//                                                           5.0),
//                                                       child: Text(
//                                                         item.email,
//                                                         style:
//                                                         const TextStyle(
//                                                           fontSize:
//                                                           12,
//                                                         ),
//                                                       ),
//                                                     ),
//                                                   ),
//                                                 ),
//                                               ],
//                                             ),
//                                             const Divider(),
//                                             const SizedBox(
//                                                 height: 3),
//                                             Container(
//                                               constraints:
//                                               BoxConstraints(
//                                                 maxWidth:
//                                                 MediaQuery.of(
//                                                     context)
//                                                     .size
//                                                     .width,
//                                               ),
//                                               alignment: Alignment
//                                                   .centerLeft,
//                                               decoration:
//                                               BoxDecoration(
//                                                 color: Colors
//                                                     .grey.shade50,
//                                                 border: Border.all(
//                                                     color: Colors
//                                                         .grey
//                                                         .shade200,
//                                                     width: 1),
//                                                 borderRadius:
//                                                 BorderRadius
//                                                     .circular(
//                                                     10),
//                                               ),
//                                               child: Stack(
//                                                   children: [
//                                                     Padding(
//                                                       padding:
//                                                       const EdgeInsets
//                                                           .all(
//                                                           8.0),
//                                                       child: Column(
//                                                         crossAxisAlignment:
//                                                         CrossAxisAlignment
//                                                             .start,
//                                                         children: [
//                                                           Row(
//                                                             children: [
//                                                               getStatusTextAndIcon(
//                                                                   item.status),
//                                                               sb(0,
//                                                                   2),
//                                                               getStatusIcon(
//                                                                   item.status),
//                                                             ],
//                                                           ),
//                                                           sb(10, 0),
//                                                           item.remark
//                                                               .isNotEmpty
//                                                               ? Text(
//                                                             "Remark: ${item.remark}",
//                                                             style: const TextStyle(
//                                                               fontSize: 12,
//                                                             ),
//                                                             maxLines: 3,
//                                                             overflow: TextOverflow.ellipsis,
//                                                           )
//                                                               : sb(
//                                                               0,
//                                                               0)
//                                                         ],
//                                                       ),
//                                                     ),
//                                                     Row(
//                                                       mainAxisAlignment: MainAxisAlignment.end,
//                                                       children: [
//                                                         Container(
//                                                           alignment:
//                                                           Alignment
//                                                               .center,
//                                                           decoration:
//                                                           BoxDecoration(
//                                                             color:  !item.is_ticket_rasied?Colors
//                                                                 .orange
//                                                                 .shade50:Colors.blue.shade50,
//                                                             border:
//                                                             Border.all(
//                                                               color:  !item.is_ticket_rasied?Colors
//                                                                   .orange
//                                                                   .shade100:Colors.blue.shade100,
//                                                               width: 1,
//                                                             ),
//                                                             borderRadius:
//                                                             BorderRadius
//                                                                 .circular(
//                                                                 10),
//                                                           ),
//                                                           child:
//                                                           GestureDetector(
//                                                             onTap: () {
//                                                               int i = 0;
//
//                                                               item.is_ticket_rasied?
//                                                               Navigator.push(
//                                                                   context,
//                                                                   MaterialPageRoute(
//                                                                       builder: (context) => TicketDetailUIController(ticketId: item.ticket_id.toString(),))):
//                                                               showDialog
//                                                                 (
//                                                                 context
//                                                                     :
//                                                                 context,
//                                                                 builder: (BuildContext context) {
//
//                                                                   return AlertDialog(
//                                                                     title: Row(
//                                                                       children: [
//                                                                         Text('Dear '),
//                                                                         Text(item.name.capitalizeFirst.toString(), style: TextStyle(
//                                                                             fontWeight: FontWeight.bold)),
//                                                                         Text(','),
//                                                                       ],
//                                                                     ),
//
//                                                                     content: StatefulBuilder(
//                                                                       builder: (BuildContext context,
//                                                                           StateSetter setState) {
//                                                                         return SingleChildScrollView(
//                                                                           child: Column(
//                                                                             children: [
//                                                                               Text(
//                                                                                 'Please describe the issue you are experiencing.',
//                                                                                 style: TextStyle(
//                                                                                     fontSize: 14),),
//                                                                               SizedBox(height: 35),
//                                                                               TextFormField(
//                                                                                 autofocus: true,
//                                                                                 controller: _.subject,
//                                                                                 maxLines: null,
//                                                                                 decoration: InputDecoration(
//                                                                                   label: Text('Subject',
//                                                                                       style: TextStyle(
//                                                                                           color: Colors
//                                                                                               .grey)),
//                                                                                   border: OutlineInputBorder(),
//                                                                                   focusedBorder: OutlineInputBorder(
//                                                                                       borderSide: BorderSide(
//                                                                                           color: Colors
//                                                                                               .blue)),
//                                                                                 ),
//                                                                               ),
//                                                                               SizedBox(height: 10),
//                                                                               TextFormField(
//                                                                                 controller: _.issue,
//                                                                                 maxLines: 4,
//                                                                                 decoration: InputDecoration(
//                                                                                   label: Text('Issue',
//                                                                                       style: TextStyle(
//                                                                                           color: Colors
//                                                                                               .grey)),
//                                                                                   border: OutlineInputBorder(),
//                                                                                   focusedBorder: OutlineInputBorder(
//                                                                                       borderSide: BorderSide(
//                                                                                           color: Colors
//                                                                                               .blue)),
//                                                                                 ),
//                                                                               ),
//                                                                             ],
//                                                                           ),
//                                                                         );
//                                                                       },
//                                                                     ),
//                                                                     actions: [
//                                                                       ElevatedButton(
//                                                                         onPressed: () {
//                                                                           setState(() {
//                                                                             _.subject.text = '';
//                                                                             _.issue.text = '';
//                                                                           });
//                                                                           Get.back();
//                                                                         },
//                                                                         child: Text('Cancel',
//                                                                           style: TextStyle(
//                                                                               color: Colors.white),),
//                                                                         style: ElevatedButton.styleFrom(
//                                                                             backgroundColor: Colors.red),
//                                                                       ),
//                                                                       ElevatedButton(
//                                                                         onPressed: () {
//                                                                           if (_.subject.text == '' ||
//                                                                               _.issue.text == '') {
//                                                                             AppUtils.showSnackBar(
//                                                                                 'Try Again!',
//                                                                                 'Required Details are Missing',
//                                                                                 false);
//                                                                           } else {
//                                                                             ///todo
//                                                                             _.submitQuery(item.lead_id_primary,_.subject, _.issue);
//                                                                             // refreshing page here!
//                                                                             _.getLeadMaster();
//                                                                             // submission is done here !
//                                                                             Navigator.of(context).pop();
//                                                                           }
//                                                                           print(_.subject.text);
//                                                                           print(_.issue.text);
//                                                                         },
//                                                                         child: Text('Submit',
//                                                                           style: TextStyle(
//                                                                               color: Colors.white),),
//                                                                         style: ElevatedButton.styleFrom(
//                                                                             backgroundColor: Colors.green),
//                                                                       ),
//
//                                                                     ],
//                                                                   );
//                                                                 },
//                                                               );
//                                                               // }else{
//                                                               //             Get.to(TicketListUIController());
//                                                               //           }
//                                                               // }
//                                                             },
//                                                             child: Padding(
//                                                               padding:
//                                                               const EdgeInsets
//                                                                   .all(
//                                                                   7.0),
//                                                               child:
//                                                               !item.is_ticket_rasied? Text(
//                                                                 'Raise Ticket',
//                                                                 style:
//                                                                 const TextStyle(
//                                                                   fontSize:
//                                                                   12,
//                                                                 ),
//                                                               ):Text(
//                                                                 'Show Ticket',
//                                                                 style:
//                                                                 const TextStyle(
//                                                                   fontSize:
//                                                                   12,
//                                                                 ),
//                                                               ),
//                                                             ),
//                                                           ),
//                                                         ),
//
//
//                                                         Align(
//                                                           alignment:
//                                                           Alignment
//                                                               .topRight,
//                                                           child:
//                                                           Padding(
//                                                             padding:
//                                                             const EdgeInsets
//                                                                 .all(
//                                                                 5.0),
//                                                             child:
//                                                             Container(
//                                                               decoration:
//                                                               BoxDecoration(
//                                                                 color: Colors
//                                                                     .green
//                                                                     .shade50,
//                                                                 border:
//                                                                 Border.all(
//                                                                   color: Colors
//                                                                       .green
//                                                                       .shade100,
//                                                                   width:
//                                                                   1,
//                                                                 ),
//                                                                 borderRadius:
//                                                                 BorderRadius.circular(10),
//                                                               ),
//                                                               child:
//                                                               Padding(
//                                                                 padding: const EdgeInsets
//                                                                     .all(
//                                                                     7.0),
//                                                                 child:
//                                                                 Container(
//                                                                   child:
//                                                                   Text(
//                                                                     item.leadId,
//                                                                     style:
//                                                                     const TextStyle(
//                                                                       fontSize: 12,
//                                                                     ),
//                                                                     textAlign:
//                                                                     TextAlign.center,
//                                                                   ),
//                                                                 ),
//                                                               ),
//                                                             ),
//                                                           ),
//                                                         ),
//
//                                                       ],
//                                                     ),
//                                                   ]),
//                                             ),
//                                             const SizedBox(
//                                                 height: 8),
//                                           ],
//                                         ),
//                                       ),
//                                     ),
//                                   ]),
//                                 ),
//                               ),
//                               if (item.payout.isNotEmpty &&
//                                   item.payout != "0" &&
//                                   item.status == "Reject")
//                                 Align(
//                                   alignment: Alignment.topRight,
//                                   child: Padding(
//                                     padding: const EdgeInsets.only(
//                                         top: 5, right: 12),
//                                     child: Container(
//                                       decoration: BoxDecoration(
//                                         color:
//                                         Colors.orange.shade100,
//                                         borderRadius:
//                                         const BorderRadius.only(
//                                             bottomLeft:
//                                             Radius.circular(
//                                                 10),
//                                             topRight:
//                                             Radius.circular(
//                                                 10)),
//                                       ),
//                                       child: Padding(
//                                         padding:
//                                         const EdgeInsets.all(
//                                             7.0),
//                                         child: Text(
//                                           "₹ " + item.payout,
//                                           style: const TextStyle(
//                                             fontSize: 13,
//                                           ),
//                                           textAlign:
//                                           TextAlign.center,
//                                         ),
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                             ],
//                           ),
//                         ),
//                       );
//                     }).toList(),
//                   ),
//                 )
//               ],
//             )]),

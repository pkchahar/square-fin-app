import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'OtpVerifyController.dart'; // Assuming this import is correct

class OtpVerifyViewController extends StatefulWidget {
  const OtpVerifyViewController({super.key});

  @override
  _OtpVerifyViewControllerState createState() => _OtpVerifyViewControllerState();
}

class _OtpVerifyViewControllerState extends State<OtpVerifyViewController> {
  final TextEditingController _otpController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey buttonKey = GlobalKey();
  String otp = '';
  late Timer _timer;
  late ValueNotifier<int> _secondsNotifier;

  @override
  void initState() {
    super.initState();
    _secondsNotifier = ValueNotifier<int>(45);
    _startTimer();
  }

  void _startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      if (_secondsNotifier.value > 0) {
        _secondsNotifier.value--;
      } else {
        timer.cancel();
      }
    });
  }

  @override
  void dispose() {
    _timer.cancel(); // Dispose of the timer when the widget is removed
    _secondsNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OtpVerifyController>(
      init: OtpVerifyController(mContext: context),
      builder: (_) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: [
              const Align(
              alignment: Alignment.bottomRight,
              child: Image(image: AssetImage('res/images/bottomDesign.png'),height: 150,)),
            Container(color: Colors.white,
              width: double.infinity,
              height: MediaQuery.of(context).size.height*0.85,
            ),
            // const Align(
            //     alignment: Alignment.topLeft,
            //     child: Image(image: AssetImage('res/images/profile/layer2.png'),height: 150,width: 100,)),
              Center(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Center(
                      child: Column(
                        children: [
                          Image.asset("res/images/lockScreen.png",height: 250,),
                          const SizedBox(height: 26),
                          Text(
                            _.title,
                            style: const TextStyle(
                              fontSize: 22,
                              // color: Color(0xFF40C4F5),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10, left: 30, right: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text(
                                  'OTP code sent to',
                                  textAlign: TextAlign.center,
                                  style:
                                  TextStyle(color: Colors.black, fontSize: 13),
                                ),
                                const SizedBox(width: 10),
                                InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    _.userMobile,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      color: Color(0xFF0067FF),
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 5),
                                InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Icon(
                                    Icons.mode_edit_outline_outlined,
                                    size: 18,
                                    color: Color(0xFFF46860),
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(height: 25.0),
                          PinFieldAutoFill(
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(6),
                            ],
                            controller: _otpController,
                            codeLength: 6,
                            onCodeChanged: (val) {
                              print(val);
                              if (val!.length == 6) {
                                otp = val.toString();
                                (buttonKey.currentContext as Element)
                                    .markNeedsBuild();
                                (buttonKey.currentContext!.widget
                                as ElevatedButton)
                                    .onPressed!();
                              }
                            },
                          ),
                          const Padding(
                            padding:
                            EdgeInsets.only(top: 10, left: 30, right: 30),
                            child: Text(
                              'Haven’t received verification code?',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.grey, fontSize: 15),
                            ),
                          ),
                          const SizedBox(height: 8),
                          GestureDetector(
                            onTap: () {
                              if (_secondsNotifier.value == 0) {
                                _.resendOtp();
                                _secondsNotifier.value = 45; // Reset countdown
                                _startTimer(); // Restart timer
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.only(bottom: 1.0),
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    width: 1.0,
                                    color: _secondsNotifier.value == 0
                                        ? const Color(0xFF0067FF)
                                        : Colors.transparent,
                                  ),
                                ),
                              ),
                              child: ValueListenableBuilder<int>(
                                valueListenable: _secondsNotifier,
                                builder: (context, secondsRemaining, _) {
                                  return Text(
                                    secondsRemaining > 0
                                        ? 'Resend OTP in $secondsRemaining seconds'
                                        : 'Resend Code',
                                    style: TextStyle(
                                      color: secondsRemaining > 0
                                          ? Colors.grey
                                          : const Color(0xFF0067FF)
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                          const SizedBox(height: 30),
                          GetBuilder<OtpVerifyController>(
                              builder: (loginController) {
                                return ElevatedButton(
                                  key: buttonKey,
                                  onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                      if (!loginController.isLoading) {
                                        _.verifyOtp(otp);
                                      }
                                    }
                                  },
                                  style: ButtonStyle(
                                    backgroundColor: WidgetStateProperty.all<Color>(
                                        const Color(0xFF0067FF)),
                                    shadowColor: WidgetStateProperty.all<Color>(Colors.transparent),
                                    minimumSize: WidgetStateProperty.all<Size>(
                                        const Size(double.infinity, 45)),
                                    shape: WidgetStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5),
                                      ),

                                    ),
                                  ),
                                  child: loginController.isLoading
                                      ? const CircularProgressIndicator(
                                      color: Colors.white)
                                      : const Text(
                                    'Verify Verification Code',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  ),
                                );
                              })
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
        ]));
      },
    );
  }
}

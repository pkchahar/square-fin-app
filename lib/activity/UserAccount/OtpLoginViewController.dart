import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sms_autofill/sms_autofill.dart';

import '../../network/ApiClient.dart';
import 'OtpLoginController.dart';

class OtpLoginViewController extends StatefulWidget {
  const OtpLoginViewController({super.key});


  @override
  State<OtpLoginViewController> createState() => _OtpLoginViewControllerState();
}

class _OtpLoginViewControllerState extends State<OtpLoginViewController> {


  final _formKey = GlobalKey<FormState>();

  late String error;

  String ipString = "";

  TextEditingController userIdController = TextEditingController();

  TextEditingController userPassController = TextEditingController();

  String imageUrl = "res/images/lockScreen.png";

  String des = "Enter your mobile number below to get OTP for Login/Sign up!!";

  final TextEditingController _mobile = TextEditingController();

  AppClient appClient = AppClient();

  bool isNewUser = false;

  String version = "";

  String userMobile = "";

  var store = GetStorage();


  @override
  void initState() {
    super.initState();
  }

  // Future<void> submit() async {
  //   if(_mobile.text.isEmpty)return;
  //   var appSignatureID = await SmsAutoFill().getAppSignature;
  //   Map sendOtpData ={
  //     "mobile_number" : _mobile.text,
  //     "app_signature_id" :appSignatureID
  //   };
  //   print(sendOtpData);
  // }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OtpLoginController>(
      init: OtpLoginController(mContext: context),
      builder: (_) {
        return Scaffold(
            body: Stack(
              children:[
                 const Align(
                alignment: Alignment.bottomRight,
                child: Image(image: AssetImage('res/images/bottomDesign.png'),height: 150,)),
                Container(color: Colors.white,
                width: double.infinity,
                  height: MediaQuery.of(context).size.height*0.85,
                ),
                // const Align(
                //     alignment: Alignment.topLeft,
                //     child: Image(image: AssetImage('res/images/profile/layer2.png'),height: 150,width: 100,)),
                Center(
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Center(
                        child: Column(children: [
                          Image.asset(imageUrl,height: 250,),

                          Padding(
                            padding:
                            const EdgeInsets.only(top: 10, left: 30, right: 30),
                            child: Text(
                              des,
                              textAlign: TextAlign.center,
                              style:
                              const TextStyle(color: Colors.black, fontSize: 15),
                            ),
                          ),
                          const SizedBox(height: 25.0),
                          TextFormField(
                            controller: _mobile,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(10),
                            ],
                            decoration: const InputDecoration(
                              labelText: 'Mobile Number',
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.lightBlue,
                                ),
                              ),
                              labelStyle: TextStyle(color: Colors.black54),
                            ),
                            keyboardType: TextInputType.phone,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter your Mobile Number';
                              }
                              return null;
                            },
                          ),
                          const SizedBox(
                            height: 50,
                          ),

                          GetBuilder<OtpLoginController>(builder: (loginController) {
                            return ElevatedButton(
                                onPressed: () async {
                                  if (!loginController.isLoading) {
                                    SharedPreferences store  = await  SharedPreferences.getInstance();
                                    userMobile = _mobile.text;
                                    store.setString(AppUtils.userMobile, userMobile);

                                    if (_formKey.currentState!.validate()) {
                                      _.signCode = await SmsAutoFill().getAppSignature;
                                      _.getLoginOtp(userMobile);
                                    }
                                  }
                                },
                                style: ButtonStyle(
                                  backgroundColor: WidgetStateProperty.all<Color>(
                                      const Color(0xFF0067FF)),
                                  shadowColor: WidgetStateProperty.all<Color>(
                            Colors.transparent),
                                  minimumSize: WidgetStateProperty.all<Size>(
                                      const Size(double.infinity, 45)),
                                  shape: WidgetStateProperty.all<
                                      RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                  ),
                                ),

                                child: loginController.isLoading
                                    ? const CircularProgressIndicator(
                                  color: Colors.white,
                                )
                                    : const Text(
                                  'Send Verification Code',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18),
                                ));
                          }),
                        ]),
                      ),
                    ),
                  ),
                ),
              ),
          ])
            );
      },
    );
  }
}

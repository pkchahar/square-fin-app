
import 'package:flutter/material.dart';
import 'package:get/get.dart';


import '../../utils/AppUtils.dart';
import 'UpdateProfileController.dart';

class UpdateProfileUIController extends StatefulWidget {
  const UpdateProfileUIController({super.key});

  @override
  _UpdateProfileUIController createState() => _UpdateProfileUIController();
}

class _UpdateProfileUIController extends State<UpdateProfileUIController> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UpdateProfileController>(
      init: UpdateProfileController(context: context),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Update Profile"),
          ),
          backgroundColor: AppUtils.backgroundColor,
          body:  _.isLoading
              ? const Center(
              child: Padding(
                padding: EdgeInsets.all(10),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                ),
              )) : SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Form(
                key: _.formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    const SizedBox(height: 16.0),
                    TextFormField(
                      controller: _.nameController,
                      decoration: const InputDecoration(
                        labelText: 'Name',
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.lightBlue,
                          ),
                        ),
                        labelStyle: TextStyle(color: Colors.black54),
                      ),
                      keyboardType: TextInputType.name,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your name';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 16.0),
                    TextFormField(
                      controller: _.emailController,
                      decoration: const InputDecoration(
                        labelText: 'Email',
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.lightBlue,
                          ),
                        ),
                        labelStyle: TextStyle(color: Colors.black54),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your email address';
                        } else if (!RegExp(r'\S+@\S+\.\S+').hasMatch(value)) {
                          return 'Please enter a valid email address';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 16.0),
                    TextFormField(
                      controller: _.messageController,
                      decoration: const InputDecoration(
                        labelText: 'Address',
                        border: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.lightBlue,
                          ),
                        ),
                        labelStyle: TextStyle(color: Colors.black54),
                      ),
                      maxLines: 4,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter a address';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 16.0),

                    Container(
                        alignment: Alignment.bottomCenter,
                        width: double.infinity,
                        padding: const EdgeInsets.all(8),
                        child: ElevatedButton(
                          onPressed: () {
                            if (_.formKey.currentState!.validate()) {
                              String name = _.nameController.text;
                              String email = _.emailController.text;
                              String address = _.messageController.text;
                              _.submitQuery(name, email, address);
                            }
                          },
                          style: ButtonStyle(
                            backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.lightBlue),
                            minimumSize: WidgetStateProperty.all<Size>(
                                const Size(double.infinity, 40)),
                            shape:
                            WidgetStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                            ),
                          ),
                          child: const Text(
                            'Update Profile',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        )),

                    Container(
                        alignment: Alignment.bottomCenter,
                        width: double.infinity,
                        padding: const EdgeInsets.all(8),
                        child: OutlinedButton(
                          onPressed: () {
                            _.deleteMyAccount();
                          },
                          style: ButtonStyle(
                            backgroundColor:
                            WidgetStateProperty.all<Color>(Colors.white),
                            minimumSize: WidgetStateProperty.all<Size>(
                                const Size(double.infinity, 40)),
                            shape:
                            WidgetStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),

                              ),
                            ),
                            side: WidgetStateProperty.all(
                              const BorderSide(color: Colors.red), // Set your desired border color
                            ),
                          ),
                          child: const Text(
                            'Delete My Account',
                            style: TextStyle(
                                color: Colors.red,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        )),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

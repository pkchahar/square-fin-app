import 'dart:convert';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:pi/activity/UserAccount/OtpLoginViewController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/Model/BasicResponse.dart';
import 'package:pi/network/ApiClient.dart';
import '../../../utils/AppUtils.dart';
import '../../utils/BottomBarScreen.dart';
import '../Kyc/KycViewController.dart';

class NewUserController extends GetxController {
  BuildContext context;

  NewUserController({required this.context});

  bool _isLoading = false;

  bool get isLoading => _isLoading;
  String version = '', userId = '', userType = '', token = '';
  late String branch,
      accountNo,
      ifscCode,
      name = '',
      mobile = '',
      email = '',
      address = '';
  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final panController = TextEditingController();
  bool skipKyc = false;

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
  }

  void submitQuery(name, email, pan) async {
    setLoading(true);
    update();
    try {
      Map<String, String> hData = {
        'Authorization': 'Bearer $token',
      };
      Map<String, dynamic> mData = {
        'name': name,
        'email': email,
        'pan_number': pan,
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.updateBasicInfo);
      var response = await http.post(url, headers: hData, body: mData);
      if (kDebugMode) {
        print(hData);
        print(mData);
        print(url);
        print(response.body);
      }
      if (response.statusCode == 200) {
        BasicResponse dash = BasicResponse.fromJson(jsonDecode(response.body));
        String message = dash.message ?? "";
        bool status = dash.status ?? false;
        if (status) {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString(AppUtils.userName, name);
          prefs.setString(AppUtils.userId, userId);
          prefs.setString(AppUtils.userMail, email);
          prefs.setString(AppUtils.userMobile, mobile);

          if(skipKyc){
            openDashboard();
          }else{
            Get.to(KycViewController(
                name: name,
                email: email,
                pan: pan));
          }
        }else {
          showMyDialog("Profile Update", message, status);
        }
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
    update();
  }

  openDashboard() async {
    BuildContext currentContext = context;
    Navigator.pushAndRemoveUntil(
      currentContext,
      MaterialPageRoute(
        builder: (context) => const BottomBarScreen(),
      ),
          (route) => false,
    );
  }

  showMyDialog(String title, String msg, bool pFlag) async {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(
          title,
          style: const TextStyle(color: Colors.lightBlue),
        ),
        content: Text(
          msg,
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () async {
                Navigator.pop(context);
            },
            child: const Text('OK', style: TextStyle(color: Colors.black38)),
          ),
        ],
      ),
    );
  }

  deleteMyAccount() async {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text(
          "Delete My Account",
          style: TextStyle(color: Colors.lightBlue),
        ),
        content: const Text(
          "After delete account, you will not able to login and fetch data again on Square Fin, \nAre you sure to Delete My Account ",
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('Cancel',
                style: TextStyle(
                    color: Colors.black38, fontWeight: FontWeight.bold)),
          ),
          TextButton(
            onPressed: () async {
              removeFcm();
              SharedPreferences preferences =
                  await SharedPreferences.getInstance();
              preferences.clear();
              Get.offAll(const OtpLoginViewController());
            },
            child: const Text('Delete', style: TextStyle(color: Colors.red)),
          ),
        ],
      ),
    );
  }

  Future<void> removeFcm() async {
    Map<String, String> hData = {
      'Authorization': 'Bearer $token',
    };
    Map<String, String> mData = {
      'Login_User_Id': userId,
      'Login_User_Type': userType,
      'platForm': AppClient.platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };

    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.deleteAccount);
      var response = await http.post(url, headers: hData, body: mData);
      if (kDebugMode) {
        print(mData);
        print(url);
        print(response.body);
      }

      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
      }
    } catch (e) {
      log(e.toString());
    }
  }
}

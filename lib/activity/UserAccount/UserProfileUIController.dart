
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pi/activity/UserAccount/AddBankViewController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timer_count_down/timer_controller.dart';
import '../../utils/AppUtils.dart';
import '../Kyc/KycViewController.dart';
import 'UserProfileController.dart';

class UserProfileUIController extends StatefulWidget {
  const UserProfileUIController({super.key});

  @override
  _UserProfileUIController createState() => _UserProfileUIController();
}

class _UserProfileUIController extends State<UserProfileUIController> {

  @override
  void initState() {
    super.initState();
  }
  var countdownController = CountdownController();
  var seconds = 60;
  int counter = 0;
bool showButton = true;
bool showSendOTPTuple = false;
bool showResendButton = false;
bool updateText = false;
  var panName;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<UserProfileController>(
      init: UserProfileController(context: context),
      builder: (_) {
        print(_.email);
        print('this is email');
         panName = _.name.capitalizeFirst;
        var userName = _.name.toLowerCase();
        // userName = userName.toLowerCase();
        // userName = '${userName.capitalizeFirst}${userName.length-2}';
        // userName = userName.capitalizeFirst.toString();
        userName = AppUtils.changeName(userName);
        // userName = AppUtils.capitalizeFirstWord(userName);
        // userName = AppUtils.capitalizeFirstWord(name.trim());
        return Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: AppUtils.backgroundColor,
          appBar: AppBar(
            title: Text(userName),
          ),
          body: Stack(
            children: [
              Container(
                height: double.infinity,
                width: double.infinity,
                  color: AppUtils.bgColor
              ),
              SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    _.kycStatus!='1'?Column(
                      children: [
                        Card(
                          margin: const EdgeInsets.only(left: 21.3,right: 21.3,top:10),
                          elevation: 2,
                          color: Colors.red.shade200,
                          child:Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(children: [
                              _.kycStatus=='0'?const Center(child: Text('Your KYC is Pending',style: TextStyle(fontWeight: FontWeight.w800),)):const Padding(
                                padding: EdgeInsets.only(top: 5,bottom:5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text('Your KYC has been Rejected',style: TextStyle(fontWeight: FontWeight.w800,fontSize:18)),
                                  ],
                                ),
                              ),
                              Text(_.remark)
                            ]),
                          ),
                        ),
                        const SizedBox(height: 5,),
                        SizedBox(
                            width: MediaQuery.of(context).size.width*0.89,
                            child:ElevatedButton(onPressed: ()async{
                              SharedPreferences preferences = await SharedPreferences.getInstance();
                              Get.to(()=>
                                  KycViewController(
                                      name: userName,
                                      email:  preferences.getString(AppUtils.userMail)!,
                                      pan: "")
                              );
                            },style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.green.shade100
                            ), child: const Text('Verify KYC',style: TextStyle(fontWeight: FontWeight.bold),),)
                        ),
                      ],
                    ):const SizedBox(),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 18.0, right: 18, top: 10, bottom: 10),
                      child: Card(
                        elevation: 2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: Center(
                            child: ExpansionTile(
                                initiallyExpanded: true,
                                leading: const Icon(
                                  Icons.person_pin_outlined,
                                  color: Colors.grey,
                                ),
                                title: const Text(
                                  "Personal Details",
                                  style: TextStyle(
                                      color: Color(0xff666666),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700),
                                ),
                                trailing: const Icon(Icons.more_horiz_rounded, size: 30,),//  Icon(Icons.list)
                                collapsedIconColor: Colors.blueGrey,iconColor: Colors.red,
                                children: [
                                  const Divider(),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.person,
                                          size: 20,
                                          color: Colors.lightBlue,
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Text(userName),
                                      ],
                                    ),
                                  ),
                                  const Divider(),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.phone_android,
                                          size: 20,
                                          color: Colors.lightBlue,
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Text(_.mobile),
                                      ],
                                    ),
                                  ),
                                  const Divider(),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.email_outlined,
                                          size: 20,
                                          color: Colors.lightBlue,
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        SizedBox(
                                            width: MediaQuery.of(context).size.width*0.57,
                                            child: Text(_.email,style: const TextStyle(overflow: TextOverflow.ellipsis,color: Colors.black),)),
                                        // Text(_.email),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width*0.2,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            children: [
                                              _.userType == "employee"?const SizedBox(): _.isMailVerified=='1'?const SizedBox():InkWell(child: const Text('Verify',style: TextStyle(color: Colors.red,),),
                                                  onTap:(){
                                                    //adding send otp code here !
                                                    setState(() {
                                                      Future.delayed(const Duration(seconds: 60),(){
                                                        updateText = true;
                                                      });
                                                    });
                                                    ///send verification mail
                                                    _.sendOtp(_.email);
                                                    showModalBottomSheet(
                                                        context: context,
                                                        scrollControlDisabledMaxHeightRatio: 0.7,
                                                        builder: (context) {
                                                          ScrollController scrollController = ScrollController();
                                                          WidgetsBinding.instance.addPostFrameCallback((_)  {
                                                            Future.delayed(const Duration(seconds: 60),(){
                                                              scrollController.animateTo(scrollController.position.extentTotal, duration: const Duration(seconds: 2), curve: Curves.linear);
                                                            });});
                                                          return StatefulBuilder(
                                                              builder: (context, setState) {
                                                                return SingleChildScrollView(
                                                                  controller: scrollController,
                                                                  child: Padding(
                                                                    padding: const EdgeInsets.all(8.0),
                                                                    child: Column(
                                                                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                      // crossAxisAlignment: CrossAxisAlignment.center,
                                                                      children: [
                                                                        Padding(
                                                                          padding: const EdgeInsets.only(top:5.0),
                                                                          child: Row(
                                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                            verticalDirection: VerticalDirection.up,
                                                                            children: [
                                                                              const Text('Verify Email',style: TextStyle(fontWeight: FontWeight.bold,fontSize:20,color: Colors.blue),),
                                                                              IconButton(onPressed: (){Get.back();}, icon: Icon(Icons.horizontal_rule,color: Colors.red.shade200,))
                                                                              // Icon(Icons.email_outlined,color: Colors.blue,size: 30,),SizedBox(width:10),
                                                                              // Text(_.email,style: TextStyle(fontWeight: FontWeight.bold,fontSize:17,color: Colors.blue))
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        const Divider(height: 10,color: Colors.grey,thickness: 0.2,),
                                                                        Row(children: [
                                                                          const Icon(Icons.alternate_email,color: Colors.grey,size: 17,),const SizedBox(width:5),
                                                                          Text(_.email,style: const TextStyle(fontWeight: FontWeight.bold,fontSize:10,color: Colors.grey)),
                                                                        ],),
                                                                        const SizedBox(height: 10,),
                                                                        // Text(_.email,style: const TextStyle(fontSize: 14),),
                                                                        Text('Verification code sent! You\'re just one step away from verifying your email address. Please enter the code sent to ${_.email}.',style: const TextStyle(fontSize: 12),),
                                                                        const SizedBox(height: 15,),
                                                                        TextFormField(maxLength: 6,autofocus: true,keyboardType: TextInputType.number,controller: _.otpController,decoration: const InputDecoration(
                                                                            label: Text('Enter OTP',style: TextStyle(color: Colors.grey),),
                                                                            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
                                                                            border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(5))
                                                                            )
                                                                        ),),const SizedBox(height: 10,),
                                                                        ElevatedButton(onPressed: (){
                                                                          var otp = _.otpController;
                                                                          // print(otp.text);
                                                                          // print('success or not');
                                                                          // print(_.email);
                                                                          _.verifyOtp(_.email, otp.text);
//verify OTP
                                                                        },style: ElevatedButton.styleFrom(backgroundColor: Colors.blueGrey.shade300,fixedSize: const Size(500, 50)), child:  const Text('Verify OTP'),),
                                                                        const SizedBox(height: 10,),
                                                                        showSendOTPTuple?!updateText?const Text('Resend OTP within a minute.'):TextButton(onPressed: (){
                                                                          ///Resend OTP button here.
                                                                          _.sendOtp(_.email);
                                                                          setState((){
                                                                            updateText = !updateText;
                                                                          });
                                                                        }, child: const Text('Resend OTP',style: TextStyle(decoration: TextDecoration.underline,color: Colors.blue,fontSize: 12),)):const SizedBox(),
                                                                        const SizedBox(height: 10,),
                                                                        // showResendButton?TextButton(onPressed: (){}, child: const Text('Resend OTP',style: TextStyle(decoration: TextDecoration.underline,color: Colors.blue
                                                                        //     ,fontSize: 12),)):SizedBox(),
                                                                        // SizedBox(height: 10,),
                                                                        SizedBox(height:MediaQuery.of(context).size.height*0.3,)
                                                                        // SvgPicture.asset('res/images/OTPimage.svg',
                                                                        // width: MediaQuery.of(context).size.width*0.5,
                                                                        //   height: MediaQuery.of(context).size.height*0.2,
                                                                        // )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                );});
                                                        });
                                                  }
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const Divider(),
                                ]),
                          ),
                        ),
                      ),
                    ),
                    if (_.userType != "employee")
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 18.0, right: 18, top: 10, bottom: 10),
                        child: Card(
                          elevation: 2,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: Center(
                              child: ExpansionTile(
                                leading: const Icon(
                                  Icons.verified_user_outlined,
                                  color: Colors.grey,
                                ),
                                title: const Text(
                                  "PAN Card Details",
                                  style: TextStyle(
                                      color: Color(0xff666666),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700),
                                ),
                                trailing: const Icon(Icons.more_horiz_rounded, size: 30,),//  Icon(Icons.list)
                                collapsedIconColor: Colors.blueGrey,iconColor: Colors.red,
                                children: [
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width,
                                    child: Card(
                                      elevation: 5.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10.0),
                                          gradient: LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                            colors: [
                                              Colors.purple.shade100,
                                              Colors.blue.shade100
                                            ],
                                          ),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 10,
                                              bottom: 45,
                                              left: 10,
                                              right: 10),
                                          child: Stack(
                                            children: [
                                              Container(
                                                alignment: Alignment.centerRight,
                                                child: Image.asset(
                                                  'res/images/satya.png',
                                                  fit: BoxFit.contain,
                                                  width: 20,
                                                ),
                                              ),
                                              Column(
                                                children: [
                                                  sb(20, 0),
                                                  Container(
                                                    alignment: Alignment.centerLeft,
                                                    child: const Padding(
                                                      padding: EdgeInsets.all(5.0),
                                                      child: Text(
                                                        "Name",
                                                        style:
                                                        TextStyle(fontSize: 13),
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                    alignment: Alignment.centerLeft,
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                      BorderRadius.circular(
                                                          8.0),
                                                      color: Colors.white,
                                                    ),
                                                    child: Padding(
                                                      padding:
                                                      const EdgeInsets.all(8.0),

                                                      child: Text(
                                                        userName,
                                                        style: const TextStyle(
                                                            fontWeight:
                                                            FontWeight.bold,
                                                            fontSize: 18),
                                                      ),
                                                    ),
                                                  ),
                                                  sb(20, 0),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                    children: [
                                                      Column(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: [
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            child: const Padding(
                                                              padding:
                                                              EdgeInsets.all(
                                                                  5.0),
                                                              child: Text(
                                                                "Pan Number",
                                                                textAlign: TextAlign
                                                                    .center,
                                                                style: TextStyle(
                                                                    fontSize: 13),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            decoration:
                                                            BoxDecoration(
                                                              borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                  8.0),
                                                              color: Colors.white,
                                                            ),
                                                            child: Padding(
                                                              padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                              child: Text(
                                                                _.pan,
                                                                style: const TextStyle(
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                    fontSize: 18),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      sb(0, 10),
                                                      Column(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: [
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            child: const Padding(
                                                              padding:
                                                              EdgeInsets.all(
                                                                  5.0),
                                                              child: Text(
                                                                "Date of Birth",
                                                                textAlign: TextAlign
                                                                    .center,
                                                                style: TextStyle(
                                                                    fontSize: 13),
                                                              ),
                                                            ),
                                                          ),
                                                          Container(
                                                            width: MediaQuery.of(context).size.width*0.4,
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            decoration:
                                                            BoxDecoration(
                                                              borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                  8.0),
                                                              color: Colors.white,
                                                            ),
                                                            child: Padding(
                                                              padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                              child: Text(
                                                                _.dob,
                                                                style: const TextStyle(
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                    fontSize: 18),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  // Row(
                                  //   mainAxisAlignment: MainAxisAlignment.end,
                                  //   children: [
                                  //     Padding(
                                  //       padding: const EdgeInsets.only(right:5),
                                  //       child: ElevatedButton(onPressed: (){
                                  //         showModalBottomSheet(
                                  //
                                  //           context: context,
                                  //           builder: (BuildContext context) {
                                  //             return Column(
                                  //               crossAxisAlignment: CrossAxisAlignment.start,
                                  //               children: [
                                  //                 Container(width: MediaQuery.of(context).size.width,height:20,color:Colors.blue.shade900,
                                  //                     child:InkWell(
                                  //                         onTap:(){
                                  //                           Get.back();
                                  //                           _.panNumberEdt.value = TextEditingValue.empty;
                                  //                           _.panImgEdt.value ='';
                                  //                         },
                                  //                         child: Icon(Icons.keyboard_arrow_down,color:Colors.white))
                                  //                 ),
                                  //                 Padding(
                                  //                   padding: const EdgeInsets.only(left: 8.0,top: 8,right:8),
                                  //                   child: Column(
                                  //                     crossAxisAlignment:CrossAxisAlignment.start,
                                  //                     children: [
                                  //                       Text('Update PAN Number'),
                                  //                       SizedBox(height: 5),
                                  //                       TextFormField(
                                  //                         textCapitalization: TextCapitalization.characters,
                                  //                         controller: _.panNumberEdt,
                                  //                         inputFormatters: [
                                  //                           FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9]')),
                                  //                           LengthLimitingTextInputFormatter(10),
                                  //                         ],
                                  //                         maxLength: 10,
                                  //                         decoration: InputDecoration(
                                  //                             border: OutlineInputBorder(
                                  //                                 borderRadius: BorderRadius.circular(11)),focusedBorder: OutlineInputBorder( borderRadius: BorderRadius.circular(11),borderSide: BorderSide(color: Colors.grey))),
                                  //                       ),
                                  //                       SizedBox(height: 10,),
                                  //                       Text("Update Photo"),
                                  //                       SizedBox(height:5),
                                  //                       Row(
                                  //                         crossAxisAlignment: CrossAxisAlignment.end,
                                  //                         children: [
                                  //                           InkWell(
                                  //                               onTap:(){
                                  //                                 setState((){
                                  //                                   _.getImage(_.panImgEdt);
                                  //                                 });
                                  //                               },
                                  //                               child:Obx(() => Container(
                                  //                                 height: 150,
                                  //                                 width: MediaQuery.of(context).size.width * 0.8,
                                  //                                 decoration: _.panImgEdt.value == ''
                                  //                                     ? BoxDecoration(
                                  //                                   color: Colors.grey.shade200,
                                  //                                   borderRadius: BorderRadius.circular(12),
                                  //                                   border: Border.all(color: Colors.grey.shade400),
                                  //                                 )
                                  //                                     : BoxDecoration(
                                  //                                   image: DecorationImage(
                                  //                                     image: FileImage(File(_.panImgEdt.value)),
                                  //                                     fit: BoxFit.contain,
                                  //                                   ),
                                  //                                 ),
                                  //                               ))
                                  //                           ),
                                  //                           IconButton(onPressed: (){
                                  //                             _.panImgEdt.value = '';
                                  //                           }, icon: Icon(Icons.delete_outline_rounded))
                                  //                         ],
                                  //                       ),
                                  //                       Padding(
                                  //                         padding: const EdgeInsets.only(top:15,right: 65),
                                  //                         child: Row(
                                  //                           mainAxisAlignment: MainAxisAlignment.end,
                                  //                           children: [
                                  //                             ElevatedButton(onPressed: (){
                                  //                               print("upload");
                                  //                               if(_.adharNumberEdt.text.isEmpty||_.adharNumberEdt.text.length!=12){
                                  //                                 AppUtils.showSnackBar('Error', 'Invalid PAN Number', false);
                                  //                               }else{
                                  //                                 AppUtils.showSnackBar('Success', 'Your PAN Card Will Be Updated Soon', true);
                                  //                                 Navigator.of(context).pop();
                                  //                               }
                                  //
                                  //                             }, child: Text("Upload"),style: ElevatedButton.styleFrom(backgroundColor: Colors.green.shade300),)
                                  //                           ],),
                                  //                       )
                                  //                     ],
                                  //                   ),
                                  //                 ),
                                  //               ],
                                  //             );
                                  //           },
                                  //         );
                                  //
                                  //       }, child: Text("Edit"),style: ElevatedButton.styleFrom(
                                  //           backgroundColor:Colors.teal.shade200
                                  //       ),),
                                  //     ), Padding(
                                  //       padding: const EdgeInsets.only(right:5),
                                  //       child: ElevatedButton(onPressed: (){
                                  //         showModalBottomSheet(
                                  //
                                  //           context: context,
                                  //           builder: (BuildContext context) {
                                  //             return Column(
                                  //               crossAxisAlignment: CrossAxisAlignment.start,
                                  //               children: [
                                  //                 Container(width: MediaQuery.of(context).size.width,height:20,color:Colors.blueGrey.shade500,
                                  //                     child:InkWell(
                                  //                         onTap:(){
                                  //                           Get.back();
                                  //                           _.panNumberVrf.value = TextEditingValue.empty;
                                  //                           _.panImgVrf.value ='';
                                  //                         },
                                  //                         child: Icon(Icons.keyboard_arrow_down,color:Colors.white))
                                  //                 ),
                                  //                 Padding(
                                  //                   padding: const EdgeInsets.only(left: 8.0,top: 8,right:8),
                                  //                   child: Column(
                                  //                     crossAxisAlignment:CrossAxisAlignment.start,
                                  //                     children: [
                                  //                       Text('PAN Number'),
                                  //                       SizedBox(height: 5),
                                  //                       TextFormField(
                                  //                         textCapitalization: TextCapitalization.characters,
                                  //                         controller: _.panNumberVrf,
                                  //                         inputFormatters: [
                                  //                           FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9]')),
                                  //                           LengthLimitingTextInputFormatter(10),
                                  //                         ],
                                  //                         maxLength: 10,
                                  //                         decoration: InputDecoration(
                                  //                             border: OutlineInputBorder(
                                  //                                 borderRadius: BorderRadius.circular(11)),focusedBorder: OutlineInputBorder( borderRadius: BorderRadius.circular(11),borderSide: BorderSide(color: Colors.grey))),
                                  //                       ),
                                  //                       SizedBox(height: 10,),
                                  //                       Text(""
                                  //                           "Upload Photo"),
                                  //                       SizedBox(height:5),
                                  //                       Row(
                                  //                         crossAxisAlignment: CrossAxisAlignment.end,
                                  //                         children: [
                                  //                           InkWell(
                                  //                               onTap:(){
                                  //                                 setState((){
                                  //                                   _.getImage(_.panImgVrf);
                                  //                                 });
                                  //                               },
                                  //                               child:Obx(() => Container(
                                  //                                 height: 150,
                                  //                                 width: MediaQuery.of(context).size.width * 0.8,
                                  //                                 decoration: _.panImgVrf.value == ''
                                  //                                     ? BoxDecoration(
                                  //                                   color: Colors.grey.shade200,
                                  //                                   borderRadius: BorderRadius.circular(12),
                                  //                                   border: Border.all(color: Colors.grey.shade400),
                                  //                                 )
                                  //                                     : BoxDecoration(
                                  //                                   image: DecorationImage(
                                  //                                     image: FileImage(File(_.panImgVrf.value)),
                                  //                                     fit: BoxFit.contain,
                                  //                                   ),
                                  //                                 ),
                                  //                               ))
                                  //                           ),
                                  //                           IconButton(onPressed: (){
                                  //                             _.panImgVrf.value = '';
                                  //                           }, icon: Icon(Icons.delete_outline_rounded))
                                  //                         ],
                                  //                       ),
                                  //                       Padding(
                                  //                         padding: const EdgeInsets.only(top:15,right: 65),
                                  //                         child: Row(
                                  //                           mainAxisAlignment: MainAxisAlignment.end,
                                  //                           children: [
                                  //                             ElevatedButton(onPressed: (){
                                  //                               print("upload");
                                  //                               if((_.panNumberVrf.text.isEmpty || _.panNumberVrf.text.length!=10)&&(_.panImgVrf.isEmpty)){
                                  //                                 AppUtils.showSnackBar('Error', 'Please Upload Photo and Enter PAN Number', false);
                                  //                               }else if(_.panImgVrf.isEmpty){
                                  //                                 AppUtils.showSnackBar('Error', 'Please Upload Photo', false);
                                  //                               }else if(_.panNumberVrf.text.isEmpty||_.panNumberVrf.text.length!=10){
                                  //                                 AppUtils.showSnackBar('Error', 'Invalid PAN Number', false);
                                  //                               }else{
                                  //                                 AppUtils.showSnackBar('Success', 'Your PAN Will Be Updated Soon', true);
                                  //                                 Navigator.of(context).pop();
                                  //                               }
                                  //                               // if(_.panNumberVrf.text.isEmpty||_.panNumberVrf.text.length!=12){
                                  //                               //   AppUtils.showSnackBar('Error', 'Invalid PAN Number', false);
                                  //                               // }else{
                                  //                               //   AppUtils.showSnackBar('Success', 'Your PAN Card Will Be Updated Soon', true);
                                  //                               //   Navigator.of(context).pop();
                                  //                               // }
                                  //                             }, child: Text("Verify"),style: ElevatedButton.styleFrom(backgroundColor: Colors.green.shade300),)
                                  //                           ],),
                                  //                       )
                                  //                     ],
                                  //                   ),
                                  //                 ),
                                  //               ],
                                  //             );
                                  //           },
                                  //         );
                                  //
                                  //       }, child: Text("Verify"),style: ElevatedButton.styleFrom(
                                  //         backgroundColor:Colors.green.shade200
                                  //       ),),
                                  //     ),
                                  //   ],
                                  // )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    if (_.userType != "employee")
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 18.0, right: 18, top: 10, bottom: 10),
                        child: Card(
                          elevation: 2,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: Center(
                              child: ExpansionTile(
                                leading: const Icon(
                                  Icons.fingerprint_outlined,
                                  color: Colors.grey,
                                ),
                                title: const Text(
                                  "Adhar Details",
                                  style: TextStyle(
                                      color: Color(0xff666666),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700),
                                ),
                                trailing: const Icon(Icons.more_horiz_rounded, size: 30,),//  Icon(Icons.list)
                                collapsedIconColor: Colors.blueGrey,iconColor: Colors.red,
                                children: [
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width,
                                    child: Card(
                                      elevation: 5.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10.0),
                                          gradient: LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                            colors: [
                                              Colors.orange.shade200,
                                              Colors.green.shade200
                                            ],
                                          ),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 20,
                                              bottom: 45,
                                              left: 10,
                                              right: 10),
                                          child: Column(
                                            children: [
                                              sb(20, 0),
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                child: const Padding(
                                                  padding: EdgeInsets.all(5.0),
                                                  child: Text(
                                                    "Name",
                                                    style: TextStyle(fontSize: 13),
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(8.0),
                                                  color: Colors.white,
                                                ),
                                                child: Padding(
                                                  padding:
                                                  const EdgeInsets.all(8.0),
                                                  child: Text(
                                                    userName,
                                                    style: const TextStyle(
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                ),
                                              ),
                                              sb(20, 0),
                                              Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment.start,
                                                children: [
                                                  Column(
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        alignment:
                                                        Alignment.centerLeft,
                                                        child: const Padding(
                                                          padding:
                                                          EdgeInsets.all(5.0),
                                                          child: Text(
                                                            "Adhar Card Number",
                                                            textAlign:
                                                            TextAlign.center,
                                                            style: TextStyle(
                                                                fontSize: 13),
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        alignment:
                                                        Alignment.centerLeft,
                                                        decoration: BoxDecoration(
                                                          borderRadius:
                                                          BorderRadius.circular(
                                                              8.0),
                                                          color: Colors.white,
                                                        ),
                                                        child: Padding(
                                                          padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                          child: Text(
                                                            _.adhar,
                                                            style: const TextStyle(
                                                                fontWeight:
                                                                FontWeight.bold,
                                                                fontSize: 18),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  sb(0, 10),
                                                  Column(
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        alignment:
                                                        Alignment.centerLeft,
                                                        child: const Padding(
                                                          padding:
                                                          EdgeInsets.all(5.0),
                                                          child: Text(
                                                            "Date of Birth",
                                                            textAlign:
                                                            TextAlign.center,
                                                            style: TextStyle(
                                                                fontSize: 13),
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        width: MediaQuery.of(context).size.width*0.4,
                                                        alignment:
                                                        Alignment.centerLeft,
                                                        decoration: BoxDecoration(
                                                          borderRadius:
                                                          BorderRadius.circular(
                                                              8.0),
                                                          color: Colors.white,
                                                        ),
                                                        child: Padding(
                                                          padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                          child: Text(
                                                            _.dob,
                                                            style: const TextStyle(
                                                                fontWeight:
                                                                FontWeight.bold,
                                                                fontSize: 18),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  // Row(
                                  //   mainAxisAlignment: MainAxisAlignment.end,
                                  //   children: [
                                  //     Padding(
                                  //       padding: const EdgeInsets.only(right:5,left:5),
                                  //       child: ElevatedButton(onPressed: (){
                                  //         showModalBottomSheet(
                                  //           context: context,
                                  //           builder: (BuildContext context) {
                                  //                 return Column(
                                  //                   crossAxisAlignment: CrossAxisAlignment.start,
                                  //                   children: [
                                  //                     Container(width: MediaQuery.of(context).size.width,height:20,color:Colors.blue.shade900,
                                  //                     child:InkWell(
                                  //                         onTap:(){
                                  //                           Get.back();
                                  //                           _.adharNumberEdt.value = TextEditingValue.empty;
                                  //                           _.adharImgEdt.value ='';
                                  //                         },
                                  //                         child: Icon(Icons.keyboard_arrow_down,color:Colors.white))
                                  //                     ),
                                  //                     Padding(
                                  //                       padding: const EdgeInsets.only(left: 8.0,top: 8,right:8),
                                  //                       child: Column(
                                  //                         crossAxisAlignment:CrossAxisAlignment.start,
                                  //                         children: [
                                  //                           Text('Update Adhar Number'),
                                  //                           SizedBox(height: 5),
                                  //                           TextFormField(
                                  //                             controller: _.adharNumberEdt,
                                  //                             keyboardType: TextInputType.number,
                                  //                             inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                  //                             maxLength: 12,
                                  //                             decoration: InputDecoration(
                                  //                                 border: OutlineInputBorder(
                                  //                                     borderRadius: BorderRadius.circular(11)),focusedBorder: OutlineInputBorder( borderRadius: BorderRadius.circular(11),borderSide: BorderSide(color: Colors.grey))),
                                  //                           ),
                                  //                           SizedBox(height: 10,),
                                  //                           Text("Update Photo"),
                                  //                           SizedBox(height:5),
                                  //                           Row(
                                  //                             crossAxisAlignment: CrossAxisAlignment.end,
                                  //                             children: [
                                  //                               InkWell(
                                  //                                 onTap:(){
                                  //                                   setState((){
                                  //                                   _.getImage(_.adharImgEdt);
                                  //                                   });
                                  //                                 },
                                  //                                 child:Obx(() => Container(
                                  //                                   height: 150,
                                  //                                   width: MediaQuery.of(context).size.width * 0.8,
                                  //                                   decoration: _.adharImgEdt.value == ''
                                  //                                       ? BoxDecoration(
                                  //                                     color: Colors.grey.shade200,
                                  //                                     borderRadius: BorderRadius.circular(12),
                                  //                                     border: Border.all(color: Colors.grey.shade400),
                                  //                                   )
                                  //                                       : BoxDecoration(
                                  //                                     image: DecorationImage(
                                  //                                       image: FileImage(File(_.adharImgEdt.value)),
                                  //                                       fit: BoxFit.contain,
                                  //                                     ),
                                  //                                   ),
                                  //                                 ))
                                  //                               ),
                                  //                               IconButton(onPressed: (){
                                  //                                 _.adharImgEdt.value = '';
                                  //                               }, icon: Icon(Icons.delete_outline_rounded))
                                  //                             ],
                                  //                           ),
                                  //                           Padding(
                                  //                             padding: const EdgeInsets.only(top:15,right: 65),
                                  //                             child: Row(
                                  //                               mainAxisAlignment: MainAxisAlignment.end,
                                  //                               children: [
                                  //                               ElevatedButton(onPressed: (){
                                  //                                 print("upload");
                                  //                                 if(_.adharNumberEdt.text.isEmpty||_.adharNumberEdt.text.length!=12){
                                  //                                   AppUtils.showSnackBar('Error', 'Invalid Adhar Number', false);
                                  //                                 }else{
                                  //                                   AppUtils.showSnackBar('Success', 'Your Adhar Will Be Updated Soon', true);
                                  //                                   Navigator.of(context).pop();
                                  //                                 }
                                  //
                                  //                               }, child: Text("Upload"),style: ElevatedButton.styleFrom(backgroundColor: Colors.green.shade300),)
                                  //                             ],),
                                  //                           )
                                  //                         ],
                                  //                       ),
                                  //                     ),
                                  //                   ],
                                  //                 );
                                  //               },
                                  //             );
                                  //
                                  //       }, child: Text("Edit"),style: ElevatedButton.styleFrom(
                                  //           backgroundColor:Colors.teal.shade200
                                  //       ),),
                                  //     ), Padding(
                                  //       padding: const EdgeInsets.only(right:5),
                                  //       child:  ElevatedButton(onPressed: (){
                                  //         showModalBottomSheet(
                                  //
                                  //           context: context,
                                  //           builder: (BuildContext context) {
                                  //             return Column(
                                  //               crossAxisAlignment: CrossAxisAlignment.start,
                                  //               children: [
                                  //                 Container(width: MediaQuery.of(context).size.width,height:20,color:Colors.blueGrey.shade500,
                                  //                     child:InkWell(
                                  //                         onTap:(){
                                  //                           Get.back();
                                  //                           _.adharNumberVrf.value = TextEditingValue.empty;
                                  //                           _.adharImgVrf.value ='';
                                  //                         },
                                  //                         child: Icon(Icons.keyboard_arrow_down,color:Colors.white))
                                  //                 ),
                                  //                 Padding(
                                  //                   padding: const EdgeInsets.only(left: 8.0,top: 8,right:8),
                                  //                   child: Column(
                                  //                     crossAxisAlignment:CrossAxisAlignment.start,
                                  //                     children: [
                                  //                       Text('Adhar Number'),
                                  //                       SizedBox(height: 5),
                                  //                       TextFormField(
                                  //                         controller: _.adharNumberVrf,
                                  //                         keyboardType: TextInputType.number,
                                  //                         inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                  //                         maxLength: 12,
                                  //                         decoration: InputDecoration(
                                  //                             border: OutlineInputBorder(
                                  //                                 borderRadius: BorderRadius.circular(11)),focusedBorder: OutlineInputBorder( borderRadius: BorderRadius.circular(11),borderSide: BorderSide(color: Colors.grey))),
                                  //                       ),
                                  //                       SizedBox(height: 10,),
                                  //                       Text("Upload Photo"),
                                  //                       SizedBox(height:5),
                                  //                       Row(
                                  //                         crossAxisAlignment: CrossAxisAlignment.end,
                                  //                         children: [
                                  //                           InkWell(
                                  //                               onTap:(){
                                  //                                 setState((){
                                  //                                   _.getImage(_.adharImgVrf);
                                  //                                 });
                                  //                               },
                                  //                               child:Obx(() => Container(
                                  //                                 height: 150,
                                  //                                 width: MediaQuery.of(context).size.width * 0.8,
                                  //                                 decoration: _.adharImgVrf.value == ''
                                  //                                     ? BoxDecoration(
                                  //                                   color: Colors.grey.shade200,
                                  //                                   borderRadius: BorderRadius.circular(12),
                                  //                                   border: Border.all(color: Colors.grey.shade400),
                                  //                                 )
                                  //                                     : BoxDecoration(
                                  //                                   image: DecorationImage(
                                  //                                     image: FileImage(File(_.adharImgVrf.value)),
                                  //                                     fit: BoxFit.contain,
                                  //                                   ),
                                  //                                 ),
                                  //                               ))
                                  //                           ),
                                  //                           IconButton(onPressed: (){
                                  //                             _.adharImgVrf.value = '';
                                  //                           }, icon: Icon(Icons.delete_outline_rounded))
                                  //                         ],
                                  //                       ),
                                  //                       Padding(
                                  //                         padding: const EdgeInsets.only(top:15,right: 65),
                                  //                         child: Row(
                                  //                           mainAxisAlignment: MainAxisAlignment.end,
                                  //                           children: [
                                  //                             ElevatedButton(onPressed: (){
                                  //                               print("upload");
                                  //                                 if((_.adharNumberVrf.text.isEmpty || _.adharNumberVrf.text.length!=12)&&(_.adharImgVrf.isEmpty)){
                                  //                                 AppUtils.showSnackBar('Error', 'Please Upload Photo and Enter Adhar Number', false);
                                  //                                 }else if(_.adharImgVrf.isEmpty){
                                  //                                   AppUtils.showSnackBar('Error', 'Please Upload Photo', false);
                                  //                               }else if(_.adharNumberVrf.text.isEmpty||_.adharNumberVrf.text.length!=12){
                                  //                                   AppUtils.showSnackBar('Error', 'Invalid Adhar Number', false);
                                  //                                 }else{
                                  //                                   AppUtils.showSnackBar('Success', 'Your Adhar Will Be Updated Soon', true);
                                  //                                 Navigator.of(context).pop();
                                  //                               }
                                  //
                                  //                             }, child: Text("Verify"),style: ElevatedButton.styleFrom(backgroundColor: Colors.green.shade300),)
                                  //                           ],),
                                  //                       )
                                  //                     ],
                                  //                   ),
                                  //                 ),
                                  //               ],
                                  //             );
                                  //           },
                                  //         );
                                  //
                                  //       }, child: Text("Verify"),style: ElevatedButton.styleFrom(
                                  //           backgroundColor:Colors.green.shade200
                                  //       ),),
                                  //     ),
                                  //   ],
                                  // )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    if (_.userType != "employee")
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 18.0, right: 18, top: 10, bottom: 10),
                        child: Card(
                          elevation: 2,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: Center(
                              child: ExpansionTile(
                                leading: const Icon(
                                  Icons.account_balance_outlined,
                                  color: Colors.grey,
                                ),
                                title: const Text(
                                  "Bank Details",
                                  style: TextStyle(
                                      color: Color(0xff666666),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700),
                                ),
                                trailing: const Icon(Icons.more_horiz_rounded, size: 30,),//  Icon(Icons.list)
                                collapsedIconColor: Colors.blueGrey,iconColor: Colors.red,
                                children: [
                                  _.isLoading
                                      ? const Center(
                                      child: Padding(
                                        padding: EdgeInsets.all(50),
                                        child: CircularProgressIndicator(
                                          valueColor:
                                          AlwaysStoppedAnimation<Color>(
                                              Colors.blue),
                                        ),
                                      ))
                                      : _.bankList.isNotEmpty
                                      ? ListView.builder(
                                    shrinkWrap: true,
                                    physics:
                                    const ClampingScrollPhysics(),
                                    itemCount: _.bankList.length,
                                    itemBuilder: (context, index) {
                                      if(_.bankList[index].kycStatus=='2'
                                          ||_.bankList[index].kycStatus=='0'){
                                        showButton = false;
                                        print('elephant $showButton');
                                      }
                                      return Padding(
                                          padding: const EdgeInsets.only(
                                              top: 5,
                                              right: 2,
                                              bottom: 5,
                                              left: 2),
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            child: Card(
                                              elevation: 5.0,
                                              shape:
                                              RoundedRectangleBorder(
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10.0),
                                              ),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius
                                                      .circular(10.0),
                                                  border: Border.all(
                                                    color:
                                                    Colors.blueGrey,
                                                    width: .1,
                                                  ),
                                                ),
                                                child: Padding(
                                                  padding:
                                                  const EdgeInsets
                                                      .only(
                                                      top: 10,
                                                      bottom: 10,
                                                      left: 10,
                                                      right: 10),
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child:
                                                        Padding(
                                                          padding:
                                                          const EdgeInsets
                                                              .all(
                                                              3.0),
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              const Text(
                                                                "Bank Name",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                    13),
                                                              ),
                                                              Container(
                                                                  alignment: Alignment
                                                                      .centerRight,
                                                                  child: getStatusIcon(_.bankList[index].kycStatus))
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Padding(
                                                          padding:
                                                          const EdgeInsets
                                                              .all(
                                                              3.0),
                                                          child: Text(
                                                            _.bankList[index].bankName,
                                                            style: const TextStyle(
                                                                fontWeight:
                                                                FontWeight
                                                                    .bold,
                                                                fontSize:
                                                                17),
                                                          ),
                                                        ),
                                                      ),
                                                      sb(20, 0),
                                                      Row(
                                                        mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                        children: [
                                                          Column(
                                                            crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                            children: [
                                                              Container(
                                                                alignment:
                                                                Alignment
                                                                    .centerLeft,
                                                                child:
                                                                Padding(
                                                                  padding:
                                                                  const EdgeInsets.all(5.0),
                                                                  child:
                                                                  Text(
                                                                    "A/C :${_.bankList[index].accountNumber}",
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                    style:
                                                                    const TextStyle(fontSize: 13),
                                                                  ),
                                                                ),
                                                              ),
                                                              Container(
                                                                alignment:
                                                                Alignment
                                                                    .centerLeft,
                                                                child:
                                                                Padding(
                                                                  padding:
                                                                  const EdgeInsets.all(5.0),
                                                                  child:
                                                                  Text(
                                                                    _.bankList[index].branchName,
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                    style:
                                                                    const TextStyle(fontSize: 13),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          sb(0, 10),
                                                          Column(
                                                            crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                            children: [
                                                              Container(
                                                                alignment:
                                                                Alignment
                                                                    .centerLeft,
                                                                child:
                                                                Padding(
                                                                  padding:
                                                                  const EdgeInsets.all(5.0),
                                                                  child:
                                                                  Text(
                                                                    "IFSC: ${_.bankList[index].ifscCode}",
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                    style:
                                                                    const TextStyle(fontSize: 13),
                                                                  ),
                                                                ),
                                                              ),
                                                              Container(
                                                                alignment:
                                                                Alignment
                                                                    .centerLeft,
                                                                child:
                                                                Padding(
                                                                  padding:
                                                                  const EdgeInsets.all(5.0),
                                                                  child:
                                                                  Text(
                                                                    "Account Type: ${_.bankList[index].accountType}",
                                                                    textAlign:
                                                                    TextAlign.center,
                                                                    style:
                                                                    const TextStyle(fontSize: 13),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                      /// 1 = verified 0 = pending 2 = cancel
                                                      _.bankList[index].kycStatus=='1'?_.bankList.length!=1? const Divider():const SizedBox():const SizedBox(),
                                                      _.bankList[index].kycStatus=='1'? _.bankList.length!=1?Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        child: Row(
                                                          children: [
                                                            Checkbox(
                                                              value: _.bankList[index]
                                                                  .isPrimary ==
                                                                  "1",
                                                              activeColor:
                                                              Colors
                                                                  .blue,
                                                              onChanged:
                                                                  (bool?
                                                              value) {
                                                                setState(
                                                                        () {
                                                                      String v = _
                                                                          .bankList[index]
                                                                          .kycStatus;
                                                                      if (v.isNotEmpty &&
                                                                          v ==
                                                                              "1") {
                                                                        bool
                                                                        isChecked =
                                                                        value!;

                                                                        if (isChecked) {
                                                                          _.bankList[index].isPrimary =
                                                                          "1";
                                                                          String
                                                                          bankId =
                                                                              _.bankList[index].id;
                                                                          _.setPrimaryData(bankId);
                                                                        } else {
                                                                          _.bankList[index].isPrimary =
                                                                          "0";
                                                                        }
                                                                      } else {
                                                                        AppUtils.showSnackBar(
                                                                            "Set As Primary Account",
                                                                            "This account KYC not approved!",
                                                                            false);
                                                                      }
                                                                      _.updateData();
                                                                    });
                                                              },
                                                            ),
                                                            const Text(
                                                              "Set as Primary Account",
                                                              style: TextStyle(
                                                                  fontSize:
                                                                  15,
                                                                  fontWeight:
                                                                  FontWeight.w600),
                                                            ),
                                                          ],
                                                        ),
                                                      ):const SizedBox():const SizedBox(),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ));
                                    },
                                  )
                                      : const Center(
                                    child: Padding(
                                      padding: EdgeInsets.all(18.0),
                                      child: Text(
                                        "No Bank Data Found",
                                        style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.black),
                                      ),
                                    ),
                                  ),
                                  !showButton || _.bankList.isEmpty ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: ElevatedButton(
                                      onPressed: () {
                                        Get.to(const AddBankViewController());
                                      },
                                      style: ButtonStyle(
                                        backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                            Colors.lightBlue),
                                        shape: WidgetStateProperty.all<
                                            RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius:
                                            BorderRadius.circular(10.0),
                                          ),
                                        ),
                                      ),
                                      child: const Text(
                                        'Add New Bank Account',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ):const SizedBox()
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Color getBackgroundColor(String status, {Color defaultColor = Colors.orange}) {
    Color backgroundColor;
    switch (status) {
      case "0":
        backgroundColor = Colors.orange.shade300;
        break;
      case "1":
        backgroundColor = Colors.green;
        break;
      case "2":
        backgroundColor = Colors.red;
        break;
      default:
        backgroundColor = defaultColor;
        break;
    }

    return backgroundColor;
  }

  getStatusIcon(String kycStatus) {
    IconData iconData;
    switch (kycStatus) {
      case "0":
        iconData = Icons.pending;
        break;
      case "1":
        iconData = Icons.verified;
        break;
      case "2":
        iconData = Icons.cancel;
        break;
      default:
        iconData = Icons.pending;
        break;
    }
    return Icon(
      iconData,
      color: getBackgroundColor(kycStatus),
    );
  }

  getStatusText(String kycStatus) {
    String text = '';
    Color textColor = Colors.white;

    switch (kycStatus) {
      case "0":
        text = 'Pending';
        break;
      case "1":
        text = 'Verified';
        break;
      case "2":
        text = 'Rejected';
        break;
      default:
        text = 'NA';
        break;
    }

    return Text(
      text,
      style: TextStyle(
        color: textColor,
        fontWeight: FontWeight.bold,
        fontSize: 12,
      ),
    );
  }
}

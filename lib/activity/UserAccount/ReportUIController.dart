import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pi/activity/UserAccount/ReportController.dart';

import '../../utils/AppUtils.dart';

class ReportUIController extends StatefulWidget {
  const ReportUIController({super.key});

  @override
  State<ReportUIController> createState() => _ReportUIControllerState();
}

class _ReportUIControllerState extends State<ReportUIController> {


  @override
  Widget build(BuildContext context) {
    return GetBuilder<ReportController>(
        init: ReportController(),
        builder: (_){
          return Scaffold(
            appBar: AppBar(
                title: Text('${_.menuBar[_.screenIndex]}')
            ),
            // backgroundColor: AppUtils.bgColor,
            body: Stack( children: [

              Container(
                height: double.infinity,
                width: double.infinity,
                color: AppUtils.bgColor,
              ),

              Padding(
                padding: const EdgeInsets.only(top:50),
                child: NotificationListener<ScrollNotification>(
                  onNotification: (ScrollNotification scrollNotification) {
                    if (scrollNotification.metrics.pixels >=
                        scrollNotification.metrics.maxScrollExtent &&
                        !_.isUpdating){
                      _.appendList(_.reportType[_.screenIndex]);
                      print("hello world");
                    }
                    return true;
                  },
                  child: SingleChildScrollView(
                    child:Container(
                      child:
                      _.isLoading ? Column(
                         children: [
                          SizedBox(height: MediaQuery.of(context).size.height*0.4,),
                          const Center(child: CircularProgressIndicator(color: Colors.blue)),
                        ],
                      ):
                                 ((_.screenIndex == 1 && _.walletResList.isEmpty) ||
                                  (_.screenIndex == 0 && _.leadResList.isEmpty) ||
                                  (_.screenIndex == 2 && _.businessResList.isEmpty) ||
                                  (_.screenIndex == 3 && _.partnerResList.isEmpty) ||
                                  (_.screenIndex == 4 && _.customerResList.isEmpty))
                              ? _.emptyImage(context):
                      ListView.builder(shrinkWrap: true,physics: const NeverScrollableScrollPhysics(),
                          itemCount: _.screenIndex == 0
                              ? _.leadResList.length
                              : _.screenIndex == 1
                              ? _.walletResList.length
                              : _.screenIndex == 2
                              ? _.businessResList.length
                              : _.screenIndex == 3
                              ? _.partnerResList.length
                              : _.screenIndex == 4
                              ? _.customerResList.length
                              : 0,
                          itemBuilder: (context, index){
                            return
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                    decoration: BoxDecoration(
                                      color:Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.shade200,
                                          spreadRadius: 3,
                                          blurRadius: 10,
                                          offset: const Offset(0, 0),
                                        ),
                                      ],
                                      // border: Border.all(color: Colors.blueGrey)
                                    ),
                                    child:

                                    Column(children:[
                                      _.screenIndex == 0?

                                      _.leadReport(context, _.leadResList[index].icon, _.leadResList[index].productname, _.leadResList[index].createdAt, _.leadResList[index].name, _.leadResList[index].mobile, _.leadResList[index].email, _.leadResList[index].status, _.leadResList[index].remark, _.leadResList[index].isTicket.toString(), _.leadResList[index].ticketId.toString(), _.leadResList[index].id, _.leadResList[index].leadId):
                                      _.screenIndex == 1?
                                      Container(
                                          decoration: BoxDecoration(
                                            color:Colors.white,
                                            borderRadius: BorderRadius.circular(10),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey.shade200,
                                                blurRadius: 10,
                                                offset: const Offset(0, 0),
                                              ),
                                            ],
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Text(_.walletResList[index].transactionReason,style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.lightBlue,fontSize: 15),),
                                                    // ),
                                                    const Spacer(),
                                                    Container(
                                                        padding:const EdgeInsets.symmetric(vertical: 2,horizontal: 8),
                                                        decoration: BoxDecoration(color: Colors.deepPurple.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.deepPurple.shade100)),
                                                        child: Text(_.walletResList[index].withdrawalStatus== '0'?'Pending':_.walletResList[index].withdrawalStatus== '1'?'Approved':'Rejected',style: TextStyle(fontWeight: FontWeight.bold,color: _.walletResList[index].withdrawalStatus== '0'? Colors.yellow.shade900: _.walletResList[index].withdrawalStatus== '1'? Colors.green:Colors.red),)),
                                                  ],
                                                ),

                                                const Divider(color: Colors.blueGrey,),
                                                _.walletResList[index].refferenceId=='0'?const SizedBox():Container(
                                                    margin: const EdgeInsets.only(bottom: 5),
                                                    padding:const EdgeInsets.symmetric(vertical: 2,horizontal: 8),
                                                    decoration: BoxDecoration(color: Colors.red.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.red.shade100)),
                                                    child: Text("Ref. no.: ${_.walletResList[index].refferenceId}")),

                                                Row(
                                                  children: [

                                                    Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 7),
                                                        decoration: BoxDecoration(color: Colors.orange.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.orange.shade100)),
                                                        child: Text(_.walletResList[index].transactionType == 'Dr'? 'Debit':'Credit')),
                                                    const SizedBox(width: 10),
                                                    Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 7),
                                                        decoration: BoxDecoration(color: Colors.lightBlue.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.lightBlue.shade100)),
                                                        child: Text('Amount : ${_.walletResList[index].amount}')),
                                                  ],
                                                ),
                                                const SizedBox(height: 5),
                                                Row(
                                                  children: [

                                                    Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 7),
                                                        decoration: BoxDecoration(color: Colors.teal.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.teal.shade100)),
                                                        child: Text('Date : ${_.walletResList[index].date}'))
                                                  ],
                                                ),
                                                const Divider(),
                                                Row(
                                                  children: [
                                                    Container(
                                                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width*0.17),
                                                        child: const Text("Remark : ",style: TextStyle(fontWeight: FontWeight.bold),)),
                                                    Container(
                                                        constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width*0.68),
                                                        child: Text( _.walletResList[index].remark)),
                                                  ],
                                                )
                                              ],
                                            ),
                                          )
                                      )
                                          : _.screenIndex ==2?
                                      Container(
                                          decoration: BoxDecoration(
                                            color:Colors.white,
                                            borderRadius: BorderRadius.circular(10),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey.shade200,
                                                blurRadius: 10,
                                                offset: const Offset(0, 0),
                                              ),
                                            ],
                                          ),
                                          child:Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    const Text("Name : ",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.lightBlue,fontSize: 15),),
                                                    Container(
                                                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width*0.4),
                                                        child: Text(_.businessResList[index].customerName?? '' ,style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.lightBlue,fontSize: 15),)),
                                                    const Spacer(),
                                                    Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 7),
                                                        decoration: BoxDecoration(color: Colors.green.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.green.shade100)),
                                                        child: Text(_.businessResList[index].customerType?? '')),
                                                    const SizedBox(width: 5,)
                                                  ],
                                                ),
                                                const Divider(color: Colors.blueGrey),
                                                Container(
                                                    padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 7),
                                                    decoration: BoxDecoration(color: Colors.purple.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.purple.shade100)),
                                                    child: Text("mobile : ${_.businessResList[index].customerMobile?? ''}")),
                                                const SizedBox(height:5),
                                                Container(
                                                    padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 7),
                                                    decoration: BoxDecoration(color: Colors.pink.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.pink.shade100)),
                                                    child: Text("RM : ${_.businessResList[index].rm?.toLowerCase().capitalizeFirst?? ''}")), const SizedBox(height:5),
                                                Container(
                                                    padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 7),
                                                    decoration: BoxDecoration(color: Colors.teal.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.teal.shade100)),
                                                    child: Text("Agent : ${_.businessResList[index].agent?.toLowerCase().capitalizeFirst?? ''}")),
                                                const Divider(thickness: 0.5,color: Colors.blueGrey),

                                                Row(
                                                  children: [
                                                    _.businessResList[index].loanApplicationAmount != ""?Container(
                                                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width*0.3),
                                                        child: const Text("Loan Amount : ",style: TextStyle(fontWeight: FontWeight.bold),)):const SizedBox(),
                                                    _.businessResList[index].loanApplicationAmount != ""?Text(_.businessResList[index].loanApplicationAmount?? '' ):const SizedBox(),
                                                    const Spacer(),
                                                    Container(
                                                        padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 7),
                                                        decoration: BoxDecoration(color: Colors.indigo.shade50,borderRadius: BorderRadius.circular(10),border: Border.all(color: Colors.indigo.shade100)),
                                                        child: Text(_.businessResList[index].fileStatus??'')),
                                                  ],
                                                ),const Divider(height: 10, color:Colors.grey,thickness: 0.2,),
                                                _.businessResList[index].productName!.isNotEmpty? Row(
                                                  children: [
                                                    Container(
                                                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width*0.3),
                                                        child: const Text("Product Name : ",style: TextStyle(fontWeight: FontWeight.bold),)),
                                                    Text(_.businessResList[index].productName?? '' )
                                                  ],
                                                ):const SizedBox(),
                                                _.businessResList[index].productName!.isNotEmpty? const Divider(height: 10, color:Colors.grey,thickness: 0.2,):const SizedBox(),
                                                Row(
                                                  children: [
                                                    Container(
                                                        constraints: BoxConstraints(minWidth: MediaQuery.of(context).size.width*0.3),
                                                        child: const Text("Product Type : ",style: TextStyle(fontWeight: FontWeight.bold),)),
                                                    Text(_.businessResList[index].productType?? '' )
                                                  ],
                                                ),

                                                const Divider(height: 10, color:Colors.grey,thickness: 0.2,),
                                                _.businessResList[index].financeCoordinator!=null?Text("Finance Coordinator : ${_.businessResList[index].financeCoordinator?? ''}",style: const TextStyle(fontSize: 11),):const SizedBox(),

                                                _.businessResList[index].sanctionAmount!=null?Text("Sanction Amount : ${_.businessResList[index].sanctionAmount?? ''}",style: const TextStyle(fontSize: 11),):const SizedBox(),
                                                const SizedBox(height: 5,),
                                                _.businessResList[index].financeManager!=null?Text("Finance Manager : ${_.businessResList[index].financeManager?.capitalizeFirst?? ''}",style: const TextStyle(fontSize: 11),):const SizedBox(),
                                                Row(
                                                  children: [
                                                    // (_.businessResList[index].sanctionAmount!=null &&  _.businessResList[index].disbursedAmount!=null)?Text("•",style: TextStyle(fontSize: 10),):SizedBox(),
                                                    _.businessResList[index].disbursedAmount!=null?Text("Disbursed Amount : ${_.businessResList[index].disbursedAmount?? ''}",style: const TextStyle(fontSize: 11),):const SizedBox(),
                                                    const Spacer(),
                                                    // Icon(Icons.info_outline)
                                                  ],
                                                ),

                                              ],
                                            ),
                                          )
                                      )
                                          :_.screenIndex == 3?
                                      _.buildCard(context, "${_.partnerResList[index].name.capitalizeFirst} : ${_.partnerResList[index].empId}",_.partnerResList[index].email,"","",_.partnerResList[index].mobile.isEmpty?"":_.partnerResList[index].mobile,"Date : ${_.partnerResList[index].date}")
                                          :_.screenIndex ==4 ?
                                      _.buildCard(context, "${_.customerResList[index].name.capitalizeFirst} : ${_.customerResList[index].empId}", _.customerResList[index].email, "", _.customerResList[index].mobile.isEmpty?'': "Tel: ${_.customerResList[index].mobile}",_.customerResList[index].pan.isEmpty?'':"PAN Number : ${_.customerResList[index].pan}", _.customerResList[index].date.isEmpty?"":"Date : ${_.customerResList[index].date}")
                                          :
                                      const SizedBox(),
                                    ])
                                ),
                              );
                          }),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 50,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: _.menuBar.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                      child: InkWell(
                        onTap:(){
                          setState(() {
                            _.screenIndex = index;
                            _.fetchReport(_.reportType[_.screenIndex] ,"1");
                            _.cmd = '';
                            _.pageCount = 2;
                            // _.appendList(_.reportType[_.screenIndex]);
                          });
                        },
                        child: Container(
                          height: 50,
                          width: 140,
                          decoration: BoxDecoration(
                            color: _.screenIndex == index? _.colors1[index]:_.colors[index],
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.shade200,
                                blurRadius: 10,
                                offset: const Offset(0, 0),
                              ),
                            ],
                          ),
                          child: Center(child: Text(_.menuBar[index],
                            style: TextStyle(fontWeight: FontWeight.bold,
                                color: _.screenIndex == index
                                    ? Colors.white
                                    : Colors.black),)),
                        ),
                      ),
                    );
                  },
                  shrinkWrap: true,
                ),
              ),
            ]),
          );
        });
  }
}


import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:pi/activity/UserAccount/OtpVerifyViewController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/Model/Occupation.dart';

import '../../../utils/AppUtils.dart';
import '../../network/ApiClient.dart';
import '../Utility Controllers/forgot_pass.dart';

class OtpLoginController extends GetxController {
  BuildContext mContext;

  OtpLoginController({required this.mContext});

  bool _isLoading = false;

  bool get isLoading => _isLoading;
  Occupation productsData = Occupation();
  String ipString = "";
  var userId, password = "";
  late String error;
  String version = "";
  String fcmToken = "";
  bool passwordVisible = false;
  AppClient appClient = AppClient();
  var signCode;

  @override
  void onInit() {
    super.onInit();
    // FirebaseMessaging _firebaseMessaging =
    //     FirebaseMessaging.instance; // Change here
    // _firebaseMessaging.getToken().then((token) {
    //   fcmToken = token ?? "";
    //   print("FCM token is $token");
    // });
    getIpAddress();
  }


  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> onForgotPassClick() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(AppUtils.newUser, false);
    Get.to(const ForgotPass());
  }

  Future<void> onCreateNewAccount() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(AppUtils.newUser, true);
    Get.to(const ForgotPass());
  }

  bool showPass(bool pas) {
    passwordVisible = !pas;
    update();
    return passwordVisible;
  }


  Future<String> getIpAddress() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    version = preferences.getString(AppUtils.appVersion) ?? "";
    for (var interface in await NetworkInterface.list()) {
      for (var address in interface.addresses) {
        if (!address.isLinkLocal) {
          ipString = address.address;
          preferences.setString(AppUtils.ipAddress, ipString);
          return address.address;
        }
      }
    }
    return 'No IP address found';
  }


  Future<void> getLoginOtp(String userMobile) async {
    setLoading(true);
    Map parsed = await appClient.getLoginOtp(userMobile,signCode);
    dynamic status = parsed["status"];
    String msg = parsed["message"];

    print(msg);
    if (status != null && status == 1) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      print(userMobile);
      preferences.setString(AppUtils.userMobile, userMobile);
      Get.to(const OtpVerifyViewController());
    }

    Fluttertoast.showToast(msg: msg);
    setLoading(false);
  }
}

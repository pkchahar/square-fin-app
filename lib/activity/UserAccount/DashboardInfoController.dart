

import 'package:get/get.dart';

import '../../Model/UtilityModel/MyDashData.dart';

class DashboardInfoController extends GetxController {
  var SelectedButton;
  var businessList;
  var revenueList;
  var teamORself;

  // var sessionList = [
  //   'FTD',
  //   'MTD',
  //   'Previous MTD',
  //   'Previous MTD Growth %',
  //   'Last Year MTD',
  //   'Last Year MTD Growth %',
  //   'YTD',
  //   'Previous YTD',
  //   'Previous YTD Growth %'
  // ];
  List<MyDashData> mList = [];

  // final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  // final ScreenshotController _screenshotController = ScreenshotController();
  //
  // Future<void> takeScreenshotAndShare() async {
  //   try {
  //     // Capture the screen as a Uint8List
  //     Uint8List? image = await _screenshotController.capture();
  //     print('elephant');
  //     // Share the image
  //     if (image != null) {
  //       // Save the image to a temporary file (optional)
  //       File tempFile = await File('screenshot.png').writeAsBytes(image);
  //
  //       // Share the image file
  //       Share.shareFiles(['screenshot.png'], text: 'Check out my screenshot!');
  //       print('elephant1');
  //     }
  //   } catch (e) {
  //     print('Error: $e');
  //   }
  // }

  @override
  void onInit() {
    MyDashData s1 = MyDashData("FTD", "1000", "2342");
    mList.add(s1);
    s1 = MyDashData("FTD", "1000", "10");
    mList.add(s1);

    s1 = MyDashData("MTD", "11000", "20");
    mList.add(s1);

    s1 = MyDashData("Previous MTD", "1000", "300");
    mList.add(s1);

    s1 = MyDashData("Previous MTD Growth %", "10", "5");
    mList.add(s1);

    s1 = MyDashData("Last Year MTD", "11000", "400");
    mList.add(s1);

    s1 = MyDashData("YTD", "10", "60");
    mList.add(s1);
    s1 = MyDashData("Last Year MTD Growth %", "10", "60");
    mList.add(s1);
    s1 = MyDashData("Previous YTD", "10", "60");
    mList.add(s1);
    s1 = MyDashData("Previous YTD Growth %", "10", "60");
    mList.add(s1);

    super.onInit();
  }
}
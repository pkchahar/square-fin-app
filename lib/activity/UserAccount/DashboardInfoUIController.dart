import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pi/activity/UserAccount/DashboardInfoController.dart';
import 'package:custom_sliding_segmented_control/custom_sliding_segmented_control.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';

class DashboardInfoUIController extends StatefulWidget {
  const DashboardInfoUIController({super.key});

  @override

  State<DashboardInfoUIController> createState() =>
      _DashboardUIControllerState();
}

class _DashboardUIControllerState extends State<DashboardInfoUIController> {
  ScreenshotController screenshotController = ScreenshotController();

  void takeScreenshot() {
    screenshotController.capture().then((Uint8List? image) {
      if (image != null) {
        print('elephant');
       _saveAndShareScreenshot(image);
      } else {
        print('elephant1');
      }
    }).catchError((onError) {
      print(onError);
    });
  }

  Future<void> _saveAndShareScreenshot(Uint8List image) async {
    final directory = await getTemporaryDirectory();
    final imagePath = '${directory.path}/screenshot.png';
    File file = File(imagePath);
    await file.writeAsBytes(image);

    if (await file.exists()) {
      // Share the image using share_plus
      await Share.shareXFiles([XFile(imagePath)], text: 'Check out this screenshot!');
    } else {
      print('File does not exist');
    }
  }

  @override
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardInfoController>(
        init: DashboardInfoController(),
        builder: (_) {
          return Scaffold(
            appBar: AppBar(
              title: const Text('Dashboard'),
              actions: [
                IconButton(
                  onPressed: () {
                    takeScreenshot();
                  },
                  icon: const Icon(Icons.share),
                )
              ],
            ),
            body: Screenshot(
              controller: screenshotController,
              child: Container(
                color: Colors.white,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 1, right: 1),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 18,
                          ),
                          CustomSlidingSegmentedControl<int>(
                            initialValue: 2,
                            children: {
                              1: Container(
                                constraints: const BoxConstraints(minWidth: 80.0),
                                child: const Text('Team'),
                              ),
                              2: Container(
                                constraints: const BoxConstraints(minWidth: 80.0),
                                child: const Text('Self'),
                              ),
                            },
                            decoration: BoxDecoration(
                              color: CupertinoColors.lightBackgroundGray,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            thumbDecoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(6),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.shade300.withOpacity(.3),
                                  blurRadius: 4.0,
                                  spreadRadius: 1.0,
                                  offset: const Offset(
                                    0.0,
                                    2.0,
                                  ),
                                ),
                              ],
                            ),
                            duration: const Duration(milliseconds: 300),
                            curve: Curves.easeInToLinear,
                            onValueChanged: (v) {
                              print('elephant');
                              print(v);
                              _.teamORself = v;
                            },
                          ),
                          sb(10, 10),
                          DataTable(
                            columnSpacing: 10,
                            dataRowMaxHeight:60,
                            // dividerThickness: 5,
                            // showBottomBorder: true,
                            columns: [
                              DataColumn(
                                label:SizedBox(
                                  width:120,
                                  height: 100,
                                  child: Card(
                                    color: Colors.blue.shade50,
                                      elevation:1,
                                      child: const Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Flexible(child: Center(child:  Text(
                                          'Status',
                                          style: TextStyle(
                                            color: Colors.blue,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),)),
                                      )),
                                )

                              ),
                              DataColumn(
                                label:SizedBox(
                                  width:120,
                                  height: 100,
                                  child: Card(
                                      color: Colors.blue.shade50,
                                      elevation:1,
                                      child: const Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Flexible(child: Center(child:  Text(
                                          'Business',
                                          style: TextStyle(
                                            color: Colors.blue,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),)),
                                      )),
                                )
                              ),
                              DataColumn(
                                label: SizedBox(
                                  width:120,
                                  height: 100,
                                  child: Card(
                                      color: Colors.blue.shade50,
                                      elevation:1,
                                      child: const Padding(
                                        padding: EdgeInsets.all(8.0),
                                        child: Flexible(child: Center(child:  Text(
                                          'Revenue',
                                          style: TextStyle(
                                            color: Colors.blue,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),)),
                                      )),
                                )
                              ),
                            ],
                            rows: _.mList.map(
                                  (currentItem) {

                                return
                                  DataRow(
                                  cells: [
                                    DataCell(
                                        SizedBox(
                                      width:120,
                                      height: 100,
                                      child: Card(
                                          elevation:1,
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Flexible(child: Center(child: Text(currentItem.label,style: const TextStyle(fontSize: 15),))),
                                          )),
                                    )),
                                    DataCell(
                                        SizedBox(
                                          width:120,
                                          height: 100,
                                          child: Card(
                                              elevation:1,
                                              child: Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Flexible(child: Center(child: Text(currentItem.business,style: const TextStyle(fontSize: 15),))),
                                              )),
                                        )),
                                    // DataCell(Text(currentItem.business.toString())),
                                    DataCell(
                                        SizedBox(
                                          width:120,
                                          height: 100,
                                          child: Card(
                                              elevation:1,
                                              child: Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Flexible(child: Center(child: Text(currentItem.revenue,style: const TextStyle(fontSize: 15),))),
                                              )),
                                        )),
                                    // DataCell(Text(currentItem.revenue.toString())),
                                  ],
                                );
                              },
                            ).toList(),
                          ),],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }

}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pi/activity/Loan/DashLoan.dart';
import 'package:pi/activity/POS%20Panel/Customer/CustomerListUIController.dart';
import 'package:pi/activity/POS%20Panel/PosListController/PartnerListUIController.dart';
import 'package:pi/activity/UserAccount/ReportUIController.dart';
import 'package:pi/activity/Utility%20Controllers/LoanTermActivity.dart';
import 'package:pi/activity/Utility%20Controllers/contact_us.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:screenshot/screenshot.dart';
import '../../utils/CustomAppBar.dart';
import '../../utils/WebviewController.dart';
import '../Product/ProductLeadViewController.dart';
import '../Utility Controllers/AboutUs.dart';
import 'LeadCollectUIController.dart';
import 'ProfileController.dart';
import 'package:url_launcher/url_launcher.dart';
import 'DashboardController.dart';

class ProfileUIController extends StatefulWidget {
  const ProfileUIController({super.key});

  @override
  _ProfileUIController createState() => _ProfileUIController();
}

class _ProfileUIController extends State<ProfileUIController> {
  // String investNowTxt = 'Invest Now';
  // String loanTermsTxt = 'Loan Terms';
  // String myLeadsTxt = 'My Leads';
  // String raiseRequestTxt = 'Raise Request';
  // String myPartnerTxt = 'My Partner';
  //
  // String mutualFundTxt = 'Mutual Fund';
  // String fixedDepositTxt = 'Fixed Deposit (FD)';
  // String recurringDepositTxt = 'Recurring Deposit\n(RD)';
  // String apkaMakaanTxt = 'Apka Makaan\n(Real Estate)';
  // List gridTxt = [];
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DashboardController dashboardController =
        DashboardController(context: context);
    dashboardController.getSharedData();

    // ProfileController pf = ProfileController(context: context);

    return GetBuilder<ProfileController>(
      init: ProfileController(context: context),
      builder: (_) {
        return Scaffold(
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(60),
            child: CustomAppBar(
                userName: _.userName, kycStatus: _.kycStatus, pageInUse: '3'),
          ),
          backgroundColor: const Color(0xfff5f5f6),
          body: Stack(children: [
            Container(decoration:
            const BoxDecoration(image: DecorationImage(
                opacity: 0.2,
               filterQuality: FilterQuality.high,
                image: AssetImage("res/images/profile/bg.jpeg"),fit: BoxFit.cover))),

            /// nice image

            Container(
                height: double.infinity,
                width: double.infinity,
                color: AppUtils.bgColor),

            // Image(
            //   image: AssetImage("res/images/profile/bg.jpeg"),
            //   height: MediaQuery.of(context).size.height,
            //   width: MediaQuery.of(context).size.width,
            //   colorBlendMode:  BlendMode.color,
            //   color: Colors.blue,
            //   filterQuality: FilterQuality.high,
            //   opacity: 0.3,
            //   fit: BoxFit.cover,
            // ),
            SingleChildScrollView(
              child: Column(
                children: [
                  (_.kycStatus.isNotEmpty && _.kycStatus != '1')
                      ? Card(
                          margin: const EdgeInsets.only(
                              left: 21.3, right: 21.3, top: 10),
                          elevation: 2,
                          color: Colors.red.shade200,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(children: [
                              _.kycStatus == '0'
                                  ? const Center(
                                      child: Text(
                                      'Your KYC is Pending',
                                      style: TextStyle(
                                          fontWeight: FontWeight.w800),
                                    ))
                                  : const Padding(
                                      padding:
                                          EdgeInsets.only(top: 5, bottom: 5),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Text('Your KYC has been Rejected',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 18)),
                                        ],
                                      ),
                                    ),
                              Text(_.kycRemark)
                            ]),
                          ),
                        )
                      : const SizedBox(),

                  (_.userType != "employee" && _.kycStatus == '1') ||
                          (_.userType == 'employee' && _.kycStatus == '1')
                      ? Padding(
                          padding: const EdgeInsets.only(
                              left: 10, right: 10, top: 20),
                          child: Screenshot(
                            controller: _.screenshotController,
                            child: InkWell(
                                onTap: () {
                                  _.takeScreenshot();
                                },
                                child: Card(
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  color: Colors.white,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.blue.shade50),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    padding: const EdgeInsets.all(16),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            // Left section with name and contact info
                                            Expanded(
                                              flex: 3,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  // Name and position section
                                                  Container(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          _.fullName,
                                                          style: const TextStyle(
                                                            fontSize: 15,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors.blue,
                                                          ),
                                                        ),
                                                        const SizedBox(height: 4),
                                                        Text(
                                                          _.userType !=
                                                                  'employee'
                                                              ? 'Verified Advisor'
                                                              : _.userCode,
                                                          style: const TextStyle(
                                                            fontSize: 14,
                                                            color:
                                                                Colors.black54,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  const SizedBox(height: 15),
                                                  // Contact details
                                                  buildContactInfo(
                                                      Icons.phone,
                                                      _.mobileNum,
                                                      12),
                                                  if (!_.email
                                                      .contains(
                                                          "squareinsurance.in"))
                                                    buildContactInfo(
                                                        Icons.email,
                                                        _.email,
                                                        12)
                                                  else buildContactInfo(
                                                      Icons.email,
                                                      'info@squarefin.in',
                                                      12)
                                                ],
                                              ),
                                            ),
                                            const SizedBox(width: 10),
                                            // Right section with logo and QR code
                                            Expanded(
                                              flex: 2,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  // Logo placeholder
                                                  Column(
                                                    children: [
                                                      const Image(
                                                          image: AssetImage(
                                                              'res/images/square_fin.png')),
                                                      sb(5, 4),
                                                      const Text(
                                                        'www.squarefin.in',
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 12),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        const Divider(),
                                        const Text(
                                          '602-603, 6th Floor Trimurty\'s V-Jai City Point,Ahinsa Circle C-Scheme, Jaipur',
                                          style: TextStyle(
                                              color: Colors.black, fontSize: 9),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                                // Container(
                                //     width: MediaQuery.of(context).size.width,
                                //     decoration: const BoxDecoration(
                                //       color: Colors.white,
                                //       borderRadius: BorderRadius.all(
                                //         Radius.circular(5),
                                //       ),
                                //     ),
                                //     child: Row(
                                //       mainAxisAlignment:
                                //           MainAxisAlignment.spaceEvenly,
                                //       children: [
                                //         Expanded(
                                //           flex: 1,
                                //           child: Container(
                                //             decoration: BoxDecoration(
                                //                 color: Colors.black,
                                //                 borderRadius:
                                //                     const BorderRadius.only(
                                //                         bottomLeft:
                                //                             Radius.circular(5),
                                //                         topLeft:
                                //                             Radius.circular(5))),
                                //             child: Column(
                                //               mainAxisAlignment:
                                //                   MainAxisAlignment.spaceBetween,
                                //               crossAxisAlignment:
                                //                   CrossAxisAlignment.start,
                                //               children: [
                                //                 Padding(
                                //                   padding: const EdgeInsets.only(
                                //                       left: 50),
                                //                   child: Container(
                                //                       height: 10,
                                //                       width: 30,
                                //                       color: Colors.white),
                                //                 ),
                                //                 sb(10, 0),
                                //                 Row(
                                //                   children: [
                                //                     sb(0, 10),
                                //                     const Icon(
                                //                       Icons.call,
                                //                       color: Colors.white,
                                //                       size: 15,
                                //                     ),
                                //                     sb(0, 10),
                                //                     const Text(
                                //                       '+91-8306300415',
                                //                       style: TextStyle(
                                //                           color: Colors.white,
                                //                           fontSize: 11),
                                //                     )
                                //                   ],
                                //                 ),
                                //                 sb(7, 0),
                                //                 Row(
                                //                   children: [
                                //                     sb(0, 10),
                                //                     const Icon(
                                //                         Icons.email_outlined,
                                //                         color: Colors.white,
                                //                         size: 15),
                                //                     sb(0, 10),
                                //                     const Text(
                                //                       'info@squarefin.in',
                                //                       style: TextStyle(
                                //                           color: Colors.white,
                                //                           fontSize: 11),
                                //                     ),
                                //                     sb(10, 0),
                                //                   ],
                                //                 ),
                                //                 sb(7, 0),
                                //                 Row(
                                //                   children: [
                                //                     sb(0, 12),
                                //                     const FaIcon(
                                //                       FontAwesomeIcons.globe,
                                //                       color: Colors.white,
                                //                       size: 13,
                                //                     ),
                                //                     sb(0, 10),
                                //                     const Text(
                                //                       'www.squarefin.in',
                                //                       style: TextStyle(
                                //                           color: Colors.white,
                                //                           fontSize: 11),
                                //                     ),
                                //                     // sb(10, 23),
                                //                     Expanded(
                                //                       child: Row(
                                //                         mainAxisAlignment:
                                //                             MainAxisAlignment
                                //                                 .spaceBetween,
                                //                         children: [
                                //                           const SizedBox(
                                //                             width: 42.01,
                                //                           ),
                                //                           Transform.rotate(
                                //                             angle: 200,
                                //                             child: CustomPaint(
                                //                                 size: const Size(
                                //                                     10, 20.9),
                                //                                 painter:
                                //                                     DrawTriangle()),
                                //                           ),
                                //
                                //                           /// upper code is for triangle in card widget
                                //                           // Container(height:10,width:20,
                                //                           //   decoration: BoxDecoration(
                                //                           //       border: Border.all(color: Colors.white),
                                //                           //       borderRadius: BorderRadius.only(topLeft: Radius.elliptical(20,20),
                                //                           //           bottomLeft: Radius.elliptical(20, 20),bottomRight: Radius.zero,
                                //                           //           topRight: Radius.zero),color: Colors.white),
                                //                           // )
                                //                         ],
                                //                       ),
                                //                     ),
                                //                   ],
                                //                 ),
                                //                 sb(10, 0),
                                //                 Row(
                                //                   children: [
                                //                     sb(0, 11),
                                //                     const FaIcon(
                                //                       FontAwesomeIcons
                                //                           .mapLocation,
                                //                       color: Colors.white,
                                //                       size: 12,
                                //                     ),
                                //                     sb(0, 10),
                                //                     const Flexible(
                                //                         child: Text(
                                //                       '602-603, 6th Floor Trimurty\'s V-Jai City Point, Ahinsa Circle C-Scheme, Jaipur',
                                //                       style: TextStyle(
                                //                           color: Colors.white,
                                //                           fontSize: 10),
                                //                     ))
                                //                   ],
                                //                 ),
                                //                 sb(10, 0),
                                //                 Padding(
                                //                   padding: const EdgeInsets.only(
                                //                       left: 50),
                                //                   child: Container(
                                //                       height: 10,
                                //                       width: 30,
                                //                       color: Colors.white),
                                //                 ),
                                //               ],
                                //             ),
                                //           ),
                                //         ),
                                //         Expanded(
                                //           flex: 1,
                                //           child: Container(
                                //             decoration: const BoxDecoration(
                                //                 borderRadius: BorderRadius.all(
                                //               Radius.circular(10),
                                //             )),
                                //             child: Column(
                                //                 mainAxisAlignment:
                                //                     MainAxisAlignment
                                //                         .spaceBetween,
                                //                 crossAxisAlignment:
                                //                     CrossAxisAlignment.center,
                                //                 children: [
                                //                   Center(
                                //                       child: Text(
                                //                     _.fullName,
                                //                     style: const TextStyle(
                                //                         fontSize: 12,
                                //                         fontWeight:
                                //                             FontWeight.bold),
                                //                   )),
                                //                   sb(5, 0),
                                //                   Text(
                                //                     _.userType != 'employee'
                                //                         ? 'Verified Advisor'
                                //                         : _.userCode,
                                //                     style: TextStyle(
                                //                         fontSize: 10,
                                //                         color:
                                //                             Colors.grey.shade800),
                                //                     textAlign: TextAlign.center,
                                //                   ),
                                //                   sb(10, 0),
                                //                   Container(
                                //                       width: 150,
                                //                       decoration: const BoxDecoration(
                                //                           color: Colors
                                //                               .transparent,
                                //                           borderRadius:
                                //                               BorderRadius.only(
                                //                                   topRight: Radius
                                //                                       .circular(
                                //                                           5),
                                //                                   bottomRight: Radius
                                //                                       .circular(
                                //                                           5))),
                                //                       child: const Image(
                                //                           image: AssetImage(
                                //                               'res/images/ic_square_fin_full.png'))),
                                //                   sb(10, 0),
                                //                   Center(
                                //                       child: Text(
                                //                     dashboardController
                                //                         .cardNumber,
                                //                     style: const TextStyle(
                                //                         fontSize: 10),
                                //                   )),
                                //                   sb(5, 0),
                                //                   Center(
                                //                       child: Text(
                                //                     dashboardController
                                //                         .cardEmailId,
                                //                     style: const TextStyle(
                                //                         fontSize: 10),
                                //                   )),
                                //                 ]),
                                //           ),
                                //         )
                                //       ],
                                //     )),
                                ),
                          ),
                        )
                      : const SizedBox(),

                  ///todo
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 17),
                      child: Column(
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(8),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "My Account",
                                style: TextStyle(
                                    color: Color(0xff333333),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              const SizedBox(
                                height: 10,
                              ),

                              InkWell(
                                onTap: () {
                                  print(AppUtils.userType);
                                  print(_.userType);
                                  print('userType');
                                  Get.to(() => const CustomerListUIController());
                                  // Get.to(() => PosFormPersonalUI());
                                },
                                child: Padding(
                                  padding:
                                  const EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        "res/images/ic_customer.svg",
                                        width: 16,
                                        height: 16,
                                      ),
                                      SizedBox(
                                          width: MediaQuery.of(context)
                                              .size
                                              .width *
                                              0.1),
                                      const Text(
                                        "My Customer",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      const Spacer(),
                                      const Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const Divider(
                                thickness: 1,
                                color: Color(0xFFD4E4F1),
                              ),
                              if (_.userType == "employee")
                                InkWell(
                                  onTap: () {
                                    print(AppUtils.userType);
                                    print(_.userType);
                                    print('userType');
                                    Get.to(() => const PartnerListUIController());
                                    // Get.to(() => PosFormPersonalUI());
                                  },
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 7),
                                    child: Row(
                                      children: [
                                        SvgPicture.asset(
                                          "res/images/mypartner.svg",
                                          width: 16,
                                          height: 16,
                                        ),
                                        SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1),
                                        const Text(
                                          "My Partner",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        const Spacer(),
                                        const Icon(
                                          Icons.arrow_forward_ios_outlined,
                                          size: 15,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              if (_.userType == "employee")
                              const Divider(
                                thickness: 1,
                                color: Color(0xFFD4E4F1),
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    _.invest = !_.invest;
                                  });
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        "res/images/investnow.svg",
                                        width: 16,
                                        height: 16,
                                      ),
                                      SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.1),
                                      const Text(
                                        "Invest Now",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      const Spacer(),
                                      _.invest
                                          ? const Icon(
                                              Icons.keyboard_arrow_up,
                                              size: 20,
                                              color: Colors.blue,
                                            )
                                          : const Icon(
                                              Icons.keyboard_arrow_down,
                                              size: 20,
                                            ),
                                    ],
                                  ),
                                ),
                              ),
                              AnimatedContainer(
                                  duration: const Duration(seconds: 0),
                                  height: _.invest
                                      ? MediaQuery.of(context).size.height *
                                          0.21
                                      : 0,
                                  curve: Curves.easeInOut,
                                  child: Column(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          String url =
                                              'https://squarewealthguru.com/';
                                          String title = "Mutual Fund";

                                          Get.to(WebviewController(
                                              url: url, title: title));
                                        },
                                        child: SizedBox(
                                            height: 35,
                                            child: Row(children: [
                                              SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.15),
                                              const Padding(
                                                padding: EdgeInsets.only(
                                                    top: 5),
                                                child: Text(
                                                  "•",
                                                  style: TextStyle(
                                                      color: Colors.blue,
                                                      fontSize: 25),
                                                ),
                                              ),
                                              const SizedBox(width: 6),
                                              const Text(
                                                "Mutual Fund",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                              const Spacer(),
                                              const Icon(
                                                  Icons
                                                      .arrow_forward_ios_outlined,
                                                  size: 15),
                                            ])),
                                      ),
                                      InkWell(
                                          onTap: () {
                                            Get.to(LeadCollectUIController(
                                                type: "FD",
                                                lead: "Fixed Deposit"));
                                          },
                                          child: Row(children: [
                                            SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.15),
                                            const Padding(
                                              padding:
                                                  EdgeInsets.only(top: 5),
                                              child: Text(
                                                "•",
                                                style: TextStyle(
                                                    color: Colors.blue,
                                                    fontSize: 25),
                                              ),
                                            ),
                                            const SizedBox(width: 6),
                                            const Text(
                                              "Fixed Deposit (FD)",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            const Spacer(),
                                            const Icon(
                                                Icons
                                                    .arrow_forward_ios_outlined,
                                                size: 15),
                                          ])),
                                      InkWell(
                                          onTap: () {
                                            Get.to(LeadCollectUIController(
                                                type: "RD",
                                                lead: "Recurring Deposit"));
                                          },
                                          child:
                                              // const ListTile(
                                              //   leading: Icon(Icons.ramen_dining_outlined),
                                              //   title: Text(
                                              //     "Recurring Deposit (RD)",
                                              //     style: TextStyle(
                                              //         color: Color(0xff666666),
                                              //         fontSize: 12,
                                              //         fontWeight: FontWeight.w700),
                                              //   ),
                                              //   trailing:
                                              //   Icon(Icons.arrow_forward_ios_outlined,size: 15,),
                                              // ),
                                              Row(children: [
                                            SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.15),
                                            const Padding(
                                              padding:
                                                  EdgeInsets.only(top: 5),
                                              child: Text(
                                                "•",
                                                style: TextStyle(
                                                    color: Colors.blue,
                                                    fontSize: 25),
                                              ),
                                            ),
                                            const SizedBox(width: 6),
                                            const Text(
                                              "Recurring Deposit (RD)",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            const Spacer(),
                                            const Icon(
                                                Icons
                                                    .arrow_forward_ios_outlined,
                                                size: 15),
                                          ])),
                                      InkWell(
                                          onTap: () {
                                            String url =
                                                'https://apkamakaan.com/';
                                            String title = 'Apka Makaan';
                                            Get.to(WebviewController(
                                                url: url, title: title));
                                          },
                                          child:
                                              // const ListTile(
                                              //   leading: Icon(Icons.real_estate_agent_outlined),
                                              //   title: Text(
                                              //     "Apka Makaan (Real Estate)",
                                              //     style: TextStyle(
                                              //         color: Color(0xff666666),
                                              //         fontSize: 12,
                                              //         fontWeight: FontWeight.w700),
                                              //   ),
                                              //   trailing:
                                              //   Icon(Icons.arrow_forward_ios_outlined,size: 15,),
                                              // ),
                                              Row(children: [
                                            SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.15),
                                            const Padding(
                                              padding:
                                                  EdgeInsets.only(top: 5),
                                              child: Text(
                                                "•",
                                                style: TextStyle(
                                                    color: Colors.blue,
                                                    fontSize: 25),
                                              ),
                                            ),
                                            const SizedBox(width: 6),
                                            const Text(
                                              "Apka Makaan (Real Estate)",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            const Spacer(),
                                            const Icon(
                                                Icons
                                                    .arrow_forward_ios_outlined,
                                                size: 15),
                                          ])),
                                    ],
                                  )),
                              const Divider(
                                thickness: 1,
                                color: Color(0xFFD4E4F1),
                              ),
                              InkWell(
                                onTap: () {
                                  Get.to(LoanTermActivity());
                                },
                                child:
                                    // ListTile(
                                    //   leading:
                                    //   Image.asset(
                                    //     "res/images/loan.png",
                                    //     height: 20,
                                    //     width: 20,
                                    //   ),
                                    //   title:
                                    //   const Text(
                                    //     "Loan Terms",
                                    //     style: TextStyle(
                                    //         fontSize: 15,
                                    //         fontWeight: FontWeight.w500),
                                    //   ),
                                    //   trailing:
                                    //   const Icon(Icons.arrow_forward_ios_outlined,size: 15,),
                                    // ),
                                    Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        "res/images/loan.png",
                                        height: 16,
                                        width: 16,
                                      ),
                                      SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.1),
                                      const Text(
                                        "Loan Terms",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      const Spacer(),
                                      const Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const Divider(
                                thickness: 1,
                                color: Color(0xFFD4E4F1),
                              ),
                              InkWell(
                                onTap: () {
                                  Get.to(const ProductLeadViewController());
                                },
                                child: Center(
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 7),
                                    child: Row(
                                      children: [
                                        SvgPicture.asset(
                                          "res/images/tracklead.svg",
                                          width: 16,
                                          height: 16,
                                        ),
                                        SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.1),
                                        const Text(
                                          "Track My Leads",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        const Spacer(),
                                        const Icon(
                                          Icons.arrow_forward_ios_outlined,
                                          size: 15,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              const Divider(
                                thickness: 1,
                                color: Color(0xFFD4E4F1),
                              ),
                              InkWell(
                                onTap: () {
                                  Get.to(const DashLoan());
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        "res/images/RaiseRequest.png",
                                        height: 16,
                                        width: 16,
                                      ),
                                      SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.1),
                                      const Text(
                                        "Raise Request",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      const Spacer(),
                                      const Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              _.userType != "employee"
                                  ? const SizedBox()
                                  : const Divider(
                                      thickness: 1,
                                      color: Color(0xFFD4E4F1),
                                    ),
                              _.userType != "employee"
                                  ? const SizedBox()
                                  : InkWell(
                                      onTap: () {
                                        Get.to(const ReportUIController());
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 7),
                                        child: Row(
                                          children: [
                                            Image.asset(
                                              "res/images/managerreports.png",
                                              height: 16,
                                              width: 16,
                                            ),
                                            SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.1),
                                            const Text(
                                              "Reports",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            const Spacer(),
                                            const Icon(
                                              Icons.arrow_forward_ios_outlined,
                                              size: 15,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                              const SizedBox(
                                height: 15,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 17),
                      child: Column(
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(8),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "More Settings",
                                style: TextStyle(
                                    color: Color(0xff333333),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              const SizedBox(
                                height: 10,
                              ),
                              InkWell(
                                onTap: () {
                                  Get.to(const AboutUs());
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        "res/images/aboutus.svg",
                                        width: 16,
                                        height: 16,
                                      ),
                                      SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.1),
                                      const Text(
                                        "About Us",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      const Spacer(),
                                      const Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const Divider(
                                thickness: 1,
                                color: Color(0xFFD4E4F1),
                              ),
                              InkWell(
                                onTap: () {
                                  Get.to(const ContactUs());
                                  // Get.to(ContactUIController());
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        "res/images/contactus.svg",
                                        height: 12,
                                        width: 12,
                                      ),
                                      SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.1),
                                      const Text(
                                        "Contact Us",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      const Spacer(),
                                      const Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const Divider(
                                thickness: 1,
                                color: Color(0xFFD4E4F1),
                              ),
                              InkWell(
                                onTap: () {
                                  String url =
                                      'https://www.squarefin.in/privacy.html';
                                  String title = "Privacy and Policies";

                                  Get.to(WebviewController(
                                      url: url, title: title));
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        "res/images/privacy_policy.svg",
                                        height: 12,
                                        width: 12,
                                      ),
                                      SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.1),
                                      const Text(
                                        "Privacy and Policies",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      const Spacer(),
                                      const Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const Divider(
                                thickness: 1,
                                color: Color(0xFFD4E4F1),
                              ),

                              InkWell(
                                onTap: () {
                                  _.showMyDialog();
                                },
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        "res/images/logout.svg",
                                        height: 12,
                                        width: 12,
                                      ),
                                      SizedBox(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.1),
                                      const Text(
                                        "Logout",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      const Spacer(),
                                      const Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              //
                              // ElevatedButton(onPressed: (){
                              //   _.showMyDialog();
                              // },
                              //   style: ElevatedButton.styleFrom(
                              //       backgroundColor: Colors.transparent,
                              //       shadowColor: Colors.transparent,
                              //       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                              //       side: BorderSide(color: Colors.grey.shade500)
                              //   ), child: const Text("Logout",style:TextStyle(
                              //       color: Colors.red, fontWeight: FontWeight.bold),),
                              // )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),

                  // altering from here.
                  /// this is new code
                  // Padding(
                  //   padding: const EdgeInsets.symmetric(horizontal: 16),
                  //   child: GridView.builder(
                  //     physics: const NeverScrollableScrollPhysics(),
                  //         itemCount: _.entries.length,
                  //         // itemCount: _.userType == "employee"?5:4,
                  //         gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  //             crossAxisCount: 2,
                  //           crossAxisSpacing: 20,
                  //           mainAxisSpacing: 20,
                  //           mainAxisExtent: 100,
                  //         ),
                  //         itemBuilder: (context, index) {
                  //           return InkWell(
                  //             onTap:(){
                  //               _.entries[index].key == _.myPartnerTxt ? Get.to(
                  //                   const PartnerListUIController()):  _.entries[index].key== _.investNowTxt ?
                  //
                  //               Get.to(const InvestOptionsUIController())
                  //                   :  _.entries[index].key == _.loanTermsTxt ? Get.to(LoanTermActivity()) :  _.entries[index].key == _.myLeadsTxt ? Get.to(ProductLeadViewController()):
                  //               _.entries[index].key == _.raiseRequestTxt ? Get.to(DashLoan()):
                  //               // _.entries[index].key == _.dsrTxt?Get.to(DsrListUIController()):
                  //               _.entries[index].key == _.reportTxt? Get.to(const ReportUIController()):
                  //               null;
                  //             },
                  //             child: Container(
                  //                               // height:MediaQuery.of(context).size.height*0.15,
                  //                               // width: MediaQuery.of(context).size.width*0.45,
                  //                                 decoration: BoxDecoration(
                  //                                   image: DecorationImage(image: AssetImage("res/images/profile/${_.entries[index].value}") ,scale: 0.1,
                  //                                       fit: BoxFit.cover,
                  //                                     // colorFilter: ColorFilter.mode(Colors.green.withOpacity(0.5), BlendMode.color)
                  //                                   ),
                  //                                     // color: Colors.deepPurpleAccent,
                  //                                     borderRadius: BorderRadius.circular(11),
                  //                                   boxShadow: const [BoxShadow(
                  //                                     blurRadius: 8, color: Colors.grey, spreadRadius: -4, offset: Offset(2, 4)
                  //                                   )]
                  //                                 ),
                  //                                 child: Align(
                  //                                 alignment: Alignment.bottomCenter,
                  //                                 child: Padding(
                  //                                 padding: const EdgeInsets.only(bottom:2),
                  //                                 child: Container(
                  //                                 decoration: BoxDecoration(borderRadius: BorderRadius.circular(11), color:Colors.black54),
                  //                                 child: Padding(
                  //                                 padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                  //                                 child: Text(_.entries[index].key.toString(),style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.white,fontSize: 15),textAlign: TextAlign.center,),
                  //                                 )),
                  //                                 ),
                  //                                 )
                  //                                 )
                  //                                   );
                  //   },shrinkWrap: true),
                  // ),
                  ///this is new code

                  /// this is new code

                  // const SizedBox(
                  //   height: 15,
                  // ),

                  // _.userType=='employee'?Padding(
                  //   padding:
                  //       const EdgeInsets.only(left: 18.0, right: 18, bottom: 10),
                  //   child: Card(
                  //     elevation: 2,
                  //     shape: RoundedRectangleBorder(
                  //       borderRadius: BorderRadius.circular(10),
                  //     ),
                  //     child: InkWell(
                  //       onTap: () {
                  //         Get.to(() => const PosViewController());
                  //       },
                  //       child: SizedBox(
                  //         width: MediaQuery.of(context).size.width,
                  //         height: MediaQuery.of(context).size.height * 0.2 - 80,
                  //         child: Center(
                  //           child: ListTile(
                  //             leading: SvgPicture.asset(
                  //               "res/images/mypartner.svg",
                  //               height: 35,
                  //               width: 35,
                  //             ),
                  //             title: const Text(
                  //               "My Partners",
                  //               style: TextStyle(
                  //                   color: Color(0xff666666),
                  //                   fontSize: 12,
                  //                   fontWeight: FontWeight.w700),
                  //             ),
                  //             trailing:
                  //                 const Icon(Icons.arrow_forward_ios_outlined),
                  //           ),
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ):SizedBox(),
                  // Padding(
                  //   padding:
                  //       const EdgeInsets.only(left: 18.0, right: 18, bottom: 10),
                  //   child: InkWell(
                  //     onTap: () {
                  //       Get.to(() => ChangePassword());
                  //     },
                  //     child: Card(
                  //       elevation: 2,
                  //       shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.circular(10),
                  //       ),
                  //       child: Container(
                  //         width: MediaQuery.of(context).size.width,
                  //         height: MediaQuery.of(context).size.height * 0.2 - 80,
                  //         child: Center(
                  //           child: ListTile(
                  //             leading: SvgPicture.asset(
                  //               "res/images/changepass.svg",
                  //               height: 35,
                  //               width: 35,
                  //             ),
                  //             title: const Text(
                  //               "Change Password",
                  //               style: TextStyle(
                  //                   color: Color(0xff666666),
                  //                   fontSize: 12,
                  //                   fontWeight: FontWeight.w700),
                  //             ),
                  //             trailing:
                  //                 const Icon(Icons.arrow_forward_ios_outlined),
                  //           ),
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),

                  //   OutlinedButton(
                  //   onPressed: () {
                  //     // CustomPopupBox(context);
                  //     _.showMyDialog();
                  //   },
                  //   style: ButtonStyle(
                  //     shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  //       RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.circular(18.0),
                  //         side: const BorderSide(color: Colors.red),
                  //       ),
                  //     ),
                  //   ),
                  //   child: const Text(
                  //     'Logout',
                  //     style: TextStyle(
                  //         color: Colors.red, fontWeight: FontWeight.bold),
                  //   ),
                  // ),
                  /// this is new code
                  const SizedBox(
                    height: 35,
                  ),
                  // SizedBox(height: 5,),

                  sb(10, 0),
                  Text(
                    'App Version ${_.version}',
                    style: const TextStyle(
                        color: Colors.grey, fontWeight: FontWeight.bold),
                  ),
                  sb(10, 0),
                  const Text(
                    "M/s Squarefin Private Limited directly provide leads as a direct selling agent (DSA) to our NBFC and Banking Partners for lending directly to the consumers in the low and middle income class groups deals in semi-urban and rural locations. We facilitate online and offline platform to our customer to get funds at best possible interest rates and also reduce the credit risk for the partners by using our unique appraisal risk process. We hereby state that our company M/s Squarefin Private Limited has entered into an agreement with RBI Registered NBFCs and Financial Institutions who will be providing loans to the leads given by us with the help of this App.",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 8),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: InkWell(
                      onTap: () async {
                        final Uri url0 =
                            Uri.parse("https://www.squarefin.in/privacy.html");
                        if (!await launchUrl(url0)) {
                          throw Exception('Could not launch $url0');
                        }
                      },
                      child: const Text(
                        "Our Privacy and Policy",
                        style: TextStyle(fontSize: 12, color: Colors.lightBlue),
                      ),
                    ),
                  ),
                  // InkWell(
                  //     onTap:(){
                  //       Get.to(AboutUs());
                  //     },
                  //     child: const Text("About Us", style: TextStyle(fontSize: 12, color: Colors.lightBlue),)),
                  const SizedBox(
                    height: 10,
                  ),
                  // InkWell(
                  //     onTap:(){
                  //       Get.to(ContactUs());
                  //     },
                  //     child: const Text("Contact Us", style: TextStyle(fontSize: 12, color: Colors.lightBlue),)
                  // ),
                  // const SizedBox(height: 10)
                ],
              ),
            )
          ]),
        );
      },
    );
  }

  Color getBackgroundColor(String status) {
    Color backgroundColor = Colors.green;

    switch (status) {
      case "0":
        backgroundColor = Colors.orange.shade300;
        break;
      case "1":
        backgroundColor = Colors.green;
        break;
      case "2":
        backgroundColor = Colors.red;
        break;
      default:
        backgroundColor = Colors.orange;
        break;
    }
    return backgroundColor;
  }
}

getStatusIcon(String kycStatus) {
  IconData iconData;
  switch (kycStatus) {
    case "0":
      iconData = Icons.pending;
      break;
    case "1":
      iconData = Icons.verified;
      break;
    case "2":
      iconData = Icons.cancel;
      break;
    default:
      iconData = Icons.pending;
      break;
  }

  return Icon(
    iconData,
    color: Colors.white,
    size: 16,
  );
}

getStatusText(String kycStatus) {
  String text = '';
  Color textColor = Colors.white;

  switch (kycStatus) {
    case "0":
      text = 'Pending';
      break;
    case "1":
      text = 'Verified';
      break;
    case "2":
      text = 'Rejected';
      break;
    default:
      text = 'NA';
      break;
  }

  return Text(
    text,
    style: TextStyle(
      color: textColor,
      fontWeight: FontWeight.bold,
      fontSize: 12,
    ),
  );
}

class DrawTriangle extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var path = Path();
    path.moveTo(size.width / 100, 0);
    path.lineTo(0, size.height);
    path.lineTo(size.height, size.width);
    path.close();
    canvas.drawPath(path, Paint()..color = Colors.lightBlueAccent);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

Widget buildContactInfo(IconData icon, String info, double size) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 3),
    child: Row(
      children: [
        Icon(icon, color: Colors.black, size: size),
        const SizedBox(width: 5),
        Expanded(
          child: Text(
            info,
            style: TextStyle(color: Colors.black, fontSize: size),
          ),
        ),
      ],
    ),
  );
}

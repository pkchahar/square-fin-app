
import 'package:custom_searchable_dropdown/custom_searchable_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:image_picker/image_picker.dart';
import '../../utils/AppUtils.dart';
import 'AddBankController.dart';
import 'dart:io';

class AddBankViewController extends StatefulWidget {
  const AddBankViewController({super.key});

  @override
  _AddBankViewController createState() => _AddBankViewController();
}

class _AddBankViewController extends State<AddBankViewController> {
  String panPath = "", adharFrontPath = '', adharBackPath = '', bankPath = '';
  final formKey = GlobalKey<FormState>();

  DateTime selectedDate = DateTime(
      DateTime.now().year - 18, DateTime.now().month, DateTime.now().day);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddBankController>(
      init: AddBankController(context: context),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Add Bank"),
          ),
          backgroundColor: AppUtils.backgroundColor,
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Center(
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      const SizedBox(height: 16.0),
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Row(
                          children: [
                            Icon(
                              Icons.currency_rupee_rounded,
                              color: Colors.purple,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "Bank Detail",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      const SizedBox(height: 16.0),
                      CustomSearchableDropDown(
                        items: _.bankList,
                        label: 'Select Bank',
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.grey,
                            ),
                            borderRadius: BorderRadius.circular(5)),
                        dropDownMenuItems: _.bankList.map((item) {
                          return item.bankName ?? "";
                        }).toList(),
                        onChanged: (value) {
                          if (value != null) {
                            setState(() {
                              _.selectedBank = value?.bankName ?? "";
                            });
                          } else {}
                        },
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        controller: _.branchController,
                        decoration: const InputDecoration(
                          labelText: 'Branch Name',
                          border: OutlineInputBorder(),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                            ),
                          ),
                          labelStyle: TextStyle(color: Colors.black54),
                        ),
                        keyboardType: TextInputType.name,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Enter Branch name';
                          }
                          _.branch = value;
                          return null;
                        },
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      DropdownButtonFormField<String>(
                        value: _.selectAccType,
                        items: const [
                          DropdownMenuItem(
                              value: 'Saving', child: Text('Saving')),
                          DropdownMenuItem(
                              value: 'Current', child: Text('Current')),
                          DropdownMenuItem(
                              value: 'Salary', child: Text('Salary')),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _.selectAccType = value!;
                          });
                        },
                        decoration: const InputDecoration(
                          labelText: 'Account Type',
                          border: OutlineInputBorder(),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                            ),
                          ),
                          labelStyle: TextStyle(color: Colors.black54),
                        ),
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        controller: _.accController,
                        decoration: const InputDecoration(
                          labelText: 'Account Number',
                          border: OutlineInputBorder(),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                            ),
                          ),
                          labelStyle: TextStyle(color: Colors.black54),
                        ),
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please Enter Account Number';
                          }
                          _.accountNo = value;
                          return null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        controller: _.ifscController,
                        decoration: const InputDecoration(
                          labelText: 'IFSC Code',
                          border: OutlineInputBorder(),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.lightBlue,
                            ),
                          ),
                          labelStyle: TextStyle(color: Colors.black54),
                        ),
                        keyboardType: TextInputType.name,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please Enter IFSC Code';
                          }
                          _.ifscCode = value;
                          return null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      Container(
                        alignment: Alignment.topLeft,
                        child: const Text(
                          "Cancel Cheque/Bank Passbook Image",
                          textAlign: TextAlign.start,
                          style: TextStyle(fontSize: 12.0, color: Colors.blue),
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.blueGrey,
                            style: BorderStyle.solid,
                            width: 0.5,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            InkWell(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: (_.bankImg.isNotEmpty)
                                    ? const Text(
                                        "Cancel Cheque/Bank Passbook Selected",
                                        style: TextStyle(
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold),
                                      )
                                    : const Padding(
                                        padding: EdgeInsets.only(
                                            top: 20, bottom: 20),
                                        child: Text(
                                          'Select Cancel Cheque/Bank Passbook',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                              ),
                              onTap: () async {
                                _showDialog(_);
                              },
                            ),
                            Expanded(
                              child: Container(),
                            ),
                            bankPath.isNotEmpty
                                ? Row(
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          showImageDialog(
                                              context,
                                              "Cancel Cheque/Bank Passbook",
                                              _.bankImg);
                                        },
                                        icon: const Icon(
                                          Icons.image_outlined,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      InkWell(
                                        child: const Padding(
                                          padding: EdgeInsets.all(8.0),
                                          child: Icon(
                                            Icons.clear,
                                            color: Colors.red,
                                          ),
                                        ),
                                        onTap: () {
                                          _.bankImg = "";
                                          bankPath = "";
                                          setState(() {});
                                        },
                                      ),
                                    ],
                                  )
                                : const SizedBox(),
                          ],
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      const SizedBox(height: 30.0),
                      ElevatedButton(
                        onPressed: () {
                          if (formKey.currentState!.validate()) {
                            if (bankPath.isEmpty) {
                              AppUtils.showSnackBar("Cancel Cheque/Passbook Document",
                                  "is mandatory!!", false);
                            } else {
                              _.AddNewBank();
                            }
                          }
                        },
                        style: ButtonStyle(
                          backgroundColor: WidgetStateProperty.all<Color>(
                              Colors.lightBlue),
                          shape:
                              WidgetStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(7.0),
                            ),
                          ),
                        ),
                        child: _.isLoading
                            ? const CircularProgressIndicator(
                          color: Colors.white,
                        )
                            : const Text(
                          'Add Bank Account',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> _showDialog(AddBankController obj) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Choose an option'),
          children: [
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                _pickImage(obj, ImageSource.camera);
              },
              child: const Row(
                children: [
                  Icon(Icons.camera),
                  SizedBox(width: 10),
                  Text('Camera'),
                ],
              ),
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                _pickImage(obj, ImageSource.gallery);
              },
              child: const Row(
                children: [
                  Icon(Icons.image),
                  SizedBox(width: 10),
                  Text('Gallery'),
                ],
              ),
            ),
          ],
        );
      },
    );
  }


  Future<void> _pickImage(AddBankController obj, ImageSource source) async {
    final imagePicker = ImagePicker();
    final XFile? image = await imagePicker
        .pickImage(source: source);
    if (image == null) {
      // User canceled image capture
      return;
    } else {
      final List<String> pathParts =
      image.path.split('/');
      final String imageName = pathParts.last;
      bankPath = imageName;
      obj.bankImg = image.path;
    }
    setState(() {});
  }

  void showImageDialog(BuildContext context, String title, String imagePath) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.file(File(imagePath), fit: BoxFit.contain),
                const SizedBox(height: 20),
                Text(title), // You can add a text description or title here
              ],
            ),
          ),
        );
      },
    );
  }
}

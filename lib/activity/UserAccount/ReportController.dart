import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:pi/Model/Profile/PartnerReport.dart';
import 'package:pi/Model/Profile/ReportResponse.dart';
import 'package:pi/activity/Product/ProductLeadViewController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import '../../Model/Profile/BusinessReport.dart';
import '../../Model/Profile/CustomerReport.dart';
import '../../Model/Profile/WalletReport.dart';
import '../../utils/AppUtils.dart';
import '../Product/ProductLeadController.dart';
import '../Utility Controllers/TicketDetailUIController.dart';
import 'ProfileUIController.dart';

class ReportController extends GetxController{
//subject.text = '';
//issue.text = '';
  TextEditingController subject = TextEditingController();
  TextEditingController issue = TextEditingController();
  ProductLeadController pc = ProductLeadController();

  var screenIndex = 0;
  ProductLeadViewController pv = const ProductLeadViewController();
  List menuBar = ['Lead Report', 'Wallet Report', 'Business Report', 'Partner Report', 'Customer Report'];
  List colors = [ Colors.indigo.shade50 , Colors.green.shade50, Colors.blue.shade50,Colors.pink.shade50, Colors.teal.shade50];
  List colors1 = [ Colors.indigo.shade500 , Colors.green.shade500, Colors.blue.shade500,Colors.pink.shade500, Colors.teal.shade500];


  List reportType = ['lead_reports','wallet_reports' ,'business_report', 'partner_report' ,'customer_report'];

  @override
  void onInit() {
    super.onInit();
    fetchReport(reportType[0], "1");
  }


  bool isLoading = true;
  List<Lead> leadResList = [];
  List<TransactionData> walletResList = [];
  List<BusinessData> businessResList = [];
  List<PartnerData> partnerResList = [];
  List<CustomerData> customerResList = [];

  var cmd='';
  var pageCount = 2;
  bool isUpdating = false;
  appendList(reportType){
    isUpdating = true;
    Future.delayed(const Duration(milliseconds: 500),(){
      if(cmd == 'return'){
        return;
      }
      print("working");
      fetchReport(reportType, pageCount.toString());
      pageCount ++;
      // appendList(reportType);
    });
    isUpdating = false;
  }
  // appendList(reportType){
  //   Future.delayed(const Duration(milliseconds: 500),(){
  //     if(cmd == 'return'){
  //       return;
  //     }
  //     fetchReport(reportType, pageCount.toString());
  //     pageCount ++;
  //     appendList(reportType);
  //   });
  // }

  void setLoading(bool value) {
    isLoading = value;
    update();
  }

  Future<void> fetchReport(reportType, page) async {
    // isUpdating = true;
    page=="1"?setLoading(true):null;
    try{
      SharedPreferences sf = await SharedPreferences.getInstance();
      Map<String, dynamic> mData = {
        'User_Id': sf.getString(AppUtils.userId).toString(),
        'report_type': reportType,
        'page': page,
      };
      print(mData);
      var url = Uri.parse(AppUtils.baseUrlNew + AppUtils.report);
      var response = await http.post(url, body: mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        if(reportType == 'lead_reports'){
          ReportResponse myResponse = ReportResponse.fromJson(responseJson);
          if(myResponse.data.isNotEmpty){
            page == "1"?leadResList = []:null;
            leadResList.addAll(myResponse.data);
          }else{
            cmd = 'return';
          }
        }else if(reportType == 'wallet_reports'){
          WalletReport myResponse = WalletReport.fromJson(responseJson);
          if(myResponse.data.isNotEmpty){
            page =="1"?walletResList = []:null;
            walletResList.addAll(myResponse.data);
          }else{
            cmd = 'return';
          }
        }else if(reportType == 'business_report'){
          BusinessReport myResponse = BusinessReport.fromJson(responseJson);
          if(myResponse.data!.isNotEmpty){
            page== "1"?businessResList = []: null;
            businessResList.addAll(myResponse.data!);
          }else{
            cmd = 'return';
          }
        }else if(reportType == 'partner_report'){
          PartnerReport myResponse = PartnerReport.fromJson(responseJson);
          if(myResponse.data.isNotEmpty){
            page =="1"?partnerResList =[]:null;
            partnerResList.addAll(myResponse.data);
          }else{
            cmd = 'return';
          }
        }else if(reportType == 'customer_report'){
          CustomerReport myResponse = CustomerReport.fromJson(responseJson);
          if(myResponse.data.isNotEmpty){
            page== "1"?customerResList = []:null;
            customerResList.addAll(myResponse.data);
          }else{
            cmd = 'return';
          }
        }
        update();
      }
      page=="1"?setLoading(false):null;
      update();
      // isUpdating = false;
    }catch(e){print(e);}
  }
  emptyImage(context){
    print("showing empty image");
    return Center(child: SvgPicture.asset('res/images/ic_empty.svg',height: MediaQuery.of(context).size.height*0.7,));
  }
  Widget buildCard(BuildContext context,  String data1, String data2, String data3, String data4, String data5, String data6) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            // boxShadow: [
            //   BoxShadow(
            //     color: Colors.grey.withOpacity(0.5),
            //     blurRadius: 0,
            //     spreadRadius: 0,
            //     offset: Offset(0, 0),
            //   ),
            // ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 10),
                child: Row(
                  children: [
                    ConstrainedBox(
                      constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width * 0.9,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 2.5, horizontal: 5),
                        child: IntrinsicWidth(
                          child: Container(
                            constraints: BoxConstraints(
                              maxWidth: MediaQuery.of(context).size.width * 0.8,
                            ),
                            child: Text(
                              data1,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                              overflow: TextOverflow.clip,
                              softWrap: true,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                height: 0.3,
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 4, top: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 5, top: 2),
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                              maxWidth: MediaQuery.of(context).size.width * 0.8,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.brown.shade50,
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(color: Colors.brown.shade100),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 2.5, horizontal: 5),
                                child: IntrinsicWidth(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Flexible(
                                        child: Padding(
                                          padding: const EdgeInsets.only(left: 2),
                                          child: Text(
                                            data2,
                                            textAlign: TextAlign.start,
                                            style: const TextStyle(fontSize: 12),
                                            overflow: TextOverflow.ellipsis,
                                            softWrap: true,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            data3.toString().isNotEmpty? Padding(
                              padding: const EdgeInsets.only(top:8,bottom: 8),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue.shade50,
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.blue.shade100),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2.5, horizontal: 5),
                                  child: Text(data3, style: const TextStyle(fontSize: 12)),
                                ),
                              ),
                            ):const SizedBox(),
                            SizedBox(width:data3.isEmpty?0: 10),
                            data4.toString().isNotEmpty?Padding(
                              padding: const EdgeInsets.only(top:8,bottom: 8),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.teal.shade50,
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.teal.shade100),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 4),
                                  child: Text(data4, style: const TextStyle(fontSize: 12)),
                                ),
                              ),
                            ):const SizedBox(),
                          ],
                        ),
                        SizedBox(height: data4.isEmpty? 5:0),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8),
                          child: Row(
                            children: [
                              data5.toString().isNotEmpty?Container(
                                decoration: BoxDecoration(
                                  color: Colors.pink.shade50,
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.pink.shade100),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 4),
                                  child: Text(data5, style: const TextStyle(fontSize: 12)),
                                ),
                              ):const SizedBox(),
                              SizedBox(width: data5.isEmpty?0:10),
                              data6.toString().isNotEmpty?Container(
                                decoration: BoxDecoration(
                                  color: Colors.green.shade50,
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.green.shade100),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 4),
                                  child: Text(data6, style: const TextStyle(fontSize: 12)),
                                ),
                              ):const SizedBox(),
                            ],
                          ),
                        )

                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            height: 30,
            width: 30,
            decoration: BoxDecoration(
              color: Colors.deepPurple.shade50,
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.elliptical(2000, 2000),
                topLeft: Radius.circular(0),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget leadReport(BuildContext context, String iconData, String productNameData, String dataData, String customerNameData, String mobileData, String emailData, String statusData, String remarkData, String isTicketData, String ticketIdData,String leadIdDataForTicket, String leadId) {
    return Container(
      child: Stack(
        children: [
          Card(
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                  10.0), // Adjust the radius to change border roundness
              // side: BorderSide(color: Colors.green, width: 0.3), // Define the border color and width
            ),
            child: Stack(children: [
              Padding(
                padding:
                const EdgeInsets.only(
                    left: 8,
                    top: 10,
                    right: 8),
                child: Container(
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment
                        .start,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: ClipOval(
                              child: SizedBox(
                                height: 48,
                                width: 48,
                                child:
                                Padding(
                                  padding:
                                  const EdgeInsets
                                      .all(
                                      1.0),
                                  child: Image
                                      .network(
                                    iconData,
                                    fit: BoxFit
                                        .fill,
                                    errorBuilder: (context,
                                        error,
                                        stackTrace) {
                                      return SvgPicture.asset(
                                          'res/images/no_image_icon.svg');
                                    },
                                    height:
                                    35,
                                    width: 35,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                              width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment:
                              CrossAxisAlignment
                                  .start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets
                                      .only(
                                      right:
                                      45),
                                  child: Text(
                                    productNameData,
                                    textAlign:
                                    TextAlign
                                        .start,
                                    style:
                                    const TextStyle(
                                      fontSize:
                                      15,
                                      fontWeight:
                                      FontWeight.bold,
                                    ),
                                    maxLines:
                                    2,
                                    overflow:
                                    TextOverflow
                                        .ellipsis,
                                  ),
                                ),
                                sb(5, 0),
                                Text(
                                  dataData,
                                  textAlign:
                                  TextAlign
                                      .start,
                                  style: const TextStyle(
                                      fontSize:
                                      12),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                          height: 10),
                      Row(
                        children: [
                          Container(
                            alignment:
                            Alignment
                                .center,
                            decoration:
                            BoxDecoration(
                              color: Colors
                                  .green
                                  .shade50,
                              border:
                              Border.all(
                                color: Colors
                                    .green
                                    .shade100,
                                width: 1,
                              ),
                              borderRadius:
                              BorderRadius
                                  .circular(
                                  10),
                            ),
                            child: Padding(
                              padding:
                              const EdgeInsets
                                  .all(
                                  7.0),
                              child: Text(
                                customerNameData,
                                style:
                                const TextStyle(
                                  fontSize:
                                  12,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                              width: 8),
                          Container(
                            constraints:
                            BoxConstraints(
                              minWidth: MediaQuery.of(
                                  context)
                                  .size
                                  .width *
                                  0.15,
                            ),
                            alignment:
                            Alignment
                                .center,
                            decoration:
                            BoxDecoration(
                              color: Colors
                                  .purple
                                  .shade50,
                              border:
                              Border.all(
                                color: Colors
                                    .purple
                                    .shade100,
                                width: 1,
                              ),
                              borderRadius:
                              BorderRadius
                                  .circular(
                                  10),
                            ),
                            child:
                            GestureDetector(
                              onTap: () {
                                launch(
                                    "tel://$mobileData");
                              },
                              child: Padding(
                                padding:
                                const EdgeInsets
                                    .all(
                                    7.0),
                                child: Text(
                                  mobileData,
                                  style:
                                  const TextStyle(
                                    fontSize:
                                    12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                          height: 8),
                      Row(
                        children: [
                          Container(
                            alignment:
                            Alignment
                                .center,
                            decoration:
                            BoxDecoration(
                              color: Colors
                                  .blue
                                  .shade50,
                              border:
                              Border.all(
                                color: Colors
                                    .blue
                                    .shade100,
                                width: 1,
                              ),
                              borderRadius:
                              BorderRadius
                                  .circular(
                                  10),
                            ),
                            child: InkWell(
                              onTap:
                                  () async {
                                if (emailData
                                    .isNotEmpty) {
                                  final Uri
                                  emailLaunchUri =
                                  Uri.dataFromString(
                                    'mailto:${Uri.encodeComponent(emailData)}',
                                    mimeType:
                                    'text/plain',
                                  );

                                  if (await canLaunch(
                                      emailLaunchUri
                                          .toString())) {
                                    await launch(
                                        emailLaunchUri
                                            .toString());
                                  } else {
                                    print(
                                        'Could not launch email app');
                                  }
                                }
                              },
                              child: Padding(
                                padding:
                                const EdgeInsets
                                    .all(
                                    5.0),
                                child: Text(
                                  emailData,
                                  style:
                                  const TextStyle(
                                    fontSize:
                                    12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const Divider(),
                      const SizedBox(
                          height: 3),
                      Container(
                        constraints:
                        BoxConstraints(
                          maxWidth:
                          MediaQuery.of(
                              context)
                              .size
                              .width,
                        ),
                        alignment: Alignment
                            .centerLeft,
                        decoration:
                        BoxDecoration(
                          color: Colors
                              .grey.shade50,
                          border: Border.all(
                              color: Colors
                                  .grey
                                  .shade200,
                              width: 1),
                          borderRadius:
                          BorderRadius
                              .circular(
                              10),
                        ),
                        child: Stack(
                            children: [
                              Padding(
                                padding:
                                const EdgeInsets
                                    .all(
                                    8.0),
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment
                                      .start,
                                  children: [
                                    Row(
                                      children: [
                                        getStatusTextAndIcon(
                                            statusData),
                                        sb(0,
                                            2),
                                        getStatusIcon(
                                            statusData),
                                      ],
                                    ),
                                    sb(10, 0),
                                    remarkData
                                        .isNotEmpty
                                        ? Text(
                                      "Remark: $remarkData",
                                      style: const TextStyle(
                                        fontSize: 12,
                                      ),
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis,
                                    )
                                        : sb(
                                        0,
                                        0)
                                  ],
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                    alignment:
                                    Alignment
                                        .center,
                                    decoration:
                                    BoxDecoration(
                                      color:  isTicketData=="0"?Colors
                                          .orange
                                          .shade50:Colors.blue.shade50,
                                      border:
                                      Border.all(
                                        color:  isTicketData == "0"?Colors
                                            .orange
                                            .shade100:Colors.blue.shade100,
                                        width: 1,
                                      ),
                                      borderRadius:
                                      BorderRadius
                                          .circular(
                                          10),
                                    ),
                                    child:
                                    GestureDetector(
                                      onTap: () {
                                        int i = 0;

                                        print(ticketIdData);
                                        print('data10');
                                        isTicketData == '1'?
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => TicketDetailUIController( ticketId: ticketIdData.toString(),))):
                                        showDialog
                                          (
                                          context
                                              :
                                          context,
                                          builder: (BuildContext context) {

                                            return AlertDialog(
                                              title: Row(
                                                children: [
                                                  const Text('Dear '),
                                                  Text(customerNameData.toLowerCase().capitalizeFirst.toString(), style: const TextStyle(
                                                      fontWeight: FontWeight.bold)),
                                                  const Text(','),
                                                ],
                                              ),

                                              content: StatefulBuilder(
                                                builder: (BuildContext context,
                                                    StateSetter setState) {
                                                  return SingleChildScrollView(
                                                    child: Column(
                                                      children: [
                                                        const Text(
                                                          'Please describe the issue you are experiencing.',
                                                          style: TextStyle(
                                                              fontSize: 14),),
                                                        const SizedBox(height: 35),
                                                        TextFormField(
                                                          autofocus: true,
                                                          controller: subject,
                                                          maxLines: null,
                                                          decoration: const InputDecoration(
                                                            label: Text('Subject',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .grey)),
                                                            border: OutlineInputBorder(),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(
                                                                    color: Colors
                                                                        .blue)),
                                                          ),
                                                        ),
                                                        const SizedBox(height: 10),
                                                        TextFormField(
                                                          controller: issue,
                                                          maxLines: 4,
                                                          decoration: const InputDecoration(
                                                            label: Text('Issue',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .grey)),
                                                            border: OutlineInputBorder(),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(
                                                                    color: Colors
                                                                        .blue)),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  );
                                                },
                                              ),
                                              actions: [
                                                ElevatedButton(
                                                  onPressed: () {
                                                    subject.text = '';
                                                    issue.text = '';
                                                    update();
                                                    Get.back();
                                                  },
                                                  style: ElevatedButton.styleFrom(
                                                      backgroundColor: Colors.red),
                                                  child: const Text('Cancel',
                                                    style: TextStyle(
                                                        color: Colors.white),),
                                                ),
                                                ElevatedButton(
                                                  onPressed: () {
                                                    if (subject.text.isEmpty || issue.text.isEmpty) {
                                                      AppUtils.showSnackBar(
                                                          'Try Again!',
                                                          'Required Details are Missing',
                                                          false);
                                                    } else {
                                                      print('else part');
                                                      pc.submitQuery(leadIdDataForTicket,subject,issue);
                                                      Navigator.of(context).pop();
                                                      update();
                                                    }
                                                  },
                                                  style: ElevatedButton.styleFrom(
                                                      backgroundColor: Colors.green),
                                                  child: const Text('Submit',
                                                    style: TextStyle(
                                                        color: Colors.white),),
                                                ),

                                              ],
                                            );
                                          },
                                        );
                                      },
                                      child: Padding(
                                        padding:
                                        const EdgeInsets
                                            .all(
                                            7.0),
                                        child:
                                        isTicketData=="0"? const Text(
                                          'Raise Ticket',
                                          style:
                                          TextStyle(
                                            fontSize:
                                            12,
                                          ),
                                        ):const Text(
                                          'Show Ticket',
                                          style:
                                          TextStyle(
                                            fontSize:
                                            12,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment:
                                    Alignment
                                        .topRight,
                                    child:
                                    Padding(
                                      padding:
                                      const EdgeInsets
                                          .all(
                                          5.0),
                                      child:
                                      Container(
                                        decoration:
                                        BoxDecoration(
                                          color: Colors
                                              .green
                                              .shade50,
                                          border:
                                          Border.all(
                                            color: Colors
                                                .green
                                                .shade100,
                                            width:
                                            1,
                                          ),
                                          borderRadius:
                                          BorderRadius.circular(10),
                                        ),
                                        child:
                                        Padding(
                                          padding: const EdgeInsets
                                              .all(
                                              7.0),
                                          child:
                                          Container(
                                            child:
                                            Text(
                                              leadId,
                                              style:
                                              const TextStyle(
                                                fontSize: 12,
                                              ),
                                              textAlign:
                                              TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                ],
                              ),
                            ]),
                      ),
                      const SizedBox(
                          height: 8),
                    ],
                  ),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
  Widget getStatusTextAndIcon(String status) {
    TextStyle textStyle;

    if (status.contains("Reject")) {
      textStyle = const TextStyle(
        color: Colors.red,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    } else if (status.contains("Pending")) {
      textStyle = const TextStyle(
        color: Colors.orangeAccent,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    } else if (status.contains("Progress")) {
      textStyle = TextStyle(
        color: Colors.green.shade400,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    } else if (status.contains("Completed")) {
      textStyle = const TextStyle(
        color: Colors.green,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    } else {
      textStyle = const TextStyle(
        color: Colors.orange,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      );
    }

    return Text(status, style: textStyle);
  }
}

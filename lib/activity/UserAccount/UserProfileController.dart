import 'dart:convert';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:pi/Model/Profile/OtpMailVarification.dart';
import 'package:pi/Model/UtilityModel/BoolResponse.dart';
import 'package:pi/activity/UserAccount/OtpLoginViewController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/network/ApiClient.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../utils/AppUtils.dart';
import '../../Model/LoginResponse.dart';
import '../../Model/bank/BankResponse.dart';

class UserProfileController extends GetxController {
  BuildContext context;

  UserProfileController({required this.context});

  bool _isLoading = false;

  var review ='';
  bool mailVerification = false;

  bool get isLoading => _isLoading;
  TextEditingController otpController = TextEditingController();

  RxString adharImgEdt=''.obs;
  RxString adharImgVrf =''.obs;
  TextEditingController adharNumberEdt = TextEditingController();
  TextEditingController adharNumberVrf = TextEditingController();

  RxString panImgEdt = ''.obs;
  RxString panImgVrf = ''.obs;
  TextEditingController panNumberEdt = TextEditingController();
  TextEditingController panNumberVrf = TextEditingController();


  String version = '',
      kycStatus = '0',
      userId = '',
      userType = '',
      userCode = '',
      userName = '',
      address = '',
      mobile = '',
      email = '',
      pan = '',
      dob = '',
      name = '',
      adhar = '',
      token = '',
      remark = '',
      isMailVerified='';
  late String branch, accountNo, ifscCode, msg;
  bool isChecked = false;
  bool bankStatus = false;

  List<BankData> bankList = [];

  @override
  void onInit() {
    super.onInit();
    _getAppVersion();
    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    name = preferences.getString(AppUtils.userName) ?? "NA";
    name = AppUtils.capitalizeFirstWord(name.trim());
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    userCode = preferences.getString(AppUtils.userCode) ?? "";
    email = preferences.getString(AppUtils.userMail) ?? "";
    mobile = preferences.getString(AppUtils.userMobile) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    kycStatus = preferences.getString(AppUtils.kycStatus) ?? "0";
    remark = preferences.getString(AppUtils.kycRemark)?? '';

    print(preferences);
    getProfile();
    fetchBank();
    update();
  }

  Future<void> removeFcm() async {
    Map<String, String> hData = {
      'Authorization': 'Bearer $token',
    };
    Map<String, String> mData = {
      'Login_User_Id': userId,
      'Login_User_Type': userType,
      'platForm': AppClient.platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };

    if (kDebugMode) {
      print(mData);
    }
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.removeFcm);
      var response = await http.post(url, headers: hData, body: mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
      }
    } catch (e) {
      log(e.toString());
    }
  }

  showMyDialog() async {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text(
          "Logout",
          style: TextStyle(color: Colors.lightBlue),
        ),
        content: const Text(
          "Are you sure, want to logout ?",
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child:
                const Text('Cancel', style: TextStyle(color: Colors.black38)),
          ),
          TextButton(
            onPressed: () async {
              removeFcm();
              SharedPreferences preferences =
                  await SharedPreferences.getInstance();
              preferences.clear();
              // Get.to(OtpLoginViewController());
              Get.offAll(const OtpLoginViewController());

            },
            child: const Text('Logout',
                style: TextStyle(color: Colors.orangeAccent)),
          ),
        ],
      ),
    );
  }

  Future<void> submitQueryVisit(String type) async {
    Map<String, String> hData = {
      'Authorization': 'Bearer $token',
    };
    Map<String, String> mData = {
      'Login_User_Id': userId,
      'Login_User_Type': userType,
      'type': type,
      'platForm': AppClient.platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };

    if (kDebugMode) {
      print(mData);
    }
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.websiteVisitTrack);
      var response = await http.post(url, headers: hData, body: mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
      }
    } catch (e) {
      log(e.toString());
    }
  }

  void getProfile() async {
    setLoading(true);
    update();
    try {
      Map<String, String> hData = {
        'Authorization': 'Bearer $token',
      };
      Map<String, dynamic> mData = {
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.getProfile);
      var response = await http.post(url, headers: hData, body: mData);
      if (kDebugMode) {
        print(response.body);
      }
      if (response.statusCode == 200) {
        print(mData);
        print("This is mData for profile ui");
        LoginResponse loginResponse = LoginResponse.fromJson(jsonDecode(response.body));
        if (loginResponse.status) {
          print(loginResponse.data!.remark??"");
          name = loginResponse.data!.name ?? "";
          print(name);
          email = loginResponse.data!.email ?? "";
          print(email);
          address = loginResponse.data!.address ?? "";
          print(address);
          dob = loginResponse.data!.dob ?? "";
          print(dob);
          pan = loginResponse.data!.panNumber ?? "";
          print(pan);
          adhar = loginResponse.data!.aadhaarNumber ?? "";
          print(adhar);
          isMailVerified = loginResponse.data!.isMailVerified??'';
          print(isMailVerified);
          print("isMailVerified");
          update();
        }
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
    update();
  }

  launchUrls(String url, String type) async {
    if (type.isNotEmpty) {
      submitQueryVisit(type);
    }
    print(type);
    final Uri url0 = Uri.parse(url);

    if (!await launchUrl(url0)) {
      throw Exception('Could not launch $url0');
    }
  }

  Future<void> _getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    version = packageInfo.version;
    update();
  }

  void fetchBank() async {
    try {
      setLoading(true);
      Map<String, String> mData = {
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.fetchBank);
      var response = await http.post(url, body: mData);
      print(mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        if (bankList.isNotEmpty) {
          bankList.clear();
        }
        Map<String, dynamic> responseJson = json.decode(response.body);
        BankResponse myResponse = BankResponse.fromJson(responseJson);
        bankList.addAll(myResponse.data);
        if(bankList.isNotEmpty){
          for(var i = 0; i < bankList.length; i++){
            if(bankList[i].kycStatus.isEmpty && bankList[i].kycStatus == "0"){
              bankStatus = true;
              break;
            }
          }
        }
      }
      setLoading(false);
      update();
    } catch (e) {
      print(e);
    }
  }

  void setPrimaryData(String bankId) async {
    try {
      setLoading(true);
      Map<String, String> mData = {
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'bank_id': bankId,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.setPrimaryBank);
      var response = await http.post(url, body: mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        if (bankList.isNotEmpty) {
          bankList.clear();
        }
        Map<String, dynamic> responseJson = json.decode(response.body);
        BoolResponse myResponse = BoolResponse.fromJson(responseJson);
        if (myResponse.status) {
          fetchBank();
        }
      }
      update();
    } catch (e) {
      print(e);
    }
  }


  void sendOtp(var email)async{
    try{
      Map<String, dynamic> mData={
        'email': email
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.sendOtpToMail);
      var response = await http.post(url,body: mData);
      print(response.statusCode);
      print('sendOtpStatusCode......................................................');
      if(response.statusCode == 200){
        Map<String , dynamic> responseJson = json.decode(response.body);
        OtpMailVerification myResponse = OtpMailVerification.fromJson(responseJson);
        print(myResponse.message);
      }
    }catch(e){
      print(e);
    }
  }

//verify OTP
  void verifyOtp(var email, var OTP) async {
    try{
      Map<String, dynamic> mData ={
        'email': email,
        'otp':OTP
      };
      print(email);
      print(OTP);
      var url = Uri.parse(AppUtils.baseUrl+AppUtils.verifyMailOtp);
      var response = await http.post(url,body: mData);
      print(response.statusCode);
      print('verifyOtp statusCode ..................................................');
      if(response.statusCode == 200){
        Map<String, dynamic> responseJson = json.decode(response.body);
        OtpMailVerification myResponse = OtpMailVerification.fromJson(responseJson);
        print(myResponse.message);

        print(myResponse.status);
        if(myResponse.status=='false'){
            review = 'OTP Mismatched';
            AppUtils.showSnackBar(review, 'Click on resend code or Try again.', false);
        }else if(myResponse.status == 'true'){
            print('i am in else part ');
            review ='Verified';
            AppUtils.showSnackBar(review, 'Now you are a verified user.', true);
            mailVerification = true;
            Get.back();
            getSharedData();
            // fetch user data again here !
            update();
        }else{
          AppUtils.showSnackBar('Something went wrong', 'Try Again', false);
        }
        update();
      }
    }catch(e){print(e);}
  }

  Future getImage(RxString imagePath)async{
    final ImagePicker picker = ImagePicker();
    showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        title: const Text('Choose Medium'),
        content: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
          IconButton(onPressed: ()async{
            final image = await picker.pickImage(source: ImageSource.gallery);
            if(image == ''){Get.back();}else{
            imagePath.value = image!.path;
            Get.back();
            update();
            }
          }, icon: const Icon(Icons.photo)),
          IconButton(onPressed: () async {
            final image = await picker.pickImage(source: ImageSource.camera);
            if(image == ''){Get.back();}else{
              imagePath.value = image!.path;
              Get.back();
              update();
            }
          }, icon: const Icon(Icons.camera)),
        ],),
      );
    });
  }

  void updateData() {
    update();
  }



}

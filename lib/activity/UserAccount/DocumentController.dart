import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../utils/AppUtils.dart';
import '../../Model/Profile/DocList.dart';
import '../../network/ApiClient.dart';

class DocumentController extends GetxController {
  bool _isLoading = false;

  bool get isLoading => _isLoading;
  DocList productsData = DocList();
  String userId = "", userMobile = "", token = "", userType = "", version = "";


  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void _getAgentList(String userId, String userType) async {
    setLoading(true);
    try {

      Map<String, String> hData = {
        'Authorization': 'Bearer $token',
      };

      Map<String, String> mData = {
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.docList);
      print(url);
      print(mData);
      var response = await http.post(url, body: mData, headers: hData);
      print(response.body);
      if (response.statusCode == 200) {
        productsData = DocList.fromJson(jsonDecode(response.body));
        update();
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    _getAgentList(userId, userType);
  }
}

import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pi/activity/UserAccount/OtpLoginViewController.dart';
import 'package:screenshot/screenshot.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/network/ApiClient.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../utils/AppUtils.dart';

class ProfileController extends GetxController {
  BuildContext context;

  ProfileController({required this.context});

  bool _isLoading = false;

  bool get isLoading => _isLoading;
  String version = '',
      kycStatus = '',
      userId = '',
      userType = '',
      userCode = '',
      userName = '',
      mobileNum = '',
      token = '',
      kycRemark ='';

  bool invest = false;

  String investNowTxt = 'Invest Now';
  String loanTermsTxt = 'Loan Terms';
  String myLeadsTxt = 'My Leads';
  String raiseRequestTxt = 'Raise Request';
  String myPartnerTxt = 'My Partner';
  String reportTxt = 'Report';


  String mutualFundTxt = 'Mutual Fund';
  String fixedDepositTxt = 'Fixed Deposit (FD)';
  String recurringDepositTxt = 'Recurring Deposit\n(RD)';
  String apkaMakaanTxt = 'Apka Makaan\n(Real Estate)';
  // String dsrTxt = 'DSR';
  Map <String, String> grids ={};
  Map <String, String> subGrid ={};

  String investNowImg = 'investNow.jpg';
  String loanTermsImg = 'RBI.jpg';
  String myLeadsImg = 'trackMyLead.jpeg';
  String raiseRequestImg ='raiseRequest.jpg';
  String myPartnerImg = 'myPartner.jpeg';
  String reportImg = 'reportImg.png';
  // String dsrImg ='dsr.png';


  String mutualFundImg = 'MutualFund.jpg';
  String fixedDepositImg = 'fixDeposit.jpg';
  String recurringDepositImg = 'recurringDeposit.jpg';
  String apkaMakaanImg = 'apkaMakaan.jpg';

  List <MapEntry<String, String>> entries =[];

  List <MapEntry<String, String>> subEntries = [];

  late String branch, accountNo, ifscCode, name, mobile, email, msg;
  final localData = GetStorage();
  var fullName;



  @override
  Future<void> onInit() async {
    _getAppVersion();
   await  getSharedData();


    grids =

    userType == "employee"?{
      investNowTxt : investNowImg,
      myPartnerTxt : myPartnerImg,
      myLeadsTxt : myLeadsImg,
      reportTxt : reportImg,
      raiseRequestTxt : raiseRequestImg,
      loanTermsTxt : loanTermsImg,
      // dsrTxt : dsrImg
    }:
    {
      investNowTxt : investNowImg,
      myLeadsTxt : myLeadsImg,
      raiseRequestTxt : raiseRequestImg,
      loanTermsTxt : loanTermsImg
    };

    entries = grids.entries.toList();

    subGrid = {
      mutualFundTxt : mutualFundImg,
      fixedDepositTxt : fixedDepositImg,
      recurringDepositTxt : recurringDepositImg,
      apkaMakaanTxt : apkaMakaanImg
    };

    subEntries = subGrid.entries.toList();




    super.onInit();


  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }
  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userName = preferences.getString(AppUtils.userName) ?? "NA";
    fullName = userName.capitalizeFirst;
    userName = AppUtils.capitalizeFirstWord(userName.trim());
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    userCode = preferences.getString(AppUtils.userCode) ?? "";
    email = preferences.getString(AppUtils.userMail) ?? "";
    mobileNum = preferences.getString(AppUtils.userMobile) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    kycStatus = preferences.getString(AppUtils.kycStatus) ?? "0";
    kycRemark = preferences.getString(AppUtils.kycRemark)??'';

    print(email);
    update();
  }

  Future<void> removeFcm() async {

    Map<String, String> mData = {
      'user_id': userId,
      'device_type': AppUtils.platForm
    };

    if (kDebugMode) {
      print(mData);
    }
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.removeFcm);
      var response = await http.post(url, body: mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
      }
    } catch (e) {
      log(e.toString());
    }
  }

  showMyDialog() async {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text(
          "Logout",
          style: TextStyle(color: Colors.lightBlue),
        ),
        content: const Text(
          "Are you sure, want to logout ?",
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child:
                const Text('Cancel', style: TextStyle(color: Colors.black38)),
          ),
          TextButton(
            onPressed: () async {
              removeFcm();
              SharedPreferences preferences =
                  await SharedPreferences.getInstance();
              preferences.clear();
              localData.erase();
              preferences.setBool(AppUtils.showSplash, false);
              print(preferences.getBool(AppUtils.showSplash));
              print('see this bool');
              Get.offAll(const OtpLoginViewController());
            },
            child: const Text('Logout',
                style: TextStyle(color: Colors.orangeAccent)),
          ),
        ],
      ),
    );
  }

  Future<void> submitQueryVisit(String type) async {
    Map<String, String> hData = {
      'Authorization': 'Bearer $token',
    };

    Map<String, String> mData = {
      'Login_User_Id': userId,
      'Login_User_Type': userType,
      'type': type,
      'platForm': AppClient.platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };

    if (kDebugMode) {
      print(mData);
    }
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.websiteVisitTrack);
      var response = await http.post(url, headers: hData, body: mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
      }
    } catch (e) {
      log(e.toString());
    }
  }

  launchUrls(String url, String type) async {
    if (type.isNotEmpty) {
      submitQueryVisit(type);
    }
    print(type);
    final Uri url0 = Uri.parse(url);

    if (!await launchUrl(url0)) {
      throw Exception('Could not launch $url0');
    }
  }

  Future<void> _getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    version = packageInfo.version;
    update();
  }

  ScreenshotController screenshotController = ScreenshotController();

  void takeScreenshot() {
    screenshotController.capture().then((Uint8List? image) {
      if (image != null) {
        print('elephant');
        _saveAndShareScreenshot(image);
      } else {
        print('elephant1');
      }
    }).catchError((onError) {
      print(onError);
    });
  }


  Future<void> _saveAndShareScreenshot(Uint8List image) async {
    final directory = await getTemporaryDirectory();
    final imagePath = '${directory.path}/screenshot.png';
    File file = File(imagePath);
    await file.writeAsBytes(image);

    if (await file.exists()) {
      // Share the image using share_plus
      await Share.shareXFiles([XFile(imagePath)], text: 'Check out this screenshot!');
    } else {
      print('File does not exist');
    }
  }
}
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pi/utils/AppUtils.dart';

import '../../utils/WebviewController.dart';
import 'LeadCollectUIController.dart';

class InvestOptionsUIController extends StatefulWidget {
  const InvestOptionsUIController({super.key});

  @override
  State<InvestOptionsUIController> createState() => _InvestOptionsUIControllerState();
}

class _InvestOptionsUIControllerState extends State<InvestOptionsUIController> {

  String mutualFundTxt = 'Mutual Fund';
  String fixedDepositTxt = 'Fixed Deposit (FD)';
  String recurringDepositTxt = 'Recurring Deposit\n(RD)';
  String apkaMakaanTxt = 'Apka Makaan\n(Real Estate)';
  String mutualFundImg = 'MutualFund.jpg';
  String fixedDepositImg = 'fixDeposit.jpg';
  String recurringDepositImg = 'recurringDeposit.jpg';
  String apkaMakaanImg = 'apkaMakaan.jpg';
  Map <String, String> subGrid ={};
  
  List <MapEntry<String, String>> subEntries = [];

 


  
  @override
  void initState() {
    subGrid = {
    mutualFundTxt : mutualFundImg,
    fixedDepositTxt : fixedDepositImg,
    recurringDepositTxt : recurringDepositImg,
    apkaMakaanTxt : apkaMakaanImg
  };
    subEntries = subGrid.entries.toList();
    super.initState();
  }
  
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Investment Options"),
      ),
      body:
        Stack(children: [
          Container(height: double.infinity,
          width: double.infinity,
          color:AppUtils.bgColor),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              GestureDetector(
                onTap:(){
                  Navigator.of(context).pop();
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 8),
                  child: GridView.builder( itemCount:  subEntries.length,gridDelegate:
                  const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 15,
                      mainAxisExtent: 120
                  ),
                      itemBuilder: (context, index) {
                        return Column(
                          children: [
                            Flexible(
                              flex:1,
                              child: GestureDetector(
                                onTap:  subEntries[index].key ==  mutualFundTxt ? () {
                                  String  url  = 'https://squarewealthguru.com/';
                                  String title = "Mutual Fund";
                                  Get.to(
                                      WebviewController(url: url, title: title));
                                }:  subEntries[index].key ==  fixedDepositTxt ? () {
                                  Get.to(LeadCollectUIController(
                                      type: "FD", lead: "Fixed Deposit"));
                                }:  subEntries[index].key ==  recurringDepositTxt ?
                                    () {
                                  Get.to(LeadCollectUIController(
                                      type: "RD", lead: "Recurring Deposit"));
                                }:  subEntries[index].key ==  apkaMakaanTxt ?  () {
                                  String url = 'https://apkamakaan.com/';
                                  String title = 'Apka Makaan';
                                  Get.to(
                                      WebviewController(url: url, title: title));
                                }: (){},
                                child: Material(
                                  color:Colors.transparent,
                                  child: Container(
                                      height: MediaQuery.of(context).size.height*0.15,
                                      width:  MediaQuery.of(context).size.width*0.8,
                                      decoration:
                                      BoxDecoration(
                                          borderRadius: BorderRadius.circular(11),
                                          image:DecorationImage(image: AssetImage('res/images/profile/${ subEntries[index].value}'),fit: BoxFit.cover),
                                          boxShadow: const [BoxShadow(
                                              blurRadius: 8, color: Colors.white, spreadRadius: -4, offset: Offset(2, 4)
                                          )]
                                      ),
                                      child: Align(
                                        alignment: Alignment.bottomCenter,
                                        child: Padding(
                                          padding: const EdgeInsets.only(bottom:2),
                                          child: Container(
                                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(11), color:Colors.black54),
                                              child: Padding(
                                                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                                                child: Text( subEntries[index].key,style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.white,fontSize: 15),textAlign: TextAlign.center,),
                                              )),
                                        ),
                                      )
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      },shrinkWrap: true),
                ),
              ),
            ],
          )
        ],)
    );
  }
}


//    // showGeneralDialog(
//                             //     context: context,
//                             //     barrierDismissible: true,
//                             //     barrierLabel: "Barrier",
//                             //     barrierColor: Colors.black.withOpacity(0.8), // Set your desired background color here
//                             //     transitionDuration: const Duration(milliseconds: 200),
//                             //     pageBuilder: (context,animation1, animation2) {
//                             //       return Column(
//                             //         mainAxisAlignment: MainAxisAlignment.start,
//                             //         children: [
//                             //           GestureDetector(
//                             //             onTap:(){
//                             //           Navigator.of(context).pop();
//                             //                                           },
//                             //             child: Padding(
//                             //           padding: const EdgeInsets.symmetric(vertical: 35,horizontal: 8),
//                             //           child: Column(
//                             //             crossAxisAlignment: CrossAxisAlignment.start,
//                             //             children: [
//                             //               GestureDetector(
//                             //                   onTap: (){
//                             //                     Get.back();
//                             //                   },
//                             //                   child: const Icon(Icons.arrow_back_outlined,color: Colors.white)),
//                             //               GridView.builder( itemCount:  subEntries.length,gridDelegate:
//                             //               const SliverGridDelegateWithFixedCrossAxisCount(
//                             //                     crossAxisCount: 2,
//                             //                     crossAxisSpacing: 15,
//                             //                     mainAxisSpacing: 15,
//                             //                     mainAxisExtent: 120
//                             //                     ),
//                             //                     itemBuilder: (context, index) {
//                             //                     return Column(
//                             //                     children: [
//                             //                       Flexible(
//                             //                         flex:1,
//                             //                         child: GestureDetector(
//                             //                           onTap:  subEntries[index].key ==  mutualFundTxt ? () {
//                             //                             Get.back();
//                             //                             String  url  = 'https://squarewealthguru.com/';
//                             //                             String title = "Mutual Fund";
//                             //                             Get.to(
//                             //                                 WebviewController(url: url, title: title));
//                             //                           }:  subEntries[index].key ==  fixedDepositTxt ? () {
//                             //                             Get.back();
//                             //                             Get.to(LeadCollectUIController(
//                             //                                 type: "FD", lead: "Fixed Deposit"));
//                             //                           }:  subEntries[index].key ==  recurringDepositTxt ?
//                             //                            () {
//                             //                              Get.back();
//                             //                             Get.to(LeadCollectUIController(
//                             //                                 type: "RD", lead: "Recurring Deposit"));
//                             //                           }:  subEntries[index].key ==  apkaMakaanTxt ?  () {
//                             //                             Get.back();
//                             //                             String url = 'https://apkamakaan.com/';
//                             //                             String title = 'Apka Makaan';
//                             //                             Get.to(
//                             //                                 WebviewController(url: url, title: title));
//                             //                           }: (){},
//                             //                             child: Material(
//                             //                               color:Colors.transparent,
//                             //                               child: Container(
//                             //                                 height: MediaQuery.of(context).size.height*0.15,
//                             //                                 width:  MediaQuery.of(context).size.width*0.8,
//                             //                                 decoration:
//                             //                                   BoxDecoration(
//                             //                                       borderRadius: BorderRadius.circular(11),
//                             //                                       image:DecorationImage(image: AssetImage('res/images/profile/${ subEntries[index].value}'),fit: BoxFit.cover),
//                             //                                       boxShadow: const [BoxShadow(
//                             //                                           blurRadius: 8, color: Colors.white, spreadRadius: -4, offset: Offset(2, 4)
//                             //                                       )]
//                             //                                   ),
//                             //                                 child: Align(
//                             //                                   alignment: Alignment.bottomCenter,
//                             //                                   child: Padding(
//                             //                                     padding: const EdgeInsets.only(bottom:2),
//                             //                                     child: Container(
//                             //                                         decoration: BoxDecoration(borderRadius: BorderRadius.circular(11), color:Colors.black54),
//                             //                                         child: Padding(
//                             //                                           padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
//                             //                                           child: Text( subEntries[index].key,style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.white,fontSize: 15),textAlign: TextAlign.center,),
//                             //                                         )),
//                             //                                   ),
//                             //                                 )
//                             //                               ),
//                             //                             ),
//                             //                         ),
//                             //                       ),
//                             //                     ],
//                             //                                                                         );
//                             //                     },shrinkWrap: true),
//                             //             ],
//                             //           ),
//                             //             ),
//                             //           ),
//                             //         ],
//                             //       );})
import 'package:flutter/material.dart';

class EmployeeCallVirtualUIController extends StatefulWidget {
  const EmployeeCallVirtualUIController({super.key});

  @override
  State<EmployeeCallVirtualUIController> createState() => _EmployeeCallVirtualUIControllerState();
}

class _EmployeeCallVirtualUIControllerState extends State<EmployeeCallVirtualUIController> {

  TextEditingController remarkController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Employee Activity - Call"),),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 0),
                      child:   Text('Employee',style: TextStyle(color: Colors.black54,fontSize: 16),),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: DropdownButtonFormField<String>(
                        items: const [
                          DropdownMenuItem(child: Text("item")),
                        ],
                        onChanged: ( newValue) {
                        },
                        decoration: const InputDecoration(
                            hintText: "Select"
                        ),
                      ),
                    ),
                  ],
                ),
              ),
        
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 0),
                      child:   Text('Remark',style: TextStyle(color: Colors.black54,fontSize: 16),),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: TextFormField(
                        controller: remarkController,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          focusedBorder: OutlineInputBorder(),
                        ),
                        maxLines: null,
                      ),
                    ),
                  ],
                ),
              ),
        
              const SizedBox(height:10),
              SizedBox(
                width: MediaQuery.of(context).size.width*0.6,
                child: ElevatedButton(onPressed: (){
        
                },
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue.shade50,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12))
                  ), child: const Text("SUBMIT"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

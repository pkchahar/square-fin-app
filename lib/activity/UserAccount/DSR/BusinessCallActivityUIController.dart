import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pi/activity/UserAccount/DSR/BusinessCallActivityController.dart';

import '../../../Model/Pos/PartnerList.dart';

class BusinessCallActivityUIController extends StatefulWidget {
  const BusinessCallActivityUIController({super.key});

  @override
  State<BusinessCallActivityUIController> createState() => _BusinessCallActivityUIControllerState();
}


GlobalKey formKey = GlobalKey();

class _BusinessCallActivityUIControllerState extends State<BusinessCallActivityUIController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<BusinessCallActivityController>
      (
        init: BusinessCallActivityController(),
        builder: (_){

     return  Scaffold(
      appBar: AppBar(title: const Text("Aspirant/Prospect"),),backgroundColor: Colors.grey.shade50,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: formKey,
             child: Padding(
               padding: const EdgeInsets.all(8.0),
               child: Column(
                 children: [
                   
                   Card(
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         const Padding(
                           padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 0),
                           child: Text('Partner',style: TextStyle(color: Colors.black54,fontSize: 16),),
                         ),
                         Padding(
                           padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                           child: DropdownButtonFormField<PartnerList>(
                             items: _.myPartner.map((PartnerList value) {
                               return DropdownMenuItem<PartnerList>(
                                 value: value,
                                 child: Text(value.name),
                               );
                             }).toList(),
                             onChanged: (PartnerList? newValue) {
                               _.partnerController = newValue?.name;
                               print('Selected partner: ${_.partnerController}');
                             },
                             decoration: const InputDecoration(
                                 hintText: "Select"

                             ),
                           ),
                         ),
                       ],
                     ),
                   ),
                  
                   const SizedBox(height: 10,),
                   Card(child:
                     Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         const Padding(
                           padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 8),
                           child: Text('Interaction Type',style: TextStyle(color: Colors.black54,fontSize: 16),),
                         ),
                         Padding(
                           padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 5),
                           child: Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: [
                               Padding(
                                 padding: const EdgeInsets.only(left: 15),
                                 child:
                                 Row(
                                   children: [
                                     InkWell(
                                       onTap:(){
                                         setState(() {
                                           _.visitWithReportingManagerBool = false;
                                           _.virtualBool = true;
                                           _.visitBool = false;
                                         });
                                       },
                                       child: Container(
                                         height: 20,
                                         width: 20,
                                         decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),border: Border.all(color: Colors.blue,width: 2)),
                                         child: _.virtualBool?Padding(
                                           padding: const EdgeInsets.all(2),
                                           child: Container(
                                             decoration: BoxDecoration( color:Colors.blue,borderRadius: BorderRadius.circular(50)),
                                             height: 5,
                                             width: 5,
                                           ),
                                         ): const SizedBox()
                                       ),
                                     ),
                                     const SizedBox(width: 5),
                                     const Text("Virtual"),
                                     const SizedBox(width: 15),
                                     InkWell(
                                       onTap:(){
                                         setState(() {
                                           _.virtualBool = false;
                                           _.visitBool = true;
                                         });
                                       },
                                       child: Container(
                                           height: 20,
                                           width: 20,
                                           decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),border: Border.all(color: Colors.blue,width: 2)),
                                           child: Padding(
                                             padding: const EdgeInsets.all(2),
                                             child: _.visitBool?Container(
                                               decoration: BoxDecoration( color:Colors.blue,borderRadius: BorderRadius.circular(50)),
                                               height: 5,
                                               width: 5,
                                             ):const SizedBox(),
                                           )
                                       ),
                                     ),
                                     const SizedBox(width: 5),
                                     const Text('Visit')
                                   ],
                                 ),
                               ),
                               _.visitBool? Row(
                                 children: [
                                   Checkbox(
                                       activeColor: Colors.blue,value: _.visitWithReportingManagerBool, onChanged: (newValue){
                                     setState(() {
                                     _.visitWithReportingManagerBool = newValue!;
                                     });
                                   }),
                                   // const SizedBox(width: 5,),

                                   const Text('Visit with Reporting Manager')
                                 ],
                               ):const SizedBox(),
                               (_.visitWithReportingManagerBool && _.visitBool)?const Divider(thickness: 1,):const SizedBox(),
                               (_.visitWithReportingManagerBool && _.visitBool)?const   Padding(
                                 padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 8),
                                 child: Text('Reporting Manager',style: TextStyle(color: Colors.black54,fontSize: 16),),
                               ):const SizedBox(),
                               (_.visitWithReportingManagerBool && _.visitBool)?
                               Padding(
                                 padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                 child: DropdownButtonFormField<PartnerList>(
                                   items: _.myPartner.map((PartnerList value) {
                                     return DropdownMenuItem<PartnerList>(
                                       value: value,
                                       child: Text(value.name),
                                     );
                                   }).toList(),
                                   onChanged: (PartnerList? newValue) {
                                     _.partnerController = newValue?.name;
                                   },
                                   decoration: const InputDecoration(
                                     hintText: "Select"
                                   ),
                                 ),
                               ):const SizedBox(),
                             ],
                           ),
                         ),
                       ],
                     ),),
        
                   // TextFormField(
                   //   controller: partnerController,
                   //   decoration: InputDecoration(
                   //     border: OutlineInputBorder(),
                   //     focusedBorder: OutlineInputBorder(),
                   //     labelText: "Select Partner",
                   //     labelStyle: TextStyle(color: Colors.black)
                   //   ),
                   // ),
                   Card(
                     child: Padding(
                       padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 8),
                       child: Row(
                         children: [
                           const Text("Follow Up Type",style: TextStyle(color: Colors.black54,fontSize: 16),),
                           const SizedBox(width: 10,),
                           InkWell(
                             onTap:(){
                               setState(() {
                                 _.followUpTypeBool = !_.followUpTypeBool;
                               });
                             },
                             child: Container(
                                 height: 20,
                                 width: 20,
                                 decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),border: Border.all(color: Colors.blue,width: 2)),
                                 child: Padding(
                                   padding: const EdgeInsets.all(2),
                                   child: _.followUpTypeBool?Container(
                                     decoration: BoxDecoration( color:Colors.blue,borderRadius: BorderRadius.circular(50)),
                                     height: 5,
                                     width: 5,
                                   ):const SizedBox(),
                                 )
                             ),
                           ),
        
                           const Padding(
                             padding: EdgeInsets.symmetric(horizontal: 10),
                             child: Text("Yes"),
                           ),
                           InkWell(
                             onTap:(){
                               setState(() {
                                 _.followUpTypeBool = !_.followUpTypeBool;
                               });
                             },
                             child: Container(
                                 height: 20,
                                 width: 20,
                                 decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),border: Border.all(color: Colors.blue,width: 2)),
                                 child: Padding(
                                   padding: const EdgeInsets.all(2),
                                   child: !_.followUpTypeBool?Container(
                                     decoration: BoxDecoration( color:Colors.blue,borderRadius: BorderRadius.circular(50)),
                                     height: 5,
                                     width: 5,
                                   ):const SizedBox(),
                                 )
                             ),
                           ),
                           const SizedBox(width: 10,),
                           const Text("No"),
                         ],
                       ),
                     ),
                   ),
                   InkWell(
                     onTap:(){
                       _.captureImage();
                     },
                     child: Card(
                       child: Padding(
                         padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 8),
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           children: [
                             SizedBox(
                                 width: MediaQuery.of(context).size.width*0.8,
                                 child: Text(_.imgName!="Capture Image"? _.imgName.length > 10 ? _.imgName.substring(_.imgName.length - 10) : _.imgName:_.imgName,style: const TextStyle(color: Colors.black54,fontSize: 16,overflow: TextOverflow.ellipsis),)),
                             const Icon(Icons.camera)
                           ],
                         ),
                       ),
                     ),
                   ),
                   Card(
                     child:Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         const Padding(
                           padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 0),
                           child:   Text('Follow Up Remark',style: TextStyle(color: Colors.black54,fontSize: 16),),
                         ),
                         Padding(
                           padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                           child: DropdownButtonFormField<PartnerList>(
                             items: _.myPartner.map((PartnerList value) {
                               return DropdownMenuItem<PartnerList>(
                                 value: value,
                                 child: Text(value.name),
                               );
                             }).toList(),
                             onChanged: (PartnerList? newValue) {
                               _.partnerController = newValue?.name;
                             },
                             decoration: const InputDecoration(
                                 hintText: "Select"
                             ),
                           ),
                         ),
        
                       ],
                     ),
                   ),
                   InkWell(
                     onTap:(){
                       _.selectDate(context);
                     },
                     child: Card(
                       child: Padding(
                         padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 8),
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           children: [
                             Text(_.followUpDate!=null?_.followUpDate.toString():"Follow Up Date(If Any)",style: const TextStyle(color: Colors.black54,fontSize: 16,overflow: TextOverflow.ellipsis),),
                             const Icon(Icons.calendar_month_sharp)
                           ],
                         ),
                       ),
                     ),
                   ),
                   Card(
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         const Padding(
                           padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 0),
                           child:   Text('Remark',style: TextStyle(color: Colors.black54,fontSize: 16),),
                         ),
                         Padding(
                           padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                           child: TextFormField(
                             controller: _.remarkController,
                             decoration: const InputDecoration(
                               border: OutlineInputBorder(),
                               focusedBorder: OutlineInputBorder(),
                             ),
                             maxLines: null,
                           ),
                         ),
                       ],
                     ),
                   ),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                     children: [
                     SizedBox(
                         width: MediaQuery.of(context).size.width*0.4,
                         child: ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(
                           backgroundColor: Colors.purple.shade100
                         ), child: const Text("SAVE OFFLINE"),)),
                     SizedBox(
                         width: MediaQuery.of(context).size.width*0.4,
                         child: ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(
                             backgroundColor: Colors.blue.shade100), child: const Text("CREATE"))),
                   ],)
                 ],
               ),
             ),
            )
          ],
        ),
      )
    );}
    );}
}

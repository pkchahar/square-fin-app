import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pi/activity/UserAccount/DSR/BusinessCallUIController.dart';
import 'package:pi/activity/UserAccount/DSR/CrossSellUIController.dart';
import 'package:pi/activity/UserAccount/DSR/EmployeeCallUIController.dart';
import 'package:pi/activity/UserAccount/DSR/EmployeeCallVirtualUIController.dart';
import 'package:pi/activity/UserAccount/DSR/ProspectCallUIController.dart';

import 'BusinessCallActivityUIController.dart';
import 'EmployeeCallMeetingUIController.dart';
import 'ProspectCallProspectUIController.dart';

class DsrListUIController extends StatefulWidget {
  const DsrListUIController({super.key});

  @override
  State<DsrListUIController> createState() => _DsrListUIControllerState();
}

class _DsrListUIControllerState extends State<DsrListUIController> {

  static const String  businessCallTxt = 'Business Call';
  static const String prospectCallTxt = 'Prospect Call';
  static const String crossSellTxt = 'Cross Sell';
  static const String employeeCallTxt = 'Employee Call';


  List listTxt = [];
  List listImg = ['businessCall.svg', 'prospectCall.svg', 'crossSell.svg', 'employeeCall.svg'];
  List listColor = [Colors.blue.shade50, Colors.purple.shade50, Colors.green.shade50, Colors.indigo.shade50];
  List listColor1 = [Colors.blue.shade200, Colors.purple.shade200, Colors.green.shade200, Colors.indigo.shade200];
  List listColor2 = [Colors.blue.shade100, Colors.purple.shade100, Colors.green.shade100, Colors.indigo.shade100];
  @override
  void initState() {
    listTxt = [businessCallTxt, prospectCallTxt, crossSellTxt, employeeCallTxt];
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: const Text("Daily Sales Report"),),
      backgroundColor: Colors.grey.shade50,
      body:

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView.builder(itemCount: 4,itemBuilder: (context, index){
            return
              Padding(
                padding: const EdgeInsets.only(bottom: 25,left: 5,right: 5),
                child: SizedBox(
                    height : MediaQuery.of(context).size.height*0.13,
                    width : 200,
                    child: InkWell(
                      onTap:(){

                        switch (listTxt[index])
                        {
                          case businessCallTxt:
                            Get.to(const BusinessCallUIController());
                            break;
                          case prospectCallTxt:
                            Get.to(const ProspectCallUIController());
                            break;
                          case crossSellTxt:
                            Get.to(const CrossSellUIController());
                            break;
                          case employeeCallTxt:
                            Get.to(const EmployeeCallUIController());
                            break;
                          default:
                            null;
                        }

                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color:Colors.white,
                            borderRadius: BorderRadius.circular(12),boxShadow: [BoxShadow(color: Colors.grey.shade200,blurRadius: 10,offset: const Offset(0, 0))]),
                        child: Stack(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 15),
                              child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Container(
                                    height: 20,
                                    width: 20,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),boxShadow: [BoxShadow(color: Colors.blue.shade50, spreadRadius: 6, offset: const Offset(0, 0))]),
                                  )
                              ),
                            ),
                            
                            Padding(
                              padding: const EdgeInsets.only(right: 15),
                              child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Icon(Icons.arrow_forward_ios,size: 15,color: Colors.blue.shade800,)),
                            ),

                            Padding(
                              padding: const EdgeInsets.only(left: 200),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: SizedBox(
                                  height: MediaQuery.of(context).size.height*0.07,
                                  child: SvgPicture.asset(
                                      'res/images/${listImg[index]}'
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top:10,left: 10),
                              child: Align(
                                alignment: Alignment.topLeft,
                                  child: Text(listTxt[index],style: const TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w500),)),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left:10,top: 50,bottom: 10),
                              child: Align(
                                alignment: Alignment.bottomLeft,
                                child: Row(
                                  children: [
                                    ElevatedButton(onPressed: (){
                                      listTxt[index] == employeeCallTxt ?Get.to(const EmployeeCallMeetingUIController()):Get.to(const BusinessCallActivityUIController());
                                    },style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),backgroundColor: Colors.blue.shade300), child: IntrinsicWidth(
                                      child: Row(
                                        children: [
                                          const Icon(Icons.add,color: Colors.white,size: 18,),
                                          listTxt[index] == employeeCallTxt?const Text("Meeting",style: TextStyle(color: Colors.white,fontSize: 15)):const Text("Activity",style: TextStyle(color: Colors.white,fontSize: 15),),
                                        ],
                                      ),
                                    ),),
                                    const SizedBox(width: 10,),
                                    (listTxt[index] == prospectCallTxt ||listTxt[index] == employeeCallTxt)?
                                    ElevatedButton(onPressed: (){
                                      listTxt[index] == prospectCallTxt? Get.to(const ProspectCallProspectUIController()):listTxt[index] == employeeCallTxt? Get.to(const EmployeeCallVirtualUIController()):null;
                                    },style: ElevatedButton.styleFrom(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),backgroundColor: Colors.purple.shade200), child: IntrinsicWidth(
                                      child: Row(
                                        children: [
                                          const Icon(Icons.add,color: Colors.white,size: 18,),
                                          Text(index ==1?"Prospect": "Virtual",style: const TextStyle(color: Colors.white,fontSize: 15),)
                                        ],
                                      ),
                                    ),):const SizedBox(),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
              );
          }),
        )

    );
  }
}

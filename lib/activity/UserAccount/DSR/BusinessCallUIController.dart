import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pi/activity/UserAccount/DSR/BusinessCallController.dart';
import 'package:url_launcher/url_launcher.dart';


class BusinessCallUIController extends StatefulWidget {
  const BusinessCallUIController({super.key});

  @override
  State<BusinessCallUIController> createState() => _BusinessCallUIControllerState();
}

class _BusinessCallUIControllerState extends State<BusinessCallUIController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<BusinessCallController>(
        init: BusinessCallController(),
        builder: (_){
          return  Scaffold(
            appBar: AppBar(title: Text('Business Call' ' (${_.myPartner.length})'),actions: [
              // IconButton(
              //   onPressed: () {
              //     showDialog(
              //       context: context,
              //       builder: (BuildContext context) {
              //         return AlertDialog(
              //           title: const Text('Add New Partner!',style: TextStyle(color: Colors.blue),),
              //           content: SingleChildScrollView(
              //             child: StatefulBuilder(
              //               builder:
              //                   (BuildContext context, StateSetter setState) {
              //                 return Form(
              //                   key: formKey,
              //                   child: Column(
              //                     children: [
              //                       TextFormField(
              //                         controller: _.nameController,
              //                         inputFormatters: [
              //                           FilteringTextInputFormatter.deny(RegExp(r'[0-9]'))
              //                         ],
              //                         decoration: const InputDecoration(
              //                           labelText: 'Name',
              //                           border: OutlineInputBorder(),
              //                           focusedBorder: OutlineInputBorder(),
              //                           labelStyle:
              //                           TextStyle(color: Colors.grey),
              //                         ),
              //                         validator: (value) {
              //                           if (value == null || value.isEmpty) {
              //                             return 'Please enter Name';
              //                           }
              //                           return null;
              //                         },
              //                       ),
              //                       const SizedBox(height: 16),
              //                       TextFormField(
              //                         controller: _.emailController,
              //                         inputFormatters: [
              //                           FilteringTextInputFormatter.allow(
              //                               RegExp(r'[a-zA-Z0-9@._]')),
              //                         ],
              //                         decoration: const InputDecoration(
              //                           labelText: 'Email',
              //                           border: OutlineInputBorder(),
              //                           focusedBorder: OutlineInputBorder(),
              //                           labelStyle:
              //                           TextStyle(color: Colors.grey),
              //                         ),
              //                         validator: (value) {
              //                           if (value == null || value.isEmpty) {
              //                             return 'Please enter an email';
              //                           } else if (!RegExp(
              //                               r'^[^@]+@[^@]+\.[^@]+')
              //                               .hasMatch(value)) {
              //                             return 'Please enter a valid email';
              //                           } else if (!value.contains('@')) {
              //                             return 'Email must contain "@"';
              //                           }
              //                           return null;
              //                         },
              //                       ),
              //                       const SizedBox(height: 16),
              //                       TextFormField(
              //                         inputFormatters: [
              //                           FilteringTextInputFormatter.digitsOnly
              //                         ],
              //                         maxLength: 10,
              //                         controller: _.mobileController,
              //                         decoration: const InputDecoration(
              //                           labelText: 'Phone Number',
              //                           border: OutlineInputBorder(),
              //                           focusedBorder: OutlineInputBorder(),
              //                           labelStyle:
              //                           TextStyle(color: Colors.black54),
              //                         ),
              //                         keyboardType: TextInputType.phone,
              //                         validator: (value) {
              //                           if (value == null || value.isEmpty) {
              //                             return 'Please enter your Phone Number';
              //                           }
              //                           return null;
              //                         },
              //                       ),
              //                       const SizedBox(height: 16),
              //                     ],
              //                   ),
              //                 );
              //               },
              //             ),
              //           ),
              //           actions: [
              //             ElevatedButton(
              //               onPressed: () {
              //                 Get.back();
              //                 _.nameController.text ='';
              //                 _.emailController.text = '';
              //                 _.mobileController.text ='';
              //               },
              //               child: Text(
              //                 'Close',
              //                 style: TextStyle(fontSize: 14),
              //               ),
              //               style: ElevatedButton.styleFrom(
              //                 backgroundColor: Colors.red.shade300,
              //                 shape: RoundedRectangleBorder(
              //                   borderRadius: BorderRadius.circular(10),
              //                 ),
              //               ),
              //             ),
              //             ElevatedButton(
              //               onPressed: () async {
              //                 if (formKey.currentState!.validate()) {
              //                   await _.addPartner();
              //                   if(_.msg == 'Partner Registered Successfully'){
              //                     Navigator.pop(context);
              //                     _.msg ='';
              //                     _.nameController.text = '';
              //                     _.emailController.text = '';
              //                     _.mobileController.text ='';
              //                     _.fetchPartner();
              //                   }
              //                 }
              //               },
              //               child: Text(
              //                 'Add Partner',
              //                 style: TextStyle(fontSize: 14),
              //               ),
              //               style: ElevatedButton.styleFrom(
              //                 backgroundColor: Colors.green.shade300,
              //                 shape: RoundedRectangleBorder(
              //                   borderRadius: BorderRadius.circular(10),
              //                 ),
              //               ),
              //             ),
              //           ],
              //         );
              //       },
              //     );
              //   },
              //   icon: Icon(Icons.add),
              // ),
              IconButton(onPressed: (){
                setState(() {
                  if(_.searchBarBool){
                    _.reloadList();
                  }
                  _.searchBarBool = !_.searchBarBool;
                });
              }, icon: _.searchBarBool?const Icon(Icons.close):const Icon(Icons.search))
            ],),
            body:


            Column(children: [
              _.searchBarBool?Padding(
                padding: const EdgeInsets.only(
                    left: 13, right: 13, top: 8),
                child: Container(
                  height:
                  MediaQuery.of(context).size.height * 0.04,
                  decoration: BoxDecoration(
                      color:
                      Colors.blue.shade100.withOpacity(0.3),
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 0.3)),
                  child: Center(
                    child: Row(
                      children: [
                        Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 14, left: 15, right: 15),
                              child: TextFormField(
                                autofocus: true,
                                decoration: const InputDecoration(
                                  hintText: 'Search',
                                  border: InputBorder.none,
                                ),
                                onChanged: (value) {
                                  _.searchData(value);
                                },
                              ),
                            )),
                        const Icon(
                          Icons.search_outlined,
                          size: 18,
                          color: Colors.blue,
                        ),
                        const SizedBox(
                          width: 10,
                        )
                      ],
                    ),
                  ),
                ),
              ):const SizedBox(),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _.myPartner.isEmpty?
                  const Center(
                      child: Center(
                          child: CircularProgressIndicator(color:Colors.blue)
                      )
                  )
                      :ListView.builder(itemCount: _.myPartner.length,itemBuilder: (context, index){
                    var use = _.myPartner[_.myPartner.length-1-index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 2),
                      child: InkWell(
                        onTap:(){
                          setState((){
                            _.searchBarBool = false;
                          });
                        },
                        child: Stack(
                            children:[
                              Container(
                                // height: 140,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(color:Colors.white,borderRadius: BorderRadius.circular(5),
                                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5), blurRadius: 5, spreadRadius: 0, offset: const Offset(0, 2),)],
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 10),
                                      child: Row(
                                        children: [
                                          // Icon(Icons.person,color: Colors.blue,size: 18,),
                                          ConstrainedBox(
                                            constraints: BoxConstraints(
                                              maxWidth: MediaQuery.of(context).size.width * 0.9,
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.symmetric(vertical: 2.5, horizontal: 5),
                                              child: IntrinsicWidth(
                                                child: Container(
                                                  constraints: BoxConstraints(
                                                    maxWidth: MediaQuery.of(context).size.width * 0.8,
                                                  ),
                                                  child: Text(
                                                    '${use.name} - ${use.empId}',
                                                    style: const TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 15,
                                                    ),
                                                    overflow: TextOverflow.clip,
                                                    softWrap: true,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(height: 0.3,width: MediaQuery.of(context).size.width,color: Colors.blue,),
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(left: 4,top: 5),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              InkWell(
                                                  onTap: () async {
                                                    final url = Uri.parse('mailto:${use.email}');
                                                    if (await canLaunchUrl(url)) {
                                                      await launchUrl(url);
                                                    } else {
                                                      ScaffoldMessenger.of(context).showSnackBar(
                                                        const SnackBar(content: Text('Could not launch email client')),
                                                      );
                                                    }
                                                  },
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(right: 5, top: 2),
                                                    child: ConstrainedBox(
                                                      constraints: BoxConstraints(
                                                          maxWidth: MediaQuery.of(context).size.width * 0.8),
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                          color: Colors.blue.shade50,
                                                          borderRadius: BorderRadius.circular(5),
                                                          border: Border.all(color: Colors.blue.shade100),
                                                        ),
                                                        child: Padding(
                                                          padding: const EdgeInsets.symmetric(vertical: 2.5, horizontal: 5),
                                                          child: IntrinsicWidth(
                                                            child: Row(
                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: [
                                                                Flexible(
                                                                  child: Padding(
                                                                    padding: const EdgeInsets.only(left: 2),
                                                                    child: Text(
                                                                      use.email,
                                                                      textAlign: TextAlign.start,
                                                                      style: const TextStyle(fontSize: 12),
                                                                      overflow: TextOverflow.ellipsis,
                                                                      softWrap: true,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top:8),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          color: Colors.blue.shade50,
                                                          borderRadius: BorderRadius.circular(5),
                                                          border: Border.all(color: Colors.blue.shade100)
                                                      ),
                                                      child: InkWell(
                                                          onTap:() async {
                                                            Uri phoneNo = Uri.parse('tel:+91${use.mobile}');
                                                            if(await launchUrl(phoneNo)){}else{
                                                              ScaffoldMessenger.of(context).showSnackBar(
                                                                const SnackBar(content: Text('Could not launch Dialer pad')),
                                                              );
                                                            }
                                                          },
                                                          child: Padding(
                                                            padding: const EdgeInsets.symmetric(vertical: 2.5,horizontal: 5),
                                                            child: Row(
                                                              children: [
                                                                // Icon(Icons.call,color: Colors.blue,size: 18,),
                                                                Text(' +91 ${use.mobile}',style: const TextStyle(fontSize: 12)),

                                                              ],
                                                            ),
                                                          )),
                                                    ),
                                                    const SizedBox(width: 10),
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          color: Colors.pink.shade50,
                                                          borderRadius: BorderRadius.circular(5),
                                                          border: Border.all(color: Colors.pink.shade100)
                                                      ),
                                                      child: Padding(
                                                        padding: const EdgeInsets.symmetric(vertical: 2,horizontal: 4),
                                                        child: Text(_.myPartner[index].empId,style: const TextStyle(fontSize: 12)),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 8.0),
                                                child: Container(height: 0.5,width: MediaQuery.of(context).size.width*0.93,color:Colors.grey,),
                                              ),
                                              // Container(),
                                             SizedBox(
                                               width: MediaQuery.of(context).size.width*0.93,
                                               child: Row(
                                                 mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                 children: [
                                                  ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(backgroundColor: Colors.cyan.shade50), child: const Text("Business Details"),),
                                                  ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(backgroundColor: Colors.green.shade50), child: const Text("Activity Track")),
                                                  ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(backgroundColor: Colors.deepPurple.shade50), child: const Text("Activity"))
                                               ],),
                                             )

                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),

                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  decoration: BoxDecoration(color: Colors.brown.shade50,borderRadius:
                                  const BorderRadius.only(bottomLeft: Radius.elliptical(2000,2000),topLeft: Radius.circular(0))),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right:35,top:5),
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    height: 10,
                                    width: 10,
                                    decoration: BoxDecoration(color:Colors.blue.shade100,borderRadius: BorderRadius.circular(20)),
                                  ),
                                ),
                              ),

                              Padding(
                                padding: const EdgeInsets.only(right:20,top:24),
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    height: 15,
                                    width: 15,
                                    decoration: BoxDecoration(color:Colors.green.shade100,borderRadius: BorderRadius.circular(20)),
                                  ),
                                ),
                              ),
                            ]),
                      ),
                    );
                  }),
                ),
              )
            ],),
          );
        });
  }
}

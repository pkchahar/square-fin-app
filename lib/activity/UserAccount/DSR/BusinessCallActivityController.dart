import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../../../Model/Pos/PartnerList.dart';
import '../../../utils/AppUtils.dart';

class BusinessCallActivityController extends GetxController{

    String? partnerController;
    bool virtualBool = false;
    bool visitBool = true;
    bool visitWithReportingManagerBool = false;
    bool followUpTypeBool = true;
    var imageController;
    String imgName = 'Capture Image';
    var followUpDate;
    var remarkController;

  List<PartnerList>  myPartner = [];

  @override
  onInit() async {
    await fetchPartner();
    super.onInit();
  }

  Future<void> selectDate(context) async {
    followUpDate = await showDatePicker(context: context, firstDate: DateTime.now(), lastDate: DateTime(2100));
    followUpDate = DateFormat("dd-MM-yyyy").format(followUpDate);
    update();
  }

  Future<void> fetchPartner()async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    print(pref.getString(AppUtils.userId));
    try{
      Map<String, dynamic> mData ={
        'userid' : pref.getString(AppUtils.userId)
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.partnerList);
      var response = await http.post(url,body: mData);
      print(response.statusCode);
      print(url);
      if(response.statusCode ==200){
        Map<String, dynamic> responseJson = jsonDecode(response.body);
        PartnerRes myResponse = PartnerRes.fromJson(responseJson);
        myPartner.addAll(myResponse.data!);
        update();
      }
    }catch(e){
      print(e);
    }
  }

  // Future getImageFromCamera() async {
//     inTime = '';
//     final ImagePicker Picker = ImagePicker();
//     final image = await Picker.pickImage(source: ImageSource.camera);
//     if (image != null) {
//       outTime = '';
//       imgPath.value = image.path;
//       inImgPath = image.path;
//       outImgPath = "";
//       isIN = true;
//       isOut = false;
//       inTimeString = thisDateTime();
//       inTime = inTimeString;
//       outTimeString = '';
//       AttendanceInfo.write('entryTime', inTimeString.toString());
//       //variable to store date of today to compare with next day
//       lastDayDate = thisDateOnly();
//       AttendanceInfo.write('lastDayDate', lastDayDate);
//       AttendanceInfo.write('exitTime', null);
//
//       collRef.add({
//         'In Time': inTimeString.toString(),
//         'Date': thisDateOnly(),
//         'Out Time': '',
//         'createdAt': FieldValue.serverTimestamp(),
//       }).then((docRef) {
//         docRef.update({'id': docRef.id});
//         docRef.update({'outId': outId});
//         inId = docRef.id;
//       });
//     }
//
//     update();
//   }
  Future captureImage() async {
    final ImagePicker picker  = ImagePicker();
    imageController = await picker.pickImage(source: ImageSource.camera);
    imgName = imageController.name;
    update();
  }
}
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import 'CrossSellController.dart';

class CrossSellUIController extends StatefulWidget {
  const CrossSellUIController({super.key});

  @override
  State<CrossSellUIController> createState() => _CrossSellUIControllerState();
}

class _CrossSellUIControllerState extends State<CrossSellUIController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CrossSellController>(
        init: CrossSellController(),
        builder: (_){
          return  Scaffold(
            appBar: AppBar(title: Text('Business Call' ' (${_.myPartner.length})'),actions: [
              IconButton(onPressed: (){
                setState(() {
                  if(_.searchBarBool){
                    _.reloadList();
                  }
                  _.searchBarBool = !_.searchBarBool;
                });
              }, icon: _.searchBarBool?const Icon(Icons.close):const Icon(Icons.search))
            ],),
            body:
            _.myPartner.isEmpty ? const Center(child: CircularProgressIndicator(color: Colors.blue,)):

            Column(children: [
              _.searchBarBool?Padding(
                padding: const EdgeInsets.only(
                    left: 13, right: 13, top: 8),
                child: Container(
                  height:
                  MediaQuery.of(context).size.height * 0.04,
                  decoration: BoxDecoration(
                      color:
                      Colors.blue.shade100.withOpacity(0.3),
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(width: 0.3)),
                  child: Center(
                    child: Row(
                      children: [
                        Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 14, left: 15, right: 15),
                              child: TextFormField(
                                autofocus: true,
                                decoration: const InputDecoration(
                                  hintText: 'Search',
                                  border: InputBorder.none,
                                ),
                                onChanged: (value) {
                                  _.searchData(value);
                                },
                              ),
                            )),
                        const Icon(
                          Icons.search_outlined,
                          size: 18,
                          color: Colors.blue,
                        ),
                        const SizedBox(
                          width: 10,
                        )
                      ],
                    ),
                  ),
                ),
              ):const SizedBox(),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _.myPartner.isEmpty?
                  const Center(
                      child: Center(
                          child: CircularProgressIndicator(color:Colors.red)
                      ))
                      :ListView.builder(itemCount: _.myPartner.length,itemBuilder: (context, index){
                    var use = _.myPartner[_.myPartner.length-1-index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 2),
                      child: InkWell(
                        onTap:(){
                          setState((){
                            _.searchBarBool = false;
                          });
                        },
                        child: Stack(
                            children:[
                              Container(
                                // height: 140,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(color:Colors.white,borderRadius: BorderRadius.circular(5),
                                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5), blurRadius: 5, spreadRadius: 0, offset: const Offset(0, 2),)],
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 10),
                                      child: Row(
                                        children: [
                                          // Icon(Icons.person,color: Colors.blue,size: 18,),
                                          ConstrainedBox(
                                            constraints: BoxConstraints(
                                              maxWidth: MediaQuery.of(context).size.width * 0.9,
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.symmetric(vertical: 2.5, horizontal: 5),
                                              child: IntrinsicWidth(
                                                child: Container(
                                                  constraints: BoxConstraints(
                                                    maxWidth: MediaQuery.of(context).size.width * 0.8,
                                                  ),
                                                  child: Text(
                                                    '${use.name} - ${use.empId}',
                                                    style: const TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 15,
                                                    ),
                                                    overflow: TextOverflow.clip,
                                                    softWrap: true,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(height: 0.3,width: MediaQuery.of(context).size.width,color: Colors.blue,),
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(left: 4,top: 5),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              InkWell(
                                                  onTap: () async {
                                                    final url = Uri.parse('mailto:${use.email}');
                                                    if (await canLaunchUrl(url)) {
                                                      await launchUrl(url);
                                                    } else {
                                                      ScaffoldMessenger.of(context).showSnackBar(
                                                        const SnackBar(content: Text('Could not launch email client')),
                                                      );
                                                    }
                                                  },
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(right: 5, top: 2),
                                                    child: ConstrainedBox(
                                                      constraints: BoxConstraints(
                                                          maxWidth: MediaQuery.of(context).size.width * 0.8),
                                                      child: Container(
                                                        decoration: BoxDecoration(
                                                          color: Colors.blue.shade50,
                                                          borderRadius: BorderRadius.circular(5),
                                                          border: Border.all(color: Colors.blue.shade100),
                                                        ),
                                                        child: Padding(
                                                          padding: const EdgeInsets.symmetric(vertical: 2.5, horizontal: 5),
                                                          child: IntrinsicWidth(
                                                            child: Row(
                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: [
                                                                Flexible(
                                                                  child: Padding(
                                                                    padding: const EdgeInsets.only(left: 2),
                                                                    child: Text(
                                                                      use.email,
                                                                      textAlign: TextAlign.start,
                                                                      style: const TextStyle(fontSize: 12),
                                                                      overflow: TextOverflow.ellipsis,
                                                                      softWrap: true,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top:8),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          color: Colors.blue.shade50,
                                                          borderRadius: BorderRadius.circular(5),
                                                          border: Border.all(color: Colors.blue.shade100)
                                                      ),
                                                      child: InkWell(
                                                          onTap:() async {
                                                            Uri phoneNo = Uri.parse('tel:+91${use.mobile}');
                                                            if(await launchUrl(phoneNo)){}else{
                                                              ScaffoldMessenger.of(context).showSnackBar(
                                                                const SnackBar(content: Text('Could not launch Dialer pad')),
                                                              );
                                                            }
                                                          },
                                                          child: Padding(
                                                            padding: const EdgeInsets.symmetric(vertical: 2.5,horizontal: 5),
                                                            child: Row(
                                                              children: [
                                                                // Icon(Icons.call,color: Colors.blue,size: 18,),
                                                                Text(' +91 ${use.mobile}',style: const TextStyle(fontSize: 12)),

                                                              ],
                                                            ),
                                                          )),
                                                    ),
                                                    const SizedBox(width: 10),
                                                    Container(
                                                      decoration: BoxDecoration(
                                                          color: Colors.pink.shade50,
                                                          borderRadius: BorderRadius.circular(5),
                                                          border: Border.all(color: Colors.pink.shade100)
                                                      ),
                                                      child: Padding(
                                                        padding: const EdgeInsets.symmetric(vertical: 2,horizontal: 4),
                                                        child: Text(_.myPartner[index].empId,style: const TextStyle(fontSize: 12)),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 8.0),
                                                child: Container(height: 0.5,width: MediaQuery.of(context).size.width*0.93,color:Colors.grey,),
                                              ),
                                              // Container(),
                                              SizedBox(
                                                width: MediaQuery.of(context).size.width*0.93,
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                  children: [
                                                    ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(backgroundColor: Colors.cyan.shade50), child: const Text("Pos")),
                                                    ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(backgroundColor: Colors.orange.shade50), child: const Text("Hot")),
                                                    ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(backgroundColor: Colors.green.shade50), child: const Text("Activity Track")),
                                                    ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(backgroundColor: Colors.deepPurple.shade50), child: const Text("Activity"))
                                                  ],),
                                              )

                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),

                              Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  decoration: BoxDecoration(color: Colors.brown.shade50,borderRadius:
                                  const BorderRadius.only(bottomLeft: Radius.elliptical(2000,2000),topLeft: Radius.circular(0))),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right:35,top:5),
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    height: 10,
                                    width: 10,
                                    decoration: BoxDecoration(color:Colors.blue.shade100,borderRadius: BorderRadius.circular(20)),
                                  ),
                                ),
                              ),

                              Padding(
                                padding: const EdgeInsets.only(right:20,top:24),
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    height: 15,
                                    width: 15,
                                    decoration: BoxDecoration(color:Colors.green.shade100,borderRadius: BorderRadius.circular(20)),
                                  ),
                                ),
                              ),
                            ]),
                      ),
                    );
                  }),
                ),
              )
            ],),
          );
        });
  }
}

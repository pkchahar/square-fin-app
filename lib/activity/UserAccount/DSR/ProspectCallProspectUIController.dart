import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pi/activity/UserAccount/DSR/ProspectCallProspectController.dart';

class ProspectCallProspectUIController extends StatefulWidget {
  const ProspectCallProspectUIController({super.key});

  @override
  State<ProspectCallProspectUIController> createState() => _ProspectCallProspectUIControllerState();
}

class _ProspectCallProspectUIControllerState extends State<ProspectCallProspectUIController> {

  GlobalKey formKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProspectCallProspectController>(
        init: ProspectCallProspectController(),
        builder: (_){
          return Scaffold(
            appBar: AppBar(title: const Text('Add Prospect Call')),backgroundColor: Colors.grey.shade50,
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                      Form(
                          key: formKey,
                          child: Column(
                        children: [
                          Card(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.only(left:3,top: 8,bottom: 0),
                                    child: Text('Name',style: TextStyle(color: Colors.black54,fontSize: 16),),
                                  ),
                                  const SizedBox(height: 10),
                                  TextFormField(
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(RegExp(r"[a-zA-Z\s'-]"))
                                    ],
                                    controller: _.nameController,
                                    decoration: const InputDecoration(
                                       focusedBorder: OutlineInputBorder(),
                                      border: OutlineInputBorder()
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Card(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.only(left:3,top: 8,bottom: 0),
                                    child: Text('Phone Number',style: TextStyle(color: Colors.black54,fontSize: 16),),
                                  ),
                                  const SizedBox(height: 10),
                                  TextFormField(
                                    inputFormatters: [
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    keyboardType: TextInputType.number,
                                    maxLength: 10,
                                    controller: _.phoneNumberController,
                                    decoration: const InputDecoration(
                                        focusedBorder: OutlineInputBorder(),
                                        border: OutlineInputBorder()
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
              
                          Card(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.only(left:3,top: 8,bottom: 0),
                                    child: Text('Email',style: TextStyle(color: Colors.black54,fontSize: 16),),
                                  ),
                                  const SizedBox(height: 10),
                                  TextFormField(
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(RegExp(r"[a-zA-Z0-9._%+-@]")),
                                    ],
                                    controller: _.emailController,
                                    decoration: const InputDecoration(
                                        focusedBorder: OutlineInputBorder(),
                                        border: OutlineInputBorder()
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
              
                          Card(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.only(left:3,top: 8,bottom: 0),
                                    child: Text('Occupation Type',style: TextStyle(color: Colors.black54,fontSize: 16),),
                                  ),
                                  const SizedBox(height: 10),
                                  TextFormField(
                                    controller: _.emailController,
                                    decoration: const InputDecoration(
                                        focusedBorder: OutlineInputBorder(),
                                        border: OutlineInputBorder()
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),

                          Card(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Padding(
                                  padding: EdgeInsets.only(left:14,top: 8,bottom: 8),
                                  child: Text('Prospect Type',style: TextStyle(color: Colors.black54,fontSize: 16),),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 20),
                                  child: Row(
                                    children: [
                                      InkWell(
                                        onTap:(){
                                          setState(() {
                                            _.prospectTypeController = 'POS';
                                          });
                                        },
                                        child: Container(
                                            height: 20,
                                            width: 20,
                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),border: Border.all(color: Colors.blue,width: 2)),
                                            child: _.prospectTypeController == "POS"?Padding(
                                              padding: const EdgeInsets.all(2),
                                              child: Container(
                                                decoration: BoxDecoration( color:Colors.blue,borderRadius: BorderRadius.circular(50)),
                                                height: 5,
                                                width: 5,
                                              ),
                                            ): const SizedBox()
                                        ),
                                      ),
                                      const SizedBox(width: 5),
                                      const Text("POS"),
                                      const SizedBox(width: 15),
                                      InkWell(
                                        onTap:(){
                                          setState(() {
                                            _.prospectTypeController = 'Customer';
                                          });
                                        },
                                        child: Container(
                                            height: 20,
                                            width: 20,
                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),border: Border.all(color: Colors.blue,width: 2)),
                                            child: Padding(
                                              padding: const EdgeInsets.all(2),
                                              child:  _.prospectTypeController == "Customer"?Container(
                                                decoration: BoxDecoration( color:Colors.blue,borderRadius: BorderRadius.circular(50)),
                                                height: 5,
                                                width: 5,
                                              ):const SizedBox(),
                                            )
                                        ),
                                      ),
                                      const SizedBox(width: 5),
                                      const Text('Customer')
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),

                          Card(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Padding(
                                  padding: EdgeInsets.only(left:14,top: 8,bottom: 8),
                                  child: Text('Call Type',style: TextStyle(color: Colors.black54,fontSize: 16),),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 20),
                                  child: Row(
                                    children: [
                                      InkWell(
                                        onTap:(){
                                          setState(() {
                                            _.callType = 'Hot';
                                          });
                                        },
                                        child: Container(
                                            height: 20,
                                            width: 20,
                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),border: Border.all(color: Colors.blue,width: 2)),
                                            child: _.callType == "Hot"?Padding(
                                              padding: const EdgeInsets.all(2),
                                              child: Container(
                                                decoration: BoxDecoration( color:Colors.blue,borderRadius: BorderRadius.circular(50)),
                                                height: 5,
                                                width: 5,
                                              ),
                                            ): const SizedBox()
                                        ),
                                      ),
                                      const SizedBox(width: 5),
                                      const Text("Hot"),
                                      const SizedBox(width: 15),
                                      InkWell(
                                        onTap:(){
                                          setState(() {
                                            _.callType = 'Cold';
                                          });
                                        },
                                        child: Container(
                                            height: 20,
                                            width: 20,
                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(50),border: Border.all(color: Colors.blue,width: 2)),
                                            child: Padding(
                                              padding: const EdgeInsets.all(2),
                                              child:  _.callType == "Cold"?Container(
                                                decoration: BoxDecoration( color:Colors.blue,borderRadius: BorderRadius.circular(50)),
                                                height: 5,
                                                width: 5,
                                              ):const SizedBox(),
                                            )
                                        ),
                                      ),
                                      const SizedBox(width: 5),
                                      const Text('Cold'),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              SizedBox(
                                  width: MediaQuery.of(context).size.width*0.4,
                                  child: ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.purple.shade100
                                  ), child: const Text("CLOSE"),)),
                              SizedBox(
                                  width: MediaQuery.of(context).size.width*0.4,
                                  child: ElevatedButton(onPressed: (){},style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.blue.shade100), child: const Text("CREATE"))),
                            ],)
                        ],
                      ))
                  ],
                ),
              ),
            )
          );
        });
  }
}

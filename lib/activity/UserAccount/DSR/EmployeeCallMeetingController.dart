import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:intl/intl.dart';

class EmployeeCallMeetingController extends GetxController{


  var MeetingDate;
  var startTime;
  var endTime;
  TextEditingController meetingVenue = TextEditingController();
  TextEditingController remarkController = TextEditingController();

  Future<void> selectDate(context) async {
    MeetingDate = await showDatePicker(context: context, firstDate: DateTime.now(), lastDate: DateTime(2100));
    MeetingDate = DateFormat("dd-MM-yyyy").format(MeetingDate);
    update();
  }

   selectTime(context) async {
    TimeOfDay? time;
     time = await showTimePicker(context: context, initialTime: TimeOfDay.now());
     return time;
    update();
  }
}
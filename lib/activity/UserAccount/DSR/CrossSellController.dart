import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import '../../../Model/Pos/AddPartnerModel.dart';
import '../../../Model/Pos/PartnerList.dart';
import '../../../utils/AppUtils.dart';

class CrossSellController extends GetxController{

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  var status;
  var msg;
  bool searchBarBool = false;
  List<PartnerList>  myPartner = [];
  List<PartnerList> searchList =[];

  @override
  onInit() async {
    await fetchPartner();
    super.onInit();
  }
  Future<void> fetchPartner()async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    print(pref.getString(AppUtils.userId));
    try{
      Map<String, dynamic> mData ={
        'userid' : pref.getString(AppUtils.userId)
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.partnerList);
      var response = await http.post(url,body: mData);
      print(response.statusCode);
      print(url);
      if(response.statusCode ==200){
        Map<String, dynamic> responseJson = jsonDecode(response.body);
        PartnerRes myResponse = PartnerRes.fromJson(responseJson);
        if(myPartner.isNotEmpty){
          myPartner =[];
          searchList = [];
        }
        myPartner.addAll(myResponse.data!);
        searchList.addAll(myResponse.data!);
        update();
        print('this is res');
        print(myPartner[0].name);
      }
    }catch(e){
      print(e);
    }
  }
  void launchEmail(String email) async {
    final Uri url = Uri(
      scheme: 'mailto',
      path: email,
    );

    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  void reloadList() {

    searchList.addAll(myPartner);
    update();
  }
  void searchData(String ask) {
    myPartner = [];
    if (ask.isNotEmpty) {
      myPartner = searchList.where((item) {
        return ((item.empId.toLowerCase().contains(ask.toLowerCase()) ||
            item.name.toLowerCase().contains(ask.toLowerCase()) ||
            item.email.toLowerCase().contains(ask.toLowerCase()) ||
            item.mobile.toLowerCase().contains(ask.toLowerCase()) ||
            item.id.toLowerCase().contains(ask.toLowerCase()) ||
            item.date.toLowerCase().contains(ask.toLowerCase())));
      }).toList();
    } else {
      myPartner.addAll(searchList);
    }
    update();
  }


  Future<void> addPartner() async {
    if(nameController.text.isEmpty|| nameController.text.length<2){
      AppUtils.showSnackBar('Error', 'Please Insert Name', false);
      return;
    }
    if(emailController.text.isEmpty){
      AppUtils.showSnackBar('Error', 'Please Insert Email', false);
      return;
    }
    if(mobileController.text.isEmpty || mobileController.text.length<10){
      AppUtils.showSnackBar('Error', 'Please Insert Valid Phone Number', false);
      return;
    }
    try {
      SharedPreferences pref = await SharedPreferences.getInstance();
      Map<String, dynamic> mData = {
        'userid': pref.getString(AppUtils.userId).toString(),
        'name': nameController.text,
        'email': emailController.text,
        'mobile': mobileController.text
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.addPartner1);
      var response = await http.post(url, body: mData);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = jsonDecode(response.body);
        AddPartnerModel myResponse = AddPartnerModel.fromJson(responseJson);
        status = myResponse.status;
        msg = myResponse.msg;
        update();
        if (msg.isEmpty) {
          AppUtils.showSnackBar('Error', 'Fill Required Details', false);
        } else {
          AppUtils.showSnackBar('Add Partner', msg, true);
        }
      } else {
        print('Failed to add partner: ${response.statusCode}');
        AppUtils.showSnackBar('Error', 'Failed to add partner', false);
      }
    } catch (e) {
      print('Exception occurred: $e');
      AppUtils.showSnackBar('Error', 'An error occurred', false);
    }
  }
}
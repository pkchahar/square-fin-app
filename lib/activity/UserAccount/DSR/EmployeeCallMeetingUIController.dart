import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'EmployeeCallMeetingController.dart';

class EmployeeCallMeetingUIController extends StatefulWidget {
  const EmployeeCallMeetingUIController({super.key});

  @override
  State<EmployeeCallMeetingUIController> createState() => _EmployeeCallMeetingUIControllerState();
}

class _EmployeeCallMeetingUIControllerState extends State<EmployeeCallMeetingUIController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<EmployeeCallMeetingController>(
        init: EmployeeCallMeetingController(),
        builder: (_){
          return Scaffold(
            appBar: AppBar(title: const Text("Employee Activity - Meeting"),),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(children: [
                  Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 0),
                          child: Text('Employee',style: TextStyle(color: Colors.black54,fontSize: 16),),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                          child: DropdownButtonFormField<String>(
                            onChanged: ( newValue) {
                            },
                            decoration: const InputDecoration(
                                hintText: "Select"
                            ), items: const [
                              DropdownMenuItem(child: Text('item'))
                          ],
                          ),
                        ),
                      ],
                    ),
                  ),

                  Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 0),
                          child: Text('Service Location',style: TextStyle(color: Colors.black54,fontSize: 16),),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                          child: DropdownButtonFormField<String>(
                            onChanged: ( newValue) {
                            },
                            decoration: const InputDecoration(
                                hintText: "Select"
                            ), items: const [
                            DropdownMenuItem(child: Text('item'))
                          ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap:(){
                      _.selectDate(context);
                    },
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(_.MeetingDate!=null?_.MeetingDate.toString():"Follow Up Date(If Any)",style: const TextStyle(color: Colors.black54,fontSize: 16,overflow: TextOverflow.ellipsis),),
                            const Icon(Icons.calendar_month_sharp,color: Colors.indigo,)
                          ],
                        ),
                      ),
                    ),
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    InkWell(
                          onTap: () async {
                            final selectedTime = await showTimePicker(
                              context: context,
                              initialTime: TimeOfDay.now(),
                            );

                            if (selectedTime != null) {
                              setState(() {
                                _.startTime = selectedTime;
                              });
                            }
                          },
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.45,
                            height: 75,
                            child: Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    const Icon(Icons.access_time_filled_outlined,color: Colors.blue,),
                                    const SizedBox(width: 10),
                                    Text(
                                      _.startTime == null
                                          ? 'Start Time'
                                          : _.startTime!.format(context),
                                      style: const TextStyle(
                                        color: Colors.black54,
                                        fontSize: 16,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      InkWell(
                        onTap: () async {
                          final selectedTime = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.now(),
                          );

                          if (selectedTime != null) {
                            setState(() {
                              _.endTime = selectedTime;
                            });
                          }
                        },
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.45,
                          height: 75,
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Icon(Icons.access_time_filled_outlined,color: Colors.purple.shade200,),
                                  const SizedBox(width: 10),
                                  Text(
                                    _.endTime == null
                                        ? 'End Time'
                                        : _.endTime!.format(context),
                                    style: const TextStyle(
                                      color: Colors.black54,
                                      fontSize: 16,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 0),
                          child:   Text('Meeting Venue',style: TextStyle(color: Colors.black54,fontSize: 16),),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                          child: TextFormField(
                            controller: _.meetingVenue,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(),
                            ),
                            maxLines: null,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(left: 8.0,top: 8,bottom: 0),
                          child:   Text('Remark',style: TextStyle(color: Colors.black54,fontSize: 16),),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                          child: TextFormField(
                            controller: _.remarkController,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(),
                            ),
                            maxLines: null,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height:20),
                  SizedBox(
                    width: MediaQuery.of(context).size.width*0.6,
                    child: ElevatedButton(onPressed: (){

                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue.shade50,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12))
                    ), child: const Text("SUBMIT"),
                    ),
                  )
                ],),
              ),
            ),
          );
        });
  }
}

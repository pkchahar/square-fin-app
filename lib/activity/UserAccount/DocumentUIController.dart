import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pi/utils/ViewImage.dart';

import '../../utils/AppUtils.dart';
import 'DocumentController.dart';

class DocumentUIController extends StatelessWidget {
  const DocumentUIController({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DocumentController>(
      init: DocumentController(),
      builder: (_) {
        return Scaffold(
          backgroundColor: AppUtils.backgroundColor,
          appBar: AppBar(
            title: const Text("Documents"),
          ),
          body: _.isLoading
              ? const Center(child: CircularProgressIndicator())
              : _.productsData.fileName == null ||
                      _.productsData.fileName!.isEmpty
                  ? const Center(child: Text("Oops No data found"))
                  : ListView.builder(
                      itemCount: _.productsData.fileName!.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: const Color(0xffF5F5F5)),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 15, bottom: 15),
                              child: ListTile(
                                leading: InkWell(
                                  onTap: () {
                                    String imgPath = _.productsData
                                        .fileName![index].documentFileName!;
                                    Get.to(() => ViewImage(imgPath: imgPath));
                                  },
                                  child: const CircleAvatar(
                                    backgroundColor: Color(0xffefd7de),
                                    child: Icon(
                                      Icons.image_outlined,
                                      color: Colors.brown,
                                    ),
                                  ),
                                ),
                                title: Text(
                                  _.productsData.fileName![index].documentType!,
                                  style: const TextStyle(
                                      color: Color(0xff1FC4F4),
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
        );
      },
    );
  }
}

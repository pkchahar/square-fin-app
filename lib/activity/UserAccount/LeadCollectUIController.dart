import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../utils/AppUtils.dart';
import 'LeadCollectController.dart';

class LeadCollectUIController extends StatefulWidget {
  String type, lead;

  LeadCollectUIController({super.key, required this.type, required this.lead});

  @override
  _LeadCollectUIController createState() => _LeadCollectUIController();
}

class _LeadCollectUIController extends State<LeadCollectUIController> {
  late String type, lead;

  @override
  void initState() {
    super.initState();
    lead = widget.lead;
    type = widget.type;
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LeadCollectController>(
      init: LeadCollectController(context: context),
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Text(lead),
          ),
          backgroundColor: AppUtils.backgroundColor,
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Form(
                  key: _.formKey,
                  child: Column(
                    children: [
                      Card(
                        elevation: 5,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Text('Post A Query',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue.shade500)),
                              Divider(
                                color: Colors.grey.shade400,
                                thickness: 1.0,
                              ),
                              const SizedBox(height: 16.0),
                              TextFormField(
                                controller: _.nameController,
                                decoration: const InputDecoration(
                                  labelText: 'Name',
                                  border: OutlineInputBorder(),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  labelStyle: TextStyle(color: Colors.black54),
                                ),
                                keyboardType: TextInputType.name,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter your name';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              TextFormField(
                                controller: _.emailController,
                                decoration: const InputDecoration(
                                  labelText: 'Email',
                                  border: OutlineInputBorder(),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  labelStyle: TextStyle(color: Colors.black54),
                                ),
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter your email address';
                                  } else if (!RegExp(r'\S+@\S+\.\S+')
                                      .hasMatch(value)) {
                                    return 'Please enter a valid email address';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              TextFormField(
                                controller: _.phoneController,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(10),
                                ],
                                decoration: const InputDecoration(
                                  labelText: 'Phone Number',
                                  border: OutlineInputBorder(),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  labelStyle: TextStyle(color: Colors.black54),
                                ),
                                keyboardType: TextInputType.phone,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter your Phone Number';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              TextFormField(
                                controller: _.messageController,
                                decoration: const InputDecoration(
                                  labelText: 'Message',
                                  border: OutlineInputBorder(),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  labelStyle: TextStyle(color: Colors.black54),
                                ),
                                maxLines: 4,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter a message';
                                  }
                                  return null;
                                },
                              ),
                              const SizedBox(height: 16.0),
                              ElevatedButton(
                                onPressed: () {
                                  if (_.formKey.currentState!.validate()) {
                                    String name = _.nameController.text ?? "";
                                    String email = _.emailController.text ?? "";
                                    String phone = _.phoneController.text ?? "";
                                    String msg = _.messageController.text ?? "";
                                    _.submitQuery(
                                        name, phone, email, msg, type, lead);
                                  }
                                },
                                style: ButtonStyle(
                                  backgroundColor: WidgetStateProperty.all<Color>(
                                      Colors.lightBlue),
                                  // Set the background color to blue
                                  minimumSize: WidgetStateProperty.all<Size>(
                                      const Size(double.infinity, 50)),
                                  shape: WidgetStateProperty.all<
                                      RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          10.0), // Set the border radius to 10.0 for rounded corners
                                    ),
                                  ), // Set the button width to full and height to 50
                                ),
                                child: const Text('Submit',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20)),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        );
      },
    );
  }
}

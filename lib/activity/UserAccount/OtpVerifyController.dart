import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/Model/Occupation.dart';
import '../../../utils/AppUtils.dart';
import '../../network/ApiClient.dart';
import '../../utils/BottomBarScreen.dart';
import 'NewUserViewController.dart';

class OtpVerifyController extends GetxController {
  BuildContext mContext;

  OtpVerifyController({required this.mContext});

  bool _isLoading = false;

  bool get isLoading => _isLoading;
  Occupation productsData = Occupation();
  String ipString = "";
  var userMobile = "", password = "";
  late String error;
  String version = "";
  String fcmToken = "";
  bool passwordVisible = false;
  AppClient appClient = AppClient();
  String title = "OTP Verification";
  int secondsRemaining = 0;
  late Timer _timer;

  @override
  void onInit() {
    super.onInit();
    // FirebaseMessaging _firebaseMessaging =
    //     FirebaseMessaging.instance; // Change here
    // _firebaseMessaging.getToken().then((token) {
    //   fcmToken = token ?? "";
    //   print("FCM token is $token");
    // });
    getIpAddress();
    getSharedData();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userMobile = preferences.getString(AppUtils.userMobile) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    //startResendTimer();
    update();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }


  Future<String> getIpAddress() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    version = preferences.getString(AppUtils.appVersion) ?? "";
    for (var interface in await NetworkInterface.list()) {
      for (var address in interface.addresses) {
        if (!address.isLinkLocal) {
          ipString = address.address;
          preferences.setString(AppUtils.ipAddress, ipString);
          return address.address;
        }
      }
    }
    return 'No IP address found';
  }

  Future<void> resendOtp() async {
    setLoading(true);
    Map parsed = await appClient.getLoginOtp(userMobile, '');
    dynamic status = parsed["status"];
    String msg = parsed["message"];
    if (status != null && status == 1) {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      preferences.setString(AppUtils.userMobile, userMobile);
    } else {
      Fluttertoast.showToast(msg: msg);
    }
    setLoading(false);
  }

  Future<void> verifyOtp(String otp) async {
    setLoading(true);
    Map parsed = await appClient.verifyOtp(userMobile, otp);
    dynamic status = parsed["status"];
    dynamic msg = parsed["message"];

    dynamic data = parsed["Data"];

    if (data != null) {
      String name = parsed["Data"]["Name"] ?? "";
      dynamic userId = parsed["Data"]["Id"] ?? "";
      dynamic userEmail = parsed["Data"]["Email"] ?? "";
      dynamic userCode = parsed["Data"]["User_Id"] ?? "";
      dynamic userType = parsed["Data"]["User_Type"] ?? "";
      dynamic mobile = parsed["Data"]["Mobile"] ?? "";
      dynamic token = parsed["Data"]["jwtToken"] ?? "";
      dynamic photo = parsed["Data"]["Photo"] ?? "";

      if (status != null && status == true) {
        Fluttertoast.showToast(msg: '$msg');
        SharedPreferences prefs = await SharedPreferences.getInstance();
        if (name.isEmpty) {
          Get.to(NewUserViewController(phone: mobile));
          prefs.setString(AppUtils.userId, userId);
          prefs.setString(AppUtils.userCode, userCode);
          prefs.setString(AppUtils.token, token);
          prefs.setString(AppUtils.userType, userType);
          prefs.setString(AppUtils.userProfile, photo);
        } else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString(AppUtils.userName, name);
          prefs.setString(AppUtils.userId, userId);
          prefs.setString(AppUtils.userCode, userCode);
          prefs.setString(AppUtils.userType, userType);
          prefs.setString(AppUtils.userMail, userEmail);
          prefs.setString(AppUtils.userMobile, mobile);
          prefs.setString(AppUtils.token, token);
          prefs.setString(AppUtils.userProfile, photo);
          openDashboard();
        }
      }
    } else {
      AppUtils.showSnackBar("OTP Verification", msg, false);
    }
    setLoading(false);
  }

  openDashboard() async {
    BuildContext currentContext = mContext;
    Navigator.pushAndRemoveUntil(
      currentContext,
      MaterialPageRoute(
        builder: (context) => const BottomBarScreen(),
      ),
      (route) => false,
    );
  }

  void startResendTimer() {
    const duration = Duration(seconds: 45);
    secondsRemaining = 45;
    _timer = Timer.periodic(const Duration(seconds: 1), (Timer timer) {
      if (secondsRemaining > 0) {
        secondsRemaining--;
      } else {
        timer.cancel();
      }
      update();
    });
  }

  @override
  void dispose() {
    _timer.cancel(); // Cancel the timer to avoid memory leaks
    super.dispose();
  }
}

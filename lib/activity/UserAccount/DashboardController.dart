//

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:in_app_update/in_app_update.dart';
import 'package:pi/Model/Product/ProductResponse.dart';
import 'package:pi/Model/UtilityModel/PosterRes.dart';
import 'package:pi/Model/Wallet/Wallet.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/Model/UtilityModel/ContactUs.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../utils/AppUtils.dart';
import '../../Model/BasicResponse.dart';
import '../../Model/DashData.dart';
import '../../Model/Product/ListingProduct.dart';
import '../../Model/UtilityModel/BannerResponse.dart';
import '../../utils/NotificationServices.dart';
import '../../network/ApiClient.dart';

class DashboardController extends GetxController {
  bool _isLoading = false;
  late BuildContext context;
  final localData = GetStorage();
  // --------------------------------------
  int gridCount =2;
  bool toggle=false;
  // --------------------------------------


  bool get isLoading => _isLoading;
  String userId = "", userType = "", token = "", name = "", version = "";
  String loginFiles = "0",
      totalLead = "0",
      kycStatus = "0",
      kycRemark ='',
      walletBalance = "0",
      totalEarning = "0",
      withdrawableBal = "0",
      revenue = "0",
      sanctioned = "0",
      disbursed = "0",
      productId = "",
      subProductId = "",
      business = "0",
      pending = "0",
      rejected = "0",
      approved = "0";

      String
      cardName ='',
      cardSubtitle='',
      cardNumber='',
      cardEmailId='';

  int selectedIdx = 0;
  String deviceToken = '';
  String fcmToken = "";
  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final messageController = TextEditingController();
  List<ProductData> productList = [];
  List<BannerItem> bannerList = [];
  List<ProductData> subProductList = [];
  List<ProductData> filterList = [];
  List<ListingProduct> selectedList = [];

  List<PosterData>  posterList = [];

  DashboardController({required this.context});

  final bool _flexibleUpdateAvailable = false;

  @override
  void onInit() {
    super.onInit();
    localData.write('cardMail', emailController.text)??'';
    NotificationServices notificationServices = NotificationServices();
    notificationServices.requestNotificationPermission();
    notificationServices.firebaseInit(context);
    notificationServices.setupInteractMessage(context);
    notificationServices.getDeviceToken().then((value) {
      deviceToken = value;
      storeFcmToken();
    });
    // FirebaseMessaging _firebaseMessaging =
    //     FirebaseMessaging.instance; // Change here
    // _firebaseMessaging.getToken().then((token) {
    //   fcmToken = token ?? "";
    //   storeFcmToken();
    // });

    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  void getDashboard() async {
    try {
      // localData.write('data', "888999");
      Map<String, String> mData = {
        'Authorization': 'Bearer $token',
      };
      var url = Uri.parse(
          '${AppUtils.baseUrl}${AppUtils.dashboard}?Login_User_Id=$userId&Login_User_Type=$userType');
      print(url);
      print('url is here');
      var response = await http.get(url, headers: mData);

      // if (kDebugMode) {
      print(url);
      print(response.body);
      // }
      if (response.statusCode == 200) {
        DashData dash = DashData.fromJson(jsonDecode(response.body));
        var lF = dash.loginNo;
        var lR = dash.totalRevenue;
        var lS = dash.senctionAmount;
        var lD = dash.disbursedAmount;
        var tl = dash.totalRevenue;
        var ks = dash.kycStatus;
        var kr = dash.kycRemark;

        var pn = dash.pending;
        var rj = dash.rejected;
        var ap = dash.approved;

        loginFiles = "$lF";
        totalLead = "$tl";
        revenue = "$lR";
        sanctioned = "$lS";
        disbursed = "$lD";
        kycStatus = "$ks";
        kycRemark = "$kr";

        if (pn != null) {
          pending = "$pn";
          rejected = "$rj";
          approved = "$ap";
        }

        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.setString(AppUtils.kycStatus, kycStatus);
        preferences.setString(AppUtils.kycRemark, kycRemark);
        print(kycStatus);
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void getCategory() async {
    try {
      var url = Uri.parse(AppUtils.baseUrlNew + AppUtils.getCategory);
      var response = await http.get(url);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        ProductResponse myResponse = ProductResponse.fromJson(responseJson);
        productList.addAll(myResponse.data);

        if (productList.isNotEmpty) {
          localData.remove('productList');
          productList = productList.toSet().toList();
        }
        localData.writeIfNull('productList', productList);
        if (productList.isNotEmpty) {
          for (var index = 0; index < productList.length; index++) {
            productId = productList[index].id;
            SharedPreferences
            preferences =
            await SharedPreferences
                .getInstance();

            preferences.setString(
                AppUtils.productId, productList[0].id);

            getSub();
          }
        }
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void getSub() async {
    try {
      var url = Uri.parse(
          "${AppUtils.baseUrlNew}${AppUtils.getSubCategory}$productId/$userId");
      var response = await http.get(url);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        ProductResponse myResponse = ProductResponse.fromJson(responseJson);

        subProductList.addAll(myResponse.data);
        if (subProductList.isNotEmpty) {
          localData.remove('subProductList');
          print(subProductList.length);
          subProductList = subProductList.toSet().toList();
          print(subProductList.length);
        }
        localData.writeIfNull('subProductList', subProductList);
        update();
      }
    } catch (e) {
      print(e);
    }
  }

  void getWalletBala() async {
    try {
      Map<String, String> mData = {
        'Login_User_Id': userId,
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.wallet);
      var response = await http.post(url, body: mData);

      if (kDebugMode) {
        print(url);
        print(mData);
        print(response.body);
      }

      if (response.statusCode == 200) {
        Map<String, dynamic> responseJson = json.decode(response.body);
        Wallet myResponse = Wallet.fromJson(responseJson);
        var status = myResponse.status ?? false;
        if (status) {
          totalEarning = myResponse.data?.totalEarning as String;
          withdrawableBal = myResponse.data?.withdrawableBalance as String;
          walletBalance = myResponse.data?.walletBalance as String;
          SharedPreferences preferences = await SharedPreferences.getInstance();
          preferences.setString('walletBalance', walletBalance);
          preferences.setString('totalEarning', totalEarning);
          preferences.setString('withdrawableBal', withdrawableBal);

          update();
        }
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    name = preferences.getString(AppUtils.userName) ?? "";
    name = AppUtils.capitalizeFirstWord(name.trim());
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
    cardNumber = preferences.getString(AppUtils.userMobile) ?? "";
    cardEmailId = preferences.getString(AppUtils.userMail) ?? "";
    cardName = preferences.getString(AppUtils.userName) ?? "";
    cardSubtitle = preferences.getString(AppUtils.userType)??'';
    try{productList = (localData.read('productList'));
    subProductList = (localData.read('subProductList'));}catch (e){print(e);}

    if (token.isNotEmpty) {
      getDashboard();
      getCategory();
    }
    getBanners();
    getWalletBala();
    update();
    _checkForUpdate();
    fetchPoster();
  }

  Future<void> _checkForUpdate() async {
    try {
      final updateInfo = await InAppUpdate.checkForUpdate();
      print(updateInfo.updateAvailability);
      if (updateInfo.updateAvailability == UpdateAvailability.updateAvailable) {
        await InAppUpdate.performImmediateUpdate();
      } else {}
    } catch (e) {
      print('Error when checking for update: $e');
    }
  }

  void submitQuery(name, subject, email, msg) async {
    setLoading(true);
    try {
      Map<String, String> hData = {
        'Authorization': 'Bearer $token',
      };
      Map<String, dynamic> mData = {
        'name': name,
        'subject': subject,
        'email': email,
        'message': msg,
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.contactUs);
      var response = await http.post(url, headers: hData, body: mData);
      if (kDebugMode) {
        print(url);
        print(response.body);
      }
      if (response.statusCode == 200) {
        ContactUs dash = ContactUs.fromJson(jsonDecode(response.body));
        String message = dash.message ?? "";
        if (dash.status != null && dash.status! == 1) {
          nameController.text = "";
          emailController.text = "";
          phoneController.text = "";
          messageController.text = "";
        }
        showMyDialog("Post a query", message);
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
    update();
  }

  void storeFcmToken() async {
    try {
      String pl = AppUtils.getPlatform();
      Map<String, dynamic> mData = {
        'fcm_token': deviceToken,
        'user_id': userId,
        'device_type': pl,
      };

      var url = Uri.parse(AppUtils.baseUrl + AppUtils.storeFcm);
      var response = await http.post(url, body: mData);
      if (kDebugMode) {
        print(url);
        print(response.body);
      }
      if (response.statusCode == 200) {
        BasicResponse mRes = BasicResponse.fromJson(jsonDecode(response.body));
        if (kDebugMode) {
          print(mRes.status);
          print(mRes.message);
          print(mData);
        }
      }
    } catch (e) {
      print(e);
    }
  }

  showMyDialog(String title, String msg) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(
          title,
          style: const TextStyle(color: Colors.lightBlue),
        ),
        content: Text(
          msg,
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('OK', style: TextStyle(color: Colors.black38)),
          ),
        ],
      ),
    );
  }

  launchUrls(String url, String type) async {
    final Uri url0 = Uri.parse(url);
    if (!await launchUrl(url0)) {
      throw Exception('Could not launch $url0');
    }
  }

  void getBanners() async {
    try {
      setLoading(true);
      Map<String, String> mData = {};
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.myBanner);
      var response = await http.post(url, body: mData);
      print(url);
      print(response.body);
      if (response.statusCode == 200) {
        if (bannerList.isNotEmpty) {
          bannerList.clear();
        }

        Map<String, dynamic> responseJson = json.decode(response.body);
        BannerResponse myResponse = BannerResponse.fromJson(responseJson);
        bannerList.addAll(myResponse.data);
      }
      setLoading(false);
      update();
    } catch (e) {
      print(e);
    }
  }

  void updateData() {
    update();
  }

  Color getBackgroundColor(String status) {
    Color backgroundColor = Colors.green;

    switch (status) {
      case "0":
        backgroundColor = Colors.orange.shade300;
        break;
      case "1":
        backgroundColor = Colors.green;
        break;
      case "2":
        backgroundColor = Colors.red;
        break;
      default:
        backgroundColor = Colors.orange;
        break;
    }
    return backgroundColor;
  }

  Future<void> fetchPoster()async{
    try{
      var url = Uri.parse(AppUtils.baseUrlNew + AppUtils.posterList);
      var response = await http.get(url);
      print(url);
      if(response.statusCode ==200){
        Map<String, dynamic> responseJson = jsonDecode(response.body);
        PosterRes myResponse = PosterRes.fromJson(responseJson);
        if(myResponse.data.isNotEmpty) {
          posterList = [];
          posterList.addAll(myResponse.data);
          update();
        }
        print(response.body);
      }
    }catch(e){
      print(e);
    }
  }
}
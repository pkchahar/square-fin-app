import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/network/ApiClient.dart';
import '../../../utils/AppUtils.dart';
import '../../Model/UtilityModel/SubmitQuery.dart';

class LeadCollectController extends GetxController {
  BuildContext context;

  LeadCollectController({required this.context});

  bool _isLoading = false;

  bool get isLoading => _isLoading;
  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final messageController = TextEditingController();
  String version = '', userId = '', userType = '', token = '';
  late String branch, accountNo, ifscCode, name, mobile, email, msg;

  @override
  void onInit() {
    super.onInit();
    getSharedData();
  }

  void setLoading(bool value) {
    _isLoading = value;
    update();
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";
  }

  void submitQuery(name, phone, email, msg, type, subject) async {
    setLoading(true);
    update();
    try {
      Map<String, String> hData = {
        'Authorization': 'Bearer $token',
      };
      Map<String, dynamic> mData = {
        'name': name,
        'mobile': phone,
        'subject': subject,
        'type': type,
        'email': email,
        'message': msg,
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.contactUs);
      var response = await http.post(url, headers: hData, body: mData);
      if (kDebugMode) {
        print(hData);
        print(mData);
        print(url);
        print(response.body);
      }
      if (response.statusCode == 200) {
        SubmitQuery dash = SubmitQuery.fromJson(jsonDecode(response.body));
        String message = dash.message ?? "";
        if (dash.status != null && dash.status! == 1) {
          nameController.text = "";
          emailController.text = "";
          phoneController.text = "";
          messageController.text = "";
        }
        showMyDialog("Post a query", message);
      }
    } catch (e) {
      print(e);
    }
    setLoading(false);
    update();
  }

  showMyDialog(String title, String msg) async {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(
          title,
          style: const TextStyle(color: Colors.lightBlue),
        ),
        content: Text(
          msg,
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('OK', style: TextStyle(color: Colors.black38)),
          ),
        ],
      ),
    );
  }
}

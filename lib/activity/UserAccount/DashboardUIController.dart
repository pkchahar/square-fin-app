import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:http/http.dart' as http;
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:pi/activity/Product/ProductUIController.dart';
import 'package:pi/utils/YouTubeVideos.dart';
import 'package:pi/utils/AppUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:youtube/youtube_thumbnail.dart';
import '../../Model/Product/ProductResponse.dart';
import '../../utils/CustomAppBar.dart';
import '../../utils/WebviewController.dart';
import '../Loan/DashLoan.dart';
import '../Utility Controllers/ViewPosterImg.dart';
import 'DashboardController.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

class DashboardUIController extends StatefulWidget {
  const DashboardUIController({super.key});

  @override
  State<DashboardUIController> createState() => _DashboardUIControllerState();
}

class _DashboardUIControllerState extends State<DashboardUIController> {
  static const String _apiKey = 'AIzaSyBzKrur3YUDLTF9kmbNdf-vhMCRN6rXYd4';
  List<Video> _videos = [];
  final String channelId = 'UCcVu0U-IU8Kw5nObp6w02HA';
  bool _isExpanded = false;


  @override
  Widget build(BuildContext context) {
    DateTime timeBackPressed = DateTime.now();
    return GetBuilder<DashboardController>(
      init: DashboardController(context: context),
      builder: (_) {
        return WillPopScope(
            onWillPop: () async {
              final difference = DateTime.now().difference(timeBackPressed);
              final isExitWarning = difference >= const Duration(seconds: 3);

              timeBackPressed = DateTime.now();

              if (isExitWarning) {
                const message = 'Back again to Exit';
                Fluttertoast.showToast(msg: message, fontSize: 18);
                return false;
              } else {
                Fluttertoast.cancel();
                return true;
              }
            },
            child: Scaffold(
              appBar: PreferredSize(
                  preferredSize: const Size.fromHeight(60),
                  child: CustomAppBar(
                      userName: _.name,
                      kycStatus: _.kycStatus,
                      pageInUse: '1')),
              backgroundColor: AppUtils.bgColor,
              // backgroundColor: AppUtils.backgroundColor,
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CarouselSlider(
                      items: _.bannerList
                          .map(
                            (e) => GestureDetector(
                              onTap: () async {
                                SharedPreferences preferences =
                                    await SharedPreferences.getInstance();
                                if (_.productList.isNotEmpty) {
                                  String pid = _.productList[0].id;
                                  String pName = _.productList[0].name;
                                  preferences.setString(
                                      AppUtils.productId, pid);
                                  preferences.setString(
                                      AppUtils.productName, pName);
                                  Get.to(() => ProductUIController(
                                        productId: pid,
                                        subProductId: "",
                                      ));
                                }
                              },
                              child: Card(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: CachedNetworkImage(
                                  imageUrl: e.image,
                                  fit: BoxFit.contain,
                                  placeholder: (context, url) => const SizedBox(
                                    height: 100,
                                    // Adjust the size to match your layout
                                    child: Center(
                                      child:
                                          CircularProgressIndicator(), // Optional loading indicator
                                    ),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Image.asset(
                                    'res/images/no_image.png',
                                  ),
                                ),
                              ),
                            ),
                          )
                          .toList(),
                      options: CarouselOptions(
                        // reverse: true,
                        // scrollDirection: Axis.vertical,
                        enlargeCenterPage: true,
                        autoPlay: true,
                        enlargeFactor: 0,
                        autoPlayCurve: Curves.fastEaseInToSlowEaseOut,
                        clipBehavior: Clip.antiAlias,
                        enableInfiniteScroll: true,
                        autoPlayAnimationDuration:
                            const Duration(milliseconds: 1100),
                        viewportFraction: 1,
                        // autoPlay: true,
                        // autoPlayCurve: Curves.easeOutQuart,
                        // enableInfiniteScroll: true,
                        // enlargeCenterPage: true,
                        height: MediaQuery.of(context).size.height * 0.34 - 100,
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 15),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          gradient: const LinearGradient(
                            colors: [
                              Color(0xFF007BFF),
                              Color(0xFF0056D2),
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              const Image(
                                image: AssetImage(
                                    'res/images/contactUs/wallet.png'),
                                height: 35,
                                width: 35,
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Balance',
                                    style: TextStyle(
                                        fontSize: 15, color: Colors.white),
                                  ),
                                  Text(
                                    '₹ ${_.walletBalance}',
                                    style: const TextStyle(
                                        fontSize: 22,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              const Spacer(),
                              InkWell(
                                onTap: () async {
                                  if (_.productList.isNotEmpty) {
                                    SharedPreferences preferences =
                                        await SharedPreferences.getInstance();
                                    String pid = _.productList[0].id;
                                    String pName = _.productList[0].name;
                                    preferences.setString(
                                        AppUtils.productId, pid);

                                    preferences.setString(
                                        AppUtils.productName, pName);
                                    Get.to(() => ProductUIController(
                                          productId: pid,
                                          subProductId: "",
                                        ));
                                  } else {
                                    Get.to(const DashLoan());
                                  }
                                },
                                child: Container(
                                  margin: const EdgeInsets.only(right: 5),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(3),
                                      color: const Color(0xFFFF5B5B)),
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 4, horizontal: 10),
                                  child: const Center(
                                    child: Text(
                                      "Quick Leads",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),

                    Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 15),
                        child: Container(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Container(
                                  padding: const EdgeInsets.symmetric(vertical: 5),
                                  // width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: const Color(0xFFA020F0)),
                                  child: Column(
                                    children: [
                                      Text(
                                        _.pending,
                                        style: const TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      const Text(
                                        'File',
                                        style: TextStyle(
                                            fontSize: 15, color: Colors.white),
                                      )
                                    ],
                                  )),
                            ),
                            const SizedBox(width: 10),
                            Expanded(
                              child: Container(
                                  padding: const EdgeInsets.symmetric(vertical: 5),
                                  width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: const Color(0xFF00A798)),
                                  child: Column(
                                    children: [
                                      Text(
                                        _.approved,
                                        style: const TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      const Text(
                                        'Approved',
                                        style: TextStyle(
                                            fontSize: 15, color: Colors.white),
                                      )
                                    ],
                                  )),
                            ),
                            const SizedBox(width: 10),
                            Expanded(
                              child: Container(
                                  padding: const EdgeInsets.symmetric(vertical: 5),
                                  width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: const Color(0xFFFF5B5B)),
                                  child: Column(
                                    children: [
                                      Text(
                                        _.rejected,
                                        style: const TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      const Text(
                                        'Rejected',
                                        style: TextStyle(
                                            fontSize: 15, color: Colors.white),
                                      )
                                    ],
                                  )),
                            ),
                          ],
                        ))),

                    if (_.productList.isNotEmpty)
                      Column(
                        children: [
                          for (var index = 0;
                              index < _.productList.length;
                              index++)
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 7.5, horizontal: 15),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  // boxShadow: [BoxShadow(
                                  //   color: Colors.grey.shade400,
                                  //   spreadRadius: -2,
                                  //   blurRadius: 10,
                                  //   offset: Offset(0, 0)
                                  // )]
                                ),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 18,
                                            right: 10,
                                            top: 15,
                                            bottom: 0),
                                        child: Container(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            _.productList[index].name,
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16,
                                              shadows: <Shadow>[],
                                            ),
                                          ),
                                        ),
                                      ),
                                      HorizontalList1(
                                          obj: _.subProductList,
                                          parentId: _.productList[index].id),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                    //UCcVu0U-IU8Kw5nObp6w02HA

                    _.posterList.isEmpty
                        ? const SizedBox()
                        : Padding(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 10, top: 4),
                            child: Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  // boxShadow: [BoxShadow(
                                  //     color: Colors.grey.shade400,
                                  //     spreadRadius: -2,
                                  //     blurRadius: 10,
                                  //     offset: Offset(0, 0)
                                  // )]
                                ),
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 18,
                                          right: 10,
                                          top: 15,
                                          bottom: 0),
                                      child: Container(
                                        alignment: Alignment.topLeft,
                                        child: InkWell(
                                          onTap: () {
                                            //TODO
                                          },
                                          child: Row(
                                            children: [
                                              const Text(
                                                'Marketing Material',
                                                style: TextStyle(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 16,
                                                  shadows: <Shadow>[],
                                                ),
                                              ),
                                              const Spacer(),
                                              // Container(
                                              //     margin:
                                              //     const EdgeInsets.only(right: 5),
                                              //     decoration: BoxDecoration(
                                              //         borderRadius:
                                              //         BorderRadius.circular(
                                              //             3),
                                              //         color: Colors.red),
                                              //     child: const Padding(
                                              //       padding: EdgeInsets
                                              //           .symmetric(
                                              //           horizontal: 10,
                                              //           vertical: 3),
                                              //       child: Text(
                                              //         'View All',
                                              //         style: TextStyle(
                                              //             color: Colors.white),
                                              //       ),
                                              //     )),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: SizedBox(
                                        height:
                                        MediaQuery.of(context).size.height *
                                            0.175,
                                        width:
                                        MediaQuery.of(context).size.width,
                                        child: Stack(
                                          children: [
                                            Column(
                                              children: [
                                                Expanded(
                                                  child: ListView.builder(
                                                    scrollDirection:
                                                    Axis.horizontal,
                                                    itemBuilder:
                                                        (context, index) {
                                                      final mPoster = _.posterList[
                                                      _.posterList.length - 1 - index];
                                                      return Padding(
                                                          padding:
                                                          const EdgeInsets
                                                              .all(4.0),
                                                          child: InkWell(
                                                              onTap: () {
                                                               Get.to(PosterImg(mPoster: mPoster));
                                                              },
                                                              child: Padding(
                                                                padding:
                                                                const EdgeInsets
                                                                    .only(
                                                                    bottom:
                                                                    8.0,right: 8),
                                                                child: Stack(
                                                                  alignment:
                                                                  Alignment
                                                                      .center,
                                                                  children: [
                                                                    Image.network(
                                                                      mPoster.file,
                                                                      fit: BoxFit.contain,
                                                                      errorBuilder: (context, error, stackTrace) {
                                                                        return Image.asset(
                                                                          'res/images/no_image.png',
                                                                          fit: BoxFit.contain,
                                                                          width: 30,
                                                                          height: 30,
                                                                        );
                                                                      },
                                                                    ),
                                                                  ],
                                                                ),
                                                              )));
                                                    },
                                                    itemCount:
                                                    _.posterList.isNotEmpty
                                                        ? _.posterList.length - 1
                                                        : 0,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),

                    _videos.isEmpty
                        ? const SizedBox()
                        : Padding(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 10, top: 4),
                            child: Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  // boxShadow: [BoxShadow(
                                  //     color: Colors.grey.shade400,
                                  //     spreadRadius: -2,
                                  //     blurRadius: 10,
                                  //     offset: Offset(0, 0)
                                  // )]
                                ),
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 18,
                                          right: 10,
                                          top: 15,
                                          bottom: 0),
                                      child: Container(
                                        alignment: Alignment.topLeft,
                                        child: InkWell(
                                          onTap: () {
                                            Get.to(const YouTubeVideos(
                                                channelId:
                                                    'UCcVu0U-IU8Kw5nObp6w02HA'));
                                          },
                                          child: Row(
                                            children: [
                                              const Text(
                                                'Videos',
                                                style: TextStyle(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 16,
                                                  shadows: <Shadow>[],
                                                ),
                                              ),
                                              const Spacer(),
                                              Container(
                                                  margin:
                                                      const EdgeInsets.only(right: 5),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              3),
                                                      color: Colors.red),
                                                  child: const Padding(
                                                    padding: EdgeInsets
                                                        .symmetric(
                                                        horizontal: 10,
                                                        vertical: 3),
                                                    child: Text(
                                                      'View All',
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.175,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Stack(
                                          children: [
                                            Column(
                                              children: [
                                                Expanded(
                                                  child: ListView.builder(
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    itemBuilder:
                                                        (context, index) {
                                                      final video = _videos[
                                                          _videos.length -
                                                              1 -
                                                              index];
                                                      return Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(4.0),
                                                          child: InkWell(
                                                              onTap: () {
                                                                _openYoutubeApp(
                                                                    'https://www.youtube.com/watch?v=${video.videoId}');
                                                              },
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                        .only(
                                                                        bottom:
                                                                            8.0),
                                                                child: Stack(
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  children: [
                                                                    Image
                                                                        .network(
                                                                      YoutubeThumbnail(
                                                                              youtubeId: video.videoId)
                                                                          .standard(),
                                                                      errorBuilder: (context,
                                                                          error,
                                                                          stackTrace) {
                                                                        return Image
                                                                            .asset(
                                                                          'res/images/no_image.png',
                                                                          fit: BoxFit
                                                                              .contain,
                                                                        );
                                                                      },
                                                                    ),
                                                                    video.videoId
                                                                            .isNotEmpty
                                                                        ? const FaIcon(
                                                                            FontAwesomeIcons.youtube,
                                                                            size:
                                                                                40,
                                                                            color:
                                                                                Colors.red,
                                                                          )
                                                                        : sb(0,
                                                                            0)
                                                                  ],
                                                                ),
                                                              )));
                                                    },
                                                    itemCount:
                                                        _videos.isNotEmpty
                                                            ? _videos.length - 1
                                                            : 0,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                    const SizedBox(
                      height: 30,
                    ),
                    // const SizedBox(
                    //   height: 15,
                    // ),

                    // sb(15, 0),
                    const Text(
                      "M/s Square fin Private Limited directly provide leads as a direct selling agent (DSA) to our NBFC and Banking Partners for lending directly to the consumers in the low and middle income class groups deals in semi-urban and rural locations. We facilitate online and offline platform to our customer to get funds at best possible interest rates and also reduce the credit risk for the partners by using our unique appraisal risk process. We hereby state that our company M/s Squarefin Private Limited has entered into an agreement with RBI Registered NBFCs and Financial Institutions who will be providing loans to the leads given by us with the help of this App.",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 8),
                    ),
                    TextButton(
                        onPressed: () async {
                          String webUrl = "https://www.squarefin.in/privacy";
                          if (webUrl.isNotEmpty) {
                            Get.to(WebviewController(
                                url: webUrl, title: 'Our Privacy and Policy'));
                          }
                        },
                        child: const Text(
                          "Our Privacy and Policy",
                          style:
                              TextStyle(fontSize: 12, color: Colors.lightBlue),
                        ))
                  ],
                ),
              ),
              floatingActionButton: _buildExpandableFab(),
            ));
      },
    );
  }

  Widget _buildExpandableFab() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (_isExpanded) ...[
          _buildActionButton(
            icon: Icons.call,
            onPressed: _makePhoneCall,
            backgroundColor: Colors.teal,
          ),
          const SizedBox(height: 10),
          _buildActionButton(
            icon: Icons.mail,
            onPressed: _sendEmail,
            backgroundColor: Colors.red,
          ),
          const SizedBox(height: 10),
          FloatingActionButton(
            mini: true,
            onPressed: () => _sendWhatsAppMessage(),
            backgroundColor: Colors.green,
            child: const FaIcon(FontAwesomeIcons.whatsapp),
          ),
          const SizedBox(height: 10),
        ],
        FloatingActionButton(
          onPressed: _toggleExpand,
          child: Icon(_isExpanded ? Icons.close : Icons.support_agent),
        ),
      ],
    );
  }

  Widget _buildActionButton(
      {required IconData icon,
      required VoidCallback onPressed,
      required Color backgroundColor}) {
    return FloatingActionButton(
      mini: true,
      backgroundColor: backgroundColor,
      onPressed: onPressed,
      child: Icon(icon),
    );
  }
  Future<void> _fetchVideos() async {
    final String url =
        'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=$channelId&key=$_apiKey&maxResults=50';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      final items = data['items'] as List;
      _videos = items.map((item) => Video.fromJson(item)).toList();
      setState(() {});
    } else {
      print('Failed to fetch videos: ${response.statusCode}');
    }
  }

  void _toggleExpand() {
    setState(() {
      _isExpanded = !_isExpanded;
    });
  }

  Future<void> _makePhoneCall() async {
    final phoneNo = Uri.parse('tel:+918306300415');
    if (await launchUrl(phoneNo)) {
      // Call initiated
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Could not launch Dialer pad')),
      );
    }
  }

  Future<void> _sendEmail() async {
    final emailUri = Uri(
      scheme: 'mailto',
      path: 'info@squarefin.in',
    );

    if (await launchUrl(emailUri, mode: LaunchMode.externalApplication)) {
      // Email client opened successfully
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Could not launch email client')),
      );
    }
  }

  Future<void> _sendWhatsAppMessage() async {
    final whatsappUrl = Uri.parse('https://wa.me/918306300415?text=Hello');
    if (await launchUrl(whatsappUrl)) {
      // WhatsApp opened successfully
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Could not launch WhatsApp')),
      );
    }
  }

  int gridCount = 2;

  void _openYoutubeApp(String youtubeLink) async {
    final Uri url = Uri.parse(youtubeLink);

    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  bool animation = false;

  void updateAnimation() {
    Future.delayed(const Duration(milliseconds: 100), () {
      setState(() {
        animation = true;
      });
    });
  }

  @override
  void initState() {
    updateAnimation();
    super.initState();
    _fetchVideos();
  }

}

class HorizontalList1 extends StatelessWidget {
  List<ProductData> obj;
  String parentId;

  HorizontalList1({super.key, required this.obj, required this.parentId});

  @override
  Widget build(BuildContext context) {
    List<ProductData> filteredList =
        obj.where((item) => parentId == item.parent).toList();

    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
      child: GridView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4, mainAxisExtent: 80),
        itemCount: filteredList.length,
        itemBuilder: (BuildContext context, int index) {
          double customHeight = 200;
          return SizedBox(
            height: customHeight,
            child: GestureDetector(
              onTap: () async {
                SharedPreferences preferences =
                    await SharedPreferences.getInstance();
                String name = filteredList[index].name;
                String isIntegrated = filteredList[index].isIntegrated;

                if (isIntegrated.isNotEmpty && isIntegrated == "0") {
                  String webUrl = filteredList[index].redirectLink;
                  if (webUrl.isNotEmpty) {
                    print(webUrl);
                    Get.to(WebviewController(url: webUrl, title: name));
                  } else {
                    AppUtils.showSnackBar(name, "Something went wrong", false);
                  }
                } else {
                  String pid = filteredList[index].parent;
                  String sid = filteredList[index].id;
                  String name = filteredList[index].name;
                  String haveForm = filteredList[index].haveForm;
                  preferences.setString(AppUtils.productId, pid);
                  preferences.setString(AppUtils.subProductId, sid);
                  preferences.setString(AppUtils.productName, name);

                  if (haveForm.isNotEmpty && haveForm == "1") {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return ChooseCustomProductDialog(pid: pid, sid: sid);
                      },
                    );
                  } else {
                    Get.to(() =>
                        ProductUIController(productId: pid, subProductId: sid));
                  }
                }
              },
              child: Align(
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      SizedBox(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 15, bottom: 5, right: 8, left: 8),
                          child: Center(
                            child: CachedNetworkImage(
                              imageUrl: filteredList[index].icon,
                              width: 35,
                              height: 35,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => const SizedBox(
                                width: 30,
                                height: 30,
                                child: CircularProgressIndicator(),
                              ),
                              errorWidget: (context, url, error) => Image.asset(
                                'res/images/no_image.png',
                                width: 24,
                                height: 24,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        child: Text(
                          filteredList[index].name,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                              fontSize: 12, color: Colors.black, height: 1),
                        ),
                      ),
                    ],
                  )),
            ),
          );
        },
      ),
    );
  }
}

class Video {
  final String title;
  final String channelId;
  final String thumbnailUrl;
  final String videoId;

  Video.fromJson(Map<String, dynamic> json)
      : title = json['snippet']['title'] as String,
        channelId = json['snippet']['channelId'] as String,
        thumbnailUrl =
            json['snippet']['thumbnails']['default']['url'] as String,
        videoId = json['id']['videoId']?.toString() ?? '';
}

class ChooseCustomProductDialog extends StatelessWidget {
  final String phoneNumber = '+91 8306300415';
  final String mMail = 'info@squarefin.in';
  final String pid;
  final String sid;

  const ChooseCustomProductDialog({
    super.key,
    required this.pid,
    required this.sid,
  });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: () {
                    Get.back();
                  },
                  child: Icon(
                    Icons.cancel_outlined,
                    color: Colors.red.shade200,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 150,
            child: SvgPicture.asset(
              'res/images/choose.svg',
              fit: BoxFit.fitHeight,
            ),
          ),
          const Text(
            '\nConfirmation',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              decoration: TextDecoration.underline,
            ),
          ),
          const SizedBox(height: 4),
          const Text(
            'Would you like to proceed with your preferred lending partner?',
            style: TextStyle(fontSize: 13),
          ),
          const SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              _buildActionButton(
                context: context,
                text: 'No',
                backgroundColor: Colors.red,
                borderColor: Colors.redAccent,
                onPressed: () {
                  Get.back();
                  Get.to(const DashLoan());
                },
              ),
              sb(10, 10),
              _buildActionButton(
                context: context,
                text: 'Yes',
                backgroundColor: Colors.green,
                borderColor: Colors.lightGreen,
                onPressed: () {
                  Get.back();
                  Get.to(() =>
                      ProductUIController(productId: pid, subProductId: sid));
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildActionButton({
    required BuildContext context,
    required String text,
    required Color backgroundColor,
    required Color borderColor,
    required VoidCallback onPressed,
  }) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        foregroundColor: Colors.white,
        backgroundColor: backgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(color: borderColor, width: 1),
        ),
      ),
      onPressed: onPressed,
      child: Text(
        text,
        style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      ),
    );
  }
}

import 'dart:convert';
import 'package:custom_searchable_dropdown/custom_searchable_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pi/Model/Loan/DsaResponse.dart';
import 'package:pi/activity/Utility%20Controllers/TicketListUIController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pi/utils/BottomBarScreen.dart';
import '../../Model/Bank.dart';
import '../../Model/BankList.dart';
import '../../Model/LoanSubmit.dart';
import '../../network/ApiClient.dart';
import '../../utils/AppUtils.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import '../../utils/WebviewController.dart';

class DashLoan extends StatefulWidget {
  const DashLoan({super.key});

  @override
  _DashLoan createState() => _DashLoan();
}

class _DashLoan extends State<DashLoan> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController contactController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController jobProfileController = TextEditingController();
  TextEditingController remarkController = TextEditingController();

  List<String> salary_items = [
    '1L - 5L',
    '5L - 10L',
    '10L - 20L',
    '20L - 40L',
    '40L - 80L',
    '80L - 1Cr',
  ];
  var token,
      name,
      email,
      phone,
      loanValue = "1L - 5L",
      jobProfile,
      remark,
      address,
      product,
      productId,
      userId,
      userType,
      version = "";

  List<BankList> productList = [];
  List<DsaData> dsaList = [];
  var dsa = '';

  @override
  void initState() {
    super.initState();
    getSharedData();
    getDsaList();
    getProductList();
  }

  // Future<void> openGooglePhotosOrInstall() async {
  //   final String packageName = 'com.google.android.apps.photos';
  //   final String playStoreUrl =
  //       'https://play.google.com/store/apps/details?id=$packageName';
  //
  //   if (await canLaunch('package:$packageName')) {
  //     await launch('package:$packageName');
  //   } else {
  //     await launch(playStoreUrl);
  //   }
  // }

  // Future<void> openApp() async {
  //   try {
  //     await launch('package:com.google.android.apps.photos');
  //   } on PlatformException catch (e) {
  //     if (e.code == '0') {
  //       print('step 1');
  //       // The app is not installed
  //       await launch('https://play.google.com/store/apps/details?id=com.google.android.apps.photos');
  //     } else {
  //       print('step 1');
  //       // Some other error occurred
  //       throw e;
  //     }
  //   }
  // }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Raise Request'),
        actions: [
          InkWell(
              onTap: () {
                Get.to(const TicketListUIController());
              },
              child: const Image(
                  image: AssetImage('res/images/profile/request-raise.png')))
        ],
        // actions: [
        //   IconButton(onPressed: () async {
        //     // await LaunchApp.openApp(androidPackageName: 'com.google.android.apps.photos',);
        //
        //     // try {
        //     //   ///checks if the app is installed on your mobile device
        //     //   bool isInstalled = await DeviceApps.isAppInstalled('com.google.android.youtube');
        //     //   if (isInstalled) {
        //     //     DeviceApps.openApp("com.google.android.youtube");
        //     //   } else {
        //     //     ///if the app is not installed it lunches google play store so you can install it from there
        //     //     launch("https://play.google.com/store/apps/details?id=com.google.android.youtube");
        //     //   }
        //     // } catch (e) {
        //     //   print(e);
        //     // }
        //
        //   }, icon: Icon(Icons.open_in_browser))
        // ],
        //TODO
        // actions: [InkWell(
        //   onTap: (){
        //     Get.to(TicketUIController());
        //   },
        //   child: Icon(Icons.image_aspect_ratio_sharp)),SizedBox(width: 20,)],
      ),
      // backgroundColor: AppUtils.backgroundColor,
      body: Stack(children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          color: AppUtils.bgColor,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(5)),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 12,
                  right: 12,
                ),
                child: Form(
                  key: _formKey,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 8.0, right: 8, bottom: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        const SizedBox(height: 16.0),
                        TextFormField(
                          controller: nameController,
                          decoration: const InputDecoration(
                              labelText: 'Name',
                              labelStyle: TextStyle(color: Colors.grey),
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFFE3F2FD))),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.blue))),
                          keyboardType: TextInputType.name,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Enter name';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 16.0),
                        TextFormField(
                          controller: emailController,
                          decoration: const InputDecoration(
                              labelText: 'Email',
                              labelStyle: TextStyle(color: Colors.grey),
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFFE3F2FD))),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.blue))),
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter email address';
                            } else if (!RegExp(r'\S+@\S+\.\S+')
                                .hasMatch(value)) {
                              return 'Enter a valid email address';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 16.0),
                        TextFormField(
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(10),
                          ],
                          controller: contactController,
                          decoration: const InputDecoration(
                              labelText: 'Contact Number',
                              labelStyle: TextStyle(color: Colors.grey),
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFFE3F2FD))),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.blue))),
                          keyboardType: TextInputType.phone,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter Contact Number';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 16.0),
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: CustomSearchableDropDown(
                            items: productList,
                            label: 'Select Product',
                            decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey.shade200,
                                ),
                                borderRadius: BorderRadius.circular(5)),
                            dropDownMenuItems: productList.map((item) {
                              var val =
                                  (item.bankName ?? "").replaceAll('Loan', '');
                              return val;
                            }).toList(),
                            onChanged: (value) {
                              if (value != null) {
                                setState(() {
                                  product = value?.bankName ?? "";
                                });
                              } else {}
                            },
                          ),
                        ),
                        const SizedBox(height: 16.0),
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: CustomSearchableDropDown(
                            items: dsaList,
                            label: 'Select DSA/NBFC',
                            decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey.shade200,
                                ),
                                borderRadius: BorderRadius.circular(5)),
                            dropDownMenuItems: dsaList.map((item) {
                              return item.name ?? "";
                            }).toList(),
                            onChanged: (value) {
                              if (value != null) {
                                setState(() {
                                  dsa = value?.name ?? "";
                                });
                              } else {}
                            },
                          ),
                        ),
                        const SizedBox(height: 16.0),
                        TextFormField(
                          controller: addressController,
                          decoration: const InputDecoration(
                              labelText: 'Address',
                              labelStyle: TextStyle(color: Colors.grey),
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFFE3F2FD))),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.blue))),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter Address';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 16.0),
                        // DropdownButtonFormField<String>(
                        //   value: salary_items[0],
                        //   onChanged: (String? value) {
                        //     loanValue = value ?? "";
                        //   },
                        //   items: salary_items.map((String value) {
                        //     return DropdownMenuItem<String>(
                        //       value: value,
                        //       child: Text(value),
                        //     );
                        //   }).toList(),
                        //   decoration: const InputDecoration(
                        //     labelText: 'Loan Requirement',
                        //     border: OutlineInputBorder(),
                        //   ),
                        // ),
                        // const SizedBox(height: 16.0),
                        // TextFormField(
                        //   controller: jobProfileController,
                        //   decoration: const InputDecoration(
                        //     labelText: 'Job Profile',
                        //     border: OutlineInputBorder(),
                        //   ),
                        //   keyboardType: TextInputType.text,
                        //   validator: (value) {
                        //     if (value == null || value.isEmpty) {
                        //       return 'Enter Job Profile';
                        //     }
                        //     return null;
                        //   },
                        // ),
                        // const SizedBox(height: 16.0),
                        TextFormField(
                          controller: remarkController,
                          decoration: const InputDecoration(
                              labelText: 'Remark',
                              labelStyle: TextStyle(color: Colors.grey),
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Color(0xFFE3F2FD))),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.blue))),
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter Remark';
                            }
                            return null;
                          },
                        ),
                        const SizedBox(height: 16.0),

                        // ElevatedButton(
                        //   onPressed: () {
                        //     if (_formKey.currentState!.validate()) {
                        //       submitLoan();
                        //       print('done');
                        //     }else{
                        //       print('elephant');
                        //     }
                        //   },
                        //   style: ButtonStyle(
                        //     backgroundColor:
                        //         MaterialStateProperty.all<Color>(Colors.lightBlue),
                        //     // Set the background color to blue
                        //     minimumSize: MaterialStateProperty.all<Size>(
                        //         const Size(double.infinity, 50)),
                        //     shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        //       RoundedRectangleBorder(
                        //         borderRadius: BorderRadius.circular(
                        //             10.0), // Set the border radius to 10.0 for rounded corners
                        //       ),
                        //     ), // Set the button width to full and height to 50
                        //   ),
                        //   child: const Text(
                        //     'Submit Request',
                        //     style: TextStyle(color: Colors.white),
                        //   ),
                        // ),

                        Row(
                          children: [
                            const Spacer(),
                            InkWell(
                              onTap: () {
                                if (_formKey.currentState!.validate()) {
                                  submitLoan();
                                  print('done');
                                } else {
                                  print('elephant');
                                }
                              },
                              child: Container(
                                  height: 35,
                                  decoration: BoxDecoration(
                                      color: const Color(0xFF00A389),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: const Center(
                                      child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 24),
                                    child: Text(
                                      'Submit Request',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                  ))),
                            ),
                          ],
                        ),

                        const SizedBox(height: 16.0),
                        const Text(
                          "M/s Squarefin Private Limited directly provide leads as a direct selling agent (DSA) to our NBFC and Banking Partners for lending directly to the consumers in the low and middle income class groups deals in semi-urban and rural locations. We facilitate online and offline platform to our customer to get funds at best possible interest rates and also reduce the credit risk for the partners by using our unique appraisal risk process. We hereby state that our company M/s Squarefin Private Limited has entered into an agreement with RBI Registered NBFCs and Financial Institutions who will be providing loans to the leads given by us with the help of this App.",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 10),
                        ),
                        TextButton(
                            onPressed: () async {
                              String url =
                                  'https://www.squarefin.in/privacy.html';
                              String title = "Privacy and Policies";

                              Get.to(WebviewController(url: url, title: title));
                            },
                            child: const Text(
                              "Privacy and Policies",
                              style: TextStyle(color: Colors.lightBlue),
                            ))
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ]),
    );
  }

  void _showSuccessDialog(String msg, bool isClose) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Loan Request'),
          content: Text(msg),
          actions: <Widget>[
            TextButton(
              child: const Text(
                'OK',
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                if (isClose) {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const BottomBarScreen()),
                      ModalRoute.withName("/BottomBarScreen"));
                }
              },
            ),
          ],
        );
      },
    );
  }

  void getProductList() async {
    try {
      Map<String, String> mData = {
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.productList);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        Bank bank = Bank.fromJson(jsonDecode(response.body));
        productList.addAll(bank.bankList as Iterable<BankList>);
        if (productList.isNotEmpty) {
          product = productList[0].bankName ?? "";
          productId = productList[0].id ?? "";
          print(product);
        }
        print(productList);
        setState(() {});
      }
    } catch (e) {
      print(e);
    }
  }

  void getDsaList() async {
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.dsaList);
      var response = await http.get(url);
      print(url);
      print(response.body);

      if (response.statusCode == 200) {
        DsaResponse res = DsaResponse.fromJson(jsonDecode(response.body));

        if (res.data != null) {
          dsaList.addAll(res.data!);
          setState(() {});
          print(dsaList);
        }
      }
    } catch (e) {
      print(e);
    }
  }

  void submitLoan() async {
    name = nameController.text ?? "";
    email = emailController.text ?? "";
    phone = contactController.text ?? "";
    jobProfile = jobProfileController.text ?? "";
    remark = remarkController.text ?? "";
    address = addressController.text ?? "";

    try {
      Map<String, dynamic> mData = {
        'Login_User_Id': userId,
        'Login_User_Type': userType,
        'url': productId,
        'name': name,
        'email': email,
        'mobile': phone,
        // 'loanValue': loanValue,
        'remark': remark,
        'product': product,
        'dsa': dsa,
        'platForm': AppClient.platForm,
        'version': version,
        'Device_Type': AppUtils.deviceType
      };

      print(mData);
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.submitLoan);
      print(url);
      var response = await http
          .post(url, body: mData, headers: {"Authorization": 'Bearer $token'});
      print(response.body);
      if (response.statusCode == 200) {
        LoanSubmit loanSubmit = LoanSubmit.fromJson(jsonDecode(response.body));
        String msg = loanSubmit.message ?? "";
        var status = loanSubmit.status ?? 0;
        _showSuccessDialog(msg, status == 1);
        setState(() {});
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> getSharedData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString(AppUtils.userId) ?? "";
    userType = preferences.getString(AppUtils.userType) ?? "";
    name = preferences.getString(AppUtils.userName) ?? "";
    token = preferences.getString(AppUtils.token) ?? "";
    version = preferences.getString(AppUtils.appVersion) ?? "";

    setState(() {});
  }
}

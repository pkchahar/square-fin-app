import 'package:flutter/material.dart';
import 'package:pi/activity/Loan/personal_loan_step_6.dart';
import '../../utils/AppUtils.dart';

class PersonalLoanStepFive extends StatefulWidget {
  Map<String, String> mapData;
  PersonalLoanStepFive({super.key, Key? mData, required this.mapData});

  @override
  State<PersonalLoanStepFive> createState() => _PersonalLoanStepFiveState();
}

class _PersonalLoanStepFiveState extends State<PersonalLoanStepFive> {
  Map<String, String> mapData = {};
  bool fifthscreen = false;
  bool fifthscreen1 = false;
  bool fifthscreen2 = false;
  bool fifthscreen3 = false;
  bool fifthscreen4 = false;

  String loanAmount = "";

  @override
  void initState() {
    super.initState();
    mapData = widget.mapData;
    mapData.addEntries({"loanAmount": "0"}.entries);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppUtils.backgroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: ListTile(
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.arrow_back_ios_new_outlined,
              color: Color(0xff1FC4F4),
              size: 22,
            ),
          ),
          title: const Text(
            "Loan Amount",
            style: TextStyle(
                color: Color(0xff1FC4F4),
                fontSize: 16,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
          trailing: const Text(
            "Steps 4/5",
            style: TextStyle(
                color: Color(0xff1FC4F4),
                fontSize: 16,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: const Color(0xffFFFFFF),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: fifthscreen
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("1 Lac",
                        style: fifthscreen
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    loanAmount = "1 Lac";
                    mapData.remove("loanAmount");
                    mapData.addEntries({"loanAmount": loanAmount}.entries);
                    fifthscreen = !fifthscreen;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalloanStepSix(mapData: mapData,)));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: fifthscreen1
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("1 - 2 Lac",
                        style: fifthscreen1
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    loanAmount = "1 - 2 Lac";
                    mapData.remove("loanAmount");
                    mapData.addEntries({"loanAmount": loanAmount}.entries);
                    fifthscreen1 = !fifthscreen1;
                    Navigator.pop(context);
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: fifthscreen2
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("2 - 3 Lac",
                        style: fifthscreen2
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    loanAmount = "2 - 3 Lac";
                    mapData.remove("loanAmount");
                    mapData.addEntries({"loanAmount": loanAmount}.entries);
                    fifthscreen2 = !fifthscreen2;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalloanStepSix(mapData: mapData,)));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: fifthscreen3
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("3 - 4 Lac",
                        style: fifthscreen3
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    loanAmount = "3 - 4 Lac";
                    mapData.remove("loanAmount");
                    mapData.addEntries({"loanAmount": loanAmount}.entries);
                    fifthscreen3 = !fifthscreen3;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalloanStepSix(mapData: mapData,)));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: fifthscreen4
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("More Then 4 Lac",
                        style: fifthscreen4
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    loanAmount = "More Then 4 Lac";
                    mapData.remove("loanAmount");
                    mapData.addEntries({"loanAmount": loanAmount}.entries);
                    fifthscreen4 = !fifthscreen4;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalloanStepSix(mapData: mapData,)));
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pi/utils/BottomBarScreen.dart';
import '../../utils/AppUtils.dart';
import 'personal_loan_step_2.dart';

class PersonalLoan extends StatefulWidget {
  const PersonalLoan({super.key});

  @override
  State<PersonalLoan> createState() => _PersonalLoanState();
}

class _PersonalLoanState extends State<PersonalLoan> {
  final _formkey = GlobalKey<FormState>();
  var name, mobile, email, company;

  TextEditingController nameController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController companyController = TextEditingController();
  Map<String, String> mapData = {};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppUtils.backgroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: ListTile(
          leading: InkWell(
            onTap: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => const BottomBarScreen()),
                  ModalRoute.withName("/NewDashboard"));
            },
            child: const Icon(
              Icons.arrow_back_ios_new_outlined,
              color: Color(0xff1FC4F4),
              size: 22,
            ),
          ),
          title: const Text(
            "Personal Loan",
            style: TextStyle(
                color: Color(0xff1FC4F4),
                fontSize: 16,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
          trailing: const Text(""),
        ),
        backgroundColor: const Color(0xffFFFFFF),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Form(
            key: _formkey,
            child: Column(
              children: [
                TextFormField(
                  controller: nameController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: TextStyle(color: Colors.black54),
                    label: Text(
                      "Enter Your Name",
                      style: TextStyle(
                          color: Color(0xff1FC4F4),
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please Enter User Name";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: mobileController,
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(10),
                  ],
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: TextStyle(color: Colors.black54),
                    label: Text(
                      "Enter Mobile Number",
                      style: TextStyle(
                          color: Color(0xff1FC4F4),
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  keyboardType: TextInputType.phone,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please Enter Mobile Number";
                    } else if (!RegExp(
                            r'^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$')
                        .hasMatch(value)) {
                      return "Invalid Phone Number.";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: TextStyle(color: Colors.black54),
                    label: Text(
                      "Enter Email ID",
                      style: TextStyle(
                          color: Color(0xff1FC4F4),
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please Enter Email ID";
                    } else if (!RegExp(r'\S+@\S+\.\S+').hasMatch(value)) {
                      return "Invalid Email";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: companyController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.lightBlue,
                      ),
                    ),
                    labelStyle: TextStyle(color: Colors.black54),
                    label: Text(
                      "Company name",
                      style: TextStyle(
                          color: Color(0xff1FC4F4),
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Please Enter Company Name";
                    }
                    return null;
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  clipBehavior: Clip.hardEdge,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(50)),
                  height: MediaQuery.of(context).size.height * 0.1 - 19,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(Colors.lightBlue),
                      // Set the background color to blue
                      minimumSize: WidgetStateProperty.all<Size>(
                          const Size(double.infinity, 50)),
                      shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              10.0), // Set the border radius to 10.0 for rounded corners
                        ),
                      ), // Set the button width to full and height to 50
                    ),
                    onPressed: () {
                      name = nameController.text;
                      mobile = mobileController.text;
                      email = emailController.text;
                      company = companyController.text;

                      if (_formkey.currentState!.validate()) {
                        mapData = {
                          'name': name,
                          'mobile': mobile,
                          'email': email,
                          'company': company,
                        };

                        print(mapData);

                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    PersonalLoanStepSecond(mapData: mapData)));
                      }
                    },
                    child: const Text(
                      "Continue",
                      style: TextStyle(
                          color: Color(0xffffffff),
                          fontSize: 16,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

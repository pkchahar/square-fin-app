import 'package:flutter/material.dart';
import 'package:pi/activity/Loan/personal_loan_step_4.dart';
import '../../utils/AppUtils.dart';

class PersonalLoanStepSecond extends StatefulWidget {
  Map<String, String> mapData;

  PersonalLoanStepSecond({super.key, Key? mData, required this.mapData});

  @override
  State<PersonalLoanStepSecond> createState() => _PersonalLoanStepSecondState();
}

class _PersonalLoanStepSecondState extends State<PersonalLoanStepSecond> {
  Map<String, String> mapData = {};

  _PersonalLoanStepSecondState();

  String annual = "";

  bool textFormField = false;
  bool textFormField1 = false;
  bool textFormField2 = false;
  bool textFormField3 = false;
  bool textFormField4 = false;

  @override
  void initState() {
    super.initState();
    mapData = widget.mapData;
    mapData.addEntries({"annual": "0"}.entries);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppUtils.backgroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0.0,
        title: ListTile(
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.arrow_back_ios_new_outlined,
              color: Color(0xff1FC4F4),
              size: 22,
            ),
          ),
          title: const Text(
            "Annual Income",
            style: TextStyle(
                color: Color(0xff1FC4F4),
                fontSize: 16,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
          trailing: const Text(
            "Steps 2/5",
            style: TextStyle(
                color: Color(0xff1FC4F4),
                fontSize: 16,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: const Color(0xffFFFFFF),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: textFormField
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("Upto ₹3 Lac",
                        style: textFormField
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    annual = "Upto ₹3 Lac";
                    mapData.remove("annual");
                    mapData.addEntries({"annual": annual}.entries);
                    textFormField = !textFormField;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFourth(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: textFormField1
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("₹3 Lac - ₹4 Lac",
                        style: textFormField1
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    annual = "₹3 Lac - ₹4 Lac";
                    mapData.remove("annual");
                    mapData.addEntries({"annual": annual}.entries);
                    textFormField1 = !textFormField1;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFourth(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: textFormField2
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("₹4 Lac - ₹5 Lac",
                        style: textFormField2
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    annual = "₹4 Lac - ₹5 Lac";
                    mapData.remove("annual");
                    mapData.addEntries({"annual": annual}.entries);
                    textFormField2 = !textFormField2;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFourth(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: textFormField3
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("₹5 Lac - ₹10 Lac",
                        style: textFormField3
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    annual = "₹₹5 Lac - ₹10 Lac";
                    mapData.remove("annual");
                    mapData.addEntries({"annual": annual}.entries);
                    textFormField3 = !textFormField3;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFourth(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: textFormField4
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("₹10 Lac +",
                        style: textFormField4
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    annual = "₹10 Lac +";
                    mapData.remove("annual");
                    mapData.addEntries({"annual": annual}.entries);
                    textFormField4 = !textFormField4;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFourth(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

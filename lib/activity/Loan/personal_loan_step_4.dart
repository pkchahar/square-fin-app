import 'package:flutter/material.dart';
import '../../utils/AppUtils.dart';
import 'personal_loan_step_5.dart';

class PersonalLoanStepFourth extends StatefulWidget {
  Map<String, String> mapData;

  PersonalLoanStepFourth({super.key, Key? mData, required this.mapData});

  @override
  State<PersonalLoanStepFourth> createState() => _PersonalLoanStepFourthState();
}

class _PersonalLoanStepFourthState extends State<PersonalLoanStepFourth> {
  Map<String, String> mapData = {};
  bool forthscreen = false;
  bool forthscreen1 = false;
  bool forthscreen2 = false;
  bool forthscreen3 = false;
  bool forthscreen4 = false;
  String exp = "";

  @override
  void initState() {
    super.initState();
    mapData = widget.mapData;
    mapData.addEntries({"exp": "0"}.entries);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppUtils.backgroundColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: ListTile(
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(
              Icons.arrow_back_ios_new_outlined,
              color: Color(0xff1FC4F4),
              size: 22,
            ),
          ),
          title: const Text(
            "Work Experience ",
            style: TextStyle(
                color: Color(0xff1FC4F4),
                fontSize: 16,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
          trailing: const Text(
            "Steps 3/5",
            style: TextStyle(
                color: Color(0xff1FC4F4),
                fontSize: 16,
                fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
        ),
        backgroundColor: const Color(0xffFFFFFF),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: forthscreen
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("0 - 1 Year",
                        style: forthscreen
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    exp = "0 - 1 Year";
                    mapData.remove("exp");
                    mapData.addEntries({"exp": exp}.entries);
                    forthscreen = !forthscreen;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFive(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: forthscreen1
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("1 - 2 Year",
                        style: forthscreen1
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    exp = "1 - 2 Year";
                    mapData.remove("exp");
                    mapData.addEntries({"exp": exp}.entries);
                    forthscreen1 = !forthscreen1;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFive(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: forthscreen2
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("2 - 3 Year",
                        style: forthscreen2
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    exp = "2 - 3 Year";
                    mapData.remove("exp");
                    mapData.addEntries({"exp": exp}.entries);
                    forthscreen2 = !forthscreen2;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFive(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: forthscreen3
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("3 - 4 Year",
                        style: forthscreen3
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    exp = "3 - 4 Year";
                    mapData.remove("exp");
                    mapData.addEntries({"exp": exp}.entries);
                    forthscreen3 = !forthscreen3;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFive(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
              const SizedBox(
                height: 20,
              ),
              InkWell(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1 - 21,
                  width: MediaQuery.of(context).size.width * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: forthscreen4
                            ? const Color(0xff1FC4F4)
                            : const Color(0xff6D6C6C)),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(22.0),
                    child: Text("4+ Year",
                        style: forthscreen4
                            ? const TextStyle(
                                color: Color(0xff1FC4F4),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)
                            : const TextStyle(
                                color: Color(0xff6D6C6C),
                                fontSize: 14,
                                fontWeight: FontWeight.w400)),
                  ),
                ),
                onTap: () {
                  setState(() {
                    exp = "4+ Year";
                    mapData.remove("exp");
                    mapData.addEntries({"exp": exp}.entries);
                    forthscreen4 = !forthscreen4;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PersonalLoanStepFive(
                                  mapData: mapData,
                                )));
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

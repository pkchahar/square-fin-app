import 'package:flutter/material.dart';

import 'package:pi/activity/Utility%20Controllers/CommingSoonScreen.dart';

import '../activity/Lead/LeadViewUI.dart';
import 'package:flutter_svg/flutter_svg.dart';

customcard(context, image, texttitle, textdesc) {
  return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.07,
        width: MediaQuery.of(context).size.width * 0.5 - 23,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              image,
              height: 30,
              width: 30,
            ),
            const SizedBox(
              width: 20,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  texttitle,
                  style: const TextStyle(
                      fontSize: 14,
                      color: Color(0xff100D40),
                      fontWeight: FontWeight.w400),
                ),
                Text(
                  '$textdesc',
                  style: const TextStyle(
                      fontSize: 20,
                      color: Color(0xff1FC4F4),
                      fontWeight: FontWeight.w700),
                ),
              ],
            )
          ],
        ),
      ));
}

customQuickLinks(BuildContext context, images, text, index) {
  return InkWell(
    onTap: () {
      if (index == 0 || index == 1) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => const LeadViewUI()));
      } else {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    CommingSoonScreen(title: "Manager Report")));
      }
    },
    child: Container(
      child: Column(
        children: [
          SvgPicture.asset(
            images,
            height: 40,
            // width: 40,
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            text,
            textAlign: TextAlign.center,
            style: const TextStyle(
                fontSize: 12,
                color: Color(0xff100D40),
                fontWeight: FontWeight.w600),
          )
        ],
      ),
    ),
  );
}

CustomPopupBox(context) {
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Stack(
          children: [
            AlertDialog(
              title: Container(
                height: MediaQuery.of(context).size.height * 0.6,
                width: MediaQuery.of(context).size.width * 0.8,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: const Color(0xffFFFFFF)),
                child: Column(
                  children: [
                    Image.asset("res/images/accountlogout.png"),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      "Want to Logout ?",
                      style: TextStyle(
                          color: Color(0xff1FC4F4),
                          fontSize: 24,
                          fontWeight: FontWeight.w700),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Align(
                      child: Text(
                        "You will back to early app if you\nclick the logout button",
                        style: TextStyle(
                            color: Color(0xff999999),
                            fontSize: 16,
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.1 - 7,
                      width: MediaQuery.of(context).size.width * 0.8,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30)),
                      child: ElevatedButton(
                          style: ButtonStyle(
                              shape: WidgetStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ))),
                          onPressed: () {},
                          child: const Text(
                            "Logout Now",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w500),
                          )),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 40.0,
              right: 25.0,
              child: FloatingActionButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80)),
                backgroundColor: Colors.white,
                mini: true,
                elevation: 5.0,
                child: const Icon(Icons.cancel_rounded),
              ),
            ),
          ],
        );
      });
}


customCard(context, cardImage) {
  return Card(
    elevation: 10,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20),
      side: const BorderSide(
        color: Color(0xffF9F9F9),
        width: 3,
      ),
    ),
    child: Container(
      height: MediaQuery.of(context).size.height * 0.2 - 30,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: const Color(0xffF9F9F9),
      ),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Image.asset(cardImage),
          ),
          const SizedBox(
            width: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RichText(
                text: const TextSpan(
                    text: 'Build your ',
                    style: TextStyle(
                        height: 1.4,
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w700),
                    children: [
                      TextSpan(
                        text: "Credit Score with Step\nUp ",
                        style: TextStyle(
                            color: Color(0xff1FC4F4),
                            fontSize: 16,
                            fontWeight: FontWeight.w700),
                      ),
                      TextSpan(
                        text: "Credit Card\n",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w700),
                      ),
                      TextSpan(
                        text: "backed by Fixed Deposit\n",
                        style: TextStyle(
                            color: Color(0xff999999),
                            fontSize: 11,
                            fontWeight: FontWeight.w400),
                      ),
                      TextSpan(
                        text: "___ Earn 7.3% interest* on FD\n",
                        style: TextStyle(
                            color: Color(0xff100D40),
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                      TextSpan(
                        text: "___ Card Limit is 100% of FD Amount",
                        style: TextStyle(
                            color: Color(0xff100D40),
                            fontSize: 12,
                            fontWeight: FontWeight.w400),
                      ),
                    ]),
              ),
            ],
          )
        ],
      ),
    ),
  );
}

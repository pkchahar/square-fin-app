import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:url_launcher/url_launcher.dart';
import 'package:youtube/youtube_thumbnail.dart';


class YouTubeVideos extends StatefulWidget {
  final String channelId;
  const YouTubeVideos({Key? key, required this.channelId}) : super(key: key);

  @override
  _YouTubeVideos createState() => _YouTubeVideos();
}

class _YouTubeVideos extends State<YouTubeVideos> {
  static const String _apiKey = 'AIzaSyBzKrur3YUDLTF9kmbNdf-vhMCRN6rXYd4';
  List<Video> _videos = [];

  Future<void> _fetchVideos() async {
    final String url =
        'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=${widget.channelId}&key=$_apiKey&maxResults=50'; // Limit results to 50
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      final items = data['items'] as List;
      _videos = items.map((item) => Video.fromJson(item)).toList();
      setState(() {});
    } else {
      print('Failed to fetch videos: ${response.statusCode}');
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchVideos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Row(
          children: [
            // FaIcon(FontAwesomeIcons.youtube,color: Colors.red,),SizedBox(width: 10,),
            Text('Grow with us'),
          ],
        ),
      ),
      body: ListView.builder(

        itemCount: _videos.isNotEmpty?_videos.length-1:0,
        itemBuilder: (context, index) {
          final video = _videos[_videos.length-1-index];
          return _videos.isEmpty?const Center(child: CircularProgressIndicator(color: Colors.blue,)):

          Padding(
            padding: const EdgeInsets.only(top:8),
            child: Card(
              elevation: 3,
              color: Colors.white,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(onTap: () {
                      _openYoutubeApp('https://www.youtube.com/watch?v=${video.videoId}');},
                        child: Stack(
                          alignment:
                          Alignment.center,
                          children: [
                            Image.network(
                              YoutubeThumbnail(youtubeId: video.videoId).standard(),
                              errorBuilder:
                                  (context, error, stackTrace) {
                                return Image.asset('res/images/no_image.png', fit: BoxFit.contain,);
                              },
                            ),
                          ],)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Row(
                      children: [
                        const SizedBox(width: 10,),
                        const FaIcon(FontAwesomeIcons.youtube,color: Colors.red,),
                        const SizedBox(width: 10,),
                        Flexible(child: Text(video.title)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
  void _openYoutubeApp(String youtubeLink) async {
    final Uri url = Uri.parse(youtubeLink);

    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }
}

class Video {
  final String title;
  final String channelId;
  final String thumbnailUrl;
  final String videoId;

  Video.fromJson(Map<String, dynamic> json)
      : title = json['snippet']['title'] as String,
        channelId = json['snippet']['channelId'] as String,
        thumbnailUrl = json['snippet']['thumbnails']['default']['url'] as String,
        videoId = json['id']['videoId']?.toString() ??
            '';
}
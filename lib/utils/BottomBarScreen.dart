import 'dart:io';
import 'package:flutter/material.dart';
import 'package:pi/activity/UserAccount/DashboardController.dart';
import 'package:pi/activity/Wallet/WalletUIController.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../activity/Product/ProductLeadViewController.dart';
import '../activity/UserAccount/DashboardUIController.dart';
import '../activity/UserAccount/ProfileUIController.dart';

class BottomBarScreen extends StatefulWidget {
  final int? screen;
  const BottomBarScreen({super.key, this.screen});


  @override
  State<BottomBarScreen> createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBarScreen> {

  void changePage(int index) {
    setState(() {
      _currentIndex = index;
      // widget.screen !=0? _currentIndex = widget.screen:_currentIndex = index;
    });
  }

  var walletBalance;
  var totalEarning;
  var withdrawableBal;

  int _currentIndex = 0;

Future<void> getWalletData() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  walletBalance = sharedPreferences.get('walletBalance')??'0';
  totalEarning = sharedPreferences.get('totalEarning')??"0";
  withdrawableBal = sharedPreferences.get('withdrawableBal')??'0';
}
  Widget callPage( int selectedBar) {
    DashboardController _ = DashboardController(context: context);
    getWalletData();
    // print(selectedBar);
    // selectedBar =2;
    // print(widget.screen.toString());
    // selectedBar=widget.screen==''?selectedBar:widget.screen;
    // print(selectedBar);
    switch (selectedBar) {
      case 0:
        return const DashboardUIController();
      case 2:
        print('data for wallet ui testing');
        print(walletBalance);
        print(totalEarning);
        print(withdrawableBal);
        return WalletUIController(walletBalance: walletBalance, totalEarning: totalEarning, withdrawableBal: withdrawableBal,);
      //  return DashLoan();
      case 1:
        return const ProductLeadViewController();
      case 3:
        return const ProfileUIController();
      default:
        return const DashboardUIController();
    }
  }

  @override
  void initState() {
    // var local = GetStorage();
    // if(local.read('screen')!=null)_currentIndex = local.read('screen');
    // print('$_currentIndex  current Index');
    if(widget.screen == null)
      {
        _currentIndex = 0;
      }else{
      _currentIndex = widget.screen ?? 0;
    }
    super.initState();
  }

  Future<bool> onWillPop() {
    int index = _currentIndex;
    if (index == 0) {

      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text(
                'Are you sure?',
                style: TextStyle(color: Colors.black),
              ),
              content: const Text('You are going to exit the application.'),
              actions: <Widget>[
                TextButton(
                  child: const Text(
                    'NO',
                    style: TextStyle(color: Colors.lightBlue),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                TextButton(
                  child: const Text(
                    'YES',
                    style: TextStyle(color: Colors.lightBlue),
                  ),
                  onPressed: () async {
                    exit(0);
                  },
                ),
              ],
            );
          });
    } else {
      setState(() {
        _currentIndex = 0;
      });
    }
    return Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false, //When false, blocks the current route from being popped.
      onPopInvoked: (didPop) {
        int index = _currentIndex;
        if (index == 0) {

          // exit(0);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text(
                    'Are you sure?',
                    style: TextStyle(color: Colors.black),
                  ),
                  content: const Text('You are going to exit the application.'),
                  actions: <Widget>[
                    TextButton(
                      child: const Text(
                        'NO',
                        style: TextStyle(color: Colors.lightBlue),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                    ),
                    TextButton(
                      child: const Text(
                        'YES',
                        style: TextStyle(color: Colors.lightBlue),
                      ),
                      onPressed: () async {
                        exit(0);
                      },
                    ),
                  ],
                );
              });
        } else {
          setState(() {
            _currentIndex = 0;
          });
        }
      },
      child: Scaffold(
        body: callPage(_currentIndex),
        bottomNavigationBar: SalomonBottomBar(
          currentIndex: _currentIndex,
          onTap: (index) => changePage(index),
          items: [
            /// Home
            SalomonBottomBarItem(

              icon: const Icon(Icons.home),
              title: const Text("Home"),
              selectedColor: Colors.blue,
            ),

            SalomonBottomBarItem(
              icon: const Icon(Icons.my_library_books_outlined),
              title: const Text("My Lead"),
              selectedColor: Colors.orange,
            ),
            /// Raise Request
            SalomonBottomBarItem(
              // icon: const Icon(Icons.offline_share_rounded),
              icon: const Icon(Icons.wallet_outlined),
              title: const Text("Balance"),
              selectedColor: Colors.purple,
            ),

            /// My Leads


            /// Profile
            SalomonBottomBarItem(
              icon: const Icon(Icons.person),
              title: const Text("My Account"),
              selectedColor: Colors.teal,
            ),
          ],
        ),
      ),
    );
  }
}

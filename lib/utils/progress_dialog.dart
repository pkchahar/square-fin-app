import 'package:flutter/material.dart';

class ProgressDialog {
  _show(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  _hide(BuildContext context) {
    Navigator.pop(context);
  }

  void show(BuildContext context) {
    _show(context);
  }

  void hide(BuildContext context) {
    _hide(context);
  }
}

import 'dart:io';
import 'dart:math';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pi/activity/Product/ProductUIController.dart';
import 'package:pi/activity/Wallet/WalletUIController.dart';
import 'package:pi/utils/BottomBarScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../activity/POS Panel/PosListController/PosViewController.dart';
import '../activity/UserAccount/UserProfileUIController.dart';


//Created by Rohit
class NotificationServices {
  //bool routebool=false;
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  Future<void> requestNotificationPermission() async {
    NotificationSettings settings = await messaging.requestPermission(
        alert: true,
        announcement: true,
        sound: true,
        carPlay: true,
        badge: true,
        criticalAlert: true,
        provisional: true
    );
    if(settings.authorizationStatus == AuthorizationStatus.authorized){
      print('user granted permission');
    }else if(settings.authorizationStatus == AuthorizationStatus.provisional){
      print('user granted provisional permissions');
    }else{
      print('user denied permissions');
    }
  }
  void initLocalNotification(BuildContext context,RemoteMessage message)async{
    var androidInitializationSettings = const AndroidInitializationSettings('@mipmap/ic_app_icon');
    var iosInitializationSettings = const DarwinInitializationSettings();

    var initializationSetting  = InitializationSettings(
        android: androidInitializationSettings,
        iOS: iosInitializationSettings
    );
    await _flutterLocalNotificationsPlugin.initialize(initializationSetting,
        onDidReceiveNotificationResponse: (payload){
          handleMessage(context, message);
        }
    );
  }
  void firebaseInit(BuildContext context){
    FirebaseMessaging.onMessage.listen((message) {

      if(Platform.isAndroid) {
        initLocalNotification(context, message);
        showNotification(message);
      }else {
        showNotification(message);
      }
    });
  }
  Future<void> showNotification(RemoteMessage message)async {

    AndroidNotificationChannel channel = AndroidNotificationChannel(
        Random.secure().nextInt(100000).toString(),
        'high importance notification',
        importance: Importance.max
    );

    AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails(
        channel.id.toString(),
        channel.name.toString(),
        channelDescription: 'your channel description',
        importance: Importance.max,
        priority: Priority.max,
        ticker: 'ticker',
      // icon: '@mipmap/ic_launcher_white',
      color: const Color.fromARGB(255, 128, 128, 128),
    );

    const DarwinNotificationDetails darwinNotificationDetails = DarwinNotificationDetails(
        presentAlert: true,
        presentBadge: true,
        presentSound: true
    );
    NotificationDetails notificationDetails =NotificationDetails(
        android: androidNotificationDetails,
        iOS: darwinNotificationDetails
    );

    Future.delayed(Duration.zero,(){
      _flutterLocalNotificationsPlugin.show(
          0,
          message.notification!.title.toString(),
          message.notification!.body.toString(),
          notificationDetails
      );
    }
    );
  }

  Future <String>getDeviceToken()async{
    String? token = await messaging.getToken();
    return token!;
  }
  void isTokenRefresh()async{
    messaging.onTokenRefresh.listen((event) {
      event.toString();
    });
  }

  Future<void> setupInteractMessage(BuildContext context)async{
    RemoteMessage? initialMessage =  await FirebaseMessaging.instance.getInitialMessage();
    if(initialMessage!=null){
      handleMessage(context, initialMessage);
    }
    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      handleMessage(context, event);
    });
  }
//redirect user on the basis of blow conditions ...
  void handleMessage(BuildContext context,RemoteMessage message)async{
    String type = message.data['type'] ?? "";
    if(type == 'lead'){

      Get.to(()=>const BottomBarScreen(screen: 2));
      var localData = GetStorage();
      localData.remove('productList');
      localData.remove('subProductList');
     // Get.to(()=>ProductLeadViewController());

    }else if(type =='profile'){

      // Get.to(()=>ProfileUIController());
      Get.to(()=>const BottomBarScreen(screen: 3));
      var localData = GetStorage();
      localData.remove('productList');
      localData.remove('subProductList');


    }else if(type =='kyc'){
      Get.to(const UserProfileUIController()
      );
    }else if(type=='partner'){
      Get.to(()=>const PosViewController());
    }else if(type=='new_product'){
      Get.to(()=>ProductUIController(productId: '', subProductId: ''));
    }else if(type == 'wallet'){
    SharedPreferences preferences = await SharedPreferences.getInstance();
      var walletBalance =preferences.getString('walletBalance')??"";
      var totalEarning = preferences.getString('totalEarning')??'';
      var withdrawableBal = preferences.getString('withdrawableBal')??'';
      Get.to(()=>WalletUIController(walletBalance:walletBalance , totalEarning: totalEarning, withdrawableBal: withdrawableBal));
    }
  }
}

//lead,profile, kyc, partner, new_product, wallet -- done !
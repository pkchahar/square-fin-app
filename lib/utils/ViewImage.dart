import 'package:flutter/material.dart';

class ViewImage extends StatefulWidget {
  String imgPath;
  ViewImage({super.key, required this.imgPath});
  @override
  _ViewImage createState() => _ViewImage();
}

class _ViewImage extends State<ViewImage> {
String imgPath = "";

@override
  void initState() {
    imgPath = widget.imgPath;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('View'),
        ),
        body: Image.network(imgPath)
    );
  }
}
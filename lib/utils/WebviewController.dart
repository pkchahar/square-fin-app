import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewController extends StatefulWidget {
  final String url;
  final String title;

  const WebviewController({super.key, required this.url, required this.title});

  @override
  _WebviewControllerState createState() => _WebviewControllerState();
}

class _WebviewControllerState extends State<WebviewController> {
  late WebViewController _webViewController;
  bool _isLoading = true;
  bool _pageLoaded = false;

  @override
  void initState() {
    super.initState();
    _webViewController = WebViewController();
    _loadPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stack(
        children: [
          WebViewWidget(
            controller: _webViewController,
          ),
          if (_isLoading)
            const Center(
              child: CircularProgressIndicator(
                color: Colors.blue,
              ),
            ),
        ],
      ),
    );
  }

  void _loadPage() {
    _webViewController.setJavaScriptMode(JavaScriptMode.unrestricted);
    _webViewController.loadRequest(Uri.parse(widget.url));
    _webViewController.setNavigationDelegate(NavigationDelegate(
      onPageFinished: (String url) {
        debugPrint('Page finished loading: $url');
        if (!_pageLoaded) {
          setState(() {
            _isLoading = false;
            _pageLoaded = true;
          });
        }
      },
    ));
  }
}

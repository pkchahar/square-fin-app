import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';

class AppUtils {
  static const String showSplash = 'showSplash';
  static const String appName = "Square Fin";
  static const String userId = "user id";
  static const String userType = "user Type";
  static const String userCode = "user code";
  static const String appVersion = "appVersion";
  static const String ipAddress = "ipAddress";
  static const String token = "token";
  static const String loginObj = "loginObj";
  static const String primaryId = "primary id";
  static const String userName = "user name";
  static const String userMobile = "user mobile";
  static const String userMail = "user email";
  static const String productId = "Product Id";
  static const String productName = "Product Name";
  static const String subProductId = "Sub Product id";
  static const dynamic blankImg = "res/images/ic_empty.svg";

  // static const String baseUrl = "http://www.squarefin.in/api/api/";
  static const String getProfileData = 'SignUp/get_profile_data';
  static const String customerList = 'v1/Partner/app_mycustomer_api';
  static const String partnerList = 'v1/Partner/app_getmypartner_api';
  static const String posterList = 'v1/Marketing/Get_poster';
  static const String baseUrl = "https://www.squarefin.in/api/api/";
  static const String baseUrlNew = "https://www.squarefin.in/api/app/";
  static const String report = 'v1/Reports/GetMyReports';
  static const String addPartner1 =
      "v1/Partner/app_add_partner_api"; // add partner added by Rohit
  static const String sendOtpToMail =
      'SignUp/SendEmailVerification'; // this link will send OTP to user's mail
  static const String verifyMailOtp =
      'SignUp/VerifyEmailOtp'; // this link will verify OTP sent on mail
  static const String loginApi = "SignUp/login";
  static const String loginOtpApi = "SignUp/LoginSendOtp";
  static const String tokenVerify = "SignUp/TokenVerify";
  static const String forgotPass = "SignUp/ForgetPassword";
  static const String resendOtp = "SignUp/ResendOtp";
  static const String changePass = "SignUp/ChangePassword";
  static const String submitLoan = 'ApplyLoan/LoanFormSubmit';
  static const String getLoanDetail = 'ApplyLoan/ViewForms';
  static const String updateProfile = 'Dashboard/UpdateProfile';
  static const String getProfile =
      'SignUp/ProfileSync'; // this api is going to return kyc remark
  static const String vehicleMaster = 'Dropdown/get_make';
  static const String occupationList = 'Dropdown/FilterOccupation';
  static const String registerUser = 'SignUp/NewUserRegistration';
  static const String updateUser = 'SignUp/UpdateUserDetails';
  static const String agentList = 'SignUp/AgentList';
  static const String bankList = 'Dropdown/FilterBank';
  static const String productList = 'Dropdown/LoanTypes';
  static const String stateList = 'Dropdown/States';
  static const String empList = 'SignUp/EmployeeList';
  static const String districtList = 'Dropdown/District';
  static const String cityList = 'Dropdown/City';
  static const String pincodeList = 'Dropdown/Pincode';
  static const String selectUsers = 'Dropdown/SelectUsers';
  static const String selectUserType = 'Dropdown/SelectUsersType';
  static const String selectDealer = 'Dropdown/SelectDealer';
  static const String addPartner = 'PartnerReport/AddSP';
  static const String getFranchisee = 'PartnerReport/Get_Franchisee1';
  static const String dsaList = 'v1/Service/DSA_list';
  static const String createTicket =
      'v1/Support/createticketapi'; // used to Raise Ticket in class TicketRaiseUIController
  static const String getAllTickets = 'v1/Support/getmyticket';
  static const String getTicketDetails = 'v1/Support/getmyticketdetails';
  static const String ticketReply = 'v1/Support/replyonticket';

  static const String viewLoanMaster = 'Dropdown/FilterLoanTypes';
  static const String dashboard = 'Dashboard/DashboardDetails';
  static const String contactUs = 'ApplyLoan/contact';
  static const String storeFcm = 'SignUp/update_fcm_token';
  static const String removeFcm = 'SignUp/delete_fcm_token';
  static const String deleteAccount = 'SignUp/UserLoginblockedandunblocked';
  static const String websiteVisitTrack = 'SignUp/WebsiteVisitTrack';
  static const String notificationList = 'SignUp/NotificationList';
  static const String docList = 'PartnerReport/DocumentList';
  static const String updateLoggedProfile = 'SignUp/UpdateProfile';
  static const String updateBasicInfo = 'SignUp/UpdateBasicInfo';
  static const String deviceType = 'App';
  static const String newUser = "new user";
  static const String notification = "res/images/notifaction.png";
  static const String getCategory = "v1/Service/categoryList";
  static const String getSubCategory = "v1/Service/subcategoryListv2/";

  static const String listingProduct = "v1/Service/App_Lead_productList";
  static const String generatedLeads = "ApplyLoan/Getreffered_leads";
  static const String walletTransaction = "v1/Wallet/TransactionList";
  static const String walletTransaction1 = "v1/Wallet/TransactionListv2";
  static const String wallet = "v1/Wallet/GetwalletBalance";
  static const String withdrawal = "v1/Wallet/CreateWithdrawal";
  static const String kycStatus = "kycStatus";
  static const String kycRemark = "kycRemark";
  static const String myBanner = "ApplyLoan/Banners";
  static const String addBankAc = "v1/Wallet/AddBank";
  static const String fetchBank = "v1/Wallet/BankList";
  static const String fetchFAQGridList = "v1/Faq/Getcategory";
  static const String fetchFAQList = "v1/Faq/GetFaq";
  static const String setPrimaryBank = "v1/Wallet/setPrimary";
  static const String sendCustomerOtp = "v1/Partner/app_send_customer_otp_api";
  static const String addCustomer = "v1/Partner/app_add_customer_api";

  static const Color backgroundColor = Color(0xF7FFFFFF);
  static String platForm = "iOS";
  static const Color bgColor = Color(0x80E3F2FD);

  static String userProfile = "userProfile";

  static String getPlatform() {
    if (Platform.isAndroid) {
      return "android";
    } else if (Platform.isIOS) {
      return "iOS";
    } else if (Platform.isFuchsia) {
      return "fuchsia";
    } else if (Platform.isLinux) {
      return "linux";
    } else if (Platform.isMacOS) {
      return "macOS";
    } else if (Platform.isWindows) {
      return "window";
    } else {
      return "";
    }
  }

  static void showSnackBar(String title, String msg, bool error) {
    if (error) {
      Get.snackbar(
        title,
        msg,
        backgroundColor: Colors.green,
        margin: const EdgeInsets.all(15.0), // Margin around the SnackBar
      );
    } else {
      Get.snackbar(
        title,
        msg,
        backgroundColor: Colors.red.shade200,
        margin: const EdgeInsets.all(15.0), // Margin around the SnackBar
      );
    }
  }

  static getStatusIcon(String kycStatus) {
    switch (kycStatus) {
      case "0":
        // iconData = Icons.pending;
        return const Icon(
          Icons.pending,
          color: Colors.amber,
        );
      case "1":
        // iconData = Icons.verified;
        return const Icon(
          Icons.verified,
          color: Colors.green,
        );
      case "2":
        // iconData = Icons.cancel;
        return const FaIcon(
          FontAwesomeIcons.exclamationCircle,
          color: Colors.red,
          size: 20,
        );
      default:
        // iconData = Icons.pending;
        return const Icon(Icons.pending, color: Colors.amber);
    }
  }

  static String capitalizeFirstWord(String input) {
    input = input.replaceAll(RegExp('  +'), ' ');
    List<String> words = input.split(' ');
    if (words.length >= 2) {
      String firstName = words.first.toLowerCase();
      String middleName = words[1].toLowerCase();

      firstName =
          firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
      middleName =
          middleName.substring(0, 1).toUpperCase() + middleName.substring(1);

      return '$firstName $middleName';
    } else {
      List<String> words = input.split(' ');
      if (words.isNotEmpty) {
        String firstWord = words.first.toLowerCase();
        String capitalizedFirstWord =
            firstWord.substring(0, 1).toUpperCase() + firstWord.substring(1);
        return capitalizedFirstWord;
      }
    }
    return '';
  }

  static String changeName(String name) {
    var words = name.split(" ");
    var firstName = words[0];
    var middleName = words.length > 1 ? words[1] : "";
    name = ("$firstName $middleName");
    return name.capitalizeFirst.toString();
  }

  static Future<String> _getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }

  static String getYMD(String ddMMyyyy) {
    print(ddMMyyyy);
    var inputFormat = DateFormat('dd-MM-yyyy');
    var date1 = inputFormat.parse(ddMMyyyy);

    var outputFormat = DateFormat('yyyy-MM-dd');
    String result = outputFormat.format(date1).toString();
    print(result);
    return result;
  }
}

const profile = "res/images/profile.png";
const file = "res/images/wallet.svg";
const notification = "res/images/notifaction.png";

sb(double h, double w) {
  return SizedBox(
    height: h,
    width: w,
  );
}

// DateFormat(
// 'dd-MM-yyyy')
// .format(
// picked.start);

//Padding(
//                   padding: const EdgeInsets.only(left: 15,right:15),
//                   child: Container(width: MediaQuery.of(context).size.width,
//                       decoration: BoxDecoration(color: Colors.blueGrey.shade50,borderRadius: BorderRadius.all(Radius.circular(10),)),
//                       child:Row(
//                         // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Container(
//                             decoration: BoxDecoration(color:Colors.blue.shade800,borderRadius: BorderRadius.only(topLeft:Radius.circular(11),bottomLeft:Radius.circular(11), ),border: Border.all(width: 0,color: Colors.transparent)),
//                             child: Column(
//                               mainAxisAlignment:  MainAxisAlignment.spaceBetween,
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 Padding(
//                                   padding: const EdgeInsets.only(left: 50),
//                                   child: Container(height: 10,width:30,color:Colors.blue),
//                                 ),
//                                 sb(3, 0),
//                                 Row(children: [sb(0, 10),Icon(Icons.call,color:Colors.white,size: 16,),sb(0, 10),
//                                   Text('+91-8306300415',style: TextStyle(color:Colors.white,fontSize: 12),)
//                                 ],),   sb(10, 0),
//                                 Row(children: [sb(0, 10),Icon(Icons.email_outlined,color:Colors.white,size:16),sb(0, 10),
//                                   Text('info@squarefin.in',style: TextStyle(color:Colors.white,fontSize: 12),),
//                                   sb(10, 0),
//                                 ],),
//                                 sb(10, 0),
//                                 Row(
//                                   children: [sb(0, 12),
//                                     FaIcon(FontAwesomeIcons.globe,color: Colors.white,size: 14,),
//                                     sb(0, 10),
//                                     Text('www.squarefin.in',style: TextStyle(color:Colors.white,fontSize: 12),),sb(10, 23),
//                                     Container(height:10,width:20.9,
//                                       decoration: BoxDecoration(
//                                           border: Border.all(color: Colors.blueGrey.shade50),
//                                           borderRadius: BorderRadius.only(topLeft: Radius.elliptical(20,20),
//                                               bottomLeft: Radius.elliptical(20, 20),bottomRight: Radius.zero,
//                                               topRight: Radius.zero),color: Colors.blueGrey.shade50),
//                                     )
//                                   ],),
//                                 sb(10, 0),
//                                 Row(children: [sb(0, 11),
//                                   FaIcon(FontAwesomeIcons.mapLocation,color: Colors.white,size: 13,),
//                                   sb(0, 10),
//                                   Text('602-603, 6th Floor \nTrimurty\'s V-Jai City \nPoint, Ahinsa Circle\n C-Scheme, Jaipur',style: TextStyle(color:Colors.white,fontSize: 10),)
//                                 ],),
//                                 sb(3, 0),
//                                 Padding(
//                                   padding: const EdgeInsets.only(left: 50),
//                                   child: Container(height: 10,width:30,color:Colors.blue),
//                                 ),
//                               ],),
//                           ),
//                           Container(
//                             width: MediaQuery.of(context).size.width*0.48,
//                             child: Column(
//                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                 crossAxisAlignment: CrossAxisAlignment.center,
//                                 children: [
//                                   Center(child: Text(_.userName,style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),)),
//                                   sb(10, 0),
//                                   Text(_.userType!='employee'?'Verified Advisior':_.userType,style: TextStyle(fontSize: 10,color: Colors.grey.shade800),textAlign: TextAlign.center,),
//                                   sb(10, 0),
//                                   Container(width: 150,
//                                       decoration: BoxDecoration(color: Colors.transparent,borderRadius: BorderRadius.only(topRight: Radius.circular(5),bottomRight: Radius.circular(5))),
//                                       child: Image(image: AssetImage('res/images/use_me.png'))),
//                                   sb(10, 0),
//                                   Center(child: Text(dashboardController.cardNumber,style: TextStyle(fontSize: 12),)),
//                                   sb(5, 0),
//                                   Center(child: Text(dashboardController.cardEmailId,style: TextStyle(fontSize: 10),)),
//                                 ]),
//                           )
//                         ],
//                       )
//                   ),
//                 ):SizedBox(),

// is_ticket_rasied
// ticket_id

/// these are parameters and will be used in Product Lead;

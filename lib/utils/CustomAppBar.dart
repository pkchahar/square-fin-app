import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../activity/Notification/NotificationUIController.dart';
import '../activity/UserAccount/DashboardInfoUIController.dart';
import '../activity/UserAccount/ProfileUIController.dart';
import '../activity/UserAccount/UserProfileUIController.dart';
import '../activity/Utility Controllers/Qr/QRCodeScreen.dart';
import 'AppUtils.dart';

class CustomAppBar extends StatelessWidget {
  final String userName;
  final String kycStatus;
  final String pageInUse;


  const CustomAppBar({super.key, required this.userName, required this.kycStatus, required this.pageInUse});
  //  name = AppUtils.capitalizeFirstWord(name.trim());

  @override
  Widget build(BuildContext context) {

    return AppBar(
      automaticallyImplyLeading:pageInUse=='1'?false:true,
      backgroundColor: Colors.blue.shade50,
      elevation: 0,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children:<Widget> [
              InkWell(
                onTap: () {
                  pageInUse =='2'?Get.to(const UserProfileUIController()):Get.to(const ProfileUIController());
                },
                child:
                ClipOval(
                  child: SizedBox(
                    height: 35,
                    width: 35,
                    child: SvgPicture.asset(
                      'res/images/profile.svg',
                      height: 30,
                      width: 30,
                    ),
                  ),
                )
              ),
              const SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Text(
                    "Welcome",
                    style: TextStyle(
                      fontSize: 10,
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                    ),
                  ),
                  Text(
                    userName,
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      color: Colors.black,
                    ),overflow: TextOverflow.ellipsis,
                  )
                ],
              ),

              kycStatus.isNotEmpty
                  ?  Padding(
                      padding: const EdgeInsets.only(top: 10, left: 5),
                    //  child: InkWell(
                     //   onTap: () async {
                      //    SharedPreferences preferences = await SharedPreferences.getInstance();
                      //   kycStatus=='2'? Get.to(
                      //        KycViewController(
                            //  name: userName,
                            //  email:  preferences.getString(AppUtils.userMail)!,
                            //  pan: "")
                        //  ):null;
                        //},
                        child:
                        AppUtils.getStatusIcon(kycStatus)
                     // ),
                    )
                  : const SizedBox(
                      height: 0,
                    )
            ],
          ),
          InkWell(
            onTap: () {
             pageInUse=='1'? Get.to(const NotificationUIController()):
             pageInUse=='3' ? Get.to(const QRCodeScreen()) :
             Get.to(const DashboardInfoUIController()); // ProfileUIController
            },
            child: pageInUse=='1'?
                const Icon(Icons.notifications,color: Colors.blue,):
            pageInUse=='3'?
                const Icon(Icons.qr_code,color: Colors.black,)

                :const SizedBox()
            // Icon(Icons.dashboard_outlined),
          ),
        ],
      ),
    );
  }
}

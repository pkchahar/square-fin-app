import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:pi/Model/UtilityModel/BannerResponse.dart';

class SliderImage extends StatefulWidget {
  List<BannerItem> bannerList = [];
   SliderImage({super.key, required this.bannerList});

  @override
  State<SliderImage> createState() => _SliderImageState();
}

class _SliderImageState extends State<SliderImage> {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider(
          items: widget.bannerList.map(
            (e) => Card(
              elevation: 1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Image.network(
                  e.image,
                fit: BoxFit.contain,
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset('res/images/no_image.png');
                },
              ),
            ),
          ).toList(),
          options: CarouselOptions(
            autoPlay: true,
            enableInfiniteScroll: true,
            enlargeCenterPage: true,
            height: MediaQuery.of(context).size.height * 0.3 - 100,
          ),
        ),
      ],
    );
  }
}

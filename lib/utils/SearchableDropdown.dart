import 'package:flutter/material.dart';

class SearchableDropdown extends StatefulWidget {
  final List<String> items;
  final String hintText;

  const SearchableDropdown({super.key, required this.items, required this.hintText});

  @override
  _SearchableDropdownState createState() => _SearchableDropdownState();
}

class _SearchableDropdownState extends State<SearchableDropdown> {
  late List<String> filteredItems;
  String? selectedItem;

  @override
  void initState() {
    super.initState();
    filteredItems = widget.items;
  }

  void filterSearchResults(String query) {
    if (query.isNotEmpty) {
      List<String> temp = [];
      for (var item in widget.items) {
        if (item.toLowerCase().contains(query.toLowerCase())) {
          temp.add(item);
        }
      }
      setState(() {
        filteredItems = temp;
      });
    } else {
      setState(() {
        filteredItems = widget.items;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextField(
          onChanged: (value) {
            filterSearchResults(value);
          },
          decoration: InputDecoration(
            hintText: widget.hintText,
          ),
        ),
        DropdownButtonFormField(
          value: selectedItem,
          items: filteredItems
              .map((item) =>
                  DropdownMenuItem(value: item, child: Text(item)))
              .toList(),
          onChanged: (value) {
            setState(() {
              selectedItem = value;
            });
          },
        ),
      ],
    );
  }
}
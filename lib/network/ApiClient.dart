import 'dart:convert';
import 'dart:developer';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:pi/utils/AppUtils.dart';

class AppClient {
  static String platForm = "iOS";

  Future<Map> getForGotPassOtp(String userId) async {
    String version = "1.0.0";
    Map<String, String> mData = {
      'MobileNo': userId,
      'type': 'Send OTP',
      'platForm': platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };

    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.forgotPass);

      if (kDebugMode) {
        print(url);
        print(mData);
      }
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
        return parsed;
      }
    } catch (e) {
      log(e.toString());
    }
    return parsed;
  }

  Future<Map> getRegistrationCode(String userId, String buttonValue,
      {String? otp}) async {
    String version = "1.0.0";
    Map<String, String> mData = {
      'mobile': userId,
      'otp': otp ?? "",
      'ButtonValue': buttonValue,
      'platForm': platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };

    if (kDebugMode) {
      print(mData);
    }
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.registerUser);
      print(url);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
        return parsed;
      }
    } catch (e) {
      log(e.toString());
    }
    return parsed;
  }


  Future<Map> resendOtp(String userId, String buttonValue,
      {String? otp}) async {
    String version = "1.0.0";
    Map<String, String> mData = {
      'mobile': userId,
      'otp': otp ?? "",
      'ButtonValue': buttonValue,
      'platForm': platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };

    if (kDebugMode) {
      print(mData);
    }
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.resendOtp);
      print(url);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
        return parsed;
      }
    } catch (e) {
      log(e.toString());
    }
    return parsed;
  }

  Future<Map> getLoginOtp(String userId, signCode) async {
    String version = "1.0.0";
    Map<String, String> mData = {
      'username': userId,
      'appSignature': signCode.toString(),
      'ButtonValue': "Send Otp",
      'platForm': platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };

    if (kDebugMode) {
      print(mData);
    }
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.loginOtpApi);
      print(url);
      var response = await http.post(url, body: mData);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
        return parsed;
      }
    } catch (e) {
      log(e.toString());
    }
    return parsed;
  }

  Future<Map> forgotPassVerify(String userId, String passcode) async {
    String version = "";
    Map<String, String> mData = {
      'MobileNo': userId,
      'ForgetOtp': passcode,
      'type': 'Verify',
      'platForm': platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.forgotPass);
      var response = await http.post(url, body: mData);
      print(url);
      print(response);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
        return parsed;
      }
    } catch (e) {
      log(e.toString());
    }
    return parsed;
  }

  Future<Map> verifyOtp(String userId, String passcode) async {
    String version = "";
    Map<String, String> mData = {
      'username': userId,
      'LoginOtp': passcode,
      'ButtonValue': 'Verify',
      'platForm': platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.loginApi);
      var response = await http.post(url, body: mData);
      print(url);
      print(mData);
      print(response);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        print(parsed);
        return parsed;
      }
    } catch (e) {
      log(e.toString());
    }
    return parsed;
  }

  Future<Map> updatePass(String userId, String userPass) async {
    String version = "";
    Map<String, String> mData = {
      'MobileNo': userId,
      'type': 'Update Password',
      'ChangePassword': userPass,
      'ConfrimPassword': userPass,
      'platForm': platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.forgotPass);
      var response = await http.post(url, body: mData);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        return parsed;
      }
    } catch (e) {
      log(e.toString());
    }
    return parsed;
  }

  Future<Map> updateDetail(String userId, String mobile, String name,
      String email, String password) async {
    String version = "";
    Map<String, String> mData = {
      'Login_User_Id': userId,
      'mobile': mobile,
      'name': name,
      'email': email,
      'password': password,
      'platForm': platForm,
      'version': version,
      'Device_Type': AppUtils.deviceType
    };
    Map parsed = {};
    try {
      var url = Uri.parse(AppUtils.baseUrl + AppUtils.updateUser);
      print(url);
      print(mData);
      var response = await http.post(url, body: mData);
      print(response);
      print(response.body);
      if (response.statusCode == 200) {
        parsed = jsonDecode(response.body);
        return parsed;
      }
    } catch (e) {
      log(e.toString());
    }
    return parsed;
  }
}

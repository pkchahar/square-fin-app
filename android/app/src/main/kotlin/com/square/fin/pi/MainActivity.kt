//package com.square.fin.pi
//
//import io.flutter.embedding.android.FlutterActivity
////import io.flutter.embedding.android.FlutterFragmentActivity
////import io.flutter.embedding.engine.flutterEngine
////import io.flutter.plugins.GeneratedPluginRegistrant
//class MainActivity: FlutterActivity() {
////    FlutterFragmentActivity() {
////    override fun configureFlutterEngine(flutterEngine: FlutterEngine){
////        GeneratedPluginRegistrant.registerWith(flutterEngine)
////    }
//}
package com.square.fin.pi

import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterFragmentActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
    }
}